# This file is for creating Python package.

# Packaging Python Projects: https://packaging.python.org/tutorials/packaging-projects/
# Setuptools documentation: https://setuptools.readthedocs.io/en/latest/index.html

import setuptools

setuptools.setup(
    name="dtop-stagegate",
    version="1.0.0",
    packages=setuptools.find_packages(where='src'),
    package_dir={'':'src'},
    install_requires=[
        "flask",
        "flask-babel",
        "flask-cors",
        "marshmallow-sqlalchemy",
        "flask-sqlalchemy",
        "flask-marshmallow<0.12",  # ModelSchema deprecated in v0.12.0
        "requests",
        "pandas"
    ],
    include_package_data=True,
    zip_safe=False,
)
