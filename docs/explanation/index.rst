.. _sg-explanation:

*********************
Background and theory
*********************

.. toctree::
  :maxdepth: 1
  :hidden:

  stages_and_gates
  evaluation_areas
  stage_activities
  data_input
  metrics
  questions

The Stage Gate module is an application of a *Stage Gate process* that is used in research and industry to provide structure to the technology development process. 
This approach supports the R&D pathway towards producing reliable and cost-effective ocean energy sub-systems, devices and arrays.

This section of the documentation describes the background and theory of the Stage Gate process and the data provided within the Stage Gate module. 
The data that forms the basis of this tool come from two main sources:

-	The *International Evaluation and Guidance Framework for Ocean Energy Technology* written by the International Energy Agency – Ocean Energy Systems [IEA-OES]_

-	The Wave Energy Scotland (WES) stage gate programme

|

.. figure:: ../figures/OES_task12_1.svg
   :align: center
   :width: 75%
   
   The six stages as defined by the [IEA-OES]_

The following sections of the report are divided according to the main features of the Stage Gate framework:

- :ref:`sg-theory-stages-gates`;  
  The key feature of the stage gate design tool is the technology development pathway split up into distinct stages, separated by stage gates. 
  The stage gates are an opportunity for users of the tool to assess the technology and make critical decisions on whether to progress to the next stage.

- :ref:`sg-theory-eval-areas`; 
  The areas in which the user wishes to measure the success of ocean energy technology to demonstrate progress and performance.

- :ref:`sg-theory-activities`;  
  This is a list of the research, development and demonstration activities that should be carried out during the prescribed stages.

- :ref:`sg-theory-data-input`;
  The types of data which must be input to support a Stage Gate Assessment, including stage gate question responses, performance data e.g. tank and sea testing results and project data, e.g. site resource data which the user inputs to support the metrics calculations.  

- :ref:`sg-theory-metrics`; 
  The parameters used to evaluate how well a technology performs in the Evaluation Areas. 
  These are outputs of the Deployment and Assessment tools.

- :ref:`sg-theory-questions`;  
  Qualitative and quantitative questions to support the Stage Gate Assessment, covering topics such as scientific and engineering credibility, future targets and readiness for the next stage.

References
----------

.. [IEA-OES] International Energy Agency - Ocean Energy Systems (2021)
  *An International Evaluation and Guidance Framework for Ocean Energy Technology*,
  Hodges J., Henderson J., Ruedy L., Soede M., Weber J., Ruiz-Minguela P., Jeffrey H., Bannon E., Holland M., Maciver R., Hume D., Villate J-L, Ramsey T., 
  Available: https://www.ocean-energy-systems.org/publications/oes-documents/
  Accessed: 2021-07-02
