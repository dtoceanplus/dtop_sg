.. _sg-theory-activities:

Stage Activities
================

Stage Activities are the engineering activities that occur during a technology development Stage. 

Clearly defined activities provide consistency in expectation between developers and investors, ensuring projects deliver the appropriate data to support the Evaluation Method and resulting Evaluation Criteria or metrics. 
The stage activities presented in DTOceanPlus align with those developed in the [IEA-OES]_ Evaluation and Guidance Framework for Ocean Energy. 

These stage activities are grouped in two ways, either in Activity Categories, along with similar engineering activities, or by Evaluation Area, along with activities focused on the type of evaluation they support. 
This helps funders, investors and technology developers use the guidance provided in the [IEA-OES]_ Evaluation and Guidance Framework.
