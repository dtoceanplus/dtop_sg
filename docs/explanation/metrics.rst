.. _sg-theory-metrics:

Metrics
=======

Metrics are the quantitative measures which are used to assess technology. 
As with data input, the complexity and confidence-level of output metrics increases through the stages, with more complex input data allowing more complex data synthesis and analysis in the Deployment and Assessment tools.

The table below shows the key metrics that are calculated through the Deployment and Assessment tools at the late Stage Gate assessments (not all are possible at the earlier stage gates due to lack of data and information). 

+----------------------------------------+------------------------+------------------+---------+
| Metric                                 | Unit                   | Evaluation Area  | Module  |
+========================================+========================+==================+=========+
| Annual captured energy                 | kWh                    | Power Capture    | SPEY    |
+----------------------------------------+------------------------+------------------+---------+
| Annual transformed energy              | kWh                    | Power Conversion | SPEY    |
+----------------------------------------+------------------------+------------------+---------+
| Annual delivered energy                | kWh                    | Power Conversion | SPEY    |
+----------------------------------------+------------------------+------------------+---------+
| Array captured efficiency              | % (decimal)            | Power Capture    | SPEY    |
+----------------------------------------+------------------------+------------------+---------+
| Array relative transformed efficiency  | % (decimal)            | Power Conversion | SPEY    |
+----------------------------------------+------------------------+------------------+---------+
| Array relative delivered efficiency    | % (decimal)            | Power Conversion | SPEY    |
+----------------------------------------+------------------------+------------------+---------+
| Installation duration                  | hours per kW           | Installability   | LMO     |
+----------------------------------------+------------------------+------------------+---------+
| Cost of installation                   | €                      | Installability   | LMO     |
+----------------------------------------+------------------------+------------------+---------+
| Mean time to failure for the array     | hours                  | Reliability      | RAMS    |
+----------------------------------------+------------------------+------------------+---------+
| Probability of failure of array        | % (decimal)            | Reliability      | RAMS    |
+----------------------------------------+------------------------+------------------+---------+
| Average availability of array          | % (decimal)            | Availability     | RAMS    |
+----------------------------------------+------------------------+------------------+---------+
| Maintenance probabilities              | % (decimal)            | Maintainability  | RAMS    |
+----------------------------------------+------------------------+------------------+---------+
| Average maintenance duration           | hours per kW per year  | Survivability    | LMO     |
+----------------------------------------+------------------------+------------------+---------+
| Survival probabilities                 | % (decimal)            | Survivability    | RAMS    |
+----------------------------------------+------------------------+------------------+---------+
| LCOE                                   | €/kWh                  | Affordability    | SLC     |
+----------------------------------------+------------------------+------------------+---------+
| CapEx                                  | €                      | Affordability    | SLC     |
+----------------------------------------+------------------------+------------------+---------+
| OpEx                                   | €                      | Affordability    | SLC     |
+----------------------------------------+------------------------+------------------+---------+
| CapEx per kW                           | €/kW                   | Affordability    | SLC     |
+----------------------------------------+------------------------+------------------+---------+
| OpEx per kW per year                   | €/kW per year          | Affordability    | SLC     |
+----------------------------------------+------------------------+------------------+---------+
| IRR                                    | % (decimal)            | Affordability    | SLC     |
+----------------------------------------+------------------------+------------------+---------+
| Global warming participation           | gCO2/kWh               | Acceptability    | ESA     |
+----------------------------------------+------------------------+------------------+---------+
| Cumulative energy demand               | kJ/kWh                 | Acceptability    | ESA     |
+----------------------------------------+------------------------+------------------+---------+
| Energy payback period                  | months                 | Acceptability    | ESA     |
+----------------------------------------+------------------------+------------------+---------+
| Jobs created                           | \-                     | Acceptability    | ESA     |
+----------------------------------------+------------------------+------------------+---------+
| Global negative EIA                    | \-                     | Acceptability    | ESA     |
+----------------------------------------+------------------------+------------------+---------+
| Global positive EIA                    | \-                     | Acceptability    | ESA     |
+----------------------------------------+------------------------+------------------+---------+
| Cost of consenting                     | €                      | Acceptability    | ESA     |
+----------------------------------------+------------------------+------------------+---------+
