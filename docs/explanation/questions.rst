.. _sg-theory-questions:

Questions
=========

When completing a Stage Gate Assessment, qualitative questions will be displayed to the user to answer with a narrative to support their stage gate application. 
These questions cover areas including scientific credibility, innovation, technology risks and future commercial offering.

This part of the Stage Gate Assessment gives value to investors and funders who are evaluating a Stage Gate assessment, as it gives the extra information on an ocean energy project needed to justify the provided data and other project results.
In particular, the qualitative questions give the opportunity to provide more detail at the earliest stage assessments.
When more detail is defined about a project and there are more measurable parameters at the later stages, there is a decreasing need for the additional qualitative narrative to support it. 
For this reason, there are qualitative questions for stage gates 0 - 1, 1 - 2 and 2 - 3 only. 
The figure below shows the transition from high-level, qualitative evaluation towards full quantitative detail in the Stage Gate design tool. 

.. figure:: ../figures/questions.svg
   :align: center
   
   Transition from qualitative to quantitative questions (icons taken from [IEA-OES]_)

The qualitative questions help assess a range of factors which affect the technical credibility of an ocean energy project. 
The questions were developed from the Wave Energy Scotland programme and the [IEA-OES]_ Evaluation and Guidance Framework, with adaptation for DTOceanPlus.
Therefore, although they have been generalised for broad applicability, the questions are based on tried and tested Stage Gate Assessments in ocean energy.
