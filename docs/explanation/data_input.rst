.. _sg-theory-data-input:

Data Input
==========

As a technology matures and more data is available, for example from tank testing and open water testing, the user is expected to input more detailed data through the Deployment and Assessment tools.
This data is produced in the technology development process and increases in detail and confidence-level as a technology matures.
As part of DTOceanPlus, the user can import this data to support their design and assessment.

As an example, for the Evaluation Area of Power Capture, the increasing detail of performance data changes for different Stage Gate Assessments as follows: 

- **Stage Gate 0-1**

   - *Tidal*: single value for power coefficient (CP) of the machine that represents the efficiency of the machine

   - *Wave*: single value for Capture Width Ratio (CWR)

- **Stage Gate 1-2** and **Stage Gate 2-3**

   - *Wave*: the power matrix or CWR matrix for tank testing or numerical modelling of device

   - *Tidal*: curve of the power coefficient (CP) and thrust coefficient (CT) of the machine for flume testing or numerical modelling of device 

- **Stage Gate 3-4** and **Stage Gate 4-5**

   - *Wave*: Numerical model power matrices, including device interaction, informed from open water testing power matrix

   - *Tidal*: Numerical model power matrices, including device interaction, informed from open water testing CP and CT curves.

As with Performance data, a user is likely to know more about their ocean energy project with increasing maturity. 
This means that when completing a Stage Gate Assessment, the higher stage gates will be able to define parameters such as the location of their chosen site, the distance between devices in an array, or have more confidence in the logistics and maintenance activities to support the project, as these will be informed from deployment in open sea. 

When completing a Stage Gate Assessment, the user will see from the Stage Activities that this expectation of knowledge of project data gets increasingly demanding. 
For example, when inputting site data through the Site Characterisation tool, the user may select a reference site for the earliest stage gate deciding on low, medium or high wave and tidal energy sites. For later stage gates, the user will be able to upload their own resource and bathymetry data as a site is known. 
