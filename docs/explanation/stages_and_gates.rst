.. _sg-theory-stages-gates:

Stages and Stage Gates
======================

The Stage Gate process splits the technology development pathway into clearly defined stages, from a  new  concept, engineering  specification, scaled  prototypes, full  devices and first arrays up until a commercially deployed array.
Splitting the development pathway into clearly defined stages enables technologies to be  compared  within each stage and allows developers to understand where their technologies lie in the development process. 

Stages are defined periods of the development process, aligned with phases of funding and decision points.
The Stage Gate design tool has six stages; Stage 0, 1, 2, 3, 4 and 5. 
These are separated by five stage gates: 0-1, 1-2, 2-3, 3-4 and 4-5, shown as diamonds in the figure below. 

.. figure:: ../figures/framework.svg
   :align: center
   
   The DTOceanPlus Stage Gate Framework

Clear, defined stages in the technology development process are  punctuated  by  the measures  of success which are defined between stages at the stage gates. 
This is where objective measures of success can be defined and applied, namely metrics.
An example of this process in industry is through the `Wave Energy Scotland (WES) <https://www.waveenergyscotland.co.uk/>`_ programme which operates in a stage gate process from early stage concepts up to large scale prototypes. 
The stage gates are the opportunities to measure success in order to decide which projects will pass through to the next stage to be awarded the next wave of funding.
The stage gate process helps to demonstrate to technology developers themselves, the public funders like Wave Energy Scotland and private investors in the sector that technologies are moving through these stages and maturing. 
The maturity of technology is often measured by Technology Readiness Levels (TRLs). 
The stage gate design tool stages align with Technology Readiness levels as seen in the table below. 

.. figure:: ../figures/trl_table.svg
   :align: center
   
   Relationship between Stages and TRL (adapted from [IEA-OES]_)

The alignment of stages and TRL levels is in accordance with the International  Electrotechnical Commission (IEC) Technical Specification *IEC TS 62600-103*. 
However, the IEC Stage 1 has been split into Stages 0 (TRL 1) and Stage 2 (TRL 2-3) in DTOceanPlus to ensure the DTOceanPlus suite of tools is  specifically able to function for the very earliest technologies (TRL  1) before any prototype testing has taken place in the technology development pathway. 
The stage gate design tool will guide the user in identifying which stage gate their technology  or project should be assessed against.
This is done by displaying a list of technology development activities for the user to assess their  progress and identify missed activities, as presented in the :ref:`Activity Checklist feature <sg-checklist-tutorial>`.
