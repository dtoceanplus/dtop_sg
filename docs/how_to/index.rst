.. _sg-how-to:

*************
How-to Guides
*************

Each *how-to guide* listed below contains step-by-step instructions on how to achieve specific outcomes using the Stage Gate module. 
These guides are intended for users who have previously completed all the :ref:`Stage Gate tutorials <sg-tutorials>` and have a good knowledge of the features and workings of the Stage Gate module, and the DTOceanPlus suite in general. 
While the tutorials give an introduction to the basic usage of the module, these *how to guides* tackle slightly more advanced topics, such as how to use the results of a Stage Gate assessment as inputs to the Structured Innovation module.
 
#. :ref:`sg-complexity-level-how-to`
#. :ref:`sg-metric-results-how-to`
#. :ref:`sg-improvement-cycle-how-to`

.. toctree::
   :maxdepth: 1
   :hidden:

   complexity_level
   metric_results
   improvement_cycle
