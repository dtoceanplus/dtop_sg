.. _sg-complexity-level-how-to:

How to use the Stage Gate tool to identify complexity levels
============================================================

The :ref:`Activity Checklist tutorial <sg-checklist-tutorial>` showed that the Stage Gate tool can be used to identify the stage of development of a device or technology.
This in turn identifies the appropriate Stage Gate that the technology should be evaluated against. 

A user can run each of the Deployment and Assessment tools at varying levels of complexity.
A pre-defined mapping between the Stages and complexity levels of the Deployment and Assessment tools has been defined. 
This is referred to as the Combination Matrix and is shown in the table below. 

+---------------------------------------------------------------------+---------+---------+---------+---------+---------+---------+
| Tool                                                                | Stage 0 | Stage 1 | Stage 2 | Stage 3 | Stage 4 | Stage 5 |
+=====================================================================+=========+=========+=========+=========+=========+=========+
| Site Characterisation (SC)                                          | 1       | 1       | 2       | 2       | 3       | 3       |
+---------------------------------------------------------------------+---------+---------+---------+---------+---------+---------+
| Machine Characterisation (MC)                                       | 1       | 1       | 2       | 2       | 3       | 3       |
+---------------------------------------------------------------------+---------+---------+---------+---------+---------+---------+
| Energy Capture (EC)                                                 | 1       | 1       | 2       | 2       | 3       | 3       |
+---------------------------------------------------------------------+---------+---------+---------+---------+---------+---------+
| Energy Transformation (ET)                                          | 1       | 1       | 2       | 2       | 3       | 3       |
+---------------------------------------------------------------------+---------+---------+---------+---------+---------+---------+
| Energy Delivery (ED)                                                | 1       | 1       | 1       | 2       | 3       | 3       |
+---------------------------------------------------------------------+---------+---------+---------+---------+---------+---------+
| Logistics and Marine Operations (LMO)                               | 1       | 1       | 1       | 2       | 3       | 3       |
+---------------------------------------------------------------------+---------+---------+---------+---------+---------+---------+
| Station Keeping (SK)                                                | 1       | 1       | 1       | 2       | 3       | 3       |                             
+---------------------------------------------------------------------+---------+---------+---------+---------+---------+---------+
| Reliability, Availability, Maintainability and Survivability (RAMS) | 1       | 1       | 1       | 2       | 3       | 3       |
+---------------------------------------------------------------------+---------+---------+---------+---------+---------+---------+
| System Performance and Energy Yield (SPEY)                          | N/A     | N/A     | N/A     | N/A     | N/A     | N/A     |
+---------------------------------------------------------------------+---------+---------+---------+---------+---------+---------+
| System Lifetime Costs (SLC)                                         | 1       | 1       | 1       | 2       | 3       | 3       |
+---------------------------------------------------------------------+---------+---------+---------+---------+---------+---------+
| Environmental and Social Acceptance (ESA)                           | 1       | 1       | 2       | 2       | 3       | 3       |
+---------------------------------------------------------------------+---------+---------+---------+---------+---------+---------+

This matrix allows the Activity Checklist functionality to inform the user of the appropriate complexity level to use for each of the Deployment and Assessment tools.
Furthermore, the Stage activities have been developed in tandem with the user inputs of the Deployment and Assessment tools for each level of complexity. 
As such, a user can ensure they have the user inputs required to run each tool at the appropriate level of complexity if they first use the Activity Checklist functionality in the Stage Gate module.

.. Note::

   The System Performance and Energy Yield (SPEY) tool works in the same way at each complexity level, which is why it is labelled N/A in the combination matrix.  

To obtain this complexity level mapping, you must do the following:

#. Create a new Stage Gate study (see :ref:`here <sg-study-tutorial>`).

.. Important::

   It is critical when creating the Stage Gate study that you do so as part of an **integrated DTOceanPlus study**.
   This means that you must create a Project, Study and then use the **Design and Assessment** tab in the main GUI of DTOceanPlus. 
   This allows data to be transferred between modules.
   See the :ref:`mm-tutorial-create_project` tutorial for more information on how to do this. 

2. Open the Stage Gate module and perform the Activity Checklist feature (see :ref:`this tutorial <sg-checklist-tutorial>`).
   Make sure to submit the answers. 

#. The results will identify the Stage of technology development that has been reached. 

#. Scroll to the bottom of the *Activity Checklist* results page, where you will see the recommended complexity level for each DTOceanPlus module. 

#. You can keep this results page open as a separate tab in the browser.
   Return to the DTOceanPlus Project Home page. 
   You can then start an integrated assessment, making your way through each module in turn. 
   When you need to create a new entity for each module, most of them will ask you for a complexity level. 
   You can switch back to the tab with Stage Gate checklist results displayed to see what was recommend for the current state of your technology. 
