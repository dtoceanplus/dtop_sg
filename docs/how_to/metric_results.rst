.. _sg-metric-results-how-to:

How to get metric results from the Deployment and Assessment tools
==================================================================

All the quantitative metrics that are requested in the **Applicant Mode** of the Stage Gate module can be calculated by the Deployment and Assessment (D&A) modules.
In fact, their availability in the D&A tools (together with the links between stage activities, complexity levels and input data) is one of the main factors for their inclusion in the set of key quantitative metrics that are requested in the Stage Gate tool.

If a user is running an integrated assessment, they have the ability to transfer the metrics from the D&A tools to the relevant section of the Applicant Mode. 
This tutorial describes how to transfer these metrics. 

#. Firstly, it is assumed that the user has provided input data and successfully run at least one of the Assessment modules.
   The Stage Gate tool consumes metrics from the following modules:
   
   * Logistics and Marine Operations (LMO),
  
   * Systems Performance and Energy Yield (SPEY),

   * Reliability, Availability, Maintainability and Survivability (RAMS),

   * System Lifetime Costs (SLC) and

   * Environmental and Social Acceptance (ESA). 

   At least one of these modules must have results available. 

.. Important::

   It is essential that the studies for these modules be completed as part of an **integrated DTOceanPlus study**.
   This means that you must first create a Project, Study and then use the **Design and Assessment** tab in the main GUI of DTOceanPlus. 
   Use the main Project home page to create all entities from each of the D&A modules.
   This allows data to be transferred between them.
   See the :ref:`mm-tutorial-create_project` tutorial for more information on how to do this. 

2. The next step is to create a Stage Gate study. 
   Again, make sure that you create the study via the main DTOceanPlus module and not from within the Stage Gate tool itself. 

#. Open the Stage Gate module from within the main DTOceanPlus module after you have created this entity. 
   You should be brought to the home page of the selected Stage Gate study. 
   Click the ``Applicant Mode`` button. 

#. Then choose any of the Stage Gates apart from Stage Gate 0 - 1 (which does not ask for any quantitative questions). 

#. On the Applicant input page, click the ``Next`` button near the bottom of the page to move on to the Quantitative Questions section. 

#. Near the top of the screen there is a button labelled ``Use results from Deployment and Assessment modules``. 
   Click this button. 

#. A loading screen will appear.
   It should take 3-4 seconds to extract the relevant metrics from the full list of D&A modules. 
   
#. Once the loading screen has disappeared, you should be able to see that the metric results have been copied over from the appropriate Deployment and Assessment tools. 
   Note that the user will still need to fill the Justification boxes for these quantitative questions. 

.. Warning::

   This operation extracts the metric results for the instantaneous values from the respective modules. 
   This means that if a module has not finished calculating results, there will be nothing to transfer across. 
   Furthermore, if a user goes back and re-runs some of the modules, any new results that are calculated will **not** be reflected in the metrics result table in the Stage Gate tool. 
   The user will need to repeat this tutorial to extract the latest results from the other modules in this case. 
