.. _sg-improvement-cycle-how-to:

Using Improvement Area results as inputs to the Structured Innovation module
============================================================================

The improvement areas that can be identified using the Stage Gate module are an ideal starting place for the Structured Innovation (SI) module. 
The SI tool can be used to either create a new concept or improve an existing one. 
For this latter scenario, the improvement areas provided by the Stage Gate tool can be used as the basis for what is known as an *improvement cycle* in the Structured Innovation module. 

.. Important::

    Once more, the user must be working as part of an **integrated DTOceanPlus study**.
    This means that you must first create a Project, Study and then use the **Design and Assessment** tab in the main GUI of DTOceanPlus. 
    Use the main Project home page to create the entities for both the Stage Gate and Structured Innovation modules. 
    This allows data to be transferred between them.
    See the :ref:`mm-tutorial-create_project` tutorial for more information on how to do this. 

#. Open up or create a Stage Gate entity from within the main DTOceanPlus GUI 

#. Complete the Activity Checklist as per the :ref:`Activity Checklist tutorial <sg-checklist-tutorial>`.
   The user can also fill out the Applicant Mode and Assessor Mode features if they want the improvement areas to be based on more data. 

#. Open up the Improvement Area features of the Stage Gate tool to see which evaluation areas have been identified as weaknesses. 

#. Return to the main DTOceanPlus GUI and create a new entity for the Structured Innovation module. 
   The improvement areas shown in the Stage Gate tool can now be used as the basis of an improvement cycle. 
   See the :ref:`Structured Innovation documentation <si-home>` for more details on how to do this. 

#. Once the user is finished with the improvement cycle in SI, they can return to the Stage Gate module. 
   If they create a new Stage Gate study, they will then be able to use the Study Comparison feature to see compare their technology before and after the Structured Innovation improvements. 
