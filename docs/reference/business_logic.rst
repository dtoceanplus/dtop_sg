.. _sg-business-logic:

Business Logic
==============

.. toctree::

   business/applicant_mode
   business/assessor_mode
   business/checklists
   business/complexity_level
   business/db_utils
   business/frameworks
   business/improvement_areas
   business/models
   business/report_generation
   business/schemas
   business/stage_gate_studies
   business/study_comparison
