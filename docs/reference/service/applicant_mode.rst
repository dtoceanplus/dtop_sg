Applicant Mode (service layer)
==============================

.. automodule:: dtop_stagegate.service.api.applicant_mode
   :members:
   :undoc-members:
