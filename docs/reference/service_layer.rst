.. _sg-service-layer:

Service Layer
=============

.. toctree::

   service/applicant_mode
   service/assessor_mode
   service/doc
   service/frameworks
   service/stage_gate_studies

Submodules
----------

Errors
^^^^^^

.. automodule:: dtop_stagegate.service.api.errors
   :members:
   :undoc-members:
   :show-inheritance:

Utilities
^^^^^^^^^

.. automodule:: dtop_stagegate.service.api.utils
   :members:
   :undoc-members:
   :show-inheritance:
