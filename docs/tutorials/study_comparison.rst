.. _sg-comparison-tutorial:

Comparing the results of two or more Stage Gate studies
=======================================================

The results of two or more Stage Gate studies can be compared if they are available. 
Only studies that have been associated with the same Threshold Set can be compared. 

.. Important::

    This tutorial assumes you have completed all previous tutorials up to this point. 
    Furthermore, it is necessary to have a second study created and completed, making sure that this uses the same Threshold Set that was created in :ref:`sg-frameworks-tutorial`.
    To do this, you can complete the tutorials again in the same order (except for the :ref:`Framework tutorial <sg-frameworks-tutorial>`), making sure to use a different  study name, description, checklist results, metric results etc.
    This is a prerequisite for this tutorial. 
    Once you have completed the tutorials for a second example study, you can proceed with this tutorial. 

#. Go to the Stage Gate Studies home page by clicking on the ``Stage Gate Studies`` button in the navigation pane on the left-hand side of the Stage Gate GUI. 

#. Along the top of the page, next to the button for creating studies, there is a button labelled ``Compare Stage Gate studies``.
   Click this button. 

#. You then need to choose which set of metric thresholds you want to consider.
   Remember that only studies that have used the same Threshold Set can be compared.
   In the drop-down box, select the ``Example threshold set`` that was created in the :ref:`Framework tutorial <sg-frameworks-tutorial>`. 
 
#. You should see the two studies listed in the subsequent table. 
   In the last column of the table, there are options to select the studies for comparison. 
   Check the box for the two studies in question. 

#. Click the ``Compare Studies`` button.
   Note that this button is only enabled after two or more studies are chosen for comparison. 

#. A comparison of the summary details of the two studies should be visible at the top of the page. 
   Further results are displayed underneath this summary. 
   Click the ``Activity Checklist`` tab. 

#. A summary results table will be displayed showing the percentage of activities complete for each Stage and for each study. 

#. Further down the page, you can get a more detailed comparison. 
   Select Stage 2 from the drop-down menu. 
   The breakdown by activity category and evaluation area should now be shown graphically. 

#. Scroll back to the top of the page and click the ``Applicant Mode`` tab. 

#. You now need to select which Stage Gate to show the comparison data for. 
   Select Stage Gate 1 - 2 from the drop-down menu. 

#. A comparison of the response and threshold success rates should be shown at the top of the page. 
   Underneath this summary, a breakdown of the quantitative metric results will be displayed. 
   This table helps compare the key performance metrics for the two studies being compared. 

#. Finally, scroll to the top and click the ``Assessor Mode`` tab. 

#. Then select the Stage Gate you wish to compare. 
   Select Stage Gate 1 - 2 from the drop-down. 

#. This should populate the 5 bar charts on this page.
   Each chart shows a comparison of the assessor scores for the two studies. 
   The first graph compares the weighted average and average overall scores. 
   The next charts show a more detailed breakdown by question category and by evaluation area. 
