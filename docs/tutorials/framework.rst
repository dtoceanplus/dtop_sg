.. _sg-frameworks-tutorial:

Exploring the Framework data and setting metric thresholds
==========================================================

A Stage Gate process splits the technology development pathway into clearly defined Stages and Stage Gates.
Each Stage consists of a set of activities that should be completed before the technology developer can progress to the next stage. 
The Stage Gates comprise a set of questions related to the activities completed in the preceding Stage and that emulate the types of questions given to technology developers as part of a Stage Gate assessment within a technology development programme, such as the one run by `Wave Energy Scotland (WES) <https://www.waveenergyscotland.co.uk/>`_. 
The Stage Gate process contains additional elements such as *Evaluation Areas* and *performance metrics*. 
The term **Framework** is given to describe the whole process, including the set of Stages, Stage Gates, activities, questions, Evaluation Areas and performance metrics.
A full description of the Stage Gate Framework and how it was defined is provided in the :ref:`sg-explanation` section of the documentation. 

The Framework *feature* of the Stage Gate tool allows the user to view and explore the Stage Gate Framework that has been defined as part of the DTOceanPlus project and is incorporated in the Stage Gate module.
This is also the place where the user can specify the metric thresholds to apply in later analyses.

This section contains two tutorials:

#. `Using the Framework Explorer`_

#. `Applying metric thresholds`_

Using the Framework Explorer
----------------------------

This tutorial describes how to view the stage activity data and stage gate question data that is incorporated in the Stage Gate tool.

#. Click the main ``Frameworks`` button in the navigation pane on the left-hand side of the Stage Gate tool.
   You will see three further options. 
   Click the ``Framework Explorer`` tab. 

#. You should see a diagram showing the six Stages and the five Stage Gates that comprise the DTOceanPlus Stage Gate Framework.
   Each of the Stage and Stage Gates are buttons that can be selected to bring up further information for that component. 
   Click ``Stage 0`` to show the activities required to be completed in Stage 0. 

#. You should see a list of 7 *activity categories*, starting with *Requirements*.
   Click the ``Concept`` category button to expand that category and reveal the associated activities. 

#. There should be 6 activities within the *Concept* section.
   Every activity can be expanded to show a detailed description.
   Click the chevron alongside an activity, in the column with an information icon as the header, to do this.
   Expand the ``Device concept definition`` activity.
   You should see additional detailed information about this particular activity. 

#. Between the Framework diagram and the main explorer component there is a switch button that is currently set to ``Activity Category``. 
   Click this button. 

#. The button should switch to the ``Evaluation Area`` option.
   The list of activities should now be categorised by *Evaluation Area* rather than by the *Activity Categories* that were shown at the start. 

#. Each of the *Evaluation Areas* can be expanded in a similar way to show the related activities. 

.. Important::

   Note that the list of activities is the same, and all the switch button does is categorise the data in a different way. 
   Note also that an activity can be categorised under more than one *Evaluation Area*

8. Next, click on ``Stage Gate 1 - 2``. 
   Two tabs will appear for *Qualitative Questions* and *Quantitative Questions* respectively. 
   Click the ``Qualitative Questions`` tab. 

#. You should now see three *Question Categories*.
   Click the ``Technology`` section to reveal a description of this section, as well as a list of the three questions that form this question category. 

#. Click the ``Degree of novelty and innovation (I)`` section. 
   This will reveal the detailed description, the weighting and the scoring criteria for this particular question. 
   This is an example of a qualitative question. 

#. Next, go back to the tabs and click ``Quantitative Questions``. 
   
#. You should now see a table of quantitative questions, or *metrics*, that are required to be answered as part of this Stage Gate. 
   The table shows the metric name, unit, the evaluation area that the metric is related to, the weighting of the question and the threshold type. 
   The table can be filtered by evaluation area. 
   The chevrons in the *Details* column can be clicked to get a further description of each metric. 

#. The user can browse through each of the Stages and Stage Gates in the same way as described above. 

Applying metric thresholds
--------------------------

This tutorial describes how metric thresholds can be applied and used in future Stage Gate studies. 
To do this, the user must first create what is referred to as a **Threshold Set**. 

#. Click the main ``Frameworks`` button in the navigation pane on the left-hand side of the Stage Gate tool.
   You will see three further options. 
   Click the ``Metric Thresholds`` tab. 

#. You should see a list of the existing *Threshold Sets*. 
   To begin with, only the default set of thresholds (with no metric thresholds applied) should be listed.
   Note that because this is the default setting, this particular threshold set cannot be edited. 

#. At the top of the screen there will be a button labelled ``Create a new Threshold Set``. 
   Click this button. 

#. Give the new threshold set a name and a description. 
   Type ``Example threshold set`` into the input box for name and ``Example description`` into the description box. 
   Then click the ``Create`` button near the bottom right of the dialog box. 

#. You should see your new threshold set added to the list, under the default threshold set. 

#. For the new row that you just created, there should be three buttons now available to you; *Edit Set*, *Re-name set* and *Delete set*.
   Click ``Re-name set``.

#. This dialog box allows you to update the name and description of the threshold set that you just created. 
   Click ``Cancel``.

#. Now click ``Edit set`` for the threshold set you just created. 

#. You will see four tabs across the top of the page, corresponding to the last four Stage Gates. 

.. Note::

   No quantitative questions are asked in Stage Gate 0 - 1 and this is why there is no corresponding tab for this Stage Gate. 

1.  Click ``Stage Gate 1 - 2``.

#. You should see a similar table to that presented for the *Quantitative questions* in the **Framework Explorer** except for two additional columns in the table. 
   For the first row in the table (the *annual captured energy* metric), tick the box under the **Apply threshold** column. 

#. A number input box should then appear for that row in the **Metric threshold** column. 
   Type ``30000`` into the number box. 

#. Click the ``Save Thresholds`` button at the top right of the screen. 
   A notification should pop up saying that the thresholds have been saved. 
   This means that a threshold of 30,000 will be applied for the *annual captured energy* metric in Stage Gate 1 - 2 for any future Stage Gate studies that are associated with this *Threshold Set*. 
   Because this metric has a threshold with a **lower** threshold type, any metric results less than 30,000 kWh will **fail**, while metric results above 30,000 kWh will **pass** this threshold.

#. Finally, set two more metric thresholds. 
   Set the threshold for the *Array captured efficiency* metric to be ``0.5`` (i.e. 50%). 
   Set the *Jobs created* metric threshold to 30. 
   Save the thresholds by clicking the ``Save Thresholds`` button. 

.. Important::

   Note that anytime you change tabs on the Metric Threshold page, the threshold set will be saved. 

See the :ref:`sg-study-tutorial` tutorial for information on how to specify which threshold set to apply in a given Stage Gate study. 
