.. _sg-checklist-tutorial: 

Using the Activity Checklist
============================

The *Activity Checklist* helps the user identify the stage that they have reached in the technology development process. 
This feature presents the set of activities that need to be completed at each Stage in the form of a checklist. 
The feature can be used to quickly identify the technology readiness level of a device or technology. 
It can also be used to highlight the outstanding activities that are required to complete a specific Stage.
Users can treat the *Activity Checklist* as an interactive reference document, saving their progress and returning to the checklist throughout the development of their technology.
Note that the results of the checklist feature can also be obtained in the format of a standardised report.
See the tutorial on :ref:`generating a standardised report of the results <sg-report-tutorial>`.

.. Note::
   A Stage Gate study must have been created previously in order to run the checklist feature.
   See the tutorial on :ref:`sg-study-tutorial`.

Completing the checklist
------------------------
   
#. Go to the Stage Gate studies home page by clicking the ``Stage Gate Study`` button in the navigation pane on the left-hand side of the tool. 

#. For the previously created study, click the ``Select`` button in the Operations column for the study.
   This will bring you to the home page of that particular study.

#. Click the ``Perform`` button under the *Activity Checklist* heading.

#. You will be presented with the set of activities required to be completed for Stage 0. 
   Expand the ``Concept`` section. 

#. As with the **Framework Explorer**, you can expand the first activity of this category by clicking the chevron next to ``Device concept definition``. 

#. Each activity has a corresponding checkbox in the final column of the table. 
   In this example, let us assume that the technology developer has completed this activity. 
   Check the ``Complete?`` box for the ``Device concept definition`` activity. 
   Underneath the main explorer component there is a separate section with the heading **Live progress**. 
   These progress bars should now show that Stage 0 is 4% complete. 

#. Still with the ``Concept`` activity category selected, find the checkbox at the top of the main explorer component that is labelled *Mark category as complete*. 
   Tick this checkbox. 

#. All 6 activities in this category should now be checked and the **Live progress** bars should show that Stage 0 is 22% complete. 

#. As with the **Framework Explorer**, the user can also categorise the activities by evaluation area. 
   Click the switch button near the top left of the screen so that the activities are categorised by evaluation area. 
   Now click and expand the ``Installability`` evaluation area. 
   Remember that the list of activities is the same, all that changes is the categorisation. 
   Note that some activities can be listed under more than one *Evaluation Area*. 
   In this case, if you check or un-check an activity under one *Evaluation Area*, the status of the activity will be reflected in each of the *Evaluation Areas* that that activity falls under. 

#. Mark this category as complete using the same checkbox as before, which should now be unticked. 
   Again, all three activities that were shown should be toggled. 
   The **Live progress** bar for Stage 0 should now be 33%. 

#. Users can also mark entire Stages as being complete. 
   Between the diagram showing the 6 Stages and the main explorer component there is another checkbox labelled *Mark Stage as complete*. 
   Check this box. 

#. All the activities in Stage 0 will now be set to *Complete* and the **Live progress** bar should read 100% complete for Stage 0. 

#. Now let's move on to Stage 1 by clicking the appropriate circle in the diagram at the top of the page. 
   Mark this stage as complete as well by checking the *Mark Stage as complete* checkbox once again. 
   Stage 1 should be showing 100% completion. 
   Then move on to the next stage by clicking Stage 2. 

#. Use the switch button to categorise by Activity Category once more. 
   Mark the activity category ``Numerical modelling`` as complete. 
   Stage 2 should be showing 21%. 

#. Click the ``Save`` button at the bottom of the screen. 
   A notification saying that the checklist activities have been updated should appear. 
   This will save the current progress so that the user can quit the programme and come back at a later time to continue the exercise. 

Viewing the checklist results
-----------------------------

#. Click the ``View Results`` button at the bottom of the screen. 

#. The summary results will be displayed showing the stage that the device or technology has reached (Stage 1 in this case), as well as the Stage Gate that the technology is eligible to be assessed against (Stage Gate 1 - 2). 
   This results page also shows the percentage of activities completed for each stage (the same data as the **Live progress** bars on the input page).
   The results should show 100% complete for Stage 0 and Stage 1, 21% complete for Stage 2 and 0% for all the other Stages. 

#. Click ``Stage 2`` to get further details of the status of activities in Stage 2. 

#. This next results page shows the percentage of activities completed for each *Activity Category* and *Evaluation Area*.
   The *Numerical modelling* activity category should be 100% complete.
   The other categories should all be 0% complete. 
   For the *Evaluation Areas*, the *Power Capture* and *Power Conversion* areas should be the joint highest at 50%.

#. The final section of this detailed results page lists the *Outstanding Activities* that are still to be completed. 
   The user can navigate through these outstanding activities as before, categorising by either *Activity Category* or *Evaluation Area* as preferred. 

#. Go back to the main results page by scrolling to the top of the page and clicking ``Go Back``.

#. Underneath the main results graphic, a table is presented that shows the recommended complexity levels at which to run the other modules in the DTOceanPlus suite. 
   For more information on how you can use these suggestions in an integrated study, see the :ref:`sg-complexity-level-how-to` guide. 
