.. _sg-assessor-tutorial:

Running an assessment in Assessor Mode
======================================

This set of tutorials describes how to complete an assessment from the point of view of an Assessor. 
The **Assessor Mode** of the Stage Gate tool simulates how a funding body or technology development programme, similar to Wave Energy Scotland (WES), would take the application form submitted by a technology developer (in this case submitted using the Applicant Mode) and evaluate the responses, metric results and justifications provided. 
This process shows how Stage Gate applications are typically assessed and what the funding body or assessors are looking for in the application forms. 

Assessing a previously submitted application
--------------------------------------------

#. Go to the home page of the Stage Gate study that has been used in the preceding tutorials. 
   You can do this by clicking on the ``Stage Gate Studies`` button in the navigation pane on the left-hand side of the Stage Gate GUI, and then pressing the ``Select`` button for the study in question. 

#. Under the *Stage Gate Assessment* heading, click the ``Assessor Mode`` button. 
   You should now see a list of the Stage Gates. 
   In the previous tutorial, the user completed Stage Gate 1 - 2 in Applicant Mode. 
   You can only run Assessor Mode for Stage Gates that have been run in Applicant Mode. 
   For this reason, you should see that only Stage Gate 1 - 2 is available for Assessor Mode. 
   Click the ``Assess`` button under the Stage Gate 1 - 2 header. 

#. On the Assessor Mode input page, click the ``View Rubric`` button near the top of the page. 
   A pop-up box should appear showing the rubric that is used for all assessments in Assessor Mode. 
   This is a 5-point scoring system ranging from 0 to 4. 
   Descriptions are provided for what is needed to achieve each score. 
   Close the dialog box. 

#. Next, click on the ``Technology`` question category and expand the ``Degree of novelty and innovation (I)`` question.
   You should see the description and the weighting of the question as well as the response to the question that was implemented in the :ref:`sg-applicant-tutorial` tutorial. 

.. Important::

   The question weightings are simple percentages. The total weighting for each Stage Gate will sum to 100. 

5. Underneath the response, you will see the scoring criteria. 
   A text-area input is provided for each scoring criterion, with a character limit of 1,500.
   These boxes are for the assessor to provide feedback on how well the applicant has addressed the specific criterion. 
   You can add some dummy text to either of the two scoring criteria boxes here. 
   Note that these *assessor comments* are optional. 

#. Underneath the scoring criteria boxes, there is another user input for the **Assessor score**.
   This is where the user must mark the applicant's response, using the prescribed rubric. 
   There is an information icon next to the drop-down box that the user can click to bring up the rubric again if necessary. 
   Use the drop-down box to give this answer a score of 4. 

#. You should see the **Live progress** bar near the bottom of the page increase to 4% for the overall completion level and 14% for the qualitative questions. 
   In reality, an assessor would spend the time assessing each of the applicant responses, adding comments and providing a score. 
   For the sake of this tutorial we will skip to the quantitative questions. 
   First, click ``Save`` to save the results so far (a successful pop-up should appear). 
   Then click ``Next`` near the bottom of the page. 
   
#. You will now be presented with the familiar table of quantitative metrics. 
   This time however, when you expand a row, the user has the option of providing an comment and a score. 
   Expand the first row, for the *annual captured energy* metric and type some dummy text into the assessor comment box.
   Give this metric result a score of 4. 

#. Give the *array captured efficiency* metric a score of 4. 

#. Scroll down and give the *jobs created* metric a score of 0.
   As with the Applicant Mode tutorial, the **Live progress** bars should now be showing 14% completion for overall, qualitative and quantitative progress.
   Click the ``Submit`` button to save the answers and go to the results page. 

#. A new dialog box will appear stating that not all the answers have been marked. 
   You now have the option of filling all those answers that were missed with a score of 2. 
   Click ``Yes`` to proceed. 

Viewing the Assessor Mode results
----------------------------------

#. The top of the results page of the Assessor Mode feature should contain a bar chart showing the overall scores for the Stage Gate. 
   The weighted average and average scores for the entire Stage Gate are shown and should be 2.28 and 2.14 respectively. 

#. Scrolling down, you will see the breakdown of results for the different qualitative question categories. 
   The weighted average and average scores should be two for both the *Impact* and *Project* categories. 
   For the *Technology* category, the average should be 2.67 and the weighted average 2.9.

#. Scrolling down further, the evaluation area score breakdown is provided. 
   The *Acceptability* area should show scores of 1.33 and 1.5 for average and weighted average respectively. 
   The *Power Capture* area should show the maximum score of 4 for both. 
   All the others should show as *Acceptable* (i.e. a score of 2).

.. Important::

   The Evaluation Area scores are calculated from the quantitative metrics. 
   Each metric corresponds to a single Evaluation Area. 

4. Underneath the bar charts, the *Assessor scores and comments* will be re-produced for future reference. 
   If you entered any assessor comments on the inputs page, you should see these reflected in the relevant question. 
   These scores and comments are divided between qualitative and quantitative questions as before. 
