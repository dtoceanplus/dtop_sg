.. _sg-improvement-areas-tutorial:

Identifying the areas of improvement for a device or technology
===============================================================

Improvement areas are those evaluation areas that have been highlighted as weaknesses of the technology or device.
The **Improvement Areas** feature of the Stage Gate module can be seen as a sort of *wrapper* around the other three main features of the tool; the Activity Checklist, Applicant Mode and Assessor Mode.
It uses the results and input data from these three features to identify the areas of improvement for a technology. 

Specifically, the user takes a summary metric from each feature to calculate an *average improvement area score*.
This number is the average of the checklist, metric and weighted average assessor scores.
The **checklist score** is the result of the Activity Checklist for each evaluation area. 
For each Evaluation Area, the **metric score** is calculated as the number of metrics that have passed their metric thresholds divided by the number of metrics for that evaluation area that have thresholds applied, expressed as a percentage. 
Finally, the **assessor score** is calculated as the weighted average assessor score for each evaluation area, expressed as a percentage with respect to the maximum possible score (4). 

The average improvement area score is based only on those scores that are available. 
For instance, in the case where the metric and assessor scores are not applicable (e.g. Applicant Mode and Assessor Mode have not been completed), then the average improvement area score is just equal to the checklist score for each evaluation area. 
If only one score is unavailable, the average improvement area score is the average of the two remaining values. 

.. Note::

    The Activity Checklist is a prerequisite for the Improvement Area feature, it will only be available after a user has completed the activity checklist. 

#. Go to the home page of the Stage Gate study that has been used in the preceding tutorials. 
   You can do this by clicking on the ``Stage Gate Studies`` button in the navigation pane on the left-hand side of the Stage Gate GUI, and then pressing the ``Select`` button for the study in question. 

#. Because we have completed the Activity Checklist in a previous tutorial (:ref:`sg-checklist-tutorial`), the button to open the improvement areas page should be enabled. 
   Click the ``View improvement areas`` button. 

#. You will be asked to optionally select the Stage on which you want to perform the improvement area analysis.

.. Important::

   By default, the improvement areas will infer the Stage to use, selecting the earliest Stage in the framework that has not yet been fully completed. 
   For this tutorial, the inferred stage will be Stage 2 as this was only 21% complete after the :ref:`sg-checklist-tutorial` tutorial. 

   The Improvement Area feature also uses the Applicant and Assessor Modes for the improvement area analysis. 
   Where applicable, the analysis uses the Stage Gate immediately preceding the selected Stage. 
   In this case, with Stage 2 being the default selected stage, the improvement area analysis will be based on Stage Gate 1 - 2. 

4. The other input required for the Improvement Area feature is the threshold level for the *average improvement area score*, to define what is considered to be an improvement area.
   The default level is 50%. 

#. Leave the selected stage as Stage 2 and the improvement area threshold as 50%. 
   Scroll down to view the results table that presents the improvement areas.
   They are ranked by the *average improvement area score* in ascending order, so the biggest area of weakness is at the top.
   
#. For this tutorial, the *controllability* metric is the area of improvement with the lowest score. 
   None of the activities linked to this evaluation area were completed in Stage 2 and there are no metrics related to this evaluation area score, so the average score is just the average of 0%, the checklist score. 
   The second biggest area of improvement is *acceptability*.
   Again this scored 0 in the checklist and 0 for the metric result (remember that the *jobs created* metric did not pass its threshold in the :ref:`tutorial for Applicant Mode <sg-applicant-tutorial>`).
   It also scored only 38% in the assessor scores (recall that we gave an assessor score of 0 for the failed metric). 
   This leads to an average improvement area score of 13%. 

#. Update the improvement area threshold to 85% and click the ``Go`` button. 
   Now all evaluation areas with an average improvement area score of less than 85% are considered to be improvement areas. 
   The loaded results will now show the final evaluation area, *Power Capture*, with an overall score of 83%. 
   Note that it has metric and weighted average assessor scores of 100% because of the answers we provided in the previous tutorials for Applicant and Assessor Modes. 

#. Finally, scroll to the top of the page and change the selected stage to Stage 1 using the drop-down button.
   The improvement area table should now be empty. 
   This makes sense because we marked all of Stage 1 complete and did not perform Applicant or Assessor Mode analysis on Stage Gate 0 - 1. 
