.. _sg-report-tutorial:

Generating a standardised Stage Gate report in PDF format
=========================================================

A standardised PDF report can be generated to provide a summary of the main data and results from a Stage Gate study. 
The results from each of the four main features described in the previous tutorials can be included in this report. 

.. Note::

    The Activity Checklist is a prerequisite for the Report Export feature, it will only be available after a user has completed the activity checklist. 

#. Go to the home page of the Stage Gate study that has been used in the preceding tutorials. 
   You can do this by clicking on the ``Stage Gate Studies`` button in the navigation pane on the left-hand side of the Stage Gate GUI, and then pressing the ``Select`` button for the study in question. 

#. Open the **Report Export** feature by clicking the ``Generate PDF report`` button. 

#. You should see a nested list of checkboxes.
   These checkboxes should be used to select the particular sections that you want to include in the report. 
   To begin with, select the main *Activity Checklist* section. 

#. Because you have selected the Checklist feature, you are required to specify the Stage that you want to use as the basis of the analysis. 
   Using the drop-down, select Stage 2. 
   Click ``Generate Report``. 

#. A loading screen should appear for about 5 seconds.
   After this the generated PDF should be downloaded to the browser that you are using. 
   You can open the report to have a look at the contents and double check that the appropriate sections are included. 
   The report should include the table of stage results, two bar-charts showing the detailed breakdown of checklist results and a bullet-point list of the outstanding activities. 

#. Return to the Stage Gate GUI.

#. Un-check the *Activity Checklist* box and tick both the *Applicant Mode* and *Assessor Mode* boxes. 

#. Now you will be asked for the selected Stage Gate to use as the basis of the analysis. 
   Select Stage Gate 1 - 2.
   Click ``Generate Report``. 

#. A new PDF document should be downloaded to your browser or your downloads folder. 
   Again, take a look at the generated document to ensure that it only contains the sections that we have requested. 

#. Return to the Stage Gate GUI.
   Un-check the *Applicant* and *Assessor* mode boxes.
   Now check the following boxes: the *detailed breakdown of Stage results*, the *metric results* from Applicant Mode, and the *improvement areas*. 
   You are asked to select both the Stage and Stage Gate in this case. 
   You are also asked to specify the improvement area threshold level. 
   Select ``Stage 2`` and ``Stage Gate 1 - 2`` as before. 
   Leave the improvement area threshold as the default value of 50%.
   Click ``Generate Report``. 

#. The newest report should be downloaded after about 5 seconds. 
   Check the report to see the results are as expected. 
