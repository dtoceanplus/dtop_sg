.. _sg-applicant-tutorial:

Running an assessment in Applicant Mode
=======================================

This set of tutorials describes how to complete an assessment from the point of view of an Applicant. 
The **Applicant Mode** of the Stage Gate tool simulates the process of a typical Stage Gate assessment, where multiple applicants need to complete an application form and answer questions about the research and development activities they have performed for their technology.
These application forms form the basis of the assessment for deciding which devices/technologies should pass through the Stage Gate and move on to the next stage. 

Completing an Applicant Mode assessment
---------------------------------------

#. Go to the home page of the Stage Gate study that has been used in the preceding tutorials. 
   You can do this by clicking on the ``Stage Gate Studies`` button in the navigation pane on the left-hand side of the Stage Gate GUI, and then pressing the ``Select`` button for the study in question. 

#. Under the *Stage Gate Assessment* heading, click the ``Applicant Mode`` button. 
   You should now see a list of the Stage Gates. 
   As the previous tutorial suggested that the mock device/technology is eligible for Stage Gate 1 - 2, click the ``Perform`` button under the *Stage Gate 1 - 2* header. 

#. First up are the **Qualitative questions**. 
   Click on the ``Technology`` question category and then on the ``Degree of novelty and innovation (I)`` question. 
   You should see the description of the question, its weighting and the associated scoring criteria. 
   Finally, there is a text area input for the user to fill in, labelled **Response**. 
   Enter some dummy text in this box. 

#. The text area will expand as you fill the box with text. 
   A character limit is also shown, with a total limit of 5,000 characters which is roughly equivalent to 800 words or 1.5 pages. 
   Similar to the input page of the **Activity Checklist** feature, there is a **Live progress** graphic towards the bottom of the page. 
   Once you have entered some text into this box, you should see the overall progress for the Stage Gate has risen to 4%. 
   The progress for the qualitative questions should be 14%. 

#. Click the ``Save`` button at the bottom of the screen.
   A pop-up should appear saying that the responses have been saved. 

#. The user would then make their way through all the qualitative questions for the Stage Gate. 
   For now, we will skip ahead to the quantitative section. 
   Click the ``Next`` button near the bottom of the screen. 

#. There is a button near the top of the page labelled *Use results from the Deployment and Assessment modules*. 
   Do **not** click this button, as this tutorial does not involve an integrated DTOceanPlus project. 
   The use of this button and integrated mode is covered in the guide for :ref:`sg-metric-results-how-to`. 

#. You should also be able to see the table of metrics that are needed for Stage Gate 1 - 2. 
   The thresholds that were created in the previous :ref:`Framework tutorial <sg-frameworks-tutorial>` should also be visible for the three appropriate metrics. 
   Expand the row for *annual captured energy* by clicking the chevron in the left-hand side column labelled *View/Edit*. 

#. The detailed metric description should now be visible, along with an input box for the **Result** and the **Justification**. 
   Applicants to a Stage Gate assessment are expected to provide sufficient justification and assumptions for all calculations and quantitative metrics. 
   For now, enter a value of 32,000 kWh for the *annual captured energy* metric.
   And enter some dummy text in the corresponding *Justification* box. 
   
#. The progress for quantitative questions should now be 5% and the overall progress for the Stage Gate 7%. 
   Note that a quantitative question is only considered to be answered when both a result *and* a justification have been provided. 

#. Normally, an applicant would be expected to fill in results and justifications for each of these quantitative metrics. 
   For this tutorial, we will only provide two more results. 
   Set the result for the *array captured efficiency* metric to 0.55 and add some dummy justification text. 
   Set the result for the *jobs created* metric to 26 and add some dummy justification text. 

#. All progress bars should now read 14%. 
   Click the ``Submit`` button at the bottom of the screen which will save the answers and bring you to the results page. 


Viewing the Applicant Mode results
----------------------------------

#. The top of the results page of the Applicant Mode feature should show a *response rate* of 14% and a *threshold success rate* of 67%. 
   The response rate is the same as the overall percentage in the **Live progress** bars in the input page; it reflects how many of the questions you have answered fully. 
   The threshold success rate is the number of metric thresholds that have passed, normalised with respect to the number of metrics that have had thresholds applied. 
   Note that the threshold success rate is only shown if metric thresholds have been set. 

#. The next section of the results page shows the main explorer component, but this time the responses will be included in the bullet points for the question. 
   Check that the dummy response you submitted for the first question is visible for the same question on the results page. 

#. The final component on the Applicant Mode results page is the summary of the metric results. 
   Any metric results that have passed will be highlighted in green, while those that have failed will be highlighted in red. 
   In this case, the *annual captured energy* and *array captured efficiency* metrics should be shown in green. 
   At the bottom of the page, the *jobs created* metric will be highlighted in red. 
   For any metrics that have failed, the **Absolute distance** and **Percentage distance** will be calculated and shown. 
   Absolute distance is the distance between the metric and the threshold, in the units of the metric.
   Percentage distance is the absolute distance divided by the threshold and expressed as a percentage. 
   In this example, the absolute distance for *jobs created* should be 5 and the percentage distance should be 17%. 

#. Additional details, including the justification provided by the applicant can be shown by clicking the chevrons in the first colum of the table. 
