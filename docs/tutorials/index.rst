.. _sg-tutorials:

*********
Tutorials
*********

Below is a set of tutorials that guide the user through each of the main functionalities of the Stage Gate tool.
They are intended for those who are new to the module.
The tutorials should be completed in the order suggested below, as each tutorial will require certain outputs from previous ones. 
For instance, you cannot complete *Assessor Mode* without first completing an analysis in *Applicant Mode*.
Moreover, all the features apart from the *Framework* component first require a *Stage Gate study* to be created.

#. :ref:`sg-frameworks-tutorial`
#. :ref:`sg-study-tutorial`
#. :ref:`sg-checklist-tutorial`
#. :ref:`sg-applicant-tutorial`
#. :ref:`sg-assessor-tutorial`
#. :ref:`sg-improvement-areas-tutorial`
#. :ref:`sg-report-tutorial`
#. :ref:`sg-comparison-tutorial`

.. toctree::
   :maxdepth: 1
   :hidden:

   framework
   sg_study
   activity_checklist
   applicant_mode
   assessor_mode
   improvement_areas
   report_generation
   study_comparison
