.. _sg-study-tutorial:

Managing Stage Gate studies
===========================

To perform a Stage Gate analysis, you must first create a *Stage Gate study*.
A Stage Gate study is the entity that is used to record and save the input and output data associated with a single analysis in the Stage Gate tool. 
Stage Gate studies must be given a *name*.
An optional *description* of the study can also be specified. 
Finally, each Stage Gate study must be associated with a single **Threshold set** (see the :ref:`Framework tutorial<sg-frameworks-tutorial>` for more information). 

This section contains three tutorials focusing on how to 

#. `Create a Stage Gate study`_

#. `Re-name a Stage Gate study`_ and 

#. `Delete a Stage Gate study`_.


.. Important::

   Stage Gate studies can be created from within the Stage Gate module itself or via the **Project** and **Study** features of the main DTOceanPlus interface.
   Studies must be created using the main interface if the user wishes to integrate with any other module in the suite of tools. 
   The following tutorials focus on the use cases of creating, editing and deleting Stage Gate studies from within the Stage Gate module, but the procedure is very similar when using the main DTOceanPlus interface.

Create a Stage Gate study
-------------------------

#. Navigate to the list of Stage Gate studies by clicking the ``Stage Gate Studies`` button in the navigation pane on the left-hand side of the Stage Gate GUI. 
   No default studies are provided with the tool so you should see an empty list if you have not yet created any studies. 

#. At the top left corner of the screen there will be a button labelled ``Create Stage Gate study``. Click this button. 

#. On the dialog box that appears, type in a *Name* and a *Description* of the study.
   Note that the description is an optional parameter, but the name is not. 

#. Next, use the dropdown menu to associate the Stage Gate study with the previously-created set of metric thresholds.
   This should be labelled **Example threshold set**. 
   Please see the :ref:`Framework tutorials <sg-frameworks-tutorial>` for instructions for how to create this set of metric thresholds. 

#. Click the ``Create`` button. 

#. The list of studies should then appear, with the newly created study visible in the table of Stage Gate studies. 

Re-name a Stage Gate study
--------------------------

#. The final column of the Stage Gate studies table is labelled *Operations* and contains a set of three buttons for each Stage Gate study. 
   Click the ``Re-name study`` button for the study created in the previous tutorial. 

#. A new dialog box will pop up with the name, *Update a Stage Gate study*. 

#. Enter a new name and description for the study.
   Note that once a study is created, it is not possible to update the threshold set that was originally applied to that study. 

#. Click the ``Update`` button. 

#. The list of studies will be displayed and the name and description of the study should be updated. 


Delete a Stage Gate study
-------------------------

#. The last button in the *Operations* column can be used to delete any pre-existing Stage Gate study. 
   For the study that was created in the first tutorial of this section and edited in the preceding tutorial, click the ``Delete`` button. 

#. A dialog box will appear asking you to confirm the decision. 
   Click ``Delete`` to permanently delete the Stage Gate study. 

#. When the Stage Gate studies home page has loaded, you should see that the selected study has been deleted. 
