.. _sg-home:

Stage Gate
==========

Introduction 
------------

The  Stage  Gate  design  tool  is  a  framework that  guides  a  user  through  a  technology  assessment process.
It is intended to be used by a wide variety of stakeholders, including: 

- Technology developers in the evaluation of their own technology and

- Investors and public funders to aid design of funding programmes and decision making on candidate technologies

The aim of this tool is to guide the technology development process and facilitate the assessment of ocean energy technologies. 
As a tool, it operates with close integration to the Structured Innovation, Deployment and Assessment tools, to support consistent assessment processes and ultimately guide decision making for the users of the tool. 

Structure
---------

This documentation is divided into four main sections:

- :ref:`sg-tutorials` to give step-by-step instructions on using SG for new users. 

- :ref:`sg-how-to` that show how to achieve specific outcomes using SG. 

- A section on :ref:`background and theory<sg-explanation>` that describes how SG works and how the data presented by the tool have been defined. 

- The :ref:`API reference <sg-reference>` section documents the code of modules, classes, API, and GUI. 

.. toctree::
   :hidden:
   :maxdepth: 1

   tutorials/index
   how_to/index
   explanation/index
   reference/index

Functionalities
---------------

The Stage Gate module has seven major functionalities:  

#. :ref:`Stage Gate Framework <sg-frameworks-tutorial>`– functionality for viewing the Stage Activity and Stage Gate Question data of the Stage Gate Framework developed for DTOceanPlus. 
   This functionality also enables the user to edit the Stage Gate Framework by specifying the metric thresholds that are applied. 

#. :ref:`Activity Checklist <sg-checklist-tutorial>` – allows the user to work through the required activities for each stage of the Stage Gate programme in turn and record whether they have been completed or not. 
   This enables SG to identify the specific stage that a device or technology has reached. 

#. :ref:`Applicant Mode <sg-applicant-tutorial>` – the first component of the Stage Gate assessment functionality. 
   Presents to the user a set of qualitative and quantitative questions about their technology that they must answer. 
   Emulates the application process at the stage gate of a typical technology development programme, from the point of view of the Applicant. 

#. :ref:`Assessor Mode <sg-assessor-tutorial>` – the second component of the Stage Gate assessment functionality.
   Presents to the user the answers supplied by an Applicant in a previous Applicant Mode assessment and requests Assessor scores and comments. 
   Emulates the assessment process of a Stage Gate of a typical technology development programme, from the point of view of the Assessor.  

#. :ref:`Improvement Areas <sg-improvement-areas-tutorial>` – the methodology for identifying the improvement areas highlighted by a Stage Gate analysis. 
   These refer to the characteristics of a device or technology that the SG module has identified as needing further development or refinement.  

#. :ref:`Report Export <sg-report-tutorial>` – generates a standardised report in PDF format that summarise all the key information associated with a Stage Gate analysis.  

#. :ref:`Study Comparison <sg-comparison-tutorial>` – compares the results of two or more Stage Gate Assessments that have been performed by the user.  
