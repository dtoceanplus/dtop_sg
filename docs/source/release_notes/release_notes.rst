.. _sg-release-notes:

*************
Release notes
*************

v1.0.0
======

* Version for release 

* Full documentation 

* Final improvements to front-end

* Adds LICENCE data

* Removes obsolete integration methods previously added as a test by OCC

v0.11.0
=======

* Make required improvements to Report Export, as suggested by Verification Cases

  * Major change: the PDF generation for the report is now done in the front-end, so the back-end does not need to generate the plots or the exact content for the report.

  * Now sends a much simpler response containing the data required to generate the report content in the front-end

  * This removes the dependency on plotly and kaleido in the front end, and reduces the size of the back-end image that is required (the front-end image is now larger, due to large SVGs and other images now being stored in the front-end)

  * Most importantly, it addresses a memory leak issue that was arising from sending the generated plotly SVGs from the back-end to the front-end

* Adds a limit to text area inputs and reflects this in the database modules

* Updates the complexity level map to the latest and final version of this, and includes machine characterisation

* Updates the survivability ULS metric extraction and fixes the error that was arising if the RAMS results-summary response is only partially completed (missing keys)

* Refactors SG tab name; adds Rajdhani font

v0.10.0
=======

* Implement suggested changes for the Improvement Areas feature 

  * Now uses average improvement area score to define improvement areas; this is the average of checklist, metric and assessor scores

  * Changes to improvement-areas and report-data routes; request bodies now require the improvement area threshold to be specified (default is 50%)

  * FE presents improvement areas in a clearer table  

v0.9.0
======

* Adds final version of Framework data

* Some minor GUI improvements for homogeneity 

* Update module ID for SG to 13

v0.8.1
******

* Finalises configuration of inter-module communication; adding environment variables and functions that can infer the DOMAIN and PROTOCOL names, depending on whether it is a production or test environment

v0.8.0
======

* Adds two new API routes for metric thresholds and metric results; required as services to be consumed by SI module. Services very similar to existing services but only dependent on Stage Gate study ID

* Also updates the provider states set-up to include these two new services

* Fixes the base URLs for inter-module communication (e.g. 'mm.dto.test' rather than 'mm/dto.test') and make corresponding changes in Mountebank 

* Also re-names docker image names for Mountebank mocks to include the new test domain "dto.test"

* Adds Framework re-vamp changes; based on the improvement list from verification

  * Framework explorer component

  * Single page for metric thresholds 

  * CRUD operations for "Threshold Sets" rather than "Frameworks"

  * Clickable SVG diagram for Framework explorer

* Removes constraint for Stage Gate studies to have unique names

v0.7.0
======

* Fixes several bugs
  
  * Most importantly, in the Report Generation feature where the generated SVG being rendered in bytes was no longer recognised in the FE pdfmake operation

  * Also fixed a logic error in the Report Generation input page which was working incorrectly when only the detailed Stage results were selected

  * Fixes a Front-end initialisation error that arose in the Checklist Input page that would lead to errors in evaluating computed properties if the tabIndex parameter had not been selected.

* Updates FE of Assessor Mode, based on improvements highlighted in verification task

  * Splits qualitative and quantitative questions

  * Vertical tab format for qualitative, tabular format for quantitative

  * Adds live progress bar showing how many questions have been assessed

  * Tidies output page of Assessor Mode, splitting qualitative and quantitative questions to match the new input page

  * Combines bar charts for average and weighted average results into single charts

* Fixes bug in Applicant Mode where a failed request to the Main Module resulted in a re-fresh and loss of data. Solved by saving data when navigating between qual and quant questions. 


v0.6.0
======

* Updates FE of Applicant Mode, based on improvements highlighted in verification task

  * Split qualitative and quantitative questions
    
  * Add buttons for integration with D&A modules
    
  * Tabular input form for quantitative questions

* Adds PACT testing; both the full set of consumer tests and the provider states setup required for SG as a provider

* Adds integration to the Main Module in the FE and completes the MM integration capability in the BE

* DTOP_DOMAIN environment variable is added to identify appropriate base URLs

* Simplifies input page for Study Comparison

v0.5.0
======

* Makes the recommended changes to the Activity Checklist feature, based on feedback from verification, this includes

  * Improved structure for displaying Stage activities

  * Select/de-select all buttons

  * Clickable navigation diagram for checklist inputs

  * Live Progress as checklist is updated

  * Includes combination matrix in output page

  * The final version of activities from Task 12 (some minor descriptions to be added)

* NB: the checklist-inputs API route has been changed; a 'stage_activities' component has been added

* Uses Makefile-based CI method for E2E Cypress tests

* Stops generating videos in Cypress

* Uses minimal requirements.txt file as basis for all python images

* Update README with appropriate instructions



v0.4.0
======

* Adds Continuous Deployment (CD) procedure developed by OCC (see Continuous Deployment page on Wiki or issue #25)

* Solves the following issues (task numbers are taken from *beta* development task list)

  * Best way to deploy a module in standalone mode; solved by the implementation of CD procedure (#25)

  * Implements code coverage for Cypress tests (#5)

  * Fixes formatting bugs in front-end (#6 and #14)

  * Update FE image in CI pipeline (#47)

  * Bug in FE code of Assessor Mode inputs page (#48)

* Adds Python back-end coverage and Cypress end-to-end testing coverage to GitLab CI summary coverage value

* Adds Mountebank testing for integration with other DTO+ modules

* Updates documentation folder, following the template created for the documentation of the full suite of DTO+ tools (`dtop_documentation`)

v0.3.0
======

* Adds the following two features

  * The **Improvement Area** feature that shows the areas of weakness of a technology or device based on the three main features of the Stage Gate tool
    
  * The **Report Export** feature that enables the user to create a summary report of the results obtained in a Stage Gate study 

v0.2.0
======

* Adds the **Study Comparison** feature that allows the results obtained from the first three, key features to be compared across Stage Gate studies


v0.1.0
======

* Same as the *alpha* version but includes finalised versions of the public Stage Gate services that will be required by the Structured Innovation module

* Version also finishes adding front-end (FE) and end-to-end (E2E) integration tests for the first 3 functionalities 

* Also includes a feature that gives the option of automatically filling the Assessor Scores of all unanswered questions with an average score of 2


v0.1.1
******

* Hotfix for a bug related to the weightings of the questions in the default DTOceanPlus Stage Gate framework. A question category was omitted. 
  Additional unit tests added in this version to ensure that the total weighting in each Stage Gate sums to 100. 


alpha
=====

* First version that corresponds to the production and release of DTOceanPlus deliverable D4.2

* Includes the following features

  * **Activity Checklist**

  * **Applicant Mode**

  * **Assessor Mode**
