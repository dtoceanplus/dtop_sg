import json
import sys

import dredd_hooks as hooks
import requests

sys.stdout = sys.stderr = open("dredd-hooks-output.txt", "w")


def if_not_skipped(func):
    def wrapper(transaction):
        if not transaction['skip']:
            func(transaction)
    return wrapper


# @hooks.before('/api/stage-gate-studies > List Stage Gate studies > 200 > application/json')
# @hooks.before('/api/stage-gate-studies > Create Stage Gate study > 201 > application/json')
# @hooks.before('/api/stage-gate-studies/{sgId} > Read Stage Gate study > 200 > application/json')
# @hooks.before('/api/stage-gate-studies/{sgId} > Update Stage Gate study > 200 > application/json')
# @hooks.before('/api/stage-gate-studies/{sgId} > Update Stage Gate study > 400 > application/json')
# @hooks.before('/api/stage-gate-studies/{sgId} > Delete a Stage Gate study from the database > 200 > application/json')
# @hooks.before('/api/stage-gate-studies/{sgId} > Delete a Stage Gate study from the database > 400 > application/json')
# @hooks.before('/api/frameworks > List Stage Gate frameworks > 200 > application/json')
# @hooks.before('/api/frameworks > Create Stage Gate framework > 201 > application/json')
# @hooks.before('/api/frameworks/{frameworkId} > Read Stage Gate framework > 200 > application/json')
# @hooks.before('/api/frameworks/{frameworkId} > Update Stage Gate framework > 200 > application/json')
# @hooks.before('/api/frameworks/{frameworkId} > Update Stage Gate framework > 400 > application/json')
# @hooks.before('/api/frameworks/{frameworkId} > Delete a Stage Gate framework from the database > 200 > application/json')
# @hooks.before('/api/frameworks/{frameworkId} > Delete a Stage Gate framework from the database > 400 > application/json')
# @hooks.before('/api/stages/{stageId} > Read a stage from a Stage Gate framework > 200 > application/json')
# @hooks.before('/api/stages/{stageId}/eval > Read a stage from a Stage Gate framework, categorised by evaluation area > 200 > application/json')
# @hooks.before('/api/stage-gates/{gateId} > Read a stage gate from a Stage Gate framework > 200 > application/json')
# @hooks.before('/api/question-metrics/{questionMetricId} > Update Stage Gate question metric > 200 > application/json')
# @hooks.before('/api/question-metrics/{questionMetricId} > Update Stage Gate question metric > 400 > application/json')
# @hooks.before('/api/question-metrics > Update multiple Stage Gate question metrics > 200 > application/json')
# @hooks.before('/api/question-metrics > Update multiple Stage Gate question metrics > 400 > application/json')
# @hooks.before('/api/frameworks/{frameworkId}/metric-thresholds > List metric thresholds entered by a user for a Stage Gate framework > 200 > application/json')
# @hooks.before('/api/stage-gate-studies/{sgId}/checklist-inputs > List completed activities > 200 > application/json')
# @hooks.before('/api/stage-gate-studies/{sgId}/checklist-inputs > Update completed activities > 200 > application/json')
# @hooks.before('/api/stage-gate-studies/{sgId}/checklist-inputs > Update completed activities > 400 > application/json')
# @hooks.before('/api/stage-gate-studies/{sgId}/checklist-outputs > Get results of a Stage Gate activity checklist study > 200 > application/json')
# @hooks.before('/api/stage-gate-studies/{sgId}/stage-gate-assessments > List Stage Gate Assessments > 200 > application/json')
# @hooks.before('/api/stage-gate-assessments/{appSgId}/applicant-answers > List Applicant Answers > 200 > application/json')
# @hooks.before('/api/stage-gate-assessments/{appSgId}/applicant-answers > Update Applicant Answers > 200 > application/json')
# @hooks.before('/api/stage-gate-assessments/{appSgId}/applicant-answers > Update Applicant Answers > 400 > application/json')
# @hooks.before('/api/stage-gate-assessments/{appSgId}/applicant-answers-integrated > Load metrics from deployment and assessment tools > 200 > application/json')
# @hooks.before('/api/stage-gate-assessments/{appSgId}/applicant-answers-integrated > Load metrics from deployment and assessment tools > 400 > application/json')
# @hooks.before('/api/stage-gate-assessments/{appSgId}/applicant-results > List Applicant Results > 200 > application/json')
# @hooks.before('/api/stage-gate-assessments/{appSgId}/applicant-status > Set applicant complete status as true > 200 > application/json')
# @hooks.before('/api/stage-gate-assessments/{appSgId}/assessor-scores > List Assessor Scores > 200 > application/json')
# @hooks.before('/api/stage-gate-assessments/{appSgId}/assessor-scores > Update Assessor Scores > 200 > application/json')
# @hooks.before('/api/stage-gate-assessments/{appSgId}/assessor-scores > Update Assessor Scores > 400 > application/json')
# @hooks.before('/api/stage-gate-assessments/{appSgId}/assessor-status > Set assessor complete status as true > 200 > application/json')
# @hooks.before('/api/stage-gate-assessments/{appSgId}/assessor-results > List Assessor Results > 200 > application/json')
# @hooks.before('/api/stage-gate-assessments/{appSgId}/assessor-results > List Assessor Results > 400 > application/json')
# @hooks.before('/api/stage-gate-studies/{sgId}/improvement-areas > List improvement areas highlighted in a Stage Gate study > 200 > application/json')
# @hooks.before('/api/stage-gate-studies/{sgId}/improvement-areas > List improvement areas highlighted in a Stage Gate study > 400 > application/json')
# @hooks.before('/api/stage-gate-studies/{sgId}/improvement-areas > Identify improvement areas for a specific Stage in a Stage Gate study > 200 > application/json')
# @hooks.before('/api/stage-gate-studies/{sgId}/improvement-areas > Identify improvement areas for a specific Stage in a Stage Gate study > 400 > application/json')
# @hooks.before('/api/stage-gate-studies/{sgId}/complexity-levels > List the evaluated stage, stage gate and corresponding complexity levels of a study > 200 > application/json')
# @hooks.before('/api/stage-gate-studies/{sgId}/complexity-levels > List the evaluated stage, stage gate and corresponding complexity levels of a study > 400 > application/json')
# @hooks.before('/api/stage-gate-studies/{sgId}/complexity-levels > List the evaluated stage, stage gate and corresponding complexity levels of a study > 404 > application/json')
# @hooks.before('/api/stage-gate-studies/checklist-comparison > Get checklist comparison data > 200 > application/json')
# @hooks.before('/api/stage-gate-assessments/applicant-comparison > Get applicant comparison data > 200 > application/json')
# @hooks.before('/api/stage-gate-assessments/assessor-comparison > Get assessor comparison data > 200 > application/json')
# @hooks.before('/api/stage-gate-studies/checklist-comparison > Get checklist comparison data > 404 > application/json')
# @hooks.before('/api/stage-gate-assessments/applicant-comparison > Get applicant comparison data > 404 > application/json')
# @hooks.before('/api/stage-gate-assessments/assessor-comparison > Get assessor comparison data > 404 > application/json')
# @hooks.before('/api/stage-gate-studies/{sgId}/report-data > Get the Stage Gate summary report data > 200 > application/json')
# @hooks.before('/api/stage-gate-studies/{sgId}/report-data > Get the Stage Gate summary report data > 400 > application/json')
# @hooks.before('/api/stage-gate-studies/{sgId}/metric-thresholds > List metric thresholds applied in a specified Stage Gate study > 200 > application/json')
# @hooks.before('/api/stage-gate-studies/{sgId}/metric-thresholds > List metric thresholds applied in a specified Stage Gate study > 400 > application/json')
# @hooks.before('/api/stage-gate-studies/{sgId}/metric-results > List metric results for latest Stage Gate in a Stage Gate study > 200 > application/json')
# @hooks.before('/api/stage-gate-studies/{sgId}/metric-results > List metric results for latest Stage Gate in a Stage Gate study > 400 > application/json')
def _skip(transaction):
    transaction['skip'] = True


@hooks.before('/api/stage-gate-studies > List Stage Gate studies > 200 > application/json')
@hooks.before('/api/stage-gate-studies/{sgId} > Read Stage Gate study > 200 > application/json')
@hooks.before('/api/stage-gate-studies/{sgId} > Update Stage Gate study > 200 > application/json')
@hooks.before('/api/stage-gate-studies/{sgId} > Update Stage Gate study > 400 > application/json')
@hooks.before('/api/stage-gate-studies/{sgId} > Delete a Stage Gate study from the database > 200 > application/json')
@hooks.before('/api/stage-gate-studies/{sgId} > Delete a Stage Gate study from the database > 400 > application/json')
@hooks.before('/api/stage-gate-studies/{sgId}/checklist-inputs > List completed activities > 200 > application/json')
@hooks.before('/api/stage-gate-studies/{sgId}/checklist-inputs > Update completed activities > 200 > application/json')
@hooks.before('/api/stage-gate-studies/{sgId}/checklist-inputs > Update completed activities > 400 > application/json')
@hooks.before('/api/stage-gate-studies/{sgId}/checklist-outputs > Get results of a Stage Gate activity checklist study > 200 > application/json')
@hooks.before('/api/stage-gate-studies/{sgId}/stage-gate-assessments > List Stage Gate Assessments > 200 > application/json')
@hooks.before('/api/stage-gate-assessments/{appSgId}/applicant-answers > List Applicant Answers > 200 > application/json')
@hooks.before('/api/stage-gate-assessments/{appSgId}/applicant-answers > Update Applicant Answers > 200 > application/json')
@hooks.before('/api/stage-gate-assessments/{appSgId}/applicant-answers > Update Applicant Answers > 400 > application/json')
@hooks.before('/api/stage-gate-assessments/{appSgId}/applicant-answers-integrated > Load metrics from deployment and assessment tools > 200 > application/json')
@hooks.before('/api/stage-gate-assessments/{appSgId}/applicant-answers-integrated > Load metrics from deployment and assessment tools > 400 > application/json')
@hooks.before('/api/stage-gate-assessments/{appSgId}/applicant-results > List Applicant Results > 200 > application/json')
@hooks.before('/api/stage-gate-assessments/{appSgId}/applicant-status > Set applicant complete status as true > 200 > application/json')
@hooks.before('/api/stage-gate-assessments/{appSgId}/assessor-scores > List Assessor Scores > 200 > application/json')
@hooks.before('/api/stage-gate-assessments/{appSgId}/assessor-scores > Update Assessor Scores > 200 > application/json')
@hooks.before('/api/stage-gate-assessments/{appSgId}/assessor-scores > Update Assessor Scores > 400 > application/json')
@hooks.before('/api/stage-gate-assessments/{appSgId}/assessor-status > Set assessor complete status as true > 200 > application/json')
@hooks.before('/api/stage-gate-assessments/{appSgId}/assessor-results > List Assessor Results > 200 > application/json')
@hooks.before('/api/stage-gate-assessments/{appSgId}/assessor-results > List Assessor Results > 400 > application/json')
@hooks.before('/api/stage-gate-studies/{sgId}/complexity-levels > List the evaluated stage, stage gate and corresponding complexity levels of a study > 200 > application/json')
@hooks.before('/api/stage-gate-studies/{sgId}/improvement-areas > List improvement areas highlighted in a Stage Gate study > 200 > application/json')
@hooks.before('/api/stage-gate-studies/{sgId}/improvement-areas > List improvement areas highlighted in a Stage Gate study > 400 > application/json')
@hooks.before('/api/stage-gate-studies/{sgId}/improvement-areas > Identify improvement areas for a specific Stage in a Stage Gate study > 200 > application/json')
@hooks.before('/api/stage-gate-studies/{sgId}/improvement-areas > Identify improvement areas for a specific Stage in a Stage Gate study > 400 > application/json')
@hooks.before('/api/stage-gate-studies/{sgId}/complexity-levels > List the evaluated stage, stage gate and corresponding complexity levels of a study > 400 > application/json')
@hooks.before('/api/stage-gate-studies/checklist-comparison > Get checklist comparison data > 200 > application/json')
@hooks.before('/api/stage-gate-assessments/applicant-comparison > Get applicant comparison data > 200 > application/json')
@hooks.before('/api/stage-gate-assessments/assessor-comparison > Get assessor comparison data > 200 > application/json')
@hooks.before('/api/stage-gate-studies/checklist-comparison > Get checklist comparison data > 404 > application/json')
@hooks.before('/api/stage-gate-assessments/applicant-comparison > Get applicant comparison data > 404 > application/json')
@hooks.before('/api/stage-gate-assessments/assessor-comparison > Get assessor comparison data > 404 > application/json')
@hooks.before('/api/stage-gate-studies/{sgId}/report-data > Get the Stage Gate summary report data > 200 > application/json')
@hooks.before('/api/stage-gate-studies/{sgId}/report-data > Get the Stage Gate summary report data > 400 > application/json')
@hooks.before('/api/stage-gate-studies/{sgId}/metric-thresholds > List metric thresholds applied in a specified Stage Gate study > 200 > application/json')
@hooks.before('/api/stage-gate-studies/{sgId}/metric-thresholds > List metric thresholds applied in a specified Stage Gate study > 400 > application/json')
@hooks.before('/api/stage-gate-studies/{sgId}/metric-results > List metric results for latest Stage Gate in a Stage Gate study > 200 > application/json')
@hooks.before('/api/stage-gate-studies/{sgId}/metric-results > List metric results for latest Stage Gate in a Stage Gate study > 400 > application/json')
@if_not_skipped
def create_temp_study(transaction):
    protocol = transaction['protocol']
    host = transaction['host']
    port = transaction['port']

    response = requests.post(f'{protocol}//{host}:{port}/api/stage-gate-studies',
                             json={'name': 'Temporary study', 'framework_id': 1})
    transaction['_study_id'] = response.json()['id']


@hooks.before('/api/stage-gate-studies/checklist-comparison > Get checklist comparison data > 200 > application/json')
@hooks.before('/api/stage-gate-assessments/applicant-comparison > Get applicant comparison data > 200 > application/json')
@hooks.before('/api/stage-gate-assessments/assessor-comparison > Get assessor comparison data > 200 > application/json')
@if_not_skipped
def create_second_temp_study(transaction):
    protocol = transaction['protocol']
    host = transaction['host']
    port = transaction['port']

    response = requests.post(f'{protocol}//{host}:{port}/api/stage-gate-studies',
                             json={'name': 'Second temp study', 'framework_id': 1})
    transaction['_second_study_id'] = response.json()['id']



# @hooks.before('/api/stage-gate-assessments/{appSgId}/applicant-answers > List Applicant Answers > 200 > application/json')
# @hooks.before('/api/stage-gate-assessments/{appSgId}/applicant-answers > Update Applicant Answers > 200 > application/json')
# @hooks.before('/api/stage-gate-assessments/{appSgId}/applicant-answers > Update Applicant Answers > 400 > application/json')
# @hooks.before('/api/stage-gate-assessments/{appSgId}/applicant-results > List Applicant Results > 200 > application/json')
# @hooks.before('/api/stage-gate-assessments/{appSgId}/applicant-status > Set applicant complete status as true > 200 > application/json')
# @hooks.before('/api/stage-gate-assessments/{appSgId}/assessor-scores > List Assessor Scores > 200 > application/json')
# @hooks.before('/api/stage-gate-assessments/{appSgId}/assessor-scores > Update Assessor Scores > 200 > application/json')
# @hooks.before('/api/stage-gate-assessments/{appSgId}/assessor-scores > Update Assessor Scores > 400 > application/json')
# @hooks.before('/api/stage-gate-assessments/{appSgId}/assessor-status > Set assessor complete status as true > 200 > application/json')
# # @hooks.before('/api/stage-gate-assessments/{appSgId}/assessor-results > List Assessor Results > 200 > application/json')
# @hooks.before('/api/stage-gate-assessments/{appSgId}/assessor-results > List Assessor Results > 400 > application/json')
# @if_not_skipped
# def get_stage_gate_assessment_id(transaction):
#     protocol = transaction['protocol']
#     host = transaction['host']
#     port = transaction['port']
#     study_id = transaction['_study_id']

#     response = requests.get(f'{protocol}//{host}:{port}/api/stage-gate-studies/{study_id}/stage-gate-assessments')
#     transaction['_sg_assess_id'] = response.json()['stage_gate_assessments'][0]['id']


@hooks.after('/api/stage-gate-studies > List Stage Gate studies > 200 > application/json')
@hooks.after('/api/stage-gate-studies/{sgId} > Read Stage Gate study > 200 > application/json')
@hooks.after('/api/stage-gate-studies/{sgId} > Update Stage Gate study > 200 > application/json')
@hooks.after('/api/stage-gate-studies/{sgId} > Update Stage Gate study > 400 > application/json')
@hooks.after('/api/stage-gate-studies/{sgId} > Delete a Stage Gate study from the database > 200 > application/json')
@hooks.after('/api/stage-gate-studies/{sgId} > Delete a Stage Gate study from the database > 400 > application/json')
@hooks.after('/api/stage-gate-studies/{sgId}/checklist-inputs > List completed activities > 200 > application/json')
@hooks.after('/api/stage-gate-studies/{sgId}/checklist-inputs > Update completed activities > 200 > application/json')
@hooks.after('/api/stage-gate-studies/{sgId}/checklist-inputs > Update completed activities > 400 > application/json')
@hooks.after('/api/stage-gate-studies/{sgId}/checklist-outputs > Get results of a Stage Gate activity checklist study > 200 > application/json')
@hooks.after('/api/stage-gate-studies/{sgId}/stage-gate-assessments > List Stage Gate Assessments > 200 > application/json')
@hooks.after('/api/stage-gate-assessments/{appSgId}/applicant-answers > List Applicant Answers > 200 > application/json')
@hooks.after('/api/stage-gate-assessments/{appSgId}/applicant-answers > Update Applicant Answers > 200 > application/json')
@hooks.after('/api/stage-gate-assessments/{appSgId}/applicant-answers > Update Applicant Answers > 400 > application/json')
@hooks.after('/api/stage-gate-assessments/{appSgId}/applicant-answers-integrated > Load metrics from deployment and assessment tools > 200 > application/json')
@hooks.after('/api/stage-gate-assessments/{appSgId}/applicant-answers-integrated > Load metrics from deployment and assessment tools > 400 > application/json')
@hooks.after('/api/stage-gate-assessments/{appSgId}/applicant-results > List Applicant Results > 200 > application/json')
@hooks.after('/api/stage-gate-assessments/{appSgId}/applicant-status > Set applicant complete status as true > 200 > application/json')
@hooks.after('/api/stage-gate-assessments/{appSgId}/assessor-scores > List Assessor Scores > 200 > application/json')
@hooks.after('/api/stage-gate-assessments/{appSgId}/assessor-scores > Update Assessor Scores > 200 > application/json')
@hooks.after('/api/stage-gate-assessments/{appSgId}/assessor-scores > Update Assessor Scores > 400 > application/json')
@hooks.after('/api/stage-gate-assessments/{appSgId}/assessor-status > Set assessor complete status as true > 200 > application/json')
@hooks.after('/api/stage-gate-assessments/{appSgId}/assessor-results > List Assessor Results > 200 > application/json')
@hooks.after('/api/stage-gate-assessments/{appSgId}/assessor-results > List Assessor Results > 400 > application/json')
@hooks.after('/api/stage-gate-studies/{sgId}/complexity-levels > List the evaluated stage, stage gate and corresponding complexity levels of a study > 200 > application/json')
@hooks.after('/api/stage-gate-studies/{sgId}/improvement-areas > List improvement areas highlighted in a Stage Gate study > 200 > application/json')
@hooks.after('/api/stage-gate-studies/{sgId}/improvement-areas > List improvement areas highlighted in a Stage Gate study > 400 > application/json')
@hooks.after('/api/stage-gate-studies/{sgId}/improvement-areas > Identify improvement areas for a specific Stage in a Stage Gate study > 200 > application/json')
@hooks.after('/api/stage-gate-studies/{sgId}/improvement-areas > Identify improvement areas for a specific Stage in a Stage Gate study > 400 > application/json')
@hooks.after('/api/stage-gate-studies/{sgId}/complexity-levels > List the evaluated stage, stage gate and corresponding complexity levels of a study > 400 > application/json')
@hooks.after('/api/stage-gate-studies/checklist-comparison > Get checklist comparison data > 200 > application/json')
@hooks.after('/api/stage-gate-assessments/applicant-comparison > Get applicant comparison data > 200 > application/json')
@hooks.after('/api/stage-gate-assessments/assessor-comparison > Get assessor comparison data > 200 > application/json')
@hooks.after('/api/stage-gate-studies/checklist-comparison > Get checklist comparison data > 404 > application/json')
@hooks.after('/api/stage-gate-assessments/applicant-comparison > Get applicant comparison data > 404 > application/json')
@hooks.after('/api/stage-gate-assessments/assessor-comparison > Get assessor comparison data > 404 > application/json')
@hooks.after('/api/stage-gate-studies/{sgId}/report-data > Get the Stage Gate summary report data > 200 > application/json')
@hooks.after('/api/stage-gate-studies/{sgId}/report-data > Get the Stage Gate summary report data > 400 > application/json')
@hooks.after('/api/stage-gate-studies/{sgId}/metric-thresholds > List metric thresholds applied in a specified Stage Gate study > 200 > application/json')
@hooks.after('/api/stage-gate-studies/{sgId}/metric-thresholds > List metric thresholds applied in a specified Stage Gate study > 400 > application/json')
@hooks.after('/api/stage-gate-studies/{sgId}/metric-results > List metric results for latest Stage Gate in a Stage Gate study > 200 > application/json')
@hooks.after('/api/stage-gate-studies/{sgId}/metric-results > List metric results for latest Stage Gate in a Stage Gate study > 400 > application/json')
@if_not_skipped
def delete_temp_study(transaction):
    protocol = transaction['protocol']
    host = transaction['host']
    port = transaction['port']

    study_id = transaction['_study_id']
    requests.delete(f'{protocol}//{host}:{port}/api/stage-gate-studies/{study_id}')


@hooks.after('/api/stage-gate-studies/checklist-comparison > Get checklist comparison data > 200 > application/json')
@hooks.after('/api/stage-gate-assessments/applicant-comparison > Get applicant comparison data > 200 > application/json')
@hooks.after('/api/stage-gate-assessments/assessor-comparison > Get assessor comparison data > 200 > application/json')
@if_not_skipped
def delete_second_temp_study(transaction):
    protocol = transaction['protocol']
    host = transaction['host']
    port = transaction['port']

    study_id = transaction['_second_study_id']
    requests.delete(f'{protocol}//{host}:{port}/api/stage-gate-studies/{study_id}')


@hooks.after('/api/stage-gate-studies > Create Stage Gate study > 201 > application/json')
@if_not_skipped
def delete_study_created_by_test(transaction):
    protocol = transaction['protocol']
    host = transaction['host']
    port = transaction['port']

    study_id = json.loads(transaction['results']['fields']['body']['values']['actual'])['id']
    requests.delete(f'{protocol}//{host}:{port}/api/stage-gate-studies/{study_id}')


@hooks.before('/api/stage-gate-studies > Create Stage Gate study > 400 > application/json')
@hooks.before('/api/frameworks > Create Stage Gate framework > 400 > application/json')
@hooks.before('/api/stage-gate-studies/{sgId} > Update Stage Gate study > 400 > application/json')
@hooks.before('/api/frameworks/{frameworkId} > Update Stage Gate framework > 400 > application/json')
@if_not_skipped
def make_query_invalid(transaction):
    data = json.loads(transaction['request']['body'])
    del data['name']
    transaction['request']['body'] = json.dumps(data)


@hooks.before('/api/stage-gate-studies/{sgId} > Read Stage Gate study > 200 > application/json')
@hooks.before('/api/stage-gate-studies/{sgId} > Update Stage Gate study > 200 > application/json')
@hooks.before('/api/stage-gate-studies/{sgId} > Delete a Stage Gate study from the database > 200 > application/json')
@hooks.before('/api/stage-gate-studies/{sgId}/checklist-inputs > List completed activities > 200 > application/json')
@hooks.before('/api/stage-gate-studies/{sgId}/checklist-inputs > Update completed activities > 200 > application/json')
@hooks.before('/api/stage-gate-studies/{sgId}/checklist-inputs > Update completed activities > 400 > application/json')
@hooks.before('/api/stage-gate-studies/{sgId}/checklist-outputs > Get results of a Stage Gate activity checklist study > 200 > application/json')
@hooks.before('/api/stage-gate-studies/{sgId}/stage-gate-assessments > List Stage Gate Assessments > 200 > application/json')
@hooks.before('/api/stage-gate-studies/{sgId}/complexity-levels > List the evaluated stage, stage gate and corresponding complexity levels of a study > 200 > application/json')
@hooks.before('/api/stage-gate-studies/{sgId}/improvement-areas > List improvement areas highlighted in a Stage Gate study > 200 > application/json')
@hooks.before('/api/stage-gate-studies/{sgId}/improvement-areas > List improvement areas highlighted in a Stage Gate study > 400 > application/json')
@hooks.before('/api/stage-gate-studies/{sgId}/improvement-areas > Identify improvement areas for a specific Stage in a Stage Gate study > 200 > application/json')
@hooks.before('/api/stage-gate-studies/{sgId}/improvement-areas > Identify improvement areas for a specific Stage in a Stage Gate study > 400 > application/json')
@hooks.before('/api/stage-gate-studies/{sgId}/complexity-levels > List the evaluated stage, stage gate and corresponding complexity levels of a study > 400 > application/json')
@hooks.before('/api/stage-gate-studies/{sgId}/report-data > Get the Stage Gate summary report data > 200 > application/json')
@hooks.before('/api/stage-gate-studies/{sgId}/report-data > Get the Stage Gate summary report data > 400 > application/json')
@hooks.before('/api/stage-gate-studies/{sgId}/metric-thresholds > List metric thresholds applied in a specified Stage Gate study > 200 > application/json')
@hooks.before('/api/stage-gate-studies/{sgId}/metric-thresholds > List metric thresholds applied in a specified Stage Gate study > 400 > application/json')
@hooks.before('/api/stage-gate-studies/{sgId}/metric-results > List metric results for latest Stage Gate in a Stage Gate study > 200 > application/json')
@hooks.before('/api/stage-gate-studies/{sgId}/metric-results > List metric results for latest Stage Gate in a Stage Gate study > 400 > application/json')
@if_not_skipped
def replace_stage_gate_study_id(transaction):
    study_id = transaction['_study_id']

    uri = transaction['request']['uri']
    full_path = transaction['fullPath']
    idx = uri.replace('/api/stage-gate-studies/', '')
    
    if 'checklist-inputs' in uri:
        idx = idx.replace('/checklist-inputs', '')
    if 'checklist-outputs' in uri:
        idx = idx.replace('/checklist-outputs', '')
    if 'stage-gate-assessments' in uri:
        idx = idx.replace('/stage-gate-assessments', '')
    if 'complexity-levels' in uri:
        idx = idx.replace('/complexity-levels', '')
    if 'improvement-areas' in uri:
        idx = idx.replace('/improvement-areas', '')
    if 'report-data' in uri:
        idx = idx.replace('/report-data', '')
    if 'metric-thresholds' in uri:
        idx = idx.replace('/metric-thresholds', '')
    if 'metric-results' in uri:
        idx = idx.replace('/metric-results', '')
    transaction['fullPath'] = full_path.replace(idx, str(study_id))


@hooks.before('/api/stage-gate-assessments/{appSgId}/applicant-answers > List Applicant Answers > 200 > application/json')
@hooks.before('/api/stage-gate-assessments/{appSgId}/applicant-answers > Update Applicant Answers > 200 > application/json')
@hooks.before('/api/stage-gate-assessments/{appSgId}/applicant-answers > Update Applicant Answers > 400 > application/json')
@hooks.before('/api/stage-gate-assessments/{appSgId}/applicant-answers-integrated > Load metrics from deployment and assessment tools > 200 > application/json')
@hooks.before('/api/stage-gate-assessments/{appSgId}/applicant-answers-integrated > Load metrics from deployment and assessment tools > 400 > application/json')
@hooks.before('/api/stage-gate-assessments/{appSgId}/applicant-results > List Applicant Results > 200 > application/json')
@hooks.before('/api/stage-gate-assessments/{appSgId}/applicant-status > Set applicant complete status as true > 200 > application/json')
@hooks.before('/api/stage-gate-assessments/{appSgId}/assessor-scores > List Assessor Scores > 200 > application/json')
@hooks.before('/api/stage-gate-assessments/{appSgId}/assessor-scores > Update Assessor Scores > 200 > application/json')
@hooks.before('/api/stage-gate-assessments/{appSgId}/assessor-scores > Update Assessor Scores > 400 > application/json')
@hooks.before('/api/stage-gate-assessments/{appSgId}/assessor-status > Set assessor complete status as true > 200 > application/json')
@hooks.before('/api/stage-gate-assessments/{appSgId}/assessor-results > List Assessor Results > 200 > application/json')
@hooks.before('/api/stage-gate-assessments/{appSgId}/assessor-results > List Assessor Results > 400 > application/json')
@if_not_skipped
def replace_stage_gate_assessment_id(transaction):
    study_id = transaction['_study_id']

    uri = transaction['request']['uri']
    full_path = transaction['fullPath']
    idx = uri.replace('/api/stage-gate-assessments/', '')
    
    if 'applicant-answers-integrated' in uri:
        idx = idx.replace('/applicant-answers-integrated', '')
    elif 'applicant-answers' in uri:
        idx = idx.replace('/applicant-answers', '')
    elif 'applicant-results' in uri:
        idx = idx.replace('/applicant-results', '')
    elif 'applicant-status' in uri:
        idx = idx.replace('/applicant-status', '')
    elif 'assessor-scores' in uri:
        idx = idx.replace('/assessor-scores', '')
    elif 'assessor-status' in uri:
        idx = idx.replace('/assessor-status', '')
    elif 'assessor-results' in uri:
        idx = idx.replace('/assessor-results', '')
    transaction['fullPath'] = full_path.replace(idx, str(study_id))


@hooks.before('/api/stage-gate-assessments/{appSgId}/applicant-answers > Update Applicant Answers > 400 > application/json')
@if_not_skipped
def make_app_answers_request_body_invalid(transaction):
    data = json.loads(transaction['request']['body'])
    del data[0]['id']
    transaction['request']['body'] = json.dumps(data)


@hooks.before('/api/stage-gate-assessments/{appSgId}/applicant-answers-integrated > Load metrics from deployment and assessment tools > 400 > application/json')
@if_not_skipped
def make_app_answers_integrated_request_body_invalid(transaction):
    data = json.loads(transaction['request']['body'])
    del data[0]
    transaction['request']['body'] = json.dumps(data)


@hooks.before('/api/stage-gate-assessments/{appSgId}/assessor-scores > Update Assessor Scores > 400 > application/json')
@if_not_skipped
def make_assessor_score_request_body_invalid(transaction):
    data = json.loads(transaction['request']['body'])
    del data['assessor_comments'][0]['id']
    transaction['request']['body'] = json.dumps(data)


@hooks.before('/api/stage-gate-assessments/{appSgId}/assessor-results > List Assessor Results > 200 > application/json')
@if_not_skipped
def update_assessor_scores_for_valid_get_request(transaction):
    protocol = transaction['protocol']
    host = transaction['host']
    port = transaction['port']
    study_id = transaction['_study_id']

    assessor_scores = [dict(id=i, score=2) for i in range(1, 20)]

    requests.put(f'{protocol}//{host}:{port}/api/stage-gate-assessments/{study_id}/assessor-scores',
                             json={'assessor_scores': assessor_scores, 'assessor_comments': []}
                             )


@hooks.before('/api/stage-gate-studies/{sgId}/complexity-levels > List the evaluated stage, stage gate and corresponding complexity levels of a study > 200 > application/json')
@hooks.before('/api/stage-gate-studies/{sgId}/improvement-areas > List improvement areas highlighted in a Stage Gate study > 200 > application/json')
@hooks.before('/api/stage-gate-studies/{sgId}/improvement-areas > Identify improvement areas for a specific Stage in a Stage Gate study > 200 > application/json')
@hooks.before('/api/stage-gate-studies/{sgId}/report-data > Get the Stage Gate summary report data > 200 > application/json')
@hooks.before('/api/stage-gate-studies/{sgId}/metric-thresholds > List metric thresholds applied in a specified Stage Gate study > 200 > application/json')
@hooks.before('/api/stage-gate-studies/{sgId}/metric-results > List metric results for latest Stage Gate in a Stage Gate study > 200 > application/json')
@if_not_skipped
def update_checklist_answers(transaction):
    protocol = transaction['protocol']
    host = transaction['host']
    port = transaction['port']
    study_id = transaction['_study_id']
    uri = transaction['request']['uri']

    if 'metric-results' in uri:
        acts = list(range(1, 58))
    else:
        acts = list(range(1, 30))
    json_data = {'completed_activities': acts}

    requests.put(f'{protocol}//{host}:{port}/api/stage-gate-studies/{study_id}/checklist-inputs',
                             json=json_data
                             )


@hooks.before('/api/stage-gate-studies/{sgId} > Update Stage Gate study > 404 > application/json')
@hooks.before('/api/stage-gate-studies/{sgId} > Update Stage Gate study > 200 > application/json')
def remove_framework_id_for_update_sg_study_request_body(transaction):
    data = json.loads(transaction['request']['body'])
    del data['framework_id']
    transaction['request']['body'] = json.dumps(data)


@hooks.before('/api/stage-gate-studies/{sgId}/checklist-inputs > Update completed activities > 400 > application/json')
def remove_completed_activities_from_request_body(transaction):
    data = json.loads(transaction['request']['body'])
    del data['completed_activities']
    transaction['request']['body'] = json.dumps(data)


@hooks.before('/api/stage-gate-studies/checklist-comparison > Get checklist comparison data > 200 > application/json')
@hooks.before('/api/stage-gate-assessments/applicant-comparison > Get applicant comparison data > 200 > application/json')
@hooks.before('/api/stage-gate-assessments/assessor-comparison > Get assessor comparison data > 200 > application/json')
@hooks.before('/api/stage-gate-studies/checklist-comparison > Get checklist comparison data > 404 > application/json')
@hooks.before('/api/stage-gate-assessments/applicant-comparison > Get applicant comparison data > 404 > application/json')
@hooks.before('/api/stage-gate-assessments/assessor-comparison > Get assessor comparison data > 404 > application/json')
@if_not_skipped
def format_multiple_id_query_string(transaction):
    uri = transaction['request']['uri']
    if 'checklist-comparison' in uri:
        secondId = '2'
    else:
        secondId = '6'
    full_path = transaction['fullPath']
    full_path = '{}?id=1&id={}'.format(full_path, secondId)
    transaction['fullPath'] = full_path


@hooks.after('/api/frameworks > Create Stage Gate framework > 201 > application/json')
@hooks.after('/api/frameworks/{frameworkId} > Update Stage Gate framework > 200 > application/json')
@hooks.after('/api/question-metrics/{questionMetricId} > Update Stage Gate question metric > 200 > application/json')
@if_not_skipped
def delete_framework_created_by_test(transaction):
    protocol = transaction['protocol']
    host = transaction['host']
    port = transaction['port']

    if '_framework_id' in transaction:
        framework_id = transaction['_framework_id']
    else:
        framework_id = json.loads(transaction['results']['fields']['body']['values']['actual'])['id']
    requests.delete(f'{protocol}//{host}:{port}/api/frameworks/{framework_id}')


@hooks.before('/api/frameworks/{frameworkId} > Read Stage Gate framework > 200 > application/json')
@hooks.before('/api/frameworks/{frameworkId} > Update Stage Gate framework > 400 > application/json')
@hooks.before('/api/frameworks/{frameworkId} > Delete a Stage Gate framework from the database > 400 > application/json')
@hooks.before('/api/frameworks/{frameworkId}/metric-thresholds > List metric thresholds entered by a user for a Stage Gate framework > 200 > application/json')
@if_not_skipped
def replace_default_framework_id(transaction):
    protocol = transaction['protocol']
    host = transaction['host']
    port = transaction['port']

    response = requests.get(f'{protocol}//{host}:{port}/api/frameworks')
    framework_id = response.json()['frameworks'][0]['id']

    uri = transaction['request']['uri']
    full_path = transaction['fullPath']
    idx = uri.replace('/api/frameworks/', '')
    if 'metric-thresholds' in idx:
        idx = idx.replace('/metric-thresholds', '')
    
    transaction['fullPath'] = full_path.replace(idx, str(framework_id))


@hooks.before('/api/frameworks/{frameworkId} > Update Stage Gate framework > 200 > application/json')
@hooks.before('/api/frameworks/{frameworkId} > Delete a Stage Gate framework from the database > 200 > application/json')
@hooks.before('/api/question-metrics/{questionMetricId} > Update Stage Gate question metric > 200 > application/json')
@if_not_skipped
def add_new_framework(transaction):
    protocol = transaction['protocol']
    host = transaction['host']
    port = transaction['port']

    response = requests.post(f'{protocol}//{host}:{port}/api/frameworks',
                             json={'name': 'Temporary framework'})
    transaction['_framework_id'] = response.json()['id']
    uri = transaction['request']['uri']
    if 'question-metrics' in uri:
        transaction['_stage_gate_uri'] = response.json()['stage_gates'][1]
    else:
        transaction['_stage_gate_uri'] = response.json()['stage_gates'][0]


@hooks.before('/api/frameworks/{frameworkId} > Update Stage Gate framework > 200 > application/json')
@hooks.before('/api/frameworks/{frameworkId} > Delete a Stage Gate framework from the database > 200 > application/json')
@if_not_skipped
def replace_added_framework_id(transaction):
    uri = transaction['request']['uri']
    full_path = transaction['fullPath']
    idx = uri.replace('/api/frameworks/', '')
    
    transaction['fullPath'] = full_path.replace(idx, str(transaction['_framework_id']))


@hooks.before('/api/stages/{stageId} > Read a stage from a Stage Gate framework > 200 > application/json')
@hooks.before('/api/stages/{stageId}/eval > Read a stage from a Stage Gate framework, categorised by evaluation area > 200 > application/json')
@if_not_skipped
def replace_stage_id(transaction):
    protocol = transaction['protocol']
    host = transaction['host']
    port = transaction['port']

    response = requests.get(f'{protocol}//{host}:{port}/api/frameworks')
    stage_uri = response.json()['frameworks'][0]['stages'][0]

    uri = transaction['request']['uri']
    full_path = transaction['fullPath']
    if 'eval' in uri:
        stage_uri = '/'.join([stage_uri, 'eval'])
    transaction['fullPath'] = full_path.replace(uri, stage_uri)


@hooks.before('/api/stage-gates/{gateId} > Read a stage gate from a Stage Gate framework > 200 > application/json')
@if_not_skipped
def replace_stage_gate_id(transaction):
    protocol = transaction['protocol']
    host = transaction['host']
    port = transaction['port']

    response = requests.get(f'{protocol}//{host}:{port}/api/frameworks')
    stage_gate_uri = response.json()['frameworks'][0]['stage_gates'][0]

    uri = transaction['request']['uri']
    full_path = transaction['fullPath']
    transaction['fullPath'] = full_path.replace(uri, stage_gate_uri)


@hooks.before('/api/question-metrics/{questionMetricId} > Update Stage Gate question metric > 200 > application/json')
@if_not_skipped
def replace_question_metric_id(transaction):
    protocol = transaction['protocol']
    host = transaction['host']
    port = transaction['port']

    stage_gate_uri = transaction['_stage_gate_uri']
    sg_response = requests.get(f'{protocol}//{host}:{port}{stage_gate_uri}')
    question_metric_id = sg_response.json()['question_categories'][3]['questions'][0]['question_metric']['id']

    uri = transaction['request']['uri']
    full_path = transaction['fullPath']
    idx = uri.replace('/api/question-metrics/', '')

    transaction['fullPath'] = full_path.replace(idx, str(question_metric_id))


@hooks.before('/api/question-metrics/{questionMetricId} > Update Stage Gate question metric > 400 > application/json')
@if_not_skipped
def replace_default_question_metric_id(transaction):
    protocol = transaction['protocol']
    host = transaction['host']
    port = transaction['port']

    f_response = requests.get(f'{protocol}//{host}:{port}/api/frameworks')
    stage_gate_uri = f_response.json()['frameworks'][0]['stage_gates'][1]
    sg_response = requests.get(f'{protocol}//{host}:{port}{stage_gate_uri}')
    question_metric_id = sg_response.json()['question_categories'][3]['questions'][0]['question_metric']['id']

    uri = transaction['request']['uri']
    full_path = transaction['fullPath']
    idx = uri.replace('/api/question-metrics/', '')

    transaction['fullPath'] = full_path.replace(idx, str(question_metric_id))

    data = json.loads(transaction['request']['body'])
    data['threshold'] = 'error'
    transaction['request']['body'] = json.dumps(data)


@hooks.before('/api/question-metrics > Update multiple Stage Gate question metrics > 200 > application/json')
@if_not_skipped
def refactor_question_metric_list_200(transaction):
    data = json.loads(transaction['request']['body'])
    data[0]['threshold'] = 43.0
    data[0]['id'] = 1
    data[0]['threshold_bool'] = True
    transaction['request']['body'] = json.dumps(data)


@hooks.before('/api/question-metrics > Update multiple Stage Gate question metrics > 400 > application/json')
def refactor_question_metric_list_400(transaction):
    data = json.loads(transaction['request']['body'])
    data[0]['threshold'] = "this will break"
    data[0]['id'] = 1
    data[0]['threshold_bool'] = True
    transaction['request']['body'] = json.dumps(data)
