FROM docker:20.10

RUN apk add --no-cache bash findutils make py3-pip gcc musl-dev build-base python3-dev libffi-dev openssl-dev cargo
RUN pip3 install --no-cache-dir docker-compose
