FROM node:13
RUN npm install -g js-yaml mountebank

ARG CONFIG
COPY $CONFIG config.yaml
RUN js-yaml config.yaml > config.json

CMD mb start --configfile config.json
