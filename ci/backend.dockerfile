# Based on: https://medium.com/@chadlagore/conda-environments-with-docker-82cdc9d25754

FROM continuumio/miniconda3

WORKDIR /app

RUN apt-get update && apt-get install make
COPY requirements.txt .
COPY setup.py .
COPY src/dtop_stagegate/ ./src/dtop_stagegate/
RUN pip install --requirement requirements.txt
RUN pip install python-dotenv

RUN pip install -e .

ENV FLASK_APP dtop_stagegate.service
EXPOSE 5000

CMD python3 -m flask run
