# Force re-build

FROM continuumio/miniconda3:latest

RUN apt-get update && apt-get install make
COPY requirements.txt .
COPY setup.py .
COPY src/dtop_stagegate/ ./src/dtop_stagegate/
RUN pip install --requirement requirements.txt

RUN pip --no-cache-dir install pytest pytest-cov pact-python

RUN pip install -e .
