FROM continuumio/miniconda3:latest

RUN apt-get update && apt-get install make
COPY requirements.txt .
COPY setup.py .
COPY src/dtop_stagegate/ ./src/dtop_stagegate/
RUN pip install --requirement requirements.txt
RUN pip install python-dotenv

RUN conda install nodejs
RUN npm install -g dredd
RUN pip --no-cache-dir install dredd-hooks
