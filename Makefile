# 'build-TEST' builds a Docker image for test
# 'TEST' runs build and then the test itself
# 'TEST-nodeps' runs test, assuming the image exists already
# 'x-TEST' is what actually runs inside the image
# Force re-build all

SHELL = bash
.PHONY: *
export MODULE_SHORT_NAME = sg
export DTOP_DOMAIN_TEST = dto.test

build-%:
	docker-compose `find ci -name '*.docker-compose' -printf ' -f %p'` build $*

dev-local:
	docker-compose --file ci/backend.docker-compose --file ci/client.docker-compose --file ci/e2e-cypress.docker-compose \
	--file ci/pytest.docker-compose up backend client nginx mm.dto.test lmo.dto.test spey.dto.test slc.dto.test esa.dto.test rams.dto.test

mb-build:
	docker-compose --file ci/backend.docker-compose --file ci/client.docker-compose --file ci/e2e-cypress.docker-compose \
	--file ci/pytest.docker-compose build mm.dto.test lmo.dto.test spey.dto.test slc.dto.test esa.dto.test rams.dto.test

pytest: build-pytest pytest-nodeps
pytest-nodeps:
	docker-compose \
	--file ci/pytest.docker-compose \
	up --abort-on-container-exit --exit-code-from=pytest \
	pytest lmo.dto.test spey.dto.test slc.dto.test esa.dto.test rams.dto.test

x-pytest:
	python -m pytest --cov=dtop_stagegate test

dredd: build-dredd dredd-nodeps
dredd-nodeps:
	docker-compose --file ci/dredd.docker-compose run --rm dredd

# Run dredd in console (not a link to make dredd line above)
x-dredd:
	dredd

e2e-cypress: build-backend build-client build-e2e-cypress e2e-cypress-nodeps
e2e-cypress-nodeps:
	docker-compose --file ci/backend.docker-compose --file ci/client.docker-compose --file ci/e2e-cypress.docker-compose \
	--file ci/pytest.docker-compose \
	up --remove-orphans --force-recreate --always-recreate-deps --abort-on-container-exit --exit-code-from=e2e-cypress \
	backend client nginx mm.dto.test lmo.dto.test spey.dto.test slc.dto.test esa.dto.test rams.dto.test e2e-cypress

fe-tests: 
	docker-compose --file ci/client.docker-compose up -d client
	docker exec client npm run test:unit

impose:
	docker-compose --file ci/contracts.docker-compose run --rm contracts

verify:
	docker-compose --project-name sg -f verifiable.docker-compose -f verifier.docker-compose run verifier
	docker-compose --project-name sg -f verifiable.docker-compose -f verifier.docker-compose stop sg
	docker cp `docker-compose --project-name sg -f verifiable.docker-compose ps -q`:/app/.coverage.make-verify
