from dtop_stagegate.business.db_utils import install_db
from dtop_stagegate.business.models import StageGateStudy, ChecklistActivity, Activity, ActivityCategory, Stage
from dtop_stagegate.service import db
from dtop_stagegate.service.api.provider_states import (
    configure_checklist_complete_state,
    configure_improvement_areas_state,
    configure_metric_results,
    configure_metric_thresholds
)


def test_checklist_state(client):
    check_setup(client, 'checklist results')


def test_improvement_state(client):
    check_setup(client, 'improvement areas')


def test_metric_result_state(client):
    check_setup(client, 'metric results')


def test_metric_threshold_state(client):
    check_setup(client, 'metric thresholds')


def check_setup(client, state):
    response = client.post(
        '/provider_states/setup',
        json={"state": f'sg 1 exists and has {state}'}
    )
    assert response.status_code == 201
    assert response.json['message'] == f'State configured for: sg 1 exists and has {state}'


def test_provider_states_bad_request(client):
    response = client.post(
        '/provider_states/setup',
        json={"state": 'error'}
    )
    assert response.status_code == 400
    assert response.json['message'] == 'No matching state'


def test_configure_checklist_complete_state(client, app):
    with app.app_context():
        install_db()
        studies = StageGateStudy.query.all()
        assert studies == []
        configure_checklist_complete_state()
        s = StageGateStudy.query.get(1)
        assert s.name == 'test study 1'
        assert s.checklist_study_complete
        acts = db.session.query(ChecklistActivity). \
            join(Activity). \
            join(ActivityCategory). \
            join(Stage). \
            filter(Stage.id == 1).all()
        for a in acts:
            assert a.complete

        response = client.get('/api/stage-gate-studies/1/complexity-levels')
        assert response.json == {
            'stage': 0,
            'stage_gate': 'Stage Gate 0 - 1',
            'complexity_levels': {
                "site_characterisation": 1,
                "machine_characterisation": 1,
                "energy_capture": 1,
                "energy_transformation": 1,
                "energy_delivery": 1,
                "station_keeping": 1,
                "logistics_marine_ops": 1,
                "system_performance_energy_yield": 1,
                "rams": 1,
                "system_lifetime_cost": 1,
                "environmental_social_acceptance": 1
            }
        }


def test_configure_improvement_areas_state(client, app):
    with app.app_context():
        install_db()
        studies = StageGateStudy.query.all()
        assert studies == []
        configure_improvement_areas_state()
        s = StageGateStudy.query.get(1)
        assert s.name == 'test study 1'
        assert s.checklist_study_complete
        acts = db.session.query(ChecklistActivity). \
            join(Activity). \
            join(ActivityCategory). \
            join(Stage). \
            filter(Stage.id == 1).all()
        assert not acts[24].complete
        for a in range(24):
            assert acts[a].complete

        response = client.get('/api/stage-gate-studies/1/improvement-areas')
        j = response.json
        assert j['stage_assessed'] == 0
        assert j['stage_gate_assessed'] == 'N/A'
        assert j['improvement_areas'] == [
            {
                'assessor_score': 'N/A',
                'average_score': 0.0,
                'checklist_score': 0,
                'metric_score': 'N/A',
                'name': 'Acceptability'
             }
        ]


def test_configure_metric_results(client, app):
    with app.app_context():
        install_db()
        configure_metric_results()
        s = StageGateStudy.query.get(1)
        assert s.name == 'test study 1'
        response = client.get('/api/stage-gate-studies/1/metric-results')
        metric_results = response.json['metric_results']
        assert metric_results[0] == {
            "absolute_distance": None,
            "metric": "Annual captured energy",
            "percent_distance": None,
            "result": 147.,
            "threshold": None,
            "threshold_passed": None,
            "unit": "kWh",
            "threshold_type": 'lower',
            "evaluation_area": "Power Capture",
            "justification": "a present for SI",
            "threshold_bool": False,
            "description": "The energy captured by the array over a year. See the Energy Capture (EC) and Systems, "
                           "Performance and Energy Yield (SPEY) modules and documentation for more information. "
        }
        assert metric_results[6] == {
            "absolute_distance": None,
            "metric": "Installation duration",
            "percent_distance": None,
            "result": 18.,
            "threshold": None,
            "threshold_passed": None,
            "unit": "hours per kW",
            "threshold_type": 'upper',
            "evaluation_area": "Installability",
            "justification": 'YOU SHALL NOT PACT!!!!!',
            "threshold_bool": False,
            "description": "Total duration of the installation phase of the project, normalised with respect to total "
                           "installed capacity. See the Logistics and Marine Operations (LMO) module and "
                           "documentation for more information. "
        }


def test_configure_metric_thresholds(client, app):
    with app.app_context():
        install_db()
        configure_metric_thresholds()
        response = client.get('/api/stage-gate-studies/1/metric-thresholds')
        assert response.json == {
            "metric_thresholds": [
                {
                    "id": 109,
                    "threshold": 150.0,
                    "threshold_bool": True,
                    "metric": {
                        "evaluation_area": {
                            "description": "Maintainability is defined as the ability to be retained in, or restored to, a state to perform as required, under given conditions of use and maintenance",
                            "id": 4,
                            "name": "Maintainability"
                        },
                        "id": 13,
                        "name": "Average maintenance duration",
                        "threshold_type": "upper",
                        "unit": "hours per kW per year"
                    }
                }
            ]
        }
