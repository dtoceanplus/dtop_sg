import pytest

from test.business.test_sg_studies_business import init_stage_gate_study
from dtop_stagegate.business.models import AssessorScore, AssessorComment, StageGateAssessment
from dtop_stagegate.service import db


def test_get_results(client, app):
    with app.app_context():
        init_stage_gate_study()

        response = client.get('/api/stage-gate-assessments/1/assessor-scores')
        assert response.status_code == 200
        scores = response.json
        assert type(scores) is dict

        q0 = scores['question_categories'][0]['questions'][0]
        assert q0['score'] == dict(id=1, score=None)
        assert q0['scoring_criteria'][0]['comment'] == dict(id=1, comment=None)

        assessor_score_1 = AssessorScore.query.get('1')
        assessor_score_1.score = 2
        assessor_comment_1 = AssessorComment.query.get('1')
        assessor_comment_1.comment = 'test'

        response = client.get('/api/stage-gate-assessments/1/assessor-scores')
        assert response.status_code == 200
        scores = response.json
        assert type(scores) is dict

        q0 = scores['question_categories'][0]['questions'][0]
        assert q0['score'] == dict(id=1, score=2)
        assert q0['scoring_criteria'][0]['comment'] == dict(id=1, comment='test')

        response = client.get('/api/stage-gate-assessments/6/assessor-scores')
        assert response.status_code == 404
        assert "ID could not be found" in response.json.get('message')


def test_update_assessor_data(client, app):
    with app.app_context():
        init_stage_gate_study()

        response = client.put('/api/stage-gate-assessments/1/assessor-scores')
        assert response.status_code == 400
        assert response.json['message'] == 'No request body provided'

        data = {
            'assessor_scores': [
                {
                    'id': 1,
                    'score': 4
                },
                {
                    'id': 3,
                    'score': 2
                }
            ],
            'assessor_comments': [
                {
                    'id': 1,
                    'comment': 'test'
                },
                {
                    'id': 4,
                    'comment': 'another test'
                }
            ]
        }

        response = client.put(
            '/api/stage-gate-assessments/6/assessor-scores',
            json=data
        )
        assert response.status_code == 404
        assert response.json['message'] == 'Stage Gate Assessment with that ID could not be found'

        response = client.put(
            '/api/stage-gate-assessments/1/assessor-scores',
            json=data
        )
        assert response.status_code == 200
        assert response.json['message'] == 'Assessor Scores updated'
        assert response.json['updated_stage_gate_assessment_id'] == '1'
        as_1 = AssessorScore.query.get('1')
        assert as_1.score == 4
        as_3 = AssessorScore.query.get('3')
        assert as_3.score == 2
        ac_1 = AssessorComment.query.get('1')
        assert ac_1.comment == 'test'
        ac_4 = AssessorComment.query.get('4')
        assert ac_4.comment == 'another test'

        data['assessor_scores'][0]['score'] = 'test'
        response = client.put(
            '/api/stage-gate-assessments/1/assessor-scores',
            json=data
        )
        assert response.status_code == 400
        assert 'Not a valid number.' in response.json['message']

        data['assessor_scores'][0]['score'] = 4
        data['assessor_comments'][0]['comment'] = True
        response = client.put(
            '/api/stage-gate-assessments/1/assessor-scores',
            json=data
        )
        assert response.status_code == 400
        assert 'Not a valid string.' in response.json['message']

        data['assessor_comments'][0]['comment'] = 'test'
        data['assessor_comments'][0]['id'] = 56
        response = client.put(
            '/api/stage-gate-assessments/1/assessor-scores',
            json=data
        )
        assert response.status_code == 400
        assert response.json['message'] == "Request body contains assessor comments with invalid IDs"


def test_update_assessor_study_status(client, app):
    with app.app_context():
        init_stage_gate_study()

        sg_assessment = StageGateAssessment.query.get('1')
        assert not sg_assessment.assessor_complete

        response = client.put('/api/stage-gate-assessments/1/assessor-status')
        assert response.status_code == 200
        assert response.json['message'] == 'Assessor Study complete status set to True'
        sg_assessment = StageGateAssessment.query.get('1')
        assert sg_assessment.assessor_complete

        response = client.put('/api/stage-gate-assessments/6/assessor-status')
        assert response.status_code == 404
        assert response.json['message'] == 'Stage Gate Assessment with that ID could not be found'


def test_get_assessor_results(client, app):
    with app.app_context():
        init_stage_gate_study()

        response = client.get('/api/stage-gate-assessments/1/assessor-results')
        assert response.status_code == 400
        assert response.json['message'] == 'Not all Assessor Scores have been filled'

        a_scores = AssessorScore.query.filter(AssessorScore.stage_gate_assessment_id == '1').all()
        for a_s in a_scores:
            a_s.score = 2
        db.session.commit()

        response = client.get('/api/stage-gate-assessments/1/assessor-results')
        assert response.status_code == 200
        r = response.json
        assert type(r) is dict

        overall = r.get('overall', None)[0]
        assert overall['id'] == 0
        assert overall['name'] == 'Stage Gate score'
        assert overall['orientation'] == 'h'
        assert overall['y'] == ['Average', 'Weighted average']
        assert overall['x'] == [2, 2]

        for c in ['question_categories', 'evaluation_areas']:
            r_cat = r[c]
            assert len(r_cat) == 2
            assert r_cat[0]['name'] == 'Weighted average'
            assert r_cat[0]['orientation'] == 'h'
            assert r_cat[0]['x'] == [pytest.approx(2) for i in range(len(r_cat[0]['x']))]
            assert r_cat[1]['name'] == 'Average'
            assert r_cat[1]['x'] == [pytest.approx(2) for i in range(len(r_cat[0]['x']))]
            assert r_cat[1]['orientation'] == 'h'

    response = client.get('/api/stage-gate-assessments/6/assessor-results')
    assert response.status_code == 404
    assert "ID could not be found" in response.json.get('message')
