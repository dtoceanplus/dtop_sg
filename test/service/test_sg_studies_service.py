import copy
import datetime

from dtop_stagegate.business.checklists import update_checklist_activity_answers
from dtop_stagegate.business.frameworks import update_question_metric
from dtop_stagegate.business.models import StageGateStudy, ChecklistActivity
from dtop_stagegate.service.api.provider_states import add_sg_study, set_complete_status_for_stage
from test.business.test_frameworks_business import install_default_and_test_framework
from test.business.test_sg_studies_business import init_stage_gate_study
from test.business.test_improvement_areas import set_checklist_complete
from test.business.test_report_generation import REPORT_REQUEST_BODY
from test.business.test_study_comparison import add_two_test_studies, check_comparison_data, complete_assessor_scores, \
    check_assessor_comparison
from dtop_stagegate.service import db

STAGE_ACTIVITIES = {
    "0": [
      1,
      2,
      3,
      4,
      5,
      6,
      7,
      8,
      9,
      10,
      11,
      12,
      13,
      14,
      15,
      16,
      17,
      18,
      19,
      20,
      21,
      22,
      23,
      24,
      25,
      26,
      27
    ],
    "1": [
      28,
      29,
      30,
      31,
      32,
      33,
      34,
      35,
      36,
      37,
      38,
      39,
      40,
      41,
      42,
      43,
      44,
      45,
      46,
      47,
      48,
      49,
      50,
      51,
      52,
      53,
      54,
      55,
      56
    ],
    "2": [
      57,
      58,
      59,
      60,
      61,
      62,
      63,
      64,
      65,
      66,
      67,
      68,
      69,
      70,
      71,
      72,
      73,
      74,
      75,
      76,
      77,
      78,
      79,
      80,
      81,
      82,
      83,
      84,
      85,
      86,
      87,
      88,
      89,
      90
    ],
    "3": [
      91,
      92,
      93,
      94,
      95,
      96,
      97,
      98,
      99,
      100,
      101,
      102,
      103,
      104,
      105,
      106,
      107,
      108,
      109,
      110,
      111,
      112,
      113,
      114,
      115,
      116,
      117,
      118,
      119,
      120,
      121,
      122,
      123,
      124,
      125,
      126
    ],
    "4": [
      127,
      128,
      129,
      130,
      131,
      132,
      133,
      134,
      135,
      136,
      137,
      138,
      139,
      140,
      141,
      142,
      143,
      144,
      145,
      146,
      147,
      148,
      149,
      150,
      151,
      152,
      153,
      154,
      155,
      156,
      157,
      158,
      159,
      160,
      161,
      162,
      163
    ],
    "5": [
      164,
      165,
      166,
      167,
      168,
      169,
      170,
      171,
      172,
      173,
      174,
      175,
      176,
      177,
      178,
      179,
      180,
      181,
      182,
      183,
      184,
      185,
      186,
      187,
      188,
      189,
      190,
      191,
      192,
      193,
      194,
      195,
      196,
      197,
      198,
      199
    ]
  }
STAGE_GATE_2_3_METRICS = [
    {
      "absolute_distance": None, 
      "description": "The energy captured by the array over a year. See the Energy Capture (EC) and Systems, Performance and Energy Yield (SPEY) modules and documentation for more information. ", 
      "evaluation_area": "Power Capture", 
      "justification": None, 
      "metric": "Annual captured energy", 
      "percent_distance": None, 
      "result": None, 
      "threshold": None, 
      "threshold_bool": False, 
      "threshold_passed": None, 
      "threshold_type": "lower", 
      "unit": "kWh"
    }, 
    {
      "absolute_distance": None, 
      "description": "The energy transformed by the array over a year. See the Energy Transformation (ET) and Systems, Performance and Energy Yield (SPEY) modules and documentation for more information. ", 
      "evaluation_area": "Power Conversion", 
      "justification": None, 
      "metric": "Annual transformed energy", 
      "percent_distance": None, 
      "result": None, 
      "threshold": None, 
      "threshold_bool": False, 
      "threshold_passed": None, 
      "threshold_type": "lower", 
      "unit": "kWh"
    }, 
    {
      "absolute_distance": None, 
      "description": "The energy delivered by the array over a year. See the Energy Delivery (ED) and Systems, Performance and Energy Yield (SPEY) modules and documentation for more information. ", 
      "evaluation_area": "Power Conversion", 
      "justification": None, 
      "metric": "Annual delivered energy", 
      "percent_distance": None, 
      "result": None, 
      "threshold": None, 
      "threshold_bool": False, 
      "threshold_passed": None, 
      "threshold_type": "lower", 
      "unit": "kWh"
    }, 
    {
      "absolute_distance": None, 
      "description": "The captured energy efficiency for the array. Defined as the ratio between annual captured energy and the annual capacity power of the array. See the Systems, Performance and Energy Yield (SPEY) module and documentation for more information. ", 
      "evaluation_area": "Power Capture", 
      "justification": None, 
      "metric": "Array captured efficiency", 
      "percent_distance": None, 
      "result": None, 
      "threshold": None, 
      "threshold_bool": False, 
      "threshold_passed": None, 
      "threshold_type": "lower", 
      "unit": "% (decimal)"
    }, 
    {
      "absolute_distance": None, 
      "description": "The relative transformation efficiency for the array. Defined as the ratio between annual transformed energy and annual captured energy. See the Systems, Performance and Energy Yield (SPEY) module and documentation for more information. ", 
      "evaluation_area": "Power Conversion", 
      "justification": None, 
      "metric": "Array relative transformed efficiency", 
      "percent_distance": None, 
      "result": None, 
      "threshold": None, 
      "threshold_bool": False, 
      "threshold_passed": None, 
      "threshold_type": "lower", 
      "unit": "% (decimal)"
    }, 
    {
      "absolute_distance": None, 
      "description": "The relative delivery efficiency for the array. Defined as the ratio between annual delivered energy and annual transformed energy. See the Systems, Performance and Energy Yield (SPEY) module and documentation for more information.  ", 
      "evaluation_area": "Power Conversion", 
      "justification": None, 
      "metric": "Array relative delivered efficiency", 
      "percent_distance": None, 
      "result": None, 
      "threshold": None, 
      "threshold_bool": False, 
      "threshold_passed": None, 
      "threshold_type": "lower", 
      "unit": "% (decimal)"
    }, 
    {
      "absolute_distance": None, 
      "description": "Total duration of the installation phase of the project, normalised with respect to total installed capacity. See the Logistics and Marine Operations (LMO) module and documentation for more information. ", 
      "evaluation_area": "Installability", 
      "justification": None, 
      "metric": "Installation duration",
      "percent_distance": None, 
      "result": None, 
      "threshold": None, 
      "threshold_bool": False, 
      "threshold_passed": None, 
      "threshold_type": "upper", 
      "unit": "hours per kW"
    }, 
    {
      "absolute_distance": None, 
      "description": "The overall cost for the installation phase of the project. See the Logistics and Marine Operations (LMO) module and documentation for more information. ", 
      "evaluation_area": "Installability", 
      "justification": None, 
      "metric": "Cost of installation", 
      "percent_distance": None, 
      "result": None, 
      "threshold": None, 
      "threshold_bool": False, 
      "threshold_passed": None, 
      "threshold_type": "upper", 
      "unit": "\u20ac"
    }, 
    {
      "absolute_distance": None, 
      "description": "The Meant Time To Failure (MTTF) calculated for the array. See the Reliability, Availability, Maintainability and Survivability (RAMS) module and documentation for more information. ", 
      "evaluation_area": "Reliability", 
      "justification": None, 
      "metric": "Mean time to failure for the array", 
      "percent_distance": None, 
      "result": None, 
      "threshold": None, 
      "threshold_bool": False, 
      "threshold_passed": None, 
      "threshold_type": "lower", 
      "unit": "hours"
    }, 
    {
      "absolute_distance": None, 
      "description": "The maximum annual probability of failure for the full array. See the Reliability, Availability, Maintainability and Survivability (RAMS) module and documentation for more information. ", 
      "evaluation_area": "Reliability", 
      "justification": None, 
      "metric": "Probability of failure of array", 
      "percent_distance": None, 
      "result": None, 
      "threshold": None, 
      "threshold_bool": False, 
      "threshold_passed": None, 
      "threshold_type": "upper", 
      "unit": "% (decimal)"
    }, 
    {
      "absolute_distance": None, 
      "description": "The average time-based availability of an array. See the Reliability, Availability, Maintainability and Survivability (RAMS) module and documentation for more information. ", 
      "evaluation_area": "Availability", 
      "justification": None, 
      "metric": "Average availability of array", 
      "percent_distance": None, 
      "result": None, 
      "threshold": None, 
      "threshold_bool": False, 
      "threshold_passed": None, 
      "threshold_type": "lower", 
      "unit": "% (decimal)"
    }, 
    {
      "absolute_distance": None, 
      "description": "The probability that the critical component can be repaired within a specific time (an available time), based upon the probabilistic distribution of the time to repair (TTR). The critical component is the component with the lowest probability of maintenance. See the Reliability, Availability, Maintainability and Survivability (RAMS) module and documentation for more information. ", 
      "evaluation_area": "Maintainability", 
      "justification": None, 
      "metric": "Maintenance probabilities", 
      "percent_distance": None, 
      "result": None, 
      "threshold": None, 
      "threshold_bool": False, 
      "threshold_passed": None, 
      "threshold_type": "lower", 
      "unit": "% (decimal)"
    }, 
    {
      "absolute_distance": None, 
      "description": "The duration of the maintenance phase for a typical year. Calculated as the total duration of the maintenance phase of the project, normalised with respect to total installed capacity and the lifetime of the project. See the Logistics and Marine Operations (LMO) module and documentation for more information. ", 
      "evaluation_area": "Maintainability", 
      "justification": None, 
      "metric": "Average maintenance duration",
      "percent_distance": None, 
      "result": None, 
      "threshold": None, 
      "threshold_bool": False, 
      "threshold_passed": None, 
      "threshold_type": "upper", 
      "unit": "hours per kW per year"
    }, 
    {
      "absolute_distance": None, 
      "description": "The probability that critical subsystem (structrual and mechanical components) can survive the ultimate loads. The critical subsystem is the subsystem with the lowest chance of survival. See the Reliability, Availability, Maintainability and Survivability (RAMS) module and documentation for more information. ", 
      "evaluation_area": "Survivability", 
      "justification": None, 
      "metric": "Survival probabilities", 
      "percent_distance": None, 
      "result": None, 
      "threshold": None, 
      "threshold_bool": False, 
      "threshold_passed": None, 
      "threshold_type": "upper", 
      "unit": "% (decimal)"
    }, 
    {
      "absolute_distance": None, 
      "description": "Indicator for the Capital Expenditure (CapEx) of the project. See the System Lifetime Costs (SLC) module and documentation for more information.  ", 
      "evaluation_area": "Affordability", 
      "justification": None, 
      "metric": "CapEx", 
      "percent_distance": None, 
      "result": None, 
      "threshold": None, 
      "threshold_bool": False, 
      "threshold_passed": None, 
      "threshold_type": "upper", 
      "unit": "\u20ac"
    }, 
    {
      "absolute_distance": None, 
      "description": "Indicator for the total Operational Expenditure (OpEx) of the project. See the System Lifetime Costs (SLC) module and documentation for more information.  ", 
      "evaluation_area": "Affordability", 
      "justification": None, 
      "metric": "OpEx", 
      "percent_distance": None, 
      "result": None, 
      "threshold": None, 
      "threshold_bool": False, 
      "threshold_passed": None, 
      "threshold_type": "upper", 
      "unit": "\u20ac"
    }, 
    {
      "absolute_distance": None, 
      "description": "The Capital Expenditure (CapEx) indicator for the project normalised with respect to total installed capacity. See the System Lifetime Costs (SLC) module and documentation for more information.  ", 
      "evaluation_area": "Affordability", 
      "justification": None, 
      "metric": "CapEx per kW", 
      "percent_distance": None, 
      "result": None, 
      "threshold": None, 
      "threshold_bool": False, 
      "threshold_passed": None, 
      "threshold_type": "upper", 
      "unit": "\u20ac/kW"
    }, 
    {
      "absolute_distance": None, 
      "description": "The Operational Expenditure (OpEx) indicator for the project normalised with respect to total installed capacity and the lifetime of the project in years. See the System Lifetime Costs (SLC) module and documentation for more information.  ", 
      "evaluation_area": "Affordability", 
      "justification": None, 
      "metric": "OpEx per kW per year", 
      "percent_distance": None, 
      "result": None, 
      "threshold": None, 
      "threshold_bool": False, 
      "threshold_passed": None, 
      "threshold_type": "upper", 
      "unit": "\u20ac/kW per year"
    }, 
    {
      "absolute_distance": None, 
      "description": "The Global Warming Participation (GWP) indicator for the project. See the Environmental and Social Acceptance (ESA) module and documentation for more information.  ", 
      "evaluation_area": "Acceptability", 
      "justification": None, 
      "metric": "Global warming participation", 
      "percent_distance": None, 
      "result": None, 
      "threshold": None, 
      "threshold_bool": False, 
      "threshold_passed": None, 
      "threshold_type": "upper", 
      "unit": "gCO2/kWh"
    }, 
    {
      "absolute_distance": None, 
      "description": "The Cumulative Energy Demand (CED) indicator for the project. See the Environmental and Social Acceptance (ESA) module and documentation for more information.  ", 
      "evaluation_area": "Acceptability", 
      "justification": None, 
      "metric": "Cumulative energy demand", 
      "percent_distance": None, 
      "result": None, 
      "threshold": None, 
      "threshold_bool": False, 
      "threshold_passed": None, 
      "threshold_type": "upper", 
      "unit": "kJ/kWh"
    }, 
    {
      "absolute_distance": None, 
      "description": "The number of jobs expected to be created as part of the project. See the Environmental and Social Acceptance (ESA) module and documentation for more information.  ", 
      "evaluation_area": "Acceptability", 
      "justification": None, 
      "metric": "Jobs created", 
      "percent_distance": None, 
      "result": None, 
      "threshold": None, 
      "threshold_bool": False, 
      "threshold_passed": None, 
      "threshold_type": "lower", 
      "unit": "-"
    }
  ]


def check_study_count(app, num_studies):
    with app.app_context():
        study_count = len(StageGateStudy.query.all())
        assert study_count == num_studies


def test_get_sg_study_list(client, app):
    with app.app_context():
        init_stage_gate_study()

    response = client.get('/api/stage-gate-studies/')
    assert response.status_code == 200
    studies = response.json.get('stage_gate_studies')
    assert len(studies) == 1
    study = [s for s in studies if s['id'] == 1][0]
    assert study.get('name') == 'test study 1'
    check_study_count(app, 1)


def test_create_sg_study(client, app):
    with app.app_context():
        init_stage_gate_study()

    response = client.post(
        '/api/stage-gate-studies/',
        json={'name': 'test study 2', 'description': 'details of test study 2', 'framework_id': 1}
    )
    assert response.status_code == 201
    assert response.json.get('name') == 'test study 2'
    check_study_count(app, 2)

    response = client.post(
        '/api/stage-gate-studies/',
        json={'description': 'no name provided', 'framework_id': 1}
    )
    assert response.status_code == 400
    assert b"Name is required" in response.data
    check_study_count(app, 2)

    response = client.post(
        '/api/stage-gate-studies/',
        json={'name': '', 'description': 'no name provided', 'framework_id': 1}
    )
    assert response.status_code == 400
    assert b"Name cannot be empty" in response.data
    check_study_count(app, 2)

    response = client.post(
        '/api/stage-gate-studies/',
        json={'name': 'test study 3', 'description': 'no name provided'}
    )
    assert response.status_code == 400
    assert b"Framework needs to be specified." in response.data
    check_study_count(app, 2)

    response = client.post(
        '/api/stage-gate-studies/',
        json={'name': 'test study 3', 'description': 'no name provided', 'framework_id': 2}
    )
    assert response.status_code == 400
    assert b"Framework with that ID does not exist" in response.data
    check_study_count(app, 2)

    response = client.post(
        '/api/stage-gate-studies/',
    )
    assert response.status_code == 400
    assert response.json.get('message') == "No request body provided"


def test_get_sg_study(client, app):
    with app.app_context():
        init_stage_gate_study()

    response = client.get('/api/stage-gate-studies/1')
    assert response.status_code == 200
    json_data = response.json
    assert json_data['id'] == 1
    assert json_data['name'] == 'test study 1'

    response = client.get('/api/stage-gate-studies/2')
    assert response.status_code == 404
    json_data = response.json
    assert json_data['error'] == "Not Found"
    assert b"could not be found" in response.data


def test_update_sg_study(client, app):
    with app.app_context():
        init_stage_gate_study()

    response = client.put(
        '/api/stage-gate-studies/1',
        json={'name': 'modified study name', 'description': 'modified study details'}
    )
    assert response.status_code == 200
    json_data = response.json
    assert json_data['message'] == 'Study updated.'
    assert json_data['updated_study']['id'] == 1
    assert json_data['updated_study']['name'] == 'modified study name'

    response = client.put(
        '/api/stage-gate-studies/2',
        json={'name': 'non-existent study', 'description': 'test'}
    )
    assert response.status_code == 404
    assert b"does not exist" in response.data

    response = client.put(
        '/api/stage-gate-studies/1',
        json={'name': '', 'description': 'test'}
    )
    assert response.status_code == 400
    assert b"Name cannot be empty" in response.data

    response = client.put(
        '/api/stage-gate-studies/2',
    )
    assert response.status_code == 400
    assert response.json.get('message') == "No request body provided"


def test_delete_sg_study(client, app):
    with app.app_context():
        init_stage_gate_study()

    response = client.delete('/api/stage-gate-studies/1')
    assert response.status_code == 200
    assert b"Study deleted." in response.data
    check_study_count(app, 0)

    response = client.delete('/api/stage-gate-studies/1')
    assert response.status_code == 400
    json_data = response.json
    assert json_data['error'] == "Bad Request"
    assert b"does not exist" in response.data


def test_get_completed_checklist_activities(client, app):
    study_id = '1'
    data = [1, 2, 3]
    with app.app_context():
        init_stage_gate_study()

        response = client.get('/api/stage-gate-studies/{}/checklist-inputs'.format(study_id))
        assert response.status_code == 200
        assert not response.json['completed_activities']
        assert response.json['stage_activities'] == STAGE_ACTIVITIES

        update_checklist_activity_answers(study_id, data)
        response = client.get('/api/stage-gate-studies/{}/checklist-inputs'.format(study_id))
        assert response.status_code == 200
        assert response.json['completed_activities'] == data
        assert response.json['stage_activities'] == STAGE_ACTIVITIES

        response = client.get('/api/stage-gate-studies/123/checklist-inputs')
        assert response.status_code == 404
        assert response.json.get('message') == 'Stage Gate study with that ID could not be found'


def test_update_checklist_answers(client, app):
    study_id = '1'
    data = [1, 2, 3]
    with app.app_context():
        init_stage_gate_study()

        response = client.put(
            '/api/stage-gate-studies/{}/checklist-inputs'.format(study_id),
            json={'completed_activities': data}
        )
        assert response.status_code == 200
        checklist_activities = ChecklistActivity.query.filter(ChecklistActivity.stage_gate_study_id == study_id)
        completed_activities = [act.activity_id for act in checklist_activities if act.complete]
        assert completed_activities == data
        assert response.json['study_id'] == '1'
        assert response.json['message'] == 'Checklist activities updated.'

        response = client.put(
            '/api/stage-gate-studies/{}/checklist-inputs'.format(study_id)
        )
        assert response.status_code == 400
        assert response.json.get('message') == "No request body provided"

        response = client.put(
            '/api/stage-gate-studies/{}/checklist-inputs'.format(study_id),
            json={'completed_activities': 43}
        )
        assert response.status_code == 400
        assert "Not a valid list" in response.json.get('message')

        response = client.put(
            '/api/stage-gate-studies/{}/checklist-inputs'.format(study_id),
            json={'completed_activities': ['a', 'a', 'v']}
        )
        assert response.status_code == 400
        assert "Not a valid integer" in response.json.get('message')

        response = client.put(
            '/api/stage-gate-studies/{}/checklist-inputs'.format(study_id),
            json={'completed_activities': [0]}
        )
        assert response.status_code == 400
        assert "invalid activity IDs" in response.json.get('message')

        study_id = '2'
        response = client.put(
            '/api/stage-gate-studies/{}/checklist-inputs'.format(study_id),
            json={'completed_activities': data}
        )
        assert response.status_code == 404
        assert b"ID could not be found" in response.data


def test_get_checklist_results(client, app):
    study_id = '1'
    with app.app_context():
        init_stage_gate_study()

        response = client.get('/api/stage-gate-studies/{}/checklist-outputs'.format(study_id))
        assert response.status_code == 200
        results = response.json['results']
        assert response.json['study_id'] == study_id
        assert len(results) == 6
        assert list(results[0].keys()) == [
            'activity_categories',
            'evaluation_areas',
            'name',
            'percent_complete'
        ]
        assert type(results[0]['activity_categories']) == list
        assert type(results[0]['activity_categories'][0]) == dict
        assert type(results[0]['evaluation_areas']) == list
        assert type(results[0]['evaluation_areas'][0]) == dict
        assert type(results[0]['name']) == str
        assert type(results[0]['percent_complete']) == int

        response = client.get('/api/stage-gate-studies/{}/checklist-outputs'.format('123'))
        assert response.status_code == 404
        assert response.json.get('message') == 'Stage Gate study with that ID could not be found'


def test_get_list_stage_gate_assessments(client, app):
    study_id = '1'
    with app.app_context():
        init_stage_gate_study()

        response = client.get('/api/stage-gate-studies/{}/stage-gate-assessments'.format(study_id))
        assert response.status_code == 200
        sg_assessments = response.json['stage_gate_assessments']
        assert len(sg_assessments) == 5
        assert sg_assessments[0]['stage_gate'] == 'Stage Gate 0 - 1'
        assert not sg_assessments[0]['applicant_complete']
        assert not sg_assessments[0]['assessor_complete']

        response = client.get('/api/stage-gate-studies/{}/stage-gate-assessments'.format('2'))
        assert response.status_code == 404
        assert "ID could not be found" in response.json.get('message')


def test_get_complexity_levels(client, app):
    study_id = '1'
    with app.app_context():
        init_stage_gate_study()

        response = client.get('/api/stage-gate-studies/{}/complexity-levels'.format(study_id))
        assert response.status_code == 400
        assert 'Activity checklist for that Stage Gate study has not been completed' in response.json.get('message')

        study = StageGateStudy.query.get('1')
        study.checklist_study_complete = True
        db.session.commit()

        response = client.get('/api/stage-gate-studies/{}/complexity-levels'.format(study_id))
        assert response.status_code == 404
        assert 'The technology/device has not completed all of the activities in any of the stages'\
               in response.json.get('message')

        set_complete_status_for_stage('1')
        response = client.get('/api/stage-gate-studies/{}/complexity-levels'.format(study_id))
        assert response.status_code == 200
        assert response.json == {
            'stage': 0,
            'stage_gate': 'Stage Gate 0 - 1',
            'complexity_levels': {
                "site_characterisation": 1,
                "machine_characterisation": 1,
                "energy_capture": 1,
                "energy_transformation": 1,
                "energy_delivery": 1,
                "station_keeping": 1,
                "logistics_marine_ops": 1,
                "system_performance_energy_yield": 1,
                "rams": 1,
                "system_lifetime_cost": 1,
                "environmental_social_acceptance": 1
            }
        }

        response = client.get('/api/stage-gate-studies/{}/complexity-levels'.format('2'))
        assert response.status_code == 404
        assert "ID could not be found" in response.json.get('message')


def test_get_improvement_areas(client, app):
    study_id = '1'
    with app.app_context():
        init_stage_gate_study()

        response = client.get(
            f"/api/stage-gate-studies/{study_id}/improvement-areas",
        )
        assert response.status_code == 400
        assert 'Activity checklist for that Stage Gate study has not been completed' in response.json.get('message')

        ca = set_checklist_complete('1')
        ca[24].complete = 0
        sg_study = StageGateStudy.query.get('1')
        sg_study.checklist_study_complete = 1
        db.session.commit()
        response = client.get(
            f"/api/stage-gate-studies/{study_id}/improvement-areas"
        )
        assert response.status_code == 200
        j = response.json
        assert j['stage_assessed'] == 0
        assert j['stage_gate_assessed'] == 'N/A'
        assert j['improvement_areas'] == [{
            'name': 'Acceptability',
            'assessor_score': 'N/A',
            'metric_score': 'N/A',
            'checklist_score': 0,
            'average_score': 0
        }]

        set_checklist_complete('1')
        set_checklist_complete('2')
        set_checklist_complete('3')
        set_checklist_complete('4')
        set_checklist_complete('5')
        set_checklist_complete('6')
        response = client.get(
            f"/api/stage-gate-studies/{study_id}/improvement-areas"
        )
        assert response.status_code == 200
        assert response.json == {
            'stage_assessed': 5,
            'stage_gate_assessed': 'N/A',
            'improvement_areas': []
        }

        response = client.get(
            "/api/stage-gate-studies/2/improvement-areas"
        )
        assert response.status_code == 404
        assert "ID could not be found" in response.json.get('message')


def test_get_improvement_areas_for_specified_stage(client, app):
    study_id = '1'
    with app.app_context():
        init_stage_gate_study()

        response = client.post(
            f"/api/stage-gate-studies/{study_id}/improvement-areas",
        )
        assert response.status_code == 400
        assert response.json.get('message') == "No request body provided"

        response = client.post(
            f"/api/stage-gate-studies/{study_id}/improvement-areas",
            json={'incorrect_key': 'this should break'}
        )
        assert response.status_code == 400
        assert "The Stage index is required" in response.json.get('message')

        response = client.post(
            f"/api/stage-gate-studies/{study_id}/improvement-areas",
            json={'stage_index': 'this should also break'}
        )
        assert response.status_code == 400
        assert "Not a valid integer" in response.json.get('message')

        response = client.post(
            f"/api/stage-gate-studies/{study_id}/improvement-areas",
            json={'stage_index': 1}
        )
        assert response.status_code == 400
        assert "The threshold for improvement areas is required" in response.json.get('message')

        response = client.post(
            f"/api/stage-gate-studies/{study_id}/improvement-areas",
            json={'stage_index': None, 'threshold': 60}
        )
        assert response.status_code == 400
        assert 'Activity checklist for that Stage Gate study has not been completed' in response.json.get('message')

        ca = set_checklist_complete('1')
        ca[24].complete = 0
        sg_study = StageGateStudy.query.get('1')
        sg_study.checklist_study_complete = 1
        db.session.commit()
        response = client.post(
            f"/api/stage-gate-studies/{study_id}/improvement-areas",
            json={'stage_index': None, 'threshold': 50}
        )
        assert response.status_code == 200
        j = response.json
        assert j['stage_assessed'] == 0
        assert j['stage_gate_assessed'] == 'N/A'
        assert j['improvement_areas'] == [{
            'name': 'Acceptability',
            'assessor_score': 'N/A',
            'metric_score': 'N/A',
            'checklist_score': 0,
            'average_score': 0
        }]

        set_checklist_complete('1')
        set_checklist_complete('2')
        set_checklist_complete('3')
        set_checklist_complete('4')
        set_checklist_complete('5')
        set_checklist_complete('6')
        response = client.post(
            f"/api/stage-gate-studies/{study_id}/improvement-areas",
            json={'stage_index': None, 'threshold': 50}
        )
        assert response.status_code == 200
        assert response.json == {
            'stage_assessed': 5,
            'stage_gate_assessed': 'N/A',
            'improvement_areas': []
        }

        response = client.post(
            "/api/stage-gate-studies/2/improvement-areas",
            json={'stage_index': '1', 'threshold': 50}
        )
        assert response.status_code == 404
        assert "ID could not be found" in response.json.get('message')


def test_get_checklist_comparison_results(client, app):

    with app.app_context():
        add_two_test_studies()

        response = client.get('/api/stage-gate-studies/checklist-comparison?id=1&id=2')
        assert response.status_code == 200
        check_comparison_data(response.json)

        response = client.get('/api/stage-gate-studies/checklist-comparison')
        assert response.status_code == 400
        assert response.json.get('message') == 'An insufficient number of study IDs were provided for the comparision'

        response = client.get('/api/stage-gate-studies/checklist-comparison?id=1')
        assert response.status_code == 400
        assert response.json.get('message') == 'An insufficient number of study IDs were provided for the comparision'

        response = client.get('/api/stage-gate-studies/checklist-comparison?id=1&id=2&id=3')
        assert response.status_code == 404
        assert response.json.get('message') == 'A Stage Gate study for one of the requested IDs does not exist'


def test_get_applicant_comparison_results(client, app):

    with app.app_context():
        add_two_test_studies()

        response = client.get('/api/stage-gate-assessments/applicant-comparison?id=1&id=6')
        assert response.status_code == 200
        assert response.json['study_columns'] == [
            {
                'prop': 'study_1',
                'label': 'test study 1'
            },
            {
                'prop': 'study_2',
                'label': 'test study 2'
            }
        ]
        assert response.json['summary_data'] == [
            {
                'study_label': 'test study 1',
                'sg_name': 'Stage Gate 0 - 1',
                'sg_assess_id': 1,
                'response_rate': 0,
                'threshold_success_rate': None
            },
            {
                'study_label': 'test study 2',
                'sg_name': 'Stage Gate 0 - 1',
                'sg_assess_id': 6,
                'response_rate': 0,
                'threshold_success_rate': None
            }
        ]
        assert response.json['metric_data'] == []

        response = client.get('/api/stage-gate-assessments/applicant-comparison')
        assert response.status_code == 400
        assert response.json.get('message') == 'An insufficient number of Stage Gate assessment IDs were provided ' \
                                               'for the comparison'

        response = client.get('/api/stage-gate-assessments/applicant-comparison?id=1')
        assert response.status_code == 400
        assert response.json.get('message') == 'An insufficient number of Stage Gate assessment IDs were provided ' \
                                               'for the comparison'

        response = client.get('/api/stage-gate-assessments/applicant-comparison?id=1&id=6&id=11')
        assert response.status_code == 404
        assert response.json.get('message') == 'A Stage Gate assessment for one of the requested IDs does not exist'


def test_get_assessor_comparison_results(client, app):

    with app.app_context():
        add_two_test_studies()

        sga_ids = ["2", "7"]
        complete_assessor_scores(sga_ids)

        response = client.get('/api/stage-gate-assessments/assessor-comparison?id=2&id=7')
        assert response.status_code == 200
        check_assessor_comparison(response.json)

        response = client.get('/api/stage-gate-assessments/assessor-comparison')
        assert response.status_code == 400
        assert response.json.get('message') == 'An insufficient number of Stage Gate assessment IDs were provided ' \
                                               'for the comparison'

        response = client.get('/api/stage-gate-assessments/assessor-comparison?id=1')
        assert response.status_code == 400
        assert response.json.get('message') == 'An insufficient number of Stage Gate assessment IDs were provided ' \
                                               'for the comparison'

        response = client.get('/api/stage-gate-assessments/assessor-comparison?id=1&id=6&id=11')
        assert response.status_code == 404
        assert response.json.get('message') == 'A Stage Gate assessment for one of the requested IDs does not exist'


def test_get_report_pdf_request_body_validation(client, app):

    with app.app_context():
        init_stage_gate_study()

        response = client.post(
            '/api/stage-gate-studies/1/report-data'
        )
        assert response.status_code == 400
        assert response.json.get('message') == "No request body provided"

        request_body = copy.deepcopy(REPORT_REQUEST_BODY)
        request_body.pop('stage_index')
        response = client.post(
            '/api/stage-gate-studies/1/report-data',
            json=request_body
        )
        assert response.status_code == 400
        assert "The Stage index is required" in response.json.get('message')

        request_body.pop('stage_gate_index')
        response = client.post(
            '/api/stage-gate-studies/1/report-data',
            json=request_body
        )
        assert response.status_code == 400
        assert "The Stage index is required" in response.json.get('message')
        assert "The Stage Gate index is required" in response.json.get('message')

        request_body['sections'].pop('checklist_summary')
        response = client.post(
            '/api/stage-gate-studies/1/report-data',
            json=request_body
        )
        assert response.status_code == 400
        assert "The Stage index is required" in response.json.get('message')
        assert "The Stage Gate index is required" in response.json.get('message')
        assert "checklist_summary field is required." in response.json.get('message')

        request_body = copy.deepcopy(REPORT_REQUEST_BODY)
        request_body.pop('sections')
        response = client.post(
            '/api/stage-gate-studies/1/report-data',
            json=request_body
        )
        assert response.status_code == 400
        assert "The nested sections field is required" in response.json.get('message')


def test_get_report_pdf(client, app):

    request_body = copy.deepcopy(REPORT_REQUEST_BODY)
    with app.app_context():
        init_stage_gate_study()

        response = client.post(
            '/api/stage-gate-studies/2/report-data',
            json=request_body
        )
        assert response.status_code == 404
        assert response.json.get('message') == "Stage Gate study with that ID could not be found"

        request_body['sections']['checklist_summary'] = True
        s = StageGateStudy.query.get(1)
        s.checklist_study_complete = True
        db.session.commit()
        response = client.post(
            '/api/stage-gate-studies/1/report-data',
            json=request_body
        )
        assert response.status_code == 200
        assert response.json == {
            'study_params': {
                'study_name': 'test study 1',
                'date': datetime.datetime.now().strftime("%d/%m/%Y"),
                'selected_stage': 'N/A',
                'selected_stage_gate': 'N/A',
                'description': 'description for test stage gate study 1',
                'threshold_settings': 'Default (no metric thresholds)',
                'stage_index': None
            },
            'section_request': {
                'checklist_summary': True,
                'checklist_stage_results': False,
                'checklist_outstanding_activities': False,
                'applicant_summary': False,
                'metric_results': False,
                'applicant_answers': False,
                'assessor_overall_scores': False,
                'assessor_question_categories': False,
                'assessor_scores_comments': False,
                'improvement_areas': False
            },
            'table_of_contents': {
                'alignment': 'justify',
                'ul': ['Introduction', 'Study details',  'Activity Checklist', {'type': 'circle', 'ul': ['Summary']}]
            },
            'checklist_summary': ['0%', '0%', '0%', '0%', '0%', '0%']
        }

        request_body['sections']['applicant_summary'] = True
        response = client.post(
            '/api/stage-gate-studies/1/report-data',
            json=request_body
        )
        assert response.status_code == 400
        assert response.json.get('message') == "Stage Gate index required but was not provided."


def test_get_metric_thresholds_by_study(client, app):

    with app.app_context():

        install_default_and_test_framework()
        data = {
            "threshold_bool": True,
            "threshold": 143.
        }
        update_question_metric(data, "130")
        add_sg_study(2)

        response = client.get(
            '/api/stage-gate-studies/1/metric-thresholds'
        )
        assert response.status_code == 400

        set_complete_status_for_stage('7')
        response = client.get(
            '/api/stage-gate-studies/1/metric-thresholds'
        )
        assert response.status_code == 200
        assert response.json.get('metric_thresholds') == []

        set_complete_status_for_stage('8')
        set_complete_status_for_stage('9')
        response = client.get(
            '/api/stage-gate-studies/1/metric-thresholds'
        )
        assert response.status_code == 200
        assert response.json.get('metric_thresholds') == [
            {
                "id": 130,
                "metric": {
                    "evaluation_area": {
                        "description": "Maintainability is defined as the ability to be retained in, or restored to, "
                                       "a state to perform as required, under given conditions of use and maintenance",
                        "id": 4,
                        "name": "Maintainability"
                    },
                    "id": 13,
                    "name": "Average maintenance duration",
                    "threshold_type": "upper",
                    "unit": "hours per kW per year"
                },
                "threshold": 143.0,
                "threshold_bool": True
            }
        ]


def test_get_metric_thresholds_by_study_errors(client, app):

    with app.app_context():
        init_stage_gate_study()

        response = client.get(
            '/api/stage-gate-studies/2/metric-thresholds'
        )
        assert response.status_code == 404
        assert response.json.get('message') == 'Stage Gate study with that ID could not be found'

        response = client.get(
            '/api/stage-gate-studies/1/metric-thresholds'
        )
        assert response.status_code == 400
        assert response.json.get('message') == 'Suitable Stage Gate could not be found'


def test_get_metric_results(client, app):

    with app.app_context():
        init_stage_gate_study()

        set_complete_status_for_stage(1)
        set_complete_status_for_stage(2)
        set_complete_status_for_stage(3)

        response = client.get(
            '/api/stage-gate-studies/1/metric-results'
        )
        assert response.status_code == 200
        assert response.json.get('metric_results') == STAGE_GATE_2_3_METRICS
        

def test_get_metric_results_errors(client, app):

    with app.app_context():
        init_stage_gate_study()

        response = client.get(
            '/api/stage-gate-studies/2/metric-results'
        )
        assert response.status_code == 404
        assert response.json.get('message') == 'Stage Gate study with that ID could not be found'

        response = client.get(
            '/api/stage-gate-studies/1/metric-results'
        )
        assert response.status_code == 400
        assert response.json.get('message') == 'Suitable Stage Gate could not be found'
