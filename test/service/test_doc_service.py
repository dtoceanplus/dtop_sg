def test_api_doc(client):

    response = client.get('/api/')
    assert b"<title>ReDoc</title>" in response.data
    assert b"/static/openapi.json" in response.data
