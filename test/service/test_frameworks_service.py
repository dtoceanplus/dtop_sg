from dtop_stagegate.business.db_utils import install_db
from dtop_stagegate.business.frameworks import update_question_metric
from test.business.test_frameworks_business import install_default_and_test_framework, check_question_metrics


def test_get_sg_framework_list(client, app):

    response = client.get('/api/frameworks/')
    assert response.status_code == 200
    json_data = response.json
    framework_list = json_data.get('frameworks')
    assert len(framework_list) == 0
    with app.app_context():
        install_db()
    response = client.get('/api/frameworks/')
    assert response.status_code == 200
    json_data = response.json
    framework_list = json_data.get('frameworks')
    assert len(framework_list) == 1
    assert framework_list[0].get('name') == "Default (no metric thresholds)"
    assert len(framework_list[0].get('stages')) == 6
    assert len(framework_list[0].get('stage_gates')) == 5


def test_create_sg_framework(client, app):

    with app.app_context():
        install_db()
    response = client.post(
        '/api/frameworks/',
        json={
            "name": "test framework name",
            "description": "test framework description"
        }
    )
    assert response.status_code == 201
    assert response.json.get('name') == "test framework name"
    assert response.json.get('description') == "test framework description"
    assert response.json.get('_links').get('self') == '/api/frameworks/2'
    assert response.json.get('stages')[0] == '/api/stages/7'

    response = client.post(
        '/api/frameworks/',
    )
    assert response.status_code == 400
    assert response.json.get('message') == "No request body provided"

    response = client.post(
        '/api/frameworks/',
        json={
            "description": "breaking test; no name provided"
        }
    )
    assert response.status_code == 400
    assert response.json.get('message') == "Name is required"

    response = client.post(
        '/api/frameworks/',
        json={
            "name": "",
            "description": "breaking test; no name provided"
        }
    )
    assert response.status_code == 400
    assert response.json.get('message') == "Name cannot be empty"

    response = client.post(
        '/api/frameworks/',
        json={
            "name": "Default (no metric thresholds)",
            "description": "breaking test; name already exists"
        }
    )
    assert response.status_code == 400
    assert "name already exists" in response.json.get('message')


def test_get_sg_framework(client, app):

    with app.app_context():
        install_db()
    response = client.get('/api/frameworks/1')
    assert response.status_code == 200
    assert response.json.get('name') == "Default (no metric thresholds)"

    response = client.get('/api/frameworks/2')
    assert response.status_code == 404
    assert response.json.get('message') == "Framework could not be found"


def test_update_sg_framework(client, app):

    with app.app_context():
        install_default_and_test_framework()
    request_body = dict(
        name='test',
        description='test'
    )
    response = client.put(
        '/api/frameworks/2',
        json=request_body
    )
    assert response.status_code == 200
    assert response.json.get('message') == 'Framework updated.'
    f = response.json.get('updated_framework')
    assert f.get('name') == 'test'
    assert f.get('description') == 'test'

    response = client.put(
        '/api/frameworks/3',
        json=request_body
    )
    assert response.status_code == 404
    assert "does not exist" in response.json.get('message')

    response = client.put(
        '/api/frameworks/1',
        json=request_body
    )
    assert response.status_code == 400
    assert "default framework template" in response.json.get('message')

    response = client.put(
        '/api/frameworks/2',
    )
    assert response.status_code == 400
    assert response.json.get('message') == "No request body provided"

    response = client.put(
        '/api/frameworks/2',
        json={
            'description': 'test'
        }
    )
    assert response.status_code == 400
    assert response.json.get('message') == "Name is required"

    response = client.put(
        '/api/frameworks/2',
        json={
            'name': ''
        }
    )
    assert response.status_code == 400
    assert response.json.get('message') == "Name cannot be empty"


def test_delete_sg_framework(client, app):

    with app.app_context():
        install_default_and_test_framework()
    response = client.delete('/api/frameworks/2')
    assert response.status_code == 200
    assert response.json.get('message') == 'Framework deleted'
    assert response.json.get('deleted_framework_id') == "2"

    response = client.delete('/api/frameworks/2')
    assert response.status_code == 400
    assert "ID does not exist" in response.json.get('message')

    response = client.delete('/api/frameworks/1')
    assert response.status_code == 400
    assert response.json.get('message') == "Cannot delete default framework template"


def test_get_sg_stage(client, app):

    with app.app_context():
        install_db()
    response = client.get('/api/stages/1')
    assert response.status_code == 200
    assert response.json.get('name') == "Stage 0"
    assert response.json.get('number') == 0

    response = client.get('/api/stages/7')
    assert response.status_code == 404
    assert response.json.get('message') == "Stage could not be found"


def test_get_eval_stage(client, app):

    with app.app_context():
        install_db()
    response = client.get('/api/stages/1/eval')
    assert response.status_code == 200
    assert response.json.get('name') == "Stage 0"
    assert response.json.get('number') == 0
    eval_areas = response.json.get('evaluation_areas')
    assert len(eval_areas) == 9
    assert eval_areas[0].get('activities')[0].get('name') == "Acceptability assessment"

    response = client.get('/api/stages/7/eval')
    assert response.status_code == 404
    assert response.json.get('message') == "Stage could not be found"


def test_get_sg_stage_gate(client, app):

    with app.app_context():
        install_db()
    response = client.get('/api/stage-gates/1')
    assert response.status_code == 200
    assert response.json.get('name') == "Stage Gate 0 - 1"
    assert response.json.get('entry_stage') == 0
    assert response.json.get('exit_stage') == 1

    response = client.get('/api/stage-gates/6')
    assert response.status_code == 404
    assert response.json.get('message') == "Stage Gate could not be found"


def test_update_sg_question_metric(client, app):

    with app.app_context():
        install_db()
    response = client.put(
        '/api/question-metrics/1',
        json={
            "threshold": 143.,
            "threshold_bool": False
        }
    )
    assert response.status_code == 200
    assert response.json.get('message') == "Question Metric updated."
    qm = response.json.get('updated_question_metric')
    assert not qm.get('threshold_bool')
    assert qm.get('threshold') == 143.

    response = client.put(
        '/api/question-metrics/1',
        json={
            "threshold": 'this will break',
            "threshold_bool": 43.0
        }
    )
    assert response.status_code == 400
    assert "Not a valid number" in response.json.get('message')
    assert "Not a valid boolean" in response.json.get('message')

    response = client.put(
        '/api/question-metrics/115',
        json={
            "threshold": 143.,
            "threshold_bool": False
        }
    )
    assert response.status_code == 404
    assert "ID does not exist" in response.json.get('message')


def test_update_sg_question_metric_list(app, client):

    with app.app_context():
        install_db()

        rb = [
            {
                'id': 1,
                'threshold': 43.0,
                'threshold_bool': True
            },
            {
                'id': 2,
                'threshold': 143.0,
                'threshold_bool': True
            }
        ]
        response = client.put('/api/question-metrics/', json=rb)

        assert response.status_code == 200
        assert response.json.get('message') == 'Question Metrics updated.'

        check_question_metrics()


def test_update_sg_question_metric_list_error(app, client):

    with app.app_context():
        install_db()

    rb_err = [
        {
            'id': 1,
            'threshold_bool': True
        }
    ]
    response = client.put('/api/question-metrics/', json=rb_err)

    assert response.status_code == 400
    assert "Missing data for required field" in response.json.get('message')


def test_get_metric_thresholds(client, app):

    response = client.get('/api/frameworks/1/metric-thresholds')
    assert response.status_code == 200
    metric_thresholds = response.json.get('metric_thresholds')
    assert len(metric_thresholds) == 0

    with app.app_context():
        install_default_and_test_framework()
        data = {
            "threshold_bool": True,
            "threshold": 150.
        }
        update_question_metric(data, "115")

    response = client.get('/api/frameworks/2/metric-thresholds')
    assert response.status_code == 200
    metric_thresholds = response.json.get('metric_thresholds')
    assert len(metric_thresholds) == 1
