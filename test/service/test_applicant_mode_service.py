from dtop_stagegate.business.stage_gate_studies import create_study
from test.business.test_sg_studies_business import init_stage_gate_study, add_sg_study
from test.business.test_frameworks_business import install_default_and_test_framework
from test.business.test_applicant_mode_business import check_applicant_answers
from test.business.test_integration import ENTITY_LIST, EXPECTED_METRIC_STATUS_SG2, EXPECTED_METRIC_STATUS_SG1
from dtop_stagegate.business.applicant_mode import get_applicant_answers_by_stage_gate_assessment_id
from dtop_stagegate.business.frameworks import update_question_metric
from dtop_stagegate.business.models import ApplicantAnswer, StageGateAssessment


def test_get_applicant_answers(client, app):
    with app.app_context():
        init_stage_gate_study()

    response = client.get('/api/stage-gate-assessments/1/applicant-answers')
    assert response.status_code == 200

    sg = response.json
    check_applicant_answers(sg)

    response = client.get('/api/stage-gate-assessments/6/applicant-answers')
    assert response.status_code == 404
    assert "ID could not be found" in response.json.get('message')


def test_update_applicant_answers(client, app):
    with app.app_context():
        init_stage_gate_study()

        data = [
            {
                'id': 1,
                'result': 43,
                'justification': 'test'
            },
            {
                'id': 2,
                'response': 'test'
            },
        ]

        response = client.put(
            '/api/stage-gate-assessments/1/applicant-answers',
            json=data
        )
        assert response.status_code == 200
        assert response.json['message'] == 'Applicant Answers updated'
        assert response.json['updated_stage_gate_assessment_id'] == '1'
        aa_1 = ApplicantAnswer.query.get('1')
        assert aa_1.result == 43
        assert aa_1.justification == 'test'
        assert not aa_1.response
        aa_2 = ApplicantAnswer.query.get('2')
        assert not aa_2.result
        assert not aa_2.justification
        assert aa_2.response == 'test'

    response = client.put(
        '/api/stage-gate-assessments/1/applicant-answers'
    )
    assert response.status_code == 400

    response = client.put(
        '/api/stage-gate-assessments/6/applicant-answers',
        json=data
    )
    assert response.status_code == 404
    assert response.json.get('message') == 'Stage Gate Assessment with that ID could not be found'

    data = [
        {
            'id': 1,
            'result': 'hello',
            'justification': 1,
            'response': True
        }
    ]
    response = client.put(
        '/api/stage-gate-assessments/1/applicant-answers',
        json=data
    )
    assert response.status_code == 400
    err_message = response.json.get('message')
    assert "'justification': ['Not a valid string.']" in err_message
    assert "'response': ['Not a valid string.']" in err_message
    assert "'result': ['Not a valid number.']" in err_message

    data = [
        {
            'result': 43,
            'justification': 'test'
        }
    ]
    response = client.put(
        '/api/stage-gate-assessments/1/applicant-answers',
        json=data
    )
    assert response.status_code == 400
    err_message = response.json.get('message')
    assert "'Missing data for required field.'" in err_message


def test_incorrect_update_applicant_answers(client, app):
    with app.app_context():
        init_stage_gate_study()

        study_data = dict(
            name='test study 2',
            description='description for test stage gate study 2',
            framework_id=1
        )
        create_study(study_data)
        data = [
            {
                'id': 1,
                'result': 43,
                'justification': 'test'
            }
        ]

        response = client.put(
            '/api/stage-gate-assessments/2/applicant-answers',
            json=data
        )
        assert response.status_code == 400
        err = response.json.get('message')
        assert err == "Request body contains applicant answers with invalid IDs"


def test_update_applicant_answers_integrated(client, app):
    with app.app_context():
        init_stage_gate_study()

        response = client.put(
            '/api/stage-gate-assessments/2/applicant-answers-integrated',
            json=ENTITY_LIST
        )

        assert response.status_code == 200
        assert response.json == EXPECTED_METRIC_STATUS_SG2


def test_update_applicant_answers_integrated_sg_1(client, app):
    with app.app_context():
        init_stage_gate_study()

        response = client.put(
            '/api/stage-gate-assessments/1/applicant-answers-integrated',
            json=ENTITY_LIST
        )

        assert response.status_code == 200
        assert response.json == EXPECTED_METRIC_STATUS_SG1


def test_update_applicant_answers_integrated_400(client):
    response = client.put('/api/stage-gate-assessments/1/applicant-answers-integrated')
    assert response.status_code == 400
    assert response.json.get('message') == "No request body provided"


def test_update_applicant_answers_integrated_404(client):

    response = client.put(
        '/api/stage-gate-assessments/1/applicant-answers-integrated',
        json=[
            {
                "projectId": 1,
                "studyId": 1,
                "moduleId": 1,
                "entityId": 1
            },
            {
                "projectId": 1,
                "studyId": 1,
                "moduleId": 2,
                "entityId": 1
            }
        ]
    )
    assert response.status_code == 404
    assert response.json.get('message') == 'Stage Gate Assessment with that ID could not be found'


def test_update_applicant_answers_integrated_dtop_domain(app, client, monkeypatch):
    with app.app_context():
        init_stage_gate_study()

        monkeypatch.delenv('DTOP_DOMAIN')
        response = client.put(
            '/api/stage-gate-assessments/2/applicant-answers-integrated',
            json=ENTITY_LIST
        )

        assert response.status_code == 400
        assert response.json.get('message') == "Error in accessing assessment modules"


def test_update_applicant_study_status(client, app):
    with app.app_context():
        init_stage_gate_study()

        sg_assessment = StageGateAssessment.query.get('1')
        assert not sg_assessment.applicant_complete

        response = client.put('/api/stage-gate-assessments/1/applicant-status')
        assert response.status_code == 200
        assert response.json['message'] == 'Applicant Study complete status set to True'
        sg_assessment = StageGateAssessment.query.get('1')
        assert sg_assessment.applicant_complete

        response = client.put('/api/stage-gate-assessments/6/applicant-status')
        assert response.status_code == 404
        assert response.json['message'] == 'Stage Gate Assessment with that ID could not be found'


def test_get_results(client, app):
    with app.app_context():
        install_default_and_test_framework()
        add_sg_study('2')

        response = client.get('/api/stage-gate-assessments/2/applicant-results')
        assert response.status_code == 200
        sg = response.json
        assert type(sg) is dict

        summary_data = sg.get('summary_data', None)
        assert summary_data['response_rate'] == 0
        assert summary_data['threshold_success_rate'] is None

        metric_results = sg.get('metric_results', None)
        assert type(metric_results) is list
        assert metric_results[6] == {
            "absolute_distance": None,
            "metric": "Installation duration",
            "percent_distance": None,
            "result": None,
            "threshold": None,
            "threshold_passed": None,
            "unit": "hours per kW",
            "threshold_type": 'upper',
            "evaluation_area": "Installability",
            "justification": None,
            "threshold_bool": False,
            "description": "Total duration of the installation phase of the project, normalised with respect to total "
                           "installed capacity. See the Logistics and Marine Operations (LMO) module and "
                           "documentation for more information. "
        }

        data = {
            "threshold_bool": True,
            "threshold": 150.
        }
        update_question_metric(data, "103")
        app_answer_list = get_applicant_answers_by_stage_gate_assessment_id('2')
        app_answer_list[11].result = 147.
        app_answer_list[11].justification = 'test'

        response = client.get('/api/stage-gate-assessments/2/applicant-results')
        assert response.status_code == 200
        sg = response.json
        assert type(sg) is dict

        summary_data = sg.get('summary_data', None)
        assert summary_data['response_rate'] == 4
        assert summary_data['threshold_success_rate'] == 100

        metric_results = sg.get('metric_results', None)
        assert type(metric_results) is list
        assert metric_results[6] == {
            "absolute_distance": None,
            "metric": "Installation duration",
            "percent_distance": None,
            "result": 147.,
            "threshold": 150.0,
            "threshold_passed": True,
            "unit": "hours per kW",
            "threshold_type": 'upper',
            "evaluation_area": "Installability",
            "justification": "test",
            "threshold_bool": True,
            "description": "Total duration of the installation phase of the project, normalised with respect to total "
                           "installed capacity. See the Logistics and Marine Operations (LMO) module and "
                           "documentation for more information. "
        }

        data = {
            "threshold_bool": True,
            "threshold": 20.
        }
        update_question_metric(data, "97")
        app_answer_list = get_applicant_answers_by_stage_gate_assessment_id('2')
        app_answer_list[5].result = 18
        app_answer_list[5].justification = 'test'

        response = client.get('/api/stage-gate-assessments/2/applicant-results')
        assert response.status_code == 200
        sg = response.json
        assert type(sg) is dict

        summary_data = sg.get('summary_data', None)
        assert summary_data['response_rate'] == 7
        assert summary_data['threshold_success_rate'] == 50

        metric_results = sg.get('metric_results', None)
        assert type(metric_results) is list
        assert metric_results[0] == {
            "absolute_distance": 2.,
            "metric": "Annual captured energy",
            "percent_distance": 10,
            "result": 18.,
            "threshold": 20.0,
            "threshold_passed": False,
            "unit": "kWh",
            "threshold_type": 'lower',
            "evaluation_area": "Power Capture",
            "justification": "test",
            "threshold_bool": True,
            "description": "The energy captured by the array over a year. See the Energy Capture (EC) and Systems, "
                           "Performance and Energy Yield (SPEY) modules and documentation for more information. "
        }

    response = client.get('/api/stage-gate-assessments/6/applicant-results')
    assert response.status_code == 404
    assert "ID could not be found" in response.json.get('message')
