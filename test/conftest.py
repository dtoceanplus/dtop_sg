import pytest

from dtop_stagegate.service import create_app
from dtop_stagegate.business.db_utils import init_db


@pytest.fixture
def app():
    app = create_app({
        'TESTING': True,
        'SQLALCHEMY_DATABASE_URI': 'sqlite://',
        'SERVER_NAME': 'localhost.dev'
    })

    with app.app_context():
        init_db()

    yield app


@pytest.fixture
def client(app):
    return app.test_client()


@pytest.fixture
def runner(app):
    return app.test_cli_runner()
