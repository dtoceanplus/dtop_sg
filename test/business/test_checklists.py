from dtop_stagegate.business.checklists import update_checklist_activity_answers, ChecklistResults, \
    group_checklist_activities, group_checklist_activities_by_eval_area, calculate_activity_totals, \
    StageCategoryAssessment
from dtop_stagegate.business.frameworks import get_eval_areas
from dtop_stagegate.business.models import ChecklistActivity

from test.business.test_sg_studies_business import get_study, init_stage_gate_study

study_id = '1'


def update_and_compare(data):
    update_checklist_activity_answers(study_id, data)
    study = get_study('1')
    assert study.get('checklist_study_complete')

    checklist_activities = ChecklistActivity.query.filter(ChecklistActivity.stage_gate_study_id == study_id)
    ca_complete = [ca.activity_id for ca in checklist_activities if ca.complete]
    assert ca_complete == data


def test_update_checklist_activity_answers(app):
    with app.app_context():
        init_stage_gate_study()

        ca_data = [1, 5, 17]
        update_and_compare(ca_data)

        ca_data = [1, 2, 3, 17]
        update_and_compare(ca_data)

        ca_data = []
        update_and_compare(ca_data)


def stage_grouping_function(ca):
    return ca.activity.activity_category.stage.name


def get_example_checklist_activities():
    return [ChecklistActivity.query.get(ca_id) for ca_id in range(1, 4)]


def test_group_checklist_activities(app):
    with app.app_context():
        init_stage_gate_study()

        checklist_activities = get_example_checklist_activities()
        grouped_ca = group_checklist_activities(checklist_activities, stage_grouping_function)
        assert len(grouped_ca['Stage 0']) == 3
        assert len(list(grouped_ca.items())) == 1
        assert type(grouped_ca['Stage 0'][0]) == ChecklistActivity


def test_group_checklist_activities_by_eval_area(app):
    with app.app_context():
        init_stage_gate_study()

        eval_areas = get_eval_areas()
        checklist_activities = get_example_checklist_activities()
        grouped_ca_eval = group_checklist_activities_by_eval_area(eval_areas, checklist_activities)
        assert len(grouped_ca_eval['Affordability']) == 1
        assert len(grouped_ca_eval['Installability']) == 1
        assert len(grouped_ca_eval['Maintainability']) == 1
        assert type(grouped_ca_eval['Affordability'][0]) == ChecklistActivity


def test_calculate_activity_totals(app):
    with app.app_context():
        init_stage_gate_study()
        checklist_activities = get_example_checklist_activities()

        activity_totals = calculate_activity_totals(checklist_activities)
        assert activity_totals['total'] == 3
        assert activity_totals['complete'] == 0
        assert activity_totals['percent'] == 0

        update_checklist_activity_answers(study_id, [1, 2])
        activity_totals = calculate_activity_totals(checklist_activities)
        assert activity_totals['total'] == 3
        assert activity_totals['complete'] == 2
        assert activity_totals['percent'] == 67

        update_checklist_activity_answers(study_id, [1, 2, 3])
        activity_totals = calculate_activity_totals(checklist_activities)
        assert activity_totals['total'] == 3
        assert activity_totals['complete'] == 3
        assert activity_totals['percent'] == 100


def test_stage_category_assessment_assess(app):
    with app.app_context():
        init_stage_gate_study()

        cr = ChecklistResults('1')
        checklist_activities = get_example_checklist_activities()
        sca = StageCategoryAssessment(cr, checklist_activities, 'activity_category')

        assess_summary = sca.assess()
        r = assess_summary[0]
        expected_cat = 'Requirements'
        assert len(assess_summary) == 1
        assert r['name'] == expected_cat
        assert r['total'] == 3
        assert r['complete'] == 0
        assert r['percent'] == 0
        assert len(r['outstanding']) == 3
        assert r['outstanding'][0]['name'] == 'Requirement definition (Affordability)'

        update_checklist_activity_answers(study_id, [1, 2])
        sca = StageCategoryAssessment(cr, checklist_activities, 'activity_category')
        assess_summary = sca.assess()
        r = assess_summary[0]
        assert len(assess_summary) == 1
        assert r['name'] == expected_cat
        assert r['total'] == 3
        assert r['complete'] == 2
        assert r['percent'] == 67
        assert len(r['outstanding']) == 1
        assert r['outstanding'][0]['name'] == 'Requirement definition (Maintainability)'


# a test case with default study and empty checklist
# a test case with some activities marked complete
def test_checklist_results(app):
    with app.app_context():
        init_stage_gate_study()

        cr = ChecklistResults('1')
        results = cr.results
        assert len(results) == 6
        assert type(results[0]) is dict
        assert len(results[0]['activity_categories']) == 7
        assert len(results[0]['evaluation_areas']) == 11
        assert results[0]['name'] == 'Stage 0'
        assert results[0]['percent_complete'] == 0

        update_checklist_activity_answers(study_id, [1, 2])
        cr = ChecklistResults('1')
        results = cr.results
        assert len(results) == 6
        assert type(results[0]) is dict
        assert len(results[0]['activity_categories']) == 7
        assert len(results[0]['evaluation_areas']) == 11
        assert results[0]['name'] == 'Stage 0'
        assert results[0]['percent_complete'] == 7
