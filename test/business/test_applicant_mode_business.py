import flask_sqlalchemy

from dtop_stagegate.business.applicant_mode import get_stage_gate_assessments, update_applicant_answers_in_db, \
    update_applicant_complete_status, get_applicant_answers_by_stage_gate_assessment_id, ApplicantStageGateData, \
    ApplicantResults, ApplicantAnswerAnalysis, ThresholdAnalysis
from dtop_stagegate.business.models import StageGateAssessment, ApplicantAnswer, QuestionMetric, Question, Metric, \
    QuestionCategory, Framework
from test.business.test_sg_studies_business import init_stage_gate_study, add_sg_study
from test.business.test_frameworks_business import install_default_and_test_framework
from dtop_stagegate.service import db


def test_get_stage_gate_assessments(app):
    with app.app_context():
        init_stage_gate_study()
        sg_assessments = get_stage_gate_assessments('1')

    assert type(sg_assessments) == flask_sqlalchemy.BaseQuery
    assert len(sg_assessments.all()) == 5
    assert sg_assessments[0].id == 1
    assert not sg_assessments[0].applicant_complete
    assert not sg_assessments[0].assessor_complete


def test_get_applicant_answers_by_stage_gate_assessment_id(app):
    with app.app_context():
        init_stage_gate_study()

        app_answers = get_applicant_answers_by_stage_gate_assessment_id('1')
        assert type(app_answers) is list
        assert len(app_answers) == 19
        assert [hasattr(app_answers, x) for x in ['result', 'justification', 'response']]


def test_join_stage_gate_data_applicant_answers(app):
    with app.app_context():
        init_stage_gate_study()

        stage_gate = StageGateAssessment.query.get('1')
        sg = ApplicantStageGateData(stage_gate)
        stage_gate_data = sg.join_data()
        check_applicant_answers(stage_gate_data)

        app_answer_list = get_applicant_answers_by_stage_gate_assessment_id('1')
        app_answer_list[0].result = 147.
        app_answer_list[0].justification = 'test'
        sg = ApplicantStageGateData(stage_gate)
        stage_gate_data = sg.join_data()
        check_applicant_answers(stage_gate_data, 147., 'test')


def check_applicant_answers(sg, result=None, justification=None, response=None, applicant_key='applicant_answers'):

    assert type(sg) is dict
    q_cats = sg.get('question_categories')
    assert type(q_cats) is list
    assert len(q_cats) == 5
    question_1 = q_cats[0]['questions'][0]
    assert question_1['name'] == 'Scientific credibility'
    applicant_answers_1 = question_1[applicant_key]
    assert applicant_answers_1['id'] == 1
    assert applicant_answers_1['result'] == result
    assert applicant_answers_1['justification'] == justification
    assert applicant_answers_1['response'] == response

    return applicant_answers_1


def check_applicant_answers_stg4(sg, result=None, justification=None, response=None, applicant_key='applicant_answers'):

    assert type(sg) is dict
    q_cats = sg.get('question_categories')
    assert type(q_cats) is list
    assert len(q_cats) == 8
    question_1 = q_cats[6]['questions'][0]
    assert question_1['name'] == 'LCOE'
    applicant_answers_1 = question_1[applicant_key]
    assert applicant_answers_1['id'] == 91
    assert applicant_answers_1['result'] == result
    assert applicant_answers_1['justification'] == justification
    assert applicant_answers_1['response'] == response

    return applicant_answers_1


def test_update_applicant_complete_status(app):
    with app.app_context():
        init_stage_gate_study()

        sg_assessment = get_stage_gate_assessments('1')
        assert all([not asg.applicant_complete for asg in sg_assessment])

        sg_assessment = StageGateAssessment.query.get('1')
        update_applicant_complete_status(sg_assessment)
        sg_assessment = get_stage_gate_assessments('1')
        assert sg_assessment[0].applicant_complete
        assert not sg_assessment[1].applicant_complete


def test_update_applicant_answers_in_db(app):
    with app.app_context():
        init_stage_gate_study()

        data = [
            {
                'id': 1,
                'result': 43,
                'justification': 'test'
            },
            {
                'id': 2,
                'response': 'test'
            },
        ]
        sg_assessment = StageGateAssessment.query.get('1')
        update_applicant_answers_in_db(sg_assessment, data)

        aa_1 = ApplicantAnswer.query.get('1')
        assert aa_1.result == 43
        assert aa_1.justification == 'test'
        assert not aa_1.response
        aa_2 = ApplicantAnswer.query.get('2')
        assert not aa_2.result
        assert not aa_2.justification
        assert aa_2.response == 'test'


def test_update_incorrect_applicant_answers(app):
    with app.app_context():
        init_stage_gate_study()

        data = [
            {
                'id': 1,
                'result': 43,
                'justification': 'test'
            }
        ]
        sg_assessment = StageGateAssessment.query.get('2')
        err = update_applicant_answers_in_db(sg_assessment, data)
        assert err == "Request body contains applicant answers with invalid IDs"


def test_calculate_threshold(app):
    with app.app_context():
        install_default_and_test_framework()
        add_sg_study(2)

        # test an 'upper' type threshold, in this case 'LCOE'
        qm = QuestionMetric.query.get(153)  # question metric for LCOE for test framework
        qm.threshold_bool = True
        qm.threshold = 150
        db.session.commit()

        # Get app answer for id 91 which is LCOE
        app_answer = ApplicantAnswer.query.get(91)
        assert qm.metric_id == 15  # make sure we are dealing with LCOE (metric id 15)
        lcoe = dict(name='LCOE', unit='€/kWh', threshold=150)
        check_threshold_analysis(
            app_answer,
            lcoe,
            None,
            'upper',
            False,
            "Affordability",
            "The Levelized Cost Of Energy for the project. See the System Lifetime Costs (SLC) module and"
            " documentation for more information.  "
        )
        check_threshold_analysis(
            app_answer,
            lcoe,
            143,
            'upper',
            True,
            "Affordability",
            "The Levelized Cost Of Energy for the project. See the System Lifetime Costs (SLC) module and"
            " documentation for more information.  "
        )
        check_threshold_analysis(
            app_answer,
            lcoe,
            150,
            'upper',
            True,
            "Affordability",
            "The Levelized Cost Of Energy for the project. See the System Lifetime Costs (SLC) module and"
            " documentation for more information.  "
        )
        check_threshold_analysis(
            app_answer,
            lcoe,
            157,
            'upper',
            False,
            "Affordability",
            "The Levelized Cost Of Energy for the project. See the System Lifetime Costs (SLC) module and"
            " documentation for more information.  ",
            7.,
            5
        )

        # test a 'lower' type threshold, in this case 'array captured efficiency'
        qm = QuestionMetric.query.get(100)  # question metric for array captured efficiency for test framework
        qm.threshold_bool = True
        qm.threshold = 20
        db.session.commit()

        # Get app answer for id 28 which is array captured efficiency
        app_answer = ApplicantAnswer.query.get(28)
        assert qm.metric_id == 4  # make sure we are dealing with array captured efficiency (metric id 4)
        cf = dict(name='Array captured efficiency', unit='% (decimal)', threshold=20)
        check_threshold_analysis(
            app_answer,
            cf,
            None,
            'lower',
            False,
            "Power Capture",
            "The captured energy efficiency for the array. Defined as the ratio between annual captured energy and "
            "the annual capacity power of the array. See the Systems, Performance and Energy Yield (SPEY) module "
            "and documentation for more information. "
        )
        check_threshold_analysis(
            app_answer,
            cf,
            23,
            'lower',
            True,
            "Power Capture",
            "The captured energy efficiency for the array. Defined as the ratio between annual captured energy and "
            "the annual capacity power of the array. See the Systems, Performance and Energy Yield (SPEY) module "
            "and documentation for more information. "
        )
        check_threshold_analysis(
            app_answer,
            cf,
            20,
            'lower',
            True,
            "Power Capture",
            "The captured energy efficiency for the array. Defined as the ratio between annual captured energy and "
            "the annual capacity power of the array. See the Systems, Performance and Energy Yield (SPEY) module "
            "and documentation for more information. "
        )
        check_threshold_analysis(
            app_answer,
            cf,
            18,
            'lower',
            False,
            "Power Capture",
            "The captured energy efficiency for the array. Defined as the ratio between annual captured energy and "
            "the annual capacity power of the array. See the Systems, Performance and Energy Yield (SPEY) module "
            "and documentation for more information. ",
            2.,
            10
        )


def check_threshold_analysis(
    app_answer,
    metric_data,
    metric_result,
    threshold_type,
    threshold_passed,
    evaluation_area,
    description,
    abs_dist=None,
    percent_dist=None,
):

    if metric_result is not None:
        app_answer.result = metric_result
    ta = ThresholdAnalysis(app_answer)
    ta.calculate()

    assert ta.threshold_bool
    assert ta.threshold_passed is threshold_passed
    assert ta.absolute_distance == abs_dist
    assert ta.percent_distance == percent_dist
    assert ta.data == dict(
        metric=metric_data['name'],
        unit=metric_data['unit'],
        threshold=metric_data['threshold'],
        result=metric_result,
        absolute_distance=abs_dist,
        percent_distance=percent_dist,
        threshold_passed=threshold_passed,
        threshold_type=threshold_type,
        evaluation_area=evaluation_area,
        justification=None,
        threshold_bool=True,
        description=description
    )


def test_applicant_answer_analysis(app):
    with app.app_context():
        install_default_and_test_framework()
        add_sg_study(2)

        # test an 'upper' type threshold, in this case 'LCOE'
        qm = QuestionMetric.query.get(153)  # question metric for LCOE for test framework
        qm.threshold_bool = True
        qm.threshold = 150
        db.session.commit()

        app_answer = ApplicantAnswer.query.get(1)
        # app answer ID 1 is not a metric question
        aaa = ApplicantAnswerAnalysis(app_answer)
        assert not aaa.metric_question
        aaa.analyse()
        assert not aaa.answered
        assert not aaa.threshold_bool
        assert not aaa.threshold_analysis

        app_answer.response = 'this is a test'
        aaa = ApplicantAnswerAnalysis(app_answer)
        assert not aaa.metric_question
        aaa.analyse()
        assert aaa.answered
        assert not aaa.threshold_bool
        assert not aaa.threshold_analysis

        # Get app answer for id 91 which is LCOE
        app_answer = ApplicantAnswer.query.get(91)
        aaa = ApplicantAnswerAnalysis(app_answer)
        assert aaa.metric_question
        aaa.analyse()
        assert not aaa.answered
        assert aaa.threshold_bool
        assert not aaa.threshold_analysis.threshold_passed
        assert not aaa.threshold_analysis.absolute_distance
        assert not aaa.threshold_analysis.percent_distance

        app_answer.result = 143.
        aaa = ApplicantAnswerAnalysis(app_answer)
        assert aaa.metric_question
        aaa.analyse()
        assert not aaa.answered
        assert aaa.threshold_bool
        assert aaa.threshold_analysis.threshold_passed
        assert not aaa.threshold_analysis.absolute_distance
        assert not aaa.threshold_analysis.percent_distance

        app_answer.justification = 'this is a test'
        aaa = ApplicantAnswerAnalysis(app_answer)
        assert aaa.metric_question
        aaa.analyse()
        assert aaa.answered
        assert aaa.threshold_bool
        assert aaa.threshold_analysis.threshold_passed
        assert not aaa.threshold_analysis.absolute_distance
        assert not aaa.threshold_analysis.percent_distance

        app_answer.result = 157.
        aaa = ApplicantAnswerAnalysis(app_answer)
        assert aaa.metric_question
        aaa.analyse()
        assert aaa.answered
        assert aaa.threshold_bool
        assert not aaa.threshold_analysis.threshold_passed
        assert aaa.threshold_analysis.absolute_distance == 7.
        assert aaa.threshold_analysis.percent_distance == 5


def test_calculate_applicant_results(app):
    with app.app_context():
        install_default_and_test_framework()
        add_sg_study(2)

        # First check that threshold success rate is not calculated if no thresholds set
        app_answer = ApplicantAnswer.query.get(91)

        app_answer.result = 147.
        app_answer.justification = 'test'
        sg_assessment = StageGateAssessment.query.get('4')
        app_result = ApplicantResults(sg_assessment)
        app_result.calculate()
        d = app_result.get_results()
        assert not d['summary_data']['threshold_success_rate']

        # Then set a threshold of 153 for LCOE
        qm = QuestionMetric.query.get(153)  # question metric for LCOE for test framework
        qm.threshold_bool = True
        qm.threshold = 150
        # And a threshold of 20 for array captured efficiency
        qm = QuestionMetric.query.get(100)  # question metric for array captured efficiency for test framework
        qm.threshold_bool = True
        qm.threshold = 20
        db.session.commit()

        app_answer = ApplicantAnswer.query.get(91)  # LCOE
        app_answer.result = 147.
        app_answer.justification = 'test'
        sg_assessment = StageGateAssessment.query.get('4')
        app_result = ApplicantResults(sg_assessment)
        app_result.calculate()
        d = app_result.get_results()
        check_applicant_summary_results(d['summary_data'], 4, 100)
        metric_dict = [
            dict(absolute_distance=None, percent_distance=None, result=None, threshold_passed=True)
            for i in range(25)
        ]
        metric_dict[13]['result'] = 147.0
        check_applicant_metric_results(d['metric_results'], 27, metric_dict)
        check_stage_gate_data(d['responses'], 147., 'test', None, True, None, None)

        # TODO: Re-do these additional tests in a more re-usable way (once framework DB models have been improved)
        # app_answer_list[1].response = 'test'
        # app_answer_list[2].response = 'test'
        # sg_assessment = StageGateAssessment.query.get('1')
        # app_result = ApplicantResults(sg_assessment)
        # app_result.calculate()
        # d = app_result.get_results()
        # check_applicant_summary_results(d['summary_data'], 75, 50)
        #
        # app_answer_list[3].result = 19
        # app_answer_list[3].justification = 'test'
        # sg_assessment = StageGateAssessment.query.get('1')
        # app_result = ApplicantResults(sg_assessment)
        # app_result.calculate()
        # d = app_result.get_results()
        # check_applicant_summary_results(d['summary_data'], 100, 50)
        # metric_dict = [
        #     dict(absolute_distance=None, percent_distance=None, result=147., threshold_passed=True),
        #     dict(absolute_distance=1, percent_distance=5, result=19, threshold_passed=False)
        # ]
        # check_applicant_metric_results(d['metric_results'], 2, metric_dict)
        #
        # app_answer_list[3].result = 22
        # sg_assessment = StageGateAssessment.query.get('1')
        # app_result = ApplicantResults(sg_assessment)
        # app_result.calculate()
        # d = app_result.get_results()
        # check_applicant_summary_results(d['summary_data'], 100, 100)
        # metric_dict = [
        #     dict(absolute_distance=None, percent_distance=None, result=147., threshold_passed=True),
        #     dict(absolute_distance=None, percent_distance=None, result=22, threshold_passed=True)
        # ]
        # check_applicant_metric_results(d['metric_results'], 2, metric_dict)


def check_applicant_summary_results(summary_results, response_rate, threshold_success_rate):

    assert summary_results['response_rate'] == response_rate
    assert summary_results['threshold_success_rate'] == threshold_success_rate


def check_applicant_metric_results(metric_results, length, metric_dict):

    assert len(metric_results) == length
    for idx in [0, 1]:
        assert [metric_results[idx][k] == metric_dict[idx][k] for k in metric_dict[idx].keys()]


def check_stage_gate_data(
    sg,
    result=None,
    justification=None,
    response=None,
    threshold_passed=False,
    abs_dist=None,
    percent_dist=None
):
    app_answer = check_applicant_answers_stg4(sg, result, justification, response, applicant_key='applicant_results')
    assert app_answer['threshold_passed'] == threshold_passed
    assert app_answer['absolute_distance'] == abs_dist
    assert app_answer['percent_distance'] == percent_dist
