from dtop_stagegate.business.db_utils import install_db
from dtop_stagegate.business.frameworks import create_framework, delete_framework, update_framework, \
    update_question_metric, update_question_metric_list, get_list_applied_metric_thresholds, \
    get_stage_by_evaluation_area, get_eval_areas, get_activity_eval_area_query, get_activities_by_framework, \
    get_stage_gates_by_framework, get_questions_by_framework, get_metric_thresholds_by_stage_gate_id
from dtop_stagegate.business.models import Framework, QuestionMetric, Activity, Question, QuestionCategory, StageGate
from test.business.test_sg_studies_business import add_sg_study


def install_default_and_test_framework():

    install_db()
    data = {
        "name": "test framework name",
        "description": "test framework description"
    }
    framework_model = create_framework(data)

    return framework_model, data


def test_create_framework(app):

    with app.app_context():
        framework_model, data = install_default_and_test_framework()
        framework_list = Framework.query.all()
        assert len(framework_list) == 2
        assert framework_model.name == "test framework name"
        assert framework_model.description == "test framework description"
        framework_db = Framework.query.get(2)
        assert framework_model == framework_db

        duplicate_name_error = create_framework(data)
        assert "name already exists" in duplicate_name_error


def test_update_framework(app):

    with app.app_context():
        install_default_and_test_framework()
        update_data = dict(
            name='test',
            description='test'
        )
        f_model = update_framework(update_data, '2')
        assert f_model.name == 'test'
        assert f_model.description == 'test'

        f_model = update_framework(update_data, '3')
        assert "does not exist" in f_model

        f_model = update_framework(update_data, '1')
        assert "default framework template" in f_model

        default_data = dict(name="Default (no metric thresholds)")
        f_model = update_framework(default_data, '2')
        assert "name already exists" in f_model


def test_delete_framework(app):

    with app.app_context():
        install_default_and_test_framework()
        delete_success_msg = delete_framework('2')
        assert delete_success_msg == "Framework deleted"
        framework_list = Framework.query.all()
        assert len(framework_list) == 1
        assert framework_list[0].name == "Default (no metric thresholds)"

        missing_id_msg = delete_framework('2')
        assert "ID does not exist" in missing_id_msg

        delete_default_msg = delete_framework('1')
        assert delete_default_msg == "Cannot delete default framework template"
        framework_list = Framework.query.all()
        assert len(framework_list) == 1


def test_delete_framework_with_associated_sg_study(app):

    with app.app_context():
        install_default_and_test_framework()
        add_sg_study(2)
        delete_fail_msg = delete_framework('2')
        assert delete_fail_msg == "This Framework is being used in a Stage Gate study. Cannot delete."
        framework_list = Framework.query.all()
        assert len(framework_list) == 2


def test_get_activities_by_framework(app):

    with app.app_context():
        install_db()
        a = get_activities_by_framework('1')
        assert type(a) == list
        assert a[0].name == 'Requirement definition (Affordability)'
        assert len(a) == 199


def test_update_question_metric(app):

    with app.app_context():
        install_default_and_test_framework()
        qm_db = QuestionMetric().query.get(1)
        assert not qm_db.threshold_bool
        data = {
            "threshold_bool": True,
            "threshold": 143.
        }
        qm_model = update_question_metric(data, "1")
        qm_db = QuestionMetric().query.get(1)
        assert qm_db == qm_model
        assert qm_db.threshold_bool
        assert qm_db.threshold == 143.

        data = {
            "threshold_bool": False,
        }
        qm_model = update_question_metric(data, "2")
        qm_db = QuestionMetric().query.get(2)
        assert qm_db == qm_model
        assert not qm_db.threshold_bool
        assert qm_db.threshold is None

        data = {
            "threshold": 143.
        }
        qm_model = update_question_metric(data, "2")
        qm_db = QuestionMetric().query.get(2)
        assert qm_db == qm_model
        assert not qm_db.threshold_bool
        assert qm_db.threshold == 143.

        missing_id_msg = update_question_metric(data, "229")  # 114 question metrics per framework
        assert "ID does not exist" in missing_id_msg


def test_update_question_metric_list(app):

    with app.app_context():
        install_default_and_test_framework()
        qm_db = QuestionMetric().query.get(1)
        assert not qm_db.threshold_bool
        assert not qm_db.threshold
        qm_db = QuestionMetric().query.get(2)
        assert not qm_db.threshold_bool
        assert not qm_db.threshold

        rb = [
            {
                'id': 1,
                'threshold': 43.0,
                'threshold_bool': True
            },
            {
                'id': 2,
                'threshold': 143.0,
                'threshold_bool': True
            }
        ]
        update_question_metric_list(rb)
        check_question_metrics()


def check_question_metrics():
    qm_db = QuestionMetric().query.get(1)
    assert qm_db.threshold_bool
    assert qm_db.threshold == 43.0
    qm_db = QuestionMetric().query.get(2)
    assert qm_db.threshold_bool
    assert qm_db.threshold == 143.0
    qm_db = QuestionMetric().query.get(3)
    assert not qm_db.threshold_bool
    assert not qm_db.threshold


def test_get_list_applied_metric_thresholds(app):

    with app.app_context():
        install_default_and_test_framework()
        metric_thresholds = get_list_applied_metric_thresholds("2")
        assert metric_thresholds.count() == 0

        data = {
            "threshold_bool": True,
            "threshold": 143.
        }
        update_question_metric(data, "115")
        metric_thresholds = get_list_applied_metric_thresholds("2")
        assert metric_thresholds.count() == 1
        assert metric_thresholds[0].threshold_bool
        assert metric_thresholds[0].threshold == 143.


def test_get_metric_thresholds_by_stage_gate_id(app):

    with app.app_context():
        install_default_and_test_framework()
        metric_thresholds = get_metric_thresholds_by_stage_gate_id("2")
        assert metric_thresholds.all() == []

        data = {
            "threshold_bool": True,
            "threshold": 143.
        }
        update_question_metric(data, "115")
        metric_thresholds = get_metric_thresholds_by_stage_gate_id("7")
        assert metric_thresholds.count() == 1
        assert metric_thresholds[0].threshold_bool
        assert metric_thresholds[0].threshold == 143.

        metric_thresholds = get_metric_thresholds_by_stage_gate_id("8")
        assert metric_thresholds.all() == []


def test_get_stage_by_evaluation_area(app):

    with app.app_context():
        install_db()
        eval_stages = get_stage_by_evaluation_area('1')
        assert eval_stages['name'] == "Stage 0"
        assert eval_stages['number'] == 0
        eval_areas = eval_stages['evaluation_areas']
        assert len(eval_areas) == 9
        ea = eval_areas[0]
        assert ea['name'] == "Acceptability"
        assert ea['description'] == "The environmental and social acceptance of ocean energy technology."
        activities = ea['activities']
        assert activities[0]['name'] == "Acceptability assessment"
        assert activities[0]['id'] == 25

        eval_stages = get_stage_by_evaluation_area('7')
        assert eval_stages is None


def test_get_eval_areas(app):

    with app.app_context():
        install_db()
        eval_areas = get_eval_areas()
        assert len(eval_areas) == 11
        assert eval_areas[-1]['id'] == 11
        assert eval_areas[-1]['name'] == "Controllability"


def test_get_activity_eval_area_query(app):

    with app.app_context():
        install_db()
        act_eval_area_query = get_activity_eval_area_query('1')
        evaluation_area_id = act_eval_area_query[0][0]
        assert evaluation_area_id == 7
        activity = act_eval_area_query[0][1]
        assert type(activity) is Activity
        assert activity.name == "Requirement definition (Affordability)"
        assert activity.id == 1


def test_get_stage_gates_by_framework(app):

    with app.app_context():
        install_db()
        sg = get_stage_gates_by_framework('1')

        assert type(sg) == list
        assert len(sg) == 5
        assert sg[0].name == 'Stage Gate 0 - 1'


def test_get_questions_by_framework(app):

    with app.app_context():
        install_db()
        q = get_questions_by_framework('1')

        assert type(q) == list
        assert len(q) == 130
        assert q[0].name == 'Scientific credibility'


def test_stage_gate_total_weightings(app):

    with app.app_context():
        install_db()

        for sg_id in range(1, 6):
            questions = Question.query. \
                join(QuestionCategory). \
                join(StageGate). \
                filter(StageGate.id == sg_id).all()
            weights = [q.weighting for q in questions]
            assert sum(weights) == 100
