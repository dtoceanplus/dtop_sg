import copy

from dtop_stagegate.business.models import StageGateStudy, QuestionMetric, ApplicantAnswer, AssessorScore
from dtop_stagegate.business.report_generation import *
from dtop_stagegate.service import db
from test.business.test_improvement_areas import set_checklist_complete
from test.business.test_frameworks_business import install_default_and_test_framework
from test.business.test_sg_studies_business import init_stage_gate_study, add_sg_study

REPORT_REQUEST_BODY = {
    'sections': {
        'checklist_summary': False,
        'checklist_stage_results': False,
        'checklist_outstanding_activities': False,
        'applicant_summary': False,
        'metric_results': False,
        'applicant_answers': False,
        'assessor_overall_scores': False,
        'assessor_question_categories': False,
        'assessor_scores_comments': False,
        'improvement_areas': False
    },
    'stage_index': None,
    'stage_gate_index': None,
    'ia_threshold': 50
}


def test_study_data(app):
    with app.app_context():
        init_stage_gate_study()
        s = StageGateStudy.query.get(1)
        request = copy.deepcopy(REPORT_REQUEST_BODY)

        rc = ReportCompiler(s, request)
        rc.construct()
        d = rc.content
        assert d['study_params'] == {
                'study_name': 'test study 1',
                'date': datetime.datetime.now().strftime("%d/%m/%Y"),
                'selected_stage': 'N/A',
                'selected_stage_gate': 'N/A',
                'description': 'description for test stage gate study 1',
                'threshold_settings': 'Default (no metric thresholds)',
                'stage_index': None
        }


def test_plotly_bar_chart():

    data = {
        'x': list(range(6)),
        'y': ['a', 'b', 'this is text that should be searchable', 'd', 'e', 'f'],
        'name': 'test'
    }
    chart = PlotlyBarChartData(data, 'percentage')
    assert chart.data == [
        {
            'x': data['x'],
            'y': data['y'],
            'orientation': 'h',
            'name': 'test',
            'type': 'bar'
        }
    ]
    assert chart.layout == {
        'title': {
            'text': 'test',
            'x': 0.5,
            'xanchor': 'center'
        },
        'xaxis': {
            'title': "Percentage activities complete (%)",
            'tickmode': 'array',
            'tickvals': [0, 25, 50, 75, 100],
            "range": [
                -5,
                105
            ],
        },
        'template': 'none',
        'yaxis': {
            'automargin': True
        },
        'margin':{
            'pad': 6
        },
        'autosize': False,
        'width': 500,
        'height': 500,
        'font': {
            'color': 'black',
        }
    }


def test_plotly_bar_chart_optional_args():

    data = {
        'x': list(range(6)),
        'y': ['a', 'b', 'this is text that should be searchable', 'd', 'e', 'f'],
        'name': 'test'
    }
    chart = PlotlyBarChartData(data, 'percentage', 350, False)
    assert chart.data == [
        {
            'x': data['x'],
            'y': data['y'],
            'orientation': 'h',
            'name': 'test',
            'type': 'bar'
        }
    ]
    assert chart.layout == {
        'title': {
            'text': '',
            'x': 0.5,
            'xanchor': 'center'
        },
        'xaxis': {
            'title': "Percentage activities complete (%)",
            'tickmode': 'array',
            'tickvals': [0, 25, 50, 75, 100],
            "range": [
                -5,
                105
            ],
        },
        'template': 'none',
        'yaxis': {
            'automargin': True
        },
        'margin':{
            'pad': 6
        },
        'autosize': False,
        'width': 500,
        'height': 350,
        'font': {
            'color': 'black',
        }
    }


def mark_checklist_as_complete():
    s = StageGateStudy.query.get(1)
    s.checklist_study_complete = True
    request = copy.deepcopy(REPORT_REQUEST_BODY)

    return s, request


def test_checklist_summary(app):
    with app.app_context():
        init_stage_gate_study()

        s, request = mark_checklist_as_complete()
        request['sections']['checklist_summary'] = True
        rc = ReportCompiler(s, request)
        cd = ChecklistData(rc)
        assert rc.content['checklist_summary'] == ['0%', '0%', '0%', '0%', '0%', '0%']


def test_format_stage_results(app):
    with app.app_context():
        init_stage_gate_study()
        s, request = mark_checklist_as_complete()
        request['sections']['checklist_stage_results'] = True
        request['stage_index'] = 0
        rc = ReportCompiler(s, request)
        cd = ChecklistData(rc)

        bar_charts = rc.content['checklist_bar_charts']
        assert bar_charts == {
            'activity_category': {
                'data': [{
                    'x': [0, 0, 0, 0, 0, 0, 0],
                    'y': [
                        'Requirements',
                        'Target selection',
                        'Concept',
                        'Comparable technology',
                        'Economic assessment',
                        'Environmental and social impacts',
                        'Environment characterisation'
                    ],
                    'orientation': 'h',
                    'name': 'Activity Categories',
                    'type': 'bar'
                }],
                'layout': {
                    'title': {
                        'text': 'Activity Categories',
                        'x': 0.5,
                        'xanchor': 'center'
                    },
                    'xaxis': {
                        'title': "Percentage activities complete (%)",
                        'tickmode': 'array',
                        'tickvals': [0, 25, 50, 75, 100],
                        "range": [
                            -5,
                            105
                        ],
                    },
                    'template': 'none',
                    'yaxis': {
                        'automargin': True
                    },
                    'margin': {
                        'pad': 6
                    },
                    'autosize': False,
                    'width': 500,
                    'height': 500,
                    'font': {
                        'color': 'black',
                    }
                }
            },
            'evaluation_area': {
                'data': [{
                    'x': [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                    'y': [
                        'Acceptability',
                        'Installability',
                        'Availability',
                        'Maintainability',
                        'Reliability',
                        'Survivability',
                        'Affordability',
                        'Manufacturability',
                        'Power Capture',
                        'Power Conversion',
                        'Controllability'
                    ],
                    'orientation': 'h',
                    'name': 'Evaluation Areas',
                    'type': 'bar'
                }],
                'layout': {
                    'title': {
                        'text': 'Evaluation Areas',
                        'x': 0.5,
                        'xanchor': 'center'
                    },
                    'xaxis': {
                        'title': "Percentage activities complete (%)",
                        'tickmode': 'array',
                        'tickvals': [0, 25, 50, 75, 100],
                        "range": [
                            -5,
                            105
                        ],
                    },
                    'template': 'none',
                    'yaxis': {
                        'automargin': True
                    },
                    'margin': {
                        'pad': 6
                    },
                    'autosize': False,
                    'width': 500,
                    'height': 500,
                    'font': {
                        'color': 'black',
                    }
                }
            }
        }


def test_format_outstanding_activities(app):
    with app.app_context():
        init_stage_gate_study()

        cas = set_checklist_complete('1')
        cas[0].complete = 0
        cas[-1].complete = 0
        db.session.commit()

        s, request = mark_checklist_as_complete()
        request['sections']['checklist_outstanding_activities'] = True
        request['stage_index'] = 0
        rc = ReportCompiler(s, request)
        cd = ChecklistData(rc)

        assert rc.content['outstanding_activities'] == [
            {
                'text': 'OUTSTANDING ACTIVITIES',
                'style': 'subheader'
            },
            '\n',
            {
                'text': 'The following is a list of the outstanding activities for Stage 0, sorted by activity '
                        'category:',
                'alignment': 'justify'
            },
            '\n',
            {
                'ul': [
                    'Requirements',
                    {
                        'type': 'circle',
                        'ul': [
                            {
                                'text': [
                                    {
                                        'text': 'Requirement definition (Affordability): ',
                                        'style': 'bold'
                                    },
                                    'Definition of technology and market requirements and challenges associated with '
                                    'Affordability (the problem statement)',
                                    '\n\n'
                                ],
                            },
                        ]
                    },
                    'Environment characterisation',
                    {
                        'type': 'circle',
                        'ul': [
                            {
                                'text': [
                                    {
                                        'text': 'Survival event characterisation: ',
                                        'style': 'bold'
                                    },
                                    'Clear definition of what the survival events may be, '
                                    'and their likely impact on systems',
                                    '\n\n'
                                ],
                            },
                        ]
                    },
                ],
                'alignment': 'justify'
            }
        ]


def find_item(l, style):
    for i in l:
        if type(i) is dict:
            if "style" in i:
                if i['style'] == style:
                    return i


def mark_applicant_as_complete(sg_idx=0):
    s = StageGateStudy.query.get(1)
    s.stage_gate_assessment[sg_idx].applicant_complete = 1
    request = copy.deepcopy(REPORT_REQUEST_BODY)
    request['sections']['applicant_summary'] = True
    request['stage_gate_index'] = sg_idx

    return s, request


def test_initialise_applicant_data(app):
    with app.app_context():
        init_stage_gate_study()

        s, request = mark_applicant_as_complete()
        rc = ReportCompiler(s, request)

        ad = ApplicantData(rc)
        assert ad._metric_results == [
            {
                'text': 'METRIC RESULTS',
                'style': 'subheader'
            },
            '\n'
        ]
        assert ad._responses == [
            {
                'text': 'APPLICANT ANSWERS',
                'style': 'subheader'
            },
            '\n'
        ]

        ar = ad._applicant_results
        assert ar['metric_results'] == []
        assert ar['responses']['name'] == 'Stage Gate 0 - 1'
        assert ar['responses']['question_categories'][0]['questions'][0]['name'] == 'Scientific credibility'
        assert ar['summary_data']['response_rate'] == ad._response_rate == 0
        assert ar['summary_data']['threshold_success_rate'] == ad._threshold_success_rate is None


def test_format_applicant_summary(app):
    with app.app_context():
        init_stage_gate_study()

        s, request = mark_applicant_as_complete()
        rc = ReportCompiler(s, request)
        ad = ApplicantData(rc)

        assert rc.content['applicant_summary'] == [
            {
                'text': 'SUMMARY',
                'style': 'subheader'
            },
            '\n',
            {
                'text': f"The response rate for Stage Gate 0 - 1 was 0%.",
                'alignment': 'justify'
            },
            '\n',
        ]


def configure_second_framework_and_answers():
    install_default_and_test_framework()
    add_sg_study(2)

    # test an 'upper' type threshold, in this case 'LCOE'
    qm = QuestionMetric.query.get(153)  # question metric for LCOE for test framework
    qm.threshold_bool = True
    qm.threshold = 150
    db.session.commit()

    # Get app answer for id 91 which is LCOE
    app_answer = ApplicantAnswer.query.get(91)
    app_answer.result = 143.
    app_answer.justification = 'this is a test'
    db.session.commit()


def test_format_applicant_summary_with_thresholds(app):
    with app.app_context():

        configure_second_framework_and_answers()
        s, request = mark_applicant_as_complete(3)
        rc = ReportCompiler(s, request)
        ad = ApplicantData(rc)
        ar = ad._applicant_results

        assert rc.content['applicant_summary'] == [
            {
                'text': 'SUMMARY',
                'style': 'subheader'
            },
            '\n',
            {
                'text': f"The response rate for Stage Gate 3 - 4 was 4%.",
                'alignment': 'justify'
            },
            '\n',
            {
                'text': "The threshold success rate for Stage Gate 3 - 4 was 100%.\n\n",
                'alignment': "justify"
            }
        ]


def test_append_metric_results(app):
    with app.app_context():
        init_stage_gate_study()

        s, request = mark_applicant_as_complete(1)
        request['sections']['applicant_summary'] = False
        request['sections']['metric_results'] = True
        rc = ReportCompiler(s, request)
        ad = ApplicantData(rc)

        metric_results = rc.content['metric_results']

        assert metric_results[0] == {
            'text': 'METRIC RESULTS',
            'style': 'subheader'
        }
        assert metric_results[2] == {
            'text': 'The table below shows the metric results for Stage Gate 1 - 2.',
            'alignment': 'justify',
        }

        assert metric_results[4]['table']['body'][1] == ['Annual captured energy', 'kWh', None]
        assert metric_results[4]['table']['body'][2] == ['Annual transformed energy', 'kWh', None]


def test_append_metric_results_with_thresholds(app):
    with app.app_context():
        configure_second_framework_and_answers()
        s, request = mark_applicant_as_complete(3)
        request['sections']['applicant_summary'] = False
        request['sections']['metric_results'] = True
        rc = ReportCompiler(s, request)
        ad = ApplicantData(rc)

        metric_results = rc.content['metric_results']

        assert metric_results[0] == {
            'text': 'METRIC RESULTS',
            'style': 'subheader'
        }
        assert metric_results[2] == {
            'text': 'The table below shows the metric results for Stage Gate 3 - 4.',
            'alignment': 'justify',
        }

        assert metric_results[4]['pageBreak'] == 'before'
        assert metric_results[4]['pageOrientation'] == 'landscape'
        assert metric_results[4]['table']['body'][1] == [
            'Annual captured energy',
            'kWh',
            None,
            None,
            None,
            None
        ]
        assert metric_results[4]['table']['body'][2] == [
            'Annual transformed energy',
            'kWh',
            None,
            None,
            None,
            None
        ]
        assert metric_results[4]['table']['body'][15] == [
            'LCOE',
            '€/kWh',
            143,
            150,
            None,
            None
        ]


def test_add_reference_to_assessor_section(app):
    with app.app_context():
        init_stage_gate_study()

        s = StageGateStudy.query.get(1)
        s.stage_gate_assessment[0].applicant_complete = 1
        s.stage_gate_assessment[1].assessor_complete = 1
        request = copy.deepcopy(REPORT_REQUEST_BODY)
        request['stage_gate_index'] = 0
        request['sections']['applicant_answers'] = True
        request['sections']['assessor_scores_comments'] = True

        rc = ReportCompiler(s, request)
        ad = ApplicantData(rc)

        assert rc.content['applicant_answers'] == [
            {
                'text': 'APPLICANT ANSWERS',
                'style': 'subheader'
            },
            '\n',
            {
                'text': [
                    "The applicant responses for the questions in Stage Gate 0 - 1 are provided in the ",
                    {
                        'text': 'Assessor Mode',
                        'style': 'bold'
                    },
                    " section, together with the assessor scores and comments."
                ],
                'alignment': 'justify'
            }
        ]


def test_question_formatter_applicant_qualitative():

    q = {
        "applicant_results": {
          "absolute_distance": None,
          "id": 20,
          "justification": None,
          "percent_distance": None,
          "response": None,
          "result": None,
          "threshold_passed": None
        },
        "description": "Please provide evidence that: <br>  - Key personnel have been identified.",
        "further_details": None,
        "id": 20,
        "name": "Project team",
        "number": 20,
        "question_category": 12,
        "question_metric": None,
        "rubric": 1,
        "scoring_criteria": [],
        "weighting": 6.0
    }

    # Qualitative with response, no scoring criteria
    test_response = 'this is a qualitative question response example'
    q['applicant_results']['response'] = test_response
    q_format = QuestionFormatter(q).format_applicant_response()
    assert q_format == [
        "Project team",
        {
            'ul': [
                "Please provide evidence that: \n  - Key personnel have been identified.",
                "Weighting (%): 6.0",
            ],
            'type': 'circle',
            'alignment': 'justify'
        },
        '\n',
        {
            'style': 'tableExample',
            'table': {
                'body': [
                    [
                        {
                            'text': [
                                {'text': "Applicant's response: \n\n", 'bold': True},
                                test_response
                            ]
                        }
                    ],
                ],
                'widths': ['*']
            },
        },
        '\n'
    ]

    # Qualitative with response and scoring criteria
    q['scoring_criteria'] = [{
        "description": "Project team members, including project partners and sub-contractors, are suitably"
                       " qualified and experienced to deliver the project.",
        "id": 21,
        "name": None,
        "question": 20
    }]
    q_format = QuestionFormatter(q).format_applicant_response()
    assert q_format == [
        "Project team",
        {
            'ul': [
                "Please provide evidence that: \n  - Key personnel have been identified.",
                "Weighting (%): 6.0",
                [
                    'Scoring criteria:',
                    {
                        'ul': [
                            "Project team members, including project partners and sub-contractors, are suitably"
                            " qualified and experienced to deliver the project."
                        ],
                        'type': 'square'
                    }
                ]
            ],
            'type': 'circle',
            'alignment': 'justify'
        },
        '\n',
        {
            'style': 'tableExample',
            'table': {
                'body': [
                    [
                        {
                            'text': [
                                {'text': "Applicant's response: \n\n", 'bold': True},
                                test_response
                            ]
                        }
                    ],
                ],
                'widths': ['*']
            },
        },
        '\n'
    ]


def test_question_formatter_applicant_quantitative():
        q = {
            "applicant_results": {
                "absolute_distance": None,
                "id": 20,
                "justification": None,
                "percent_distance": None,
                "response": None,
                "result": None,
                "threshold_passed": None
            },
            "description": "Please provide evidence that: <br>  - Key personnel have been identified.",
            "further_details": None,
            "id": 20,
            "name": "Project team",
            "number": 20,
            "question_category": 12,
            "question_metric": {
                "id": 1,
                "metric": {
                    "evaluation_area": {
                        "description": "The ease with which an ocean energy technology can be installed. ",
                        "id": 2,
                        "name": "Installability"
                    },
                    "id": 1,
                    "name": "Average installation duration",
                    "threshold_type": "upper",
                    "unit": "hours per kW"
                },
                "threshold": None,
                "threshold_bool": False
            },
            "rubric": 1,
            "scoring_criteria": [],
            "weighting": 6.0
        }

        # Quantitative with result and justification, no scoring criteria
        q['applicant_results']['result'] = 43.0
        q['applicant_results']['justification'] = 'example justification'
        q_format = QuestionFormatter(q).format_applicant_response()
        assert q_format == [
            "Project team",
            {
                'ul': [
                    "Please provide evidence that: \n  - Key personnel have been identified.",
                    "Weighting (%): 6.0"
                ],
                'type': 'circle',
                'alignment': 'justify'
            },
            '\n',
            {
                'style': 'tableExample',
                'table': {
                    'body': [
                        [
                            {
                                'text': [
                                    {'text': f"Applicant's metric result (hours per kW): ", 'bold': True},
                                    "43.0\n\n",
                                    {'text': "Applicant's justification: ", 'bold': True},
                                    "example justification"
                                ]
                            }
                        ],
                    ],
                    'widths': ['*']
                },
            },
            '\n'
        ]


def test_format_applicant_responses(app):
    with app.app_context():
        init_stage_gate_study()
        s, request = mark_applicant_as_complete(0)
        request['sections']['applicant_summary'] = False
        request['sections']['applicant_answers'] = True

        rc = ReportCompiler(s, request)
        ad = ApplicantData(rc)

        app_response = rc.content['applicant_answers']

        assert app_response[0] == {
            'text': 'APPLICANT ANSWERS',
            'style': 'subheader'
        }
        assert app_response[2] == {
            'text': 'The applicant responses for the questions in Stage Gate 0 - 1 are shown below, '
                    'ordered by question category.',
            'alignment': 'justify'
        }

        assert app_response[4] == {'text': 'Technology', 'style': 'subsubheader'}
        assert app_response[7]['ul'][0] == [
            'Scientific credibility',
            {
                'ul': [
                    'Please explain, through a concise technical description relating to the proposed concept: '
                    '\n - The underlying physical and scientific principles of energy conversion have been '
                    'identified and are understood,  \n - The basic hydrodynamic philosophies and properties,  '
                    '\n - The expected interaction with the wave/tidal resource, \n - The proposed operational '
                    'characteristics.',
                    'Weighting (%): 6.0',
                    [
                        'Scoring criteria:',
                        {
                            'ul':
                                [
                                    'Concept operation and performance is demonstrated to be in alignment with '
                                    'scientific and hydrodynamic principles.',
                                    'The principles of operation are insightful and are driven by a '
                                    'good understanding of the underlying physics. '
                                ],
                            'type': 'square'
                        }
                    ]
                ],
                'type': 'circle',
                'alignment': 'justify'
            },
            '\n',
            {
                'style': 'tableExample',
                'table': {
                    'body': [
                        [
                            {'text': [{'text': "Applicant's response: \n\n", 'bold': True}, None]}
                        ]
                    ],
                    'widths': ['*']
                }
            },
            '\n'
        ]


def test_question_formatter_assessor_qualitative():
    
    q = {
        "applicant_results": {
          "absolute_distance": None, 
          "id": 23, 
          "justification": None, 
          "percent_distance": None, 
          "response": "A test response.",
          "result": None, 
          "threshold_passed": None
        }, 
        "description": "Describe the technological innovation being implemented, how it will improve best-in-class.", 
        "further_details": None, 
        "id": 23, 
        "name": "Degree of novelty and innovation (I)", 
        "number": 23, 
        "question_category": 13, 
        "question_metric": None, 
        "rubric": 1, 
        "score": {
          "id": 23, 
          "score": 3.0
        }, 
        "scoring_criteria": [],
        "weighting": 6.0
    }

    # Qualitative with response, no scoring criteria
    q_format = QuestionFormatter(q).format_assessor_response()
    assert q_format == [
        "Degree of novelty and innovation (I)",
        {
            'ul': [
                "Describe the technological innovation being implemented,"
                " how it will improve best-in-class.",
                "Weighting (%): 6.0",
            ],
            'type': 'circle',
            'alignment': 'justify'
        },
        '\n',
        {
            'style': 'tableExample',
            'table': {
                'body': [
                    [
                        {
                            'text': [
                                {'text': "Applicant's response: \n\n", 'bold': True},
                                "A test response."
                            ]
                        }
                    ],
                ],
                'widths': ['*']
            },
        },
        '\n',
        []
    ]

    # Qualitative with response, scoring criteria and assessor comments
    q['scoring_criteria'] = [
        {
          "comment": {
            "comment": "This is a test comment to make sure that it works.",
            "id": 23
          },
          "description": "The novelty and innovation of the technology is based on sound scientific, technical "
                         "and engineering principles and remains likely to improve best-in-class performance.",
          "id": 23,
          "name": None,
          "question": 23
        },
        {
          "comment": {
            "comment": "A second test comment.",
            "id": 24
          },
          "description": "Identification of any dependencies on wider technical breakthroughs, and the "
                         "likelihood of this being successful.",
          "id": 24,
          "name": None,
          "question": 23
        }
    ]
    q_format = QuestionFormatter(q).format_assessor_response()
    assert q_format == [
        'Degree of novelty and innovation (I)',
        {
            'ul':
                [
                    'Describe the technological innovation being implemented, how it will improve best-in-class.',
                    'Weighting (%): 6.0'
                ],
            'type': 'circle',
            'alignment': 'justify'
        },
        '\n',
        {
            'style': 'tableExample',
            'table': {
                'body': [
                    [
                        {
                            'text': [
                                {'text': "Applicant's response: \n\n", 'bold': True},
                                'A test response.'
                            ]
                        }
                    ]
                ],
                'widths': ['*']
            }
        },
        '\n',
        {
            'style': 'tableExample',
            'table': {
                'body': [
                    [
                        [
                            {
                                'text': [
                                    {'text': "Assessor's score: ", 'bold': True},
                                    '3.0\n\n',
                                    {'text': "Assessor's comments:\n\n", 'bold': True}
                                ]
                            },
                            {
                                'ol':
                                    [
                                        [
                                            {
                                                'text': 'The novelty and innovation of the technology is based on '
                                                        'sound scientific, technical and engineering principles and'
                                                        ' remains likely to improve best-in-class performance.\n\n',
                                                'italics': True
                                            },
                                            {'text': 'Comment:', 'decoration': 'underline'},
                                            ' This is a test comment to make sure that it works.\n\n'
                                        ],
                                        [
                                            {
                                                'text': 'Identification of any dependencies on wider technical '
                                                        'breakthroughs, and the likelihood of this being '
                                                        'successful.\n\n',
                                                'italics': True
                                            },
                                            {'text': 'Comment:', 'decoration': 'underline'},
                                            ' A second test comment.\n\n'
                                        ]
                                    ]
                            }
                        ]
                    ]
                ]
                ,
                'widths': ['*']
            }
        }
    ]


def test_question_formatter_assessor_quantitative():
        q = {
            "applicant_results": {
                "absolute_distance": None,
                "id": 20,
                "justification": None,
                "percent_distance": None,
                "response": None,
                "result": None,
                "threshold_passed": None
            },
            "description": "Please provide evidence that: <br>  - Key personnel have been identified.",
            "further_details": None,
            "id": 20,
            "name": "Project team",
            "number": 20,
            "question_category": 12,
            "question_metric": {
                "id": 1,
                "metric": {
                    "evaluation_area": {
                        "description": "The ease with which an ocean energy technology can be installed. ",
                        "id": 2,
                        "name": "Installability"
                    },
                    "id": 1,
                    "name": "Average installation duration",
                    "threshold_type": "upper",
                    "unit": "hours per kW"
                },
                "threshold": None,
                "threshold_bool": False
            },
            "rubric": 1,
            "score": {
              "id": 23,
              "score": 3.0
            },
            "scoring_criteria": [],
            "weighting": 6.0
        }

        # Quantitative with result and justification, no scoring criteria
        q['applicant_results']['result'] = 43.0
        q['applicant_results']['justification'] = 'example justification'
        q_format = QuestionFormatter(q).format_assessor_response()
        assert q_format == [
            'Project team',
            {
                'ul': [
                    'Please provide evidence that: \n  - Key personnel have been identified.',
                    'Weighting (%): 6.0'
                ],
                'type': 'circle',
                'alignment': 'justify'
            },
            '\n',
            {
                'style': 'tableExample',
                'table': {
                    'body': [
                        [
                            {
                                'text': [
                                    {'text': "Applicant's metric result (hours per kW): ", 'bold': True},
                                    '43.0\n\n',
                                    {'text': "Applicant's justification: ", 'bold': True},
                                    'example justification'
                                ]
                            }
                        ]
                    ],
                    'widths': ['*']
                }
            },
            '\n',
            []
        ]


def mark_assessor_as_complete(sg_idx=0):
    s = StageGateStudy.query.get(1)
    s.stage_gate_assessment[sg_idx].assessor_complete = 1
    assessor_scores_list = AssessorScore.query.filter_by(stage_gate_assessment_id=sg_idx+1).all()
    for a_s in assessor_scores_list:
        a_s.score = 2.0
    db.session.commit()
    request = copy.deepcopy(REPORT_REQUEST_BODY)
    request['sections']['assessor_overall_scores'] = True
    request['stage_gate_index'] = sg_idx

    return s, request


def test_assessor_format_summary_bar_chart(app):
    with app.app_context():
        init_stage_gate_study()
        s, request = mark_assessor_as_complete()
        rc = ReportCompiler(s, request)
        ad = AssessorData(rc)

        assert rc.content['assessor_overall_scores'] == {
            'data': [
                {
                    'x': [2.0, 2.0],
                    'y': ['Average', 'Weighted average'],
                    'orientation': 'h',
                    'type': 'bar',
                    'name': 'Stage Gate score'
                }
            ],
            'layout': {
                'title': {
                    'text': '',
                    'x': 0.5,
                    'xanchor': 'center'
                },
                'xaxis': {
                    'title': 'Assessor score (5-point scale)',
                    'tickmode': 'array',
                    'tickvals': [0, 1, 2, 3, 4, 5],
                    'ticktext': ['Unacceptable', 'Poor', 'Acceptable', 'Good', 'Excellent'],
                    'range': [-0.2, 4.2]
                },
                'template': 'none',
                'yaxis': {'automargin': True},
                'margin': {'pad': 6},
                'autosize': False,
                'width': 500,
                'height': 300,
                'font': {'color': 'black'}
            }
        }


def test_assessor_format_question_cat_bar_charts(app):
    with app.app_context():
        init_stage_gate_study()
        s, request = mark_assessor_as_complete()
        request['sections']['assessor_overall_scores'] = False
        request['sections']['assessor_question_categories'] = True
        rc = ReportCompiler(s, request)
        ad = AssessorData(rc)

        assert rc.content['assessor_question_categories'] == {
            'data': [
                {
                    'x': [2.0, 2.0, 2.0, 2.0, 2.0],
                    'y': ['Technology', 'Project', 'Innovation', 'Evaluation Area considerations', 'Cost of Energy'],
                    'orientation': 'h',
                    'type': 'bar',
                    'name': 'Weighted average'
                },
                {
                    'x': [2.0, 2.0, 2.0, 2.0, 2.0],
                    'y': ['Technology', 'Project', 'Innovation', 'Evaluation Area considerations', 'Cost of Energy'],
                    'orientation': 'h',
                    'type': 'bar',
                    'name': 'Average'
                }
            ],
            'layout':
                {
                    'title': {'text': '', 'x': 0.5, 'xanchor': 'center'},
                    'xaxis': {
                        'title': 'Assessor score (5-point scale)',
                        'tickmode': 'array',
                        'tickvals': [0, 1, 2, 3, 4, 5],
                        'ticktext': ['Unacceptable', 'Poor', 'Acceptable', 'Good', 'Excellent'],
                        'range': [-0.2, 4.2]
                    },
                    'template': 'none',
                    'yaxis': {'automargin': True},
                    'margin': {'pad': 6},
                    'autosize': False,
                    'width': 500,
                    'height': 500,
                    'font': {'color': 'black'}
                }
        }


def test_format_assessor_responses(app):
    with app.app_context():
        init_stage_gate_study()
        s, request = mark_assessor_as_complete()
        request['sections']['assessor_overall_scores'] = False
        request['sections']['assessor_scores_comments'] = True

        rc = ReportCompiler(s, request)
        ad = AssessorData(rc)

        scores = rc.content['assessor_scores_comments']
        assert scores[0] == {'pageBreak': 'before', 'text': 'ASSESSOR SCORES AND COMMENTS', 'style': 'subheader'}
        assert scores[1] == '\n'
        assert scores[2] == {
            'text': 'The assessor scores and comments for each of the applicant answers in Stage Gate 0 - 1 are '
                    'shown below, ordered by question category.',
            'alignment': 'justify'}
        assert scores[4] == {'text': 'Technology', 'style': 'subsubheader'}
        assert scores[5] == 'In this section, Applicants should address the following:\n - What is the current' \
                            ' baseline layout of the WEC/TEC?\n - What challenges and technical risks exist prior' \
                            ' to commercial adoption?\n - What mitigations are required in order to overcome these ' \
                            'challenges?\n - What activities have already been completed to reduce the technical risks?'
        assert scores[7] == {
            'ul':
                [
                    [
                        'Scientific credibility',
                        {
                            'ul': [
                                'Please explain, through a concise technical description relating to the proposed '
                                'concept: \n - The underlying physical and scientific principles of energy conversion '
                                'have been identified and are understood,  \n - The basic hydrodynamic philosophies'
                                ' and properties,  \n - The expected interaction with the wave/tidal resource, '
                                '\n - The proposed operational characteristics.',
                                'Weighting (%): 6.0'
                            ],
                            'type': 'circle',
                            'alignment': 'justify'
                        },
                        '\n',
                        {
                             'style': 'tableExample',
                             'table': {
                                 'body': [[{'text': [{'text': "Applicant's response: \n\n", 'bold': True}, None]}]],
                                 'widths': ['*']
                             }
                         },
                        '\n',
                        {
                            'style': 'tableExample',
                            'table': {'body': [[[{'text': [
                                {'text': "Assessor's score: ", 'bold': True},
                                '2.0\n\n',
                                {'text': "Assessor's comments:\n\n", 'bold': True}
                            ]}, {'ol': [[{
                                'text': 'Concept operation and performance is demonstrated to be in alignment with '
                                        'scientific and hydrodynamic principles.\n\n',
                                'italics': True
                            },
                                {
                                    'text': 'Comment:',
                                    'decoration': 'underline'
                                },
                                ' None\n\n'
                            ],
                                [
                                    {
                                        'text': 'The principles of operation are insightful and are driven by a good '
                                                'understanding of the underlying physics. \n\n',
                                        'italics': True
                                    },
                                    {
                                        'text': 'Comment:',
                                        'decoration': 'underline'
                                    },
                                    ' None\n\n'
                                ]
                            ]}]]],
                                'widths': ['*']}}],
                    ['Technical credibility', {'ul': [
                        'Please provide a qualitative description, supplemented where appropriate by outcomes of'
                        ' analysis and/or simulations, which details the following: \n - '
                        'How the concept is proposed to be designed or controlled so as to perform efficiently in '
                        'the operational wave/tidal conditions,  \n - If, and how, the concept is proposed to regulate'
                        ' power and shed load in more extreme conditions, \n - The factors which are likely to'
                        ' influence performance, response and loading, and how these will be explored, '
                        '\n - How the primary concept might interface with other elements of the wider system, '
                        '\n - The approach to early conceptual development, including background to the identification '
                        'of this particular concept and the critical techniques applied (e.g. such as design '
                        'inversion and lateral thought), \n - Any concept evaluation testing and/or proof of concept '
                        'testing has been performed. ',
                        'Weighting (%): 9.0'
                    ],
                        'type': 'circle',
                        'alignment': 'justify'},
                     '\n',
                     {
                         'style': 'tableExample',
                         'table': {'body': [[{
                             'text': [
                                 {
                                     'text': "Applicant's response: \n\n",
                                     'bold': True},
                                 None]}]],
                             'widths': [
                                 '*']}},
                     '\n',
                     {'style': 'tableExample', 'table': {'body': [[[{'text': [
                        {'text': "Assessor's score: ", 'bold': True},
                        '2.0\n\n',
                        {'text': "Assessor's comments:\n\n", 'bold': True}]},
                        {'ol': [[{
                            'text': 'The concept addresses the need for efficient performance in the design wave/tidal '
                                    'conditions.\n\n',
                            'italics': True},
                            {'text': 'Comment:',
                             'decoration': 'underline'},
                            ' None\n\n'], [{
                            'text': 'The concept addresses the need to regulate power and shed load in more extreme '
                                    'conditions.\n\n',
                            'italics': True},
                            {'text': 'Comment:',
                             'decoration': 'underline'},
                            ' None\n\n']]}]]],
                         'widths': ['*']}}],
                    ['Engineering credibility',
                     {'ul': [
                         "To demonstrate there is credibility in the proposed design solutions, please provide: "
                         "\n - An outline describing the main engineering challenges and options with the proposed "
                         "technical concept,  \n - Evidence of a systems engineering approach that led to the proposed"
                         " solution, \n - A qualitative assessment of the concept's physical and "
                         "functional characteristics, \n - Evidence supporting the use of any novel materials and "
                         "processes, and their potential impact on manufacture, installation and operation of the "
                         "overall system, \n - A set of aspirational targets, justified using the above qualitative "
                         "considerations, for the concept's future achievement in terms of the evaluation areas of "
                         "survivability, reliability, energy capture, energy conversion, cost, manufacturability,"
                         " and installability.",
                         'Weighting (%): 9.0'],
                         'type': 'circle',
                         'alignment': 'justify'},
                     '\n',
                     {
                         'style': 'tableExample',
                         'table': {'body': [[{'text': [{
                             'text': "Applicant's response: \n\n",
                             'bold': True},
                             None]}]],
                             'widths': ['*']}},
                     '\n', {'style': 'tableExample',
                            'table': {'body': [[[{'text': [{
                                'text': "Assessor's score: ",
                                'bold': True},
                                '2.0\n\n',
                                {
                                    'text': "Assessor's comments:\n\n",
                                    'bold': True}]},
                                {'ol': [[{
                                    'text': 'The concept has the potential to survive in an extreme ocean'
                                            ' environment\n\n',
                                    'italics': True},
                                    {
                                        'text': 'Comment:',
                                        'decoration': 'underline'},
                                    ' None\n\n'],
                                    [{
                                        'text': 'The concept has the potential to be reliable\n\n',
                                        'italics': True},
                                        {
                                            'text': 'Comment:',
                                            'decoration': 'underline'},
                                        ' None\n\n'],
                                    [{
                                        'text': 'The concept offers good prospects of being engineered using known or '
                                                'emerging techniques and materials. \n\n',
                                        'italics': True},
                                        {
                                            'text': 'Comment:',
                                            'decoration': 'underline'},
                                        ' None\n\n'],
                                    [{
                                        'text': 'There is a credible narrative surrounding installation, operation '
                                                'and maintenance.\n\n',
                                        'italics': True},
                                        {
                                            'text': 'Comment:',
                                            'decoration': 'underline'},
                                        ' None\n\n'],
                                    [{
                                        'text': 'Credible targets are provided for aspirational performance in key '
                                                'evaluation areas\n\n',
                                        'italics': True},
                                        {
                                            'text': 'Comment:',
                                            'decoration': 'underline'},
                                        ' None\n\n']]}]]],
                                'widths': ['*']
                            }
                            }
                     ]
                ]
        }


def check_ia(s, request):
    rc = ReportCompiler(s, request)
    ia = ImprovementAreaData(rc)

    ia = rc.content['improvement_areas']
    print(ia)
    assert ia == [
        {'pageBreak': 'before', 'text': 'IMPROVEMENT AREAS', 'style': 'header'},
        '\n',
        {'text': [
            'Below is the list of improvement areas that have been identified for this Stage Gate study. '
            'The improvement areas are those evaluation areas that have been highlighted as weaknesses of the '
            'technology or device. Improvement areas are defined by the ',
            {'text': 'average improvement area score', 'italics': True},
            ', which is the average of the ',
            {'text': 'checklist', 'italics': True},
            ', ',
            {'text': 'metric', 'italics': True},
            ' and ',
            { 'text': 'weighted average assessor', 'italics': True},
            ' scores.\n\n'
        ],
            'alignment': 'justify'
        },
        {
            'ul': [
                'The checklist score is the result of the Activity Checklist for each evaluation area.',
                'The metric score is calculated for each evaluation area as the number of metrics that have passed '
                'their metric thresholds, divided by the number of metrics for that evaluation area that have '
                'thresholds applied, and expressed as a percentage.',
                'The assessor score is calculated as the weighted average assessor score for each evaluation area, '
                'expressed as a percentage with respect to the maximum possible score (4)'
            ], 'alignment': 'justify'
        },
        '\n',
        {
            'text': 'The average improvement area score is based only on those scores that are available. '
                    'For instance, in the case where the metric and assessor scores are not applicable (e.g. '
                    'Applicant Mode and Assessor Mode have not been completed), then the average improvement area '
                    'score is just equal to the checklist score for each evaluation area. If only one column is '
                    'unavailable, the average improvement area score is the average of the two remaining columns. '
                    '\n\n The table of results is sorted by the average improvement area score in ascending order. '
                    'The threshold level for what is considered an improvement area has been set as 50%.\n\n',
            'alignment': 'justify'
        },
        {
            'style': 'tableExample',
            'table': {
                'headerRows': 1,
                'widths':
                    ['auto', '*', '*', '*', '*'],
                'body': [[{'fillColor': '#022c5f', 'text': 'Evaluation Area', 'style': 'tableHeader'},
                          {'fillColor': '#022c5f', 'text': 'Checklist score (%)', 'style': 'tableHeader'},
                          {'fillColor': '#022c5f', 'text': 'Metric score (%)', 'style': 'tableHeader'},
                          {'fillColor': '#022c5f', 'text': 'Weighted average assessor score (%)',
                           'style': 'tableHeader'},
                          {'fillColor': '#022c5f', 'text': 'Average improvement area score (%)',
                           'style': 'tableHeader'}
                          ],
                         ['Acceptability', 0, 'N/A', 'N/A', 0.0],
                         ['Installability', 0, 'N/A', 'N/A', 0.0],
                         ['Maintainability', 0, 'N/A', 'N/A', 0.0],
                         ['Reliability', 0, 'N/A', 'N/A', 0.0],
                         ['Survivability', 0, 'N/A', 'N/A', 0.0],
                         ['Affordability', 0, 'N/A', 'N/A', 0.0],
                         ['Power Capture', 0, 'N/A' , 'N/A', 0.0],
                         ['Power Conversion', 0, 'N/A', 'N/A', 0.0],
                         ['Controllability', 0, 'N/A', 'N/A', 0.0]
                         ]
            }
        }
    ]


def test_improvement_area_data(app):
    with app.app_context():
        init_stage_gate_study()

        s, request = mark_checklist_as_complete()
        request['sections']['improvement_areas'] = True

        check_ia(s, request)


def test_improvement_area_no_threshold(app):
    with app.app_context():
        init_stage_gate_study()

        s, request = mark_checklist_as_complete()
        request['sections']['improvement_areas'] = True
        request['ia_threshold'] = None

        check_ia(s, request)


def test_report_compiler_section_validation(app):
    with app.app_context():
        init_stage_gate_study()

        s = StageGateStudy.query.get(1)
        s.checklist_study_complete = True
        request = copy.deepcopy(REPORT_REQUEST_BODY)
        rc = ReportCompiler(s, request)
        assert rc.study_data is None
        assert rc.errors is None
        assert rc.stage_index is None
        assert rc.stage_gate_index is None
        assert rc.selected_stage == 'N/A'
        assert rc.selected_stage_gate == 'N/A'

        request['sections']['checklist_stage_results'] = True
        rc = ReportCompiler(s, request)
        assert rc.errors == 'Stage index required but was not provided.'

        request['sections']['applicant_summary'] = True
        rc = ReportCompiler(s, request)
        assert rc.errors == 'Stage index required but was not provided. Stage Gate index required but was not provided.'

        # request = copy.deepcopy(REPORT_REQUEST_BODY)
        # request['sections']['improvement_areas'] = True
        # rc = ReportCompiler(s, request)
        # assert rc.errors == 'Stage index required but was not provided.'

        request = copy.deepcopy(REPORT_REQUEST_BODY)
        request['sections']['assessor_scores_comments'] = True
        rc = ReportCompiler(s, request)
        assert rc.errors == 'Stage Gate index required but was not provided.'


def test_report_compiler_checklist_complete_validation(app):
    with app.app_context():
        init_stage_gate_study()

        s = StageGateStudy.query.get(1)
        request = copy.deepcopy(REPORT_REQUEST_BODY)
        request['sections']['checklist_summary'] = True
        request['stage_index'] = 1
        rc = ReportCompiler(s, request)
        assert rc.errors == 'The activity checklist for the requested study has not been completed.'

        s.checklist_study_complete = True
        rc = ReportCompiler(s, request)
        assert rc.errors is None


def test_report_compiler_sg_assessment_complete_validation(app):
    with app.app_context():
        init_stage_gate_study()

        s = StageGateStudy.query.get(1)
        request = copy.deepcopy(REPORT_REQUEST_BODY)
        request['sections']['applicant_summary'] = True
        request['stage_gate_index'] = 0
        rc = ReportCompiler(s, request)
        assert rc.errors == 'The Applicant Mode for the requested Stage Gate Assessment has not been completed.'

        s.stage_gate_assessment[0].applicant_complete = True
        rc = ReportCompiler(s, request)
        assert rc.errors is None

        request = copy.deepcopy(REPORT_REQUEST_BODY)
        request['sections']['assessor_question_categories'] = True
        request['stage_gate_index'] = 0
        rc = ReportCompiler(s, request)
        assert rc.errors == 'The Assessor Mode for the requested Stage Gate Assessment has not been completed.'

        s.stage_gate_assessment[0].assessor_complete = True
        rc = ReportCompiler(s, request)
        assert rc.errors is None


def test_report_compiler_no_content(app):
    with app.app_context():
        init_stage_gate_study()
        s = StageGateStudy.query.get(1)
        request = copy.deepcopy(REPORT_REQUEST_BODY)

        rc = ReportCompiler(s, request)
        rc.construct()
        d = rc.content
        assert d == {
            'study_params': {
                'study_name': 'test study 1',
                'date': datetime.datetime.now().strftime("%d/%m/%Y"),
                'selected_stage': 'N/A',
                'selected_stage_gate': 'N/A',
                'description': 'description for test stage gate study 1',
                'threshold_settings': 'Default (no metric thresholds)',
                'stage_index': None
            },
            'section_request': {
                'checklist_summary': False,
                'checklist_stage_results': False,
                'checklist_outstanding_activities': False,
                'applicant_summary': False,
                'metric_results': False,
                'applicant_answers': False,
                'assessor_overall_scores': False,
                'assessor_question_categories': False,
                'assessor_scores_comments': False,
                'improvement_areas': False
            },
            'table_of_contents': {
                'alignment': 'justify',
                'ul': ['Introduction', 'Study details']
            }
        }


def test_report_compiler_checklist(app):
    with app.app_context():
        init_stage_gate_study()
        s = StageGateStudy.query.get(1)
        request = copy.deepcopy(REPORT_REQUEST_BODY)

        request['sections']['checklist_summary'] = True
        request['stage_index'] = 0
        rc = ReportCompiler(s, request)
        rc.construct()
        d = rc.content
        assert 'checklist_summary' in d.keys()
        assert rc.selected_stage == "Stage 0"


def test_report_compiler_applicant(app):
    with app.app_context():
        init_stage_gate_study()
        s = StageGateStudy.query.get(1)
        request = copy.deepcopy(REPORT_REQUEST_BODY)

        request['sections']['applicant_summary'] = True
        request['stage_gate_index'] = 0
        rc = ReportCompiler(s, request)
        rc.construct()
        d = rc.content
        assert 'applicant_summary' in d.keys()


def test_report_compiler_assessor(app):
    with app.app_context():
        init_stage_gate_study()
        s = StageGateStudy.query.get(1)
        request = copy.deepcopy(REPORT_REQUEST_BODY)

        s, request = mark_assessor_as_complete()
        rc = ReportCompiler(s, request)
        rc.construct()
        d = rc.content

        assert 'assessor_overall_scores' in d.keys()


def test_report_compiler_improv_areas(app):
    with app.app_context():
        init_stage_gate_study()
        s = StageGateStudy.query.get(1)
        request = copy.deepcopy(REPORT_REQUEST_BODY)

        request['sections']['improvement_areas'] = True
        rc = ReportCompiler(s, request)
        rc.construct()
        d = rc.content

        assert 'improvement_areas' in d.keys()
