from dtop_stagegate.business.complexity_level import ComplexityLevelMap
from dtop_stagegate.business.models import StageGateStudy
from dtop_stagegate.service.api.provider_states import set_complete_status_for_stage
from test.business.test_sg_studies_business import init_stage_gate_study
from dtop_stagegate.service import db


def test_get_complexity_levels(app):
    with app.app_context():
        init_stage_gate_study()

        study = StageGateStudy.query.get('1')

        clm = ComplexityLevelMap(study)
        assert not clm.checklist_complete

        study.checklist_study_complete = True
        db.session.commit()

        clm = ComplexityLevelMap(study)
        assert clm.checklist_complete
        cpx_levels = clm.get_complexity_levels()
        assert cpx_levels['message'] == 'The technology/device has not completed all of the' \
                                        ' activities in any of the stages'

        set_complete_status_for_stage('3')
        clm = ComplexityLevelMap(study)
        assert clm.checklist_complete
        cpx_levels = clm.get_complexity_levels()
        assert cpx_levels['message'] == 'The technology/device has not completed all of the' \
                                        ' activities in any of the stages'

        set_complete_status_for_stage('1')
        clm = ComplexityLevelMap(study)
        assert clm.checklist_complete
        cpx_levels = clm.get_complexity_levels()
        assert cpx_levels == {
                    'stage': 0,
                    'stage_gate': 'Stage Gate 0 - 1',
                    'complexity_levels': {
                        "site_characterisation": 1,
                        "machine_characterisation": 1,
                        "energy_capture": 1,
                        "energy_transformation": 1,
                        "energy_delivery": 1,
                        "station_keeping": 1,
                        "logistics_marine_ops": 1,
                        "system_performance_energy_yield": 1,
                        "rams": 1,
                        "system_lifetime_cost": 1,
                        "environmental_social_acceptance": 1
                    }
                }

        set_complete_status_for_stage('2')
        set_complete_status_for_stage('4')
        set_complete_status_for_stage('5')
        set_complete_status_for_stage('6')
        clm = ComplexityLevelMap(study)
        assert clm.checklist_complete
        cpx_levels = clm.get_complexity_levels()
        assert cpx_levels == {
                    'stage': 5,
                    'stage_gate': 'NA',
                    'complexity_levels': {
                        "site_characterisation": 3,
                        "machine_characterisation": 3,
                        "energy_capture": 3,
                        "energy_transformation": 3,
                        "energy_delivery": 3,
                        "station_keeping": 3,
                        "logistics_marine_ops": 3,
                        "system_performance_energy_yield": 1,
                        "rams": 3,
                        "system_lifetime_cost": 3,
                        "environmental_social_acceptance": 3
                    }
                }
