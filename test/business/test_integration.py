from copy import deepcopy
import requests

import pytest

from dtop_stagegate.business.integration import extract_entity_id, MetricExtractor, get_url_and_headers, \
    call_intermodule_service, calculate_min_uls, update_rams_metric, extract_rams_data
from dtop_stagegate.business.models import ApplicantAnswer, Question, QuestionMetric
from dtop_stagegate.service import db
from test.business.test_sg_studies_business import init_stage_gate_study

ENTITY_LIST_NO_D_AND_A = [
    {
        "projectId": 1,
        "studyId": 1,
        "moduleId": 12,
        "entityId": 1,
    },
    {
        "projectId": 1,
        "studyId": 1,
        "moduleId": 13,
        "entityId": 1
    }
]
EXPECTED_METRIC_STATUS_SG1 = {
    'metric_status':
        {
            'esa': [
                {'message': "Metric not required for this Stage Gate", 'metric_id': 21, 'status': "Warning"},
                {'message': "Metric not required for this Stage Gate", 'metric_id': 22, 'status': "Warning"},
                {'message': "Metric not required for this Stage Gate", 'metric_id': 23, 'status': "Warning"},
                {'message': "Metric not required for this Stage Gate", 'metric_id': 25, 'status': "Warning"},
                {'message': "Metric not required for this Stage Gate", 'metric_id': 26, 'status': "Warning"},
                {'message': "Metric not required for this Stage Gate", 'metric_id': 24, 'status': "Warning"},
                {'message': "Metric not required for this Stage Gate", 'metric_id': 27, 'status': "Warning"}
            ],
            'lmo': [
                {'message': "Metric not required for this Stage Gate", 'metric_id': 8, 'status': "Warning"},
                {'message': "Metric not required for this Stage Gate", 'metric_id': 7, 'status': "Warning"},
                {'message': "Metric not required for this Stage Gate", 'metric_id': 13, 'status': "Warning"}
            ],
            'rams': [
                {'message': "Metric not required for this Stage Gate", 'metric_id': 9, 'status': "Warning"},
                {'message': "Metric not required for this Stage Gate", 'metric_id': 10, 'status': "Warning"},
                {'message': "Metric not required for this Stage Gate", 'metric_id': 11, 'status': "Warning"},
                {'message': "Metric not required for this Stage Gate", 'metric_id': 12, 'status': "Warning"},
                {'message': "Metric not required for this Stage Gate", 'metric_id': 14, 'status': "Warning"}
            ],
            'slc': [
                {'message': "Metric not required for this Stage Gate", 'metric_id': 16, 'status': "Warning"},
                {'message': "Metric not required for this Stage Gate", 'metric_id': 20, 'status': "Warning"},
                {'message': "Metric not required for this Stage Gate", 'metric_id': 15, 'status': "Warning"},
                {'message': "Metric not required for this Stage Gate", 'metric_id': 17, 'status': "Warning"},
                {'message': "Metric not required for this Stage Gate", 'metric_id': 18, 'status': "Warning"},
                {'message': "Metric not required for this Stage Gate", 'metric_id': 19, 'status': "Warning"}
            ],
            'spey': [
                {'message': "Metric not required for this Stage Gate", 'metric_id': 1, 'status': "Warning"},
                {'message': "Metric not required for this Stage Gate", 'metric_id': 2, 'status': "Warning"},
                {'message': "Metric not required for this Stage Gate", 'metric_id': 3, 'status': "Warning"},
                {'message': "Metric not required for this Stage Gate", 'metric_id': 4, 'status': "Warning"},
                {'message': "Metric not required for this Stage Gate", 'metric_id': 5, 'status': "Warning"},
                {'message': "Metric not required for this Stage Gate", 'metric_id': 6, 'status': "Warning"}
            ]
        }
}
EXPECTED_METRIC_STATUS_SG2 = {
    'metric_status':
        {
            'esa': [
                {'message': "Metric updated", 'metric_id': 21, 'status': "Success"},
                {'message': "Metric updated", 'metric_id': 22, 'status': "Success"},
                {'message': "Metric not required for this Stage Gate", 'metric_id': 23, 'status': "Warning"},
                {'message': "Metric not required for this Stage Gate", 'metric_id': 25, 'status': "Warning"},
                {'message': "Metric not required for this Stage Gate", 'metric_id': 26, 'status': "Warning"},
                {'message': "Metric updated", 'metric_id': 24, 'status': "Success"},
                {'message': "Metric not required for this Stage Gate", 'metric_id': 27, 'status': "Warning"}
            ],
            'lmo': [
                {'message': "Metric updated", 'metric_id': 8, 'status': "Success"},
                {'message': "Metric updated", 'metric_id': 7, 'status': "Success"},
                {'message': "Metric updated", 'metric_id': 13, 'status': "Success"}
            ],
            'rams': [
                {'message': "Metric updated", 'metric_id': 9, 'status': "Success"},
                {'message': "Metric updated", 'metric_id': 10, 'status': "Success"},
                {'message': "Metric updated", 'metric_id': 11, 'status': "Success"},
                {'message': "Metric updated", 'metric_id': 12, 'status': "Success"},
                {'message': "Metric updated", 'metric_id': 14, 'status': "Success"},
            ],
            'slc': [
                {'message': "Metric updated", 'metric_id': 16, 'status': "Success"},
                {'message': "Metric not required for this Stage Gate", 'metric_id': 20, 'status': "Warning"},
                {'message': "Metric not required for this Stage Gate", 'metric_id': 15, 'status': "Warning"},
                {'message': "Metric updated", 'metric_id': 17, 'status': "Success"},
                {'message': "Metric updated", 'metric_id': 18, 'status': "Success"},
                {'message': "Metric updated", 'metric_id': 19, 'status': "Success"}
            ],
            'spey': [
                {'message': "Metric updated", 'metric_id': 1, 'status': "Success"},
                {'message': "Metric updated", 'metric_id': 2, 'status': "Success"},
                {'message': "Metric updated", 'metric_id': 3, 'status': "Success"},
                {'message': "Metric updated", 'metric_id': 4, 'status': "Success"},
                {'message': "Metric updated", 'metric_id': 5, 'status': "Success"},
                {'message': "Metric updated", 'metric_id': 6, 'status': "Success"}
            ]
        }
}
EXPECTED_METRIC_STATUS_NO_ENTITIES = {
    'metric_status':
        {
            'esa': [
                {'message': "No ESA entity has been registered", 'metric_id': 21, 'status': "Error"},
                {'message': "No ESA entity has been registered", 'metric_id': 22, 'status': "Error"},
                {'message': "No ESA entity has been registered", 'metric_id': 23, 'status': "Error"},
                {'message': "No ESA entity has been registered", 'metric_id': 25, 'status': "Error"},
                {'message': "No ESA entity has been registered", 'metric_id': 26, 'status': "Error"},
                {'message': "No ESA entity has been registered", 'metric_id': 24, 'status': "Error"},
                {'message': "No ESA entity has been registered", 'metric_id': 27, 'status': "Error"}
            ],
            'lmo': [
                {'message': "No LMO entity has been registered", 'metric_id': 8, 'status': "Error"},
                {'message': "No LMO entity has been registered", 'metric_id': 7, 'status': "Error"},
                {'message': "No LMO entity has been registered", 'metric_id': 13, 'status': "Error"}
            ],
            'rams': [
                {'message': "No RAMS entity has been registered", 'metric_id': 9, 'status': "Error"},
                {'message': "No RAMS entity has been registered", 'metric_id': 10, 'status': "Error"},
                {'message': "No RAMS entity has been registered", 'metric_id': 11, 'status': "Error"},
                {'message': "No RAMS entity has been registered", 'metric_id': 12, 'status': "Error"},
                {'message': "No RAMS entity has been registered", 'metric_id': 14, 'status': "Error"}
            ],
            'slc': [
                {'message': "No SLC entity has been registered", 'metric_id': 16, 'status': "Error"},
                {'message': "No SLC entity has been registered", 'metric_id': 20, 'status': "Error"},
                {'message': "No SLC entity has been registered", 'metric_id': 15, 'status': "Error"},
                {'message': "No SLC entity has been registered", 'metric_id': 17, 'status': "Error"},
                {'message': "No SLC entity has been registered", 'metric_id': 18, 'status': "Error"},
                {'message': "No SLC entity has been registered", 'metric_id': 19, 'status': "Error"}
            ],
            'spey': [
                {'message': "No SPEY entity has been registered", 'metric_id': 1, 'status': "Error"},
                {'message': "No SPEY entity has been registered", 'metric_id': 2, 'status': "Error"},
                {'message': "No SPEY entity has been registered", 'metric_id': 3, 'status': "Error"},
                {'message': "No SPEY entity has been registered", 'metric_id': 4, 'status': "Error"},
                {'message': "No SPEY entity has been registered", 'metric_id': 5, 'status': "Error"},
                {'message': "No SPEY entity has been registered", 'metric_id': 6, 'status': "Error"}
            ]
        }
}
EXPECTED_METRIC_STATUS_404 = {
    'metric_status':
        {
            'esa': [
                {'message': "Failed in GET request", 'metric_id': 21, 'status': "Error"},
                {'message': "Failed in GET request", 'metric_id': 22, 'status': "Error"},
                {'message': "Failed in GET request", 'metric_id': 23, 'status': "Error"},
                {'message': "Failed in GET request", 'metric_id': 25, 'status': "Error"},
                {'message': "Failed in GET request", 'metric_id': 26, 'status': "Error"},
                {'message': "Failed in GET request", 'metric_id': 24, 'status': "Error"},
                {'message': "Failed in GET request", 'metric_id': 27, 'status': "Error"}
            ],
            'lmo': [
                {'message': "Failed in GET request", 'metric_id': 8, 'status': "Error"},
                {'message': "Failed in GET request", 'metric_id': 7, 'status': "Error"},
                {'message': "Failed in GET request", 'metric_id': 13, 'status': "Error"}
            ],
            'rams': [
                {'message': "Failed in GET request", 'metric_id': 9, 'status': "Error"},
                {'message': "Failed in GET request", 'metric_id': 10, 'status': "Error"},
                {'message': "Failed in GET request", 'metric_id': 11, 'status': "Error"},
                {'message': "Failed in GET request", 'metric_id': 12, 'status': "Error"},
                {'message': "Failed in GET request", 'metric_id': 14, 'status': "Error"}
            ],
            'slc': [
                {'message': "Failed in GET request", 'metric_id': 16, 'status': "Error"},
                {'message': "Failed in GET request", 'metric_id': 20, 'status': "Error"},
                {'message': "Failed in GET request", 'metric_id': 15, 'status': "Error"},
                {'message': "Failed in GET request", 'metric_id': 17, 'status': "Error"},
                {'message': "Failed in GET request", 'metric_id': 18, 'status': "Error"},
                {'message': "Failed in GET request", 'metric_id': 19, 'status': "Error"}
            ],
            'spey': [
                {'message': "Failed in GET request", 'metric_id': 1, 'status': "Error"},
                {'message': "Failed in GET request", 'metric_id': 2, 'status': "Error"},
                {'message': "Failed in GET request", 'metric_id': 3, 'status': "Error"},
                {'message': "Failed in GET request", 'metric_id': 4, 'status': "Error"},
                {'message': "Failed in GET request", 'metric_id': 5, 'status': "Error"},
                {'message': "Failed in GET request", 'metric_id': 6, 'status': "Error"}
            ]
        }
}
ENTITY_LIST = [
    {
        "projectId": 1,
        "studyId": 1,
        "moduleId": 1,
        "entityId": 1
    },
    {
        "projectId": 1,
        "studyId": 1,
        "moduleId": 2,
        "entityId": 1,
    },
    {
        "projectId": 1,
        "studyId": 1,
        "moduleId": 3,
        "entityId": 1,
    },
    {
        "projectId": 1,
        "studyId": 1,
        "moduleId": 4,
        "entityId": 1
    },
    {
        "projectId": 1,
        "studyId": 1,
        "moduleId": 5,
        "entityId": 1
    },
    {
        "projectId": 1,
        "studyId": 1,
        "moduleId": 6,
        "entityId": 1,
    },
    {
        "projectId": 1,
        "studyId": 1,
        "moduleId": 7,
        "entityId": 1
    },
    {
        "projectId": 1,
        "studyId": 1,
        "moduleId": 8,
        "entityId": 1
    },
    {
        "projectId": 1,
        "studyId": 1,
        "moduleId": 9,
        "entityId": 1,
    },
    {
        "projectId": 1,
        "studyId": 1,
        "moduleId": 10,
        "entityId": 1,
    },
    {
        "projectId": 1,
        "studyId": 1,
        "moduleId": 11,
        "entityId": 1,
    },
    {
        "projectId": 1,
        "studyId": 1,
        "moduleId": 12,
        "entityId": 1,
    },
    {
        "projectId": 1,
        "studyId": 1,
        "moduleId": 13,
        "entityId": 1
    }
]
ENTITY_LIST_404 = [
    {
        "projectId": 1,
        "studyId": 1,
        "moduleId": 1,
        "entityId": 43
    },
    {
        "projectId": 1,
        "studyId": 1,
        "moduleId": 2,
        "entityId": 43,
    },
    {
        "projectId": 1,
        "studyId": 1,
        "moduleId": 3,
        "entityId": 43,
    },
    {
        "projectId": 1,
        "studyId": 1,
        "moduleId": 4,
        "entityId": 43
    },
    {
        "projectId": 1,
        "studyId": 1,
        "moduleId": 5,
        "entityId": 43
    },
    {
        "projectId": 1,
        "studyId": 1,
        "moduleId": 6,
        "entityId": 43,
    },
    {
        "projectId": 1,
        "studyId": 1,
        "moduleId": 7,
        "entityId": 43
    },
    {
        "projectId": 1,
        "studyId": 1,
        "moduleId": 8,
        "entityId": 43
    },
    {
        "projectId": 1,
        "studyId": 1,
        "moduleId": 9,
        "entityId": 43,
    },
    {
        "projectId": 1,
        "studyId": 1,
        "moduleId": 10,
        "entityId": 43,
    },
    {
        "projectId": 1,
        "studyId": 1,
        "moduleId": 11,
        "entityId": 43,
    },
    {
        "projectId": 1,
        "studyId": 1,
        "moduleId": 12,
        "entityId": 1,
    },
    {
        "projectId": 1,
        "studyId": 1,
        "moduleId": 13,
        "entityId": 1
    }
]


def test_extract_entity_id():
    entity_id = extract_entity_id(ENTITY_LIST, 3)
    assert entity_id == 1


def test_extract_entity_id_edit():
    entity_list_copy = deepcopy(ENTITY_LIST)
    entity_list_copy[0]['entityId'] = 43
    entity_id = extract_entity_id(entity_list_copy, 1)
    assert entity_id == 43


def test_get_url_and_headers():
    url, headers = get_url_and_headers('lmo')
    assert url == "http://lmo.dto.test"
    assert headers == {'Authorization': ''}


def test_get_url_and_headers_workstation():
    url, headers = get_url_and_headers('fake_module')
    assert url == "http://172.17.0.1:80"
    assert headers == {
        'Authorization': '',
        'Host': 'fake_module.dto.test'
    }


def test_call_intermodule_service():
    response = call_intermodule_service('lmo', '/api/1/phases/installation/cost')
    assert response.status_code == 200


def test_call_intermodule_service_connection():
    resp = call_intermodule_service('fake-module', '/fake-uri')
    assert resp[1] == 400


def test_call_intermodule_service_http():
    resp = call_intermodule_service('lmo', '/api/43/phases/installation/cost')
    assert resp[1] == 404


def test_metric_extractor(app):
    with app.app_context():
        init_stage_gate_study()

        updated_metrics = MetricExtractor("2", ENTITY_LIST).extract()
        assert updated_metrics == EXPECTED_METRIC_STATUS_SG2
        check_rams_db()
        check_esa_db()
        check_slc_db()
        check_spey_db()
        check_lmo_db()


# def test_metric_extractor_dtop_domain(app, monkeypatch):
#     with app.app_context():
#         init_stage_gate_study()
#
#         monkeypatch.delenv('DTOP_DOMAIN')
#         with pytest.raises(KeyError) as e_info:
#             metric_extractor = MetricExtractor("2", ENTITY_LIST)


def check_metric(metric_id, expected_value):
    aa = db.session.query(ApplicantAnswer).join(Question).join(QuestionMetric).filter(
        ApplicantAnswer.stage_gate_assessment_id == '2',
        QuestionMetric.metric_id == metric_id
    ).all()
    if expected_value or expected_value is None:
        assert aa[0].result == expected_value
    else:
        assert aa == []


def check_lmo_db():
    check_metric(8, 19188188.02)
    check_metric(7, 6588.1)
    check_metric(13, 7654.3)


def check_spey_db():
    check_metric(1, 100)
    check_metric(2, 100)
    check_metric(3, 100)
    check_metric(4, 0.5)
    check_metric(5, 0.5)
    check_metric(6, 0.5)


def check_slc_db():
    check_metric(16, 5000000)
    check_metric(20, False)
    check_metric(15, False)
    check_metric(17, 5000000)
    check_metric(18, 15945.24)
    check_metric(19, 6782.4)


def check_esa_db():
    check_metric(21, 0.01648182091586897)
    check_metric(22, 0.21426071736369878)
    check_metric(23, False)
    check_metric(25, False)
    check_metric(26, False)
    check_metric(24, 434.5)
    check_metric(27, False)


def check_rams_db():
    check_metric(9, 110909.0)
    check_metric(10, 0.890)
    check_metric(11, 0.92)
    check_metric(12, 0.987)
    check_metric(14, 0.987)


def test_metric_extractor_sg1(app):
    with app.app_context():
        init_stage_gate_study()

        updated_metrics = MetricExtractor("1", ENTITY_LIST).extract()
        assert updated_metrics == EXPECTED_METRIC_STATUS_SG1


def test_metric_extractor_missing_entities(app):
    # Delete some entities from a deepcopy of entity list and make sure smaller list comes out
    with app.app_context():
        init_stage_gate_study()

        updated_metrics = MetricExtractor("2", ENTITY_LIST_NO_D_AND_A).extract()
        assert updated_metrics == EXPECTED_METRIC_STATUS_NO_ENTITIES

        # Also assert the changes in the database for the relevant module metrics
        check_database_not_updated()


def check_database_not_updated():
    check_metric(8, None)
    check_metric(7, None)
    check_metric(13, None)
    check_metric(1, None)
    check_metric(2, None)
    check_metric(3, None)
    check_metric(4, None)
    check_metric(5, None)
    check_metric(6, None)
    check_metric(16, None)
    check_metric(20, False)
    check_metric(15, False)
    check_metric(17, None)
    check_metric(18, None)
    check_metric(19, None)
    check_metric(21, None)
    check_metric(22, None)
    check_metric(23, False)
    check_metric(25, False)
    check_metric(26, False)
    check_metric(24, None)
    check_metric(27, False)


def test_metric_extractor_entity_error(app):
    with app.app_context():
        init_stage_gate_study()

        updated_metrics = MetricExtractor("2", ENTITY_LIST_404).extract()
        assert updated_metrics == EXPECTED_METRIC_STATUS_404
        check_database_not_updated()


def test_extract_rams_data_ra():
    rams_json = {
        "availability": {
            "device_id": ["Id1", "Id2", "Id3"],
            "availability_tb": [0.987, 0.976, 0.998],
            "availability_array": 0.92
        },
        "maintainability": {},
        "reliability_system": {
            "max_annual_pof_ed": 0.890,
            "max_annual_pof_et": 0.890,
            "max_annual_pof_sk": 0.890,
            "max_annual_pof_array": 0.890,
            "max_ttf_ed": 1237629.0,
            "max_ttf_et": 1237629.0,
            "max_ttf_sk": 1237629.0,
            "max_ttf_array": 1237629.0,
            "mttf_ed": 110909.0,
            "mttf_et": 110909.0,
            "mttf_sk": 110909.0,
            "mttf_array": 110909.0,
            "std_ttf_ed": 4628.0,
            "std_ttf_et": 4628.0,
            "std_ttf_sk": 4628.0,
            "std_ttf_array": 4628.0
        },
        "survivability_uls": {}
    }
    d = extract_rams_data(rams_json)
    assert d == [
        (110909.0, 9),
        (0.890, 10),
        (0.92, 11),
        (False, 12),
        (False, 14)
    ]


def test_extract_rams_data_ms():
    rams_json = {
        "availability": {},
        "maintainability": {
            "probability_maintenance": [0.987],
            "critical_component": "Id1"
        },
        "reliability_system": {},
        "survivability_uls": {
            "ET_Subsystem": {
                "device_id": "Device0",
                "survival_uls": [0.987]
            },
            "SK_Subsystem": {
                "device_id": "Device2",
                "survival_uls": [0.998]
            }
        }
    }
    d = extract_rams_data(rams_json)
    assert d == [
        (False, 9),
        (False, 10),
        (False, 11),
        (0.987, 12),
        (0.987, 14)
    ]


def test_extract_rams_data_nested():
    rams_json = {
        "availability": {},
        "maintainability": {
            "critical_component": "Id1"
        },
        "reliability_system": {},
        "survivability_uls": {
            "ET_Subsystem": {
                "device_id": "Device0",
            },
            "SK_Subsystem": {
                "device_id": "Device2",
            }
        }
    }
    d = extract_rams_data(rams_json)
    assert d == [
        (False, 9),
        (False, 10),
        (False, 11),
        (False, 12),
        (False, 14)
    ]


def test_calculate_min_uls():

    data = {
        "survivability_uls":
            {
                "ET_Subsystem": {
                    "device_id": "Device0",
                    "survival_uls": 0.5
                },
                "SK_Subsystem": {
                    "device_id": "Device2",
                    "survival_uls": None
                }
            }
    }
    t = calculate_min_uls(data)
    assert t == None


def test_update_rams_metrics_error(app):
    with app.app_context():
        init_stage_gate_study()

        me = MetricExtractor("1", ENTITY_LIST)
        update_rams_metric(me, False, 1)
        assert me.status == {
            'lmo': [],
            'spey': [],
            'slc': [],
            'esa': [],
            'rams': [
                dict(
                    metric_id=1,
                    status='Warning',
                    message='Metric result has not been computed in RAMS'
                )
            ]
        }
