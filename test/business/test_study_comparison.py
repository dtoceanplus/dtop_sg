from dtop_stagegate.business.study_comparison import StudyComparison, ChecklistComparison, \
    StageGateAssessmentComparison
from test.business.test_sg_studies_business import init_stage_gate_study
from dtop_stagegate.business.models import ApplicantAnswer, AssessorScore, StageGateAssessment
from dtop_stagegate.business.assessor_mode import update_assessor_complete_status
from dtop_stagegate.business.stage_gate_studies import create_study
from dtop_stagegate.service import db

import pytest


def add_two_test_studies():
    init_stage_gate_study()
    study_data = dict(
        name='test study 2',
        description='description for test stage gate study 2',
        framework_id=1
    )
    create_study(study_data)


def test_init_study_comparison(app):
    with app.app_context():
        add_two_test_studies()

        study_ids = ["1", "2"]
        sc = StudyComparison(study_ids)
        assert len(sc._sg_studies) == 2
        assert sc._sg_studies[0].name == 'test study 1'
        assert not sc.study_error

        study_ids = ["1", "2", "3"]
        sc = StudyComparison(study_ids)
        assert sc.study_error


def test_get_checklist_comparison(app):
    with app.app_context():
        add_two_test_studies()

        study_ids = ["1", "2"]
        sc = StudyComparison(study_ids)

        d = sc.get_checklist_comparison()
        check_comparison_data(d)


def check_comparison_data(d):
    assert d['summary'] == [
        {
            "name": "test study 1",
            "stage_0": "0%",
            "stage_1": "0%",
            "stage_2": "0%",
            "stage_3": "0%",
            "stage_4": "0%",
            "stage_5": "0%"
        },
        {
            "name": "test study 2",
            "stage_0": "0%",
            "stage_1": "0%",
            "stage_2": "0%",
            "stage_3": "0%",
            "stage_4": "0%",
            "stage_5": "0%"
        }
    ]

    assert d['bar_chart_data'][0]['idx'] == 0
    assert d['bar_chart_data'][0]['name'] == "Stage 0"
    ac = d['bar_chart_data'][0]['activity_categories']
    assert len(ac) == 2
    assert ac[0] == {
      "name": "test study 1",
      "orientation": "h",
      "type": "bar",
      "x": [
        0,
        0,
        0,
        0,
        0,
        0,
        0
      ],
      "y": [
          'Requirements',
          'Target selection',
          'Concept',
          'Comparable technology',
          'Economic assessment',
          'Environmental and social impacts',
          'Environment characterisation'
      ]
    }
    ea = d['bar_chart_data'][0]['evaluation_areas']
    assert len(ea) == 2
    assert ea[0] == {
            "name": "test study 1",
            "orientation": "h",
            "type": "bar",
            "x": [
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0,
                0
            ],
            "y": [
                'Acceptability',
                'Installability',
                'Availability',
                'Maintainability',
                'Reliability',
                'Survivability',
                'Affordability',
                'Manufacturability',
                'Power Capture',
                'Power Conversion',
                'Controllability'
            ]
        }


def test_init_checklist_comparison(app):
    with app.app_context():
        add_two_test_studies()

        study_ids = ["1", "2"]
        sc = StudyComparison(study_ids)
        studies = sc._sg_studies
        cc = ChecklistComparison(studies)

        assert studies == cc._sg_studies
        assert cc.data['summary'] == []
        assert len(cc.data['bar_chart_data']) == 6
        assert cc.data['bar_chart_data'][0] == {
            'idx': 0,
            'name': 'Stage 0',
            'activity_categories': [],
            'evaluation_areas': []
        }


def test_init_sg_assessment_comparison(app):
    with app.app_context():
        add_two_test_studies()

        sg_assess_ids = ["1", "6"]
        sga = StageGateAssessmentComparison(sg_assess_ids)
        assert len(sga._sg_assessments) == 2
        assert sga._sg_assessments[0].stage_gate.name == 'Stage Gate 0 - 1'
        assert not sga.sg_assess_error

        sg_assess_ids = ["1", "6",  "11"]
        sga = StageGateAssessmentComparison(sg_assess_ids)
        assert sga.sg_assess_error


def test_get_applicant_comparison(app):
    with app.app_context():
        add_two_test_studies()

        sg_assess_ids = ["2", "7"]
        for sga_id in sg_assess_ids:
            fill_all_applicant_answers(sga_id)

        sga = StageGateAssessmentComparison(sg_assess_ids)
        sga_comparison = sga.get_applicant_comparison()
        assert sga_comparison['study_columns'] == [
            {
                'prop': 'study_1',
                'label': 'test study 1'
            },
            {
                'prop': 'study_2',
                'label': 'test study 2'
            }
        ]
        assert sga_comparison['summary_data'] == [
            {
                'study_label': 'test study 1',
                'sg_name': 'Stage Gate 1 - 2',
                'sg_assess_id': 2,
                'response_rate': 100,
                'threshold_success_rate': None
            },
            {
                'study_label': 'test study 2',
                'sg_name': 'Stage Gate 1 - 2',
                'sg_assess_id': 7,
                'response_rate': 100,
                'threshold_success_rate': None
            }
        ]
        assert len(sga_comparison['metric_data']) == 21
        assert sga_comparison['metric_data'][0] == {
          "data": [
            {
              "name": "study_1",
              "result": 100
            },
            {
              "name": "study_2",
              "result": 100
            }
          ],
          "metric": "Annual captured energy",
          "threshold": None,
          "threshold_type": "lower",
          "unit": "kWh"
        }


def fill_all_applicant_answers(sga_id):

    app_answer_list = ApplicantAnswer.query.filter_by(stage_gate_assessment_id=sga_id).all()
    for aa in app_answer_list:
        aa.result = 100
        aa.justification = "test"
        aa.response = "test"
    db.session.commit()


def test_get_assessor_comparison(app):
    with app.app_context():
        add_two_test_studies()

        sga_ids = ["2", "7"]
        complete_assessor_scores(sga_ids)

        sga = StageGateAssessmentComparison(sga_ids)
        res = sga.get_assessor_comparison()

        check_assessor_comparison(res)


def check_assessor_comparison(results):

    assert results['summary_data'] == [
        {
            "name": "test study 1",
            "orientation": "h",
            "type": "bar",
            "x": [
                2.0,
                2.0
            ],
            "y": [
                "Average",
                "Weighted average"
            ]
        },
        {
            "name": "test study 2",
            "orientation": "h",
            "type": "bar",
            "x": [
                2.0,
                2.0
            ],
            "y": [
                "Average",
                "Weighted average"
            ]
        }
    ]
    assert results['evaluation_areas'] == {
        "average": [
          {
            "id": 4,
            "name": "test study 1",
            "orientation": "h",
            "type": "bar",
            "x": [
              2.0,
              2.0,
              2.0,
              2.0,
              2.0,
              2.0,
              2.0,
              2.0,
              2.0
            ],
            "y": [
              "Survivability",
              "Reliability",
              "Power Conversion",
              "Power Capture",
              "Maintainability",
              "Installability",
              "Availability",
              "Affordability",
              "Acceptability"
            ]
          },
          {
            "id": 4,
            "name": "test study 2",
            "orientation": "h",
            "type": "bar",
            "x": [
              2.0,
              2.0,
              2.0,
              2.0,
              2.0,
              2.0,
              2.0,
              2.0,
              2.0
            ],
            "y": [
              "Survivability",
              "Reliability",
              "Power Conversion",
              "Power Capture",
              "Maintainability",
              "Installability",
              "Availability",
              "Affordability",
              "Acceptability"
            ]
          }
        ],
        "weighted_average": [
          {
            "id": 3,
            "name": "test study 1",
            "orientation": "h",
            "type": "bar",
            "x": [pytest.approx(2.0) for x in range(9)],
            "y": [
              "Survivability",
              "Reliability",
              "Power Conversion",
              "Power Capture",
              "Maintainability",
              "Installability",
              "Availability",
              "Affordability",
              "Acceptability"
            ]
          },
          {
            "id": 3,
            "name": "test study 2",
            "orientation": "h",
            "type": "bar",
            "x": [pytest.approx(2.0) for x in range(9)],
            "y": [
              "Survivability",
              "Reliability",
              "Power Conversion",
              "Power Capture",
              "Maintainability",
              "Installability",
              "Availability",
              "Affordability",
              "Acceptability"
            ]
          }
        ]
      }
    assert results['question_categories'] == {
        "average": [
          {
            "id": 2,
            "name": "test study 1",
            "orientation": "h",
            "type": "bar",
            "x": [
              2.0,
              2.0,
              2.0
            ],
            "y": [
              "Technology",
              "Project",
              "Impact"
            ]
          },
          {
            "id": 2,
            "name": "test study 2",
            "orientation": "h",
            "type": "bar",
            "x": [
              2.0,
              2.0,
              2.0
            ],
            "y": [
              "Technology",
              "Project",
              "Impact"
            ]
          }
        ],
        "weighted_average": [
          {
            "id": 1,
            "name": "test study 1",
            "orientation": "h",
            "type": "bar",
            "x": [
              pytest.approx(2),
              pytest.approx(2),
              pytest.approx(2)
            ],
            "y": [
              "Technology",
              "Project",
              "Impact"
            ]
          },
          {
            "id": 1,
            "name": "test study 2",
            "orientation": "h",
            "type": "bar",
            "x": [
              pytest.approx(2),
              pytest.approx(2),
              pytest.approx(2)
            ],
            "y": [
              "Technology",
              "Project",
              "Impact"
            ]
          }
        ]
      }


def complete_assessor_scores(sga_ids):

    for sga_id in sga_ids:
        fill_all_assessor_scores(sga_id)
        sga = StageGateAssessment.query.get(sga_id)
        update_assessor_complete_status(sga)
    db.session.commit()


def fill_all_assessor_scores(sga_id):

    assessor_scores = AssessorScore.query.filter_by(stage_gate_assessment_id=sga_id).all()
    for a_s in assessor_scores:
        a_s.score = 2
