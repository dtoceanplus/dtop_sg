from test.business.test_sg_studies_business import init_stage_gate_study
from dtop_stagegate.business.models import StageGateAssessment, AssessorScore, AssessorComment
from dtop_stagegate.business.applicant_mode import ApplicantResults, get_stage_gate_assessments
from dtop_stagegate.business.assessor_mode import AssessorStageGateData, update_assessor_scores_in_db,\
    update_assessor_comments_in_db, update_assessor_data_in_db, AssessorResults, update_assessor_complete_status
from dtop_stagegate.business.applicant_mode import get_applicant_answers_by_stage_gate_assessment_id
from dtop_stagegate.service import db

import pytest


def check_assessor_score_and_comment(stage_gate_assessment, score=None, comment=None):

    app_result = ApplicantResults(stage_gate_assessment)
    app_result.calculate()
    d = app_result.get_results()

    assessor_stage_gate = AssessorStageGateData(stage_gate_assessment)
    assert assessor_stage_gate.applicant_responses == d['responses']

    assessor_stage_gate_data = assessor_stage_gate.join_data()
    q0 = assessor_stage_gate_data['question_categories'][0]['questions'][0]
    assert q0['score'] == dict(id=1, score=score)
    assert q0['scoring_criteria'][0]['comment'] == dict(id=1, comment=comment)


def test_get_assessor_stage_gate_data(app):
    with app.app_context():
        init_stage_gate_study()

        stage_gate_assessment = StageGateAssessment.query.get('1')

        check_assessor_score_and_comment(stage_gate_assessment)

        app_answer_list = get_applicant_answers_by_stage_gate_assessment_id('1')
        app_answer_list[0].result = 147.
        app_answer_list[0].justification = 'test'
        check_assessor_score_and_comment(stage_gate_assessment)

        assessor_score_1 = AssessorScore.query.get('1')
        assessor_score_1.score = 2
        assessor_comment_1 = AssessorComment.query.get('1')
        assessor_comment_1.comment = 'test'
        check_assessor_score_and_comment(stage_gate_assessment, 2, 'test')


def test_update_assessor_scores_in_db(app):
    with app.app_context():
        init_stage_gate_study()

        data = [
            {
                'id': 1,
                'score':  4
            },
            {
                'id': 2,
                'score':  3
            },
        ]
        sg_assessment = StageGateAssessment.query.get('1')
        update_assessor_scores_in_db(sg_assessment, data)

        as_1 = AssessorScore.query.get('1')
        as_2 = AssessorScore.query.get('2')

        assert as_1.score == 4
        assert as_2.score == 3


def test_update_assessor_comments_in_db(app):
    with app.app_context():
        init_stage_gate_study()

        data = [
            {
                'id': 1,
                'comment': 'test'
            },
            {
                'id': 2,
                'comment': 'another test'
            },
        ]
        sg_assessment = StageGateAssessment.query.get('1')
        update_assessor_comments_in_db(sg_assessment, data)

        ac_1 = AssessorComment.query.get('1')
        ac_2 = AssessorComment.query.get('2')

        assert ac_1.comment == 'test'
        assert ac_2.comment == 'another test'


def test_update_assessor_data(app):
    with app.app_context():
        init_stage_gate_study()

        good_score_data = [dict(id=34, score=2)]
        bad_score_data = [dict(id=1, score=2)]
        good_comment_data = [dict(id=56, comment='test')]
        bad_comment_data = [dict(id=1, comment='test')]
        sg_assessment = StageGateAssessment.query.get('2')

        update_assessor_data_in_db(sg_assessment, good_score_data, good_comment_data)
        as_34 = AssessorScore.query.get('34')
        assert as_34.score == 2
        ac_56 = AssessorComment.query.get('56')
        assert ac_56.comment == 'test'

        err = update_assessor_scores_in_db(sg_assessment, bad_score_data)
        assert err == "Request body contains assessor scores with invalid IDs"

        err = update_assessor_comments_in_db(sg_assessment, bad_comment_data)
        assert err == "Request body contains assessor comments with invalid IDs"

        err = update_assessor_data_in_db(sg_assessment, good_score_data, bad_comment_data)
        assert err == "Request body contains assessor comments with invalid IDs"

        err = update_assessor_data_in_db(sg_assessment, bad_score_data, good_comment_data)
        assert err == "Request body contains assessor scores with invalid IDs"


def test_update_assessor_complete_status(app):
    with app.app_context():
        init_stage_gate_study()

        sg_assessment = get_stage_gate_assessments('1')
        assert all([not sga.assessor_complete for sga in sg_assessment])

        sg_assessment = StageGateAssessment.query.get('1')
        update_assessor_complete_status(sg_assessment)
        sg_assessment = get_stage_gate_assessments('1')
        assert sg_assessment[0].assessor_complete
        assert not sg_assessment[1].assessor_complete


def test_calculate_assessor_results(app):
    with app.app_context():
        init_stage_gate_study()

        a_scores = AssessorScore.query.filter(AssessorScore.stage_gate_assessment_id == '1').all()
        for a_s in a_scores:
            a_s.score = 2
        db.session.commit()

        sg_assessment = StageGateAssessment.query.get('1')
        as_results = AssessorResults(sg_assessment)
        as_results.calculate()
        r = as_results.get_results()

        assessor_sg_data = AssessorStageGateData(sg_assessment).join_data()
        assert r['responses'] == assessor_sg_data
        overall = r['overall'][0]
        assert overall['id'] == 0
        assert overall['name'] == 'Stage Gate score'
        assert overall['orientation'] == 'h'
        assert overall['y'] == ['Average', 'Weighted average']
        assert overall['x'] == [2, 2]

        for c in ['question_categories', 'evaluation_areas']:
            r_cat = r[c]
            assert len(r_cat) == 2
            assert r_cat[0]['name'] == 'Weighted average'
            assert r_cat[0]['orientation'] == 'h'
            assert r_cat[0]['x'] == [pytest.approx(2) for i in range(len(r_cat[0]['x']))]
            assert r_cat[1]['name'] == 'Average'
            assert r_cat[1]['x'] == [pytest.approx(2) for i in range(len(r_cat[0]['x']))]
            assert r_cat[1]['orientation'] == 'h'

        qcs = [
            'Technology credibility'
            'Technology development'
            'Technology risks'
            'Technology applicability'
            'Cost of energy potential'
            'Future targets'
            'Future commercial offering'
            'Installability metrics'
            'Reliability metrics'
            'Maintainability metrics'
            'Survivability metrics'
            'Availability metrics'
            'Power Capture metrics'
            'Power Conversion metrics'
            'Affordability metrics'
            'Acceptability metrics'
            'Scope of work'
            'Testing'
        ]
        assert [qc['y'] == qcs for qc in r['question_categories']]

        eas = [
                "Acceptability",
                "Installability",
                "Availability",
                "Maintainability",
                "Reliability",
                "Survivability",
                "Affordability",
                "Manufacturability",
                "Power Capture",
                "Power Conversion",
        ]
        assert [ea['y'] == eas for ea in r['evaluation_areas']]

        a_scores = AssessorScore.query.filter(AssessorScore.stage_gate_assessment_id == '1').all()
        for i, a_s in enumerate(a_scores):
            if i % 2 == 0:
                a_s.score = 1
            else:
                a_s.score = 4
        db.session.commit()

        sg_assessment = StageGateAssessment.query.get('1')
        as_results = AssessorResults(sg_assessment)
        as_results.calculate()
        r = as_results.get_results()

        assessor_sg_data = AssessorStageGateData(sg_assessment).join_data()
        assert r['responses'] == assessor_sg_data
        overall = r['overall'][0]
        assert overall['id'] == 0
        assert overall['name'] == 'Stage Gate score'
        assert overall['y'] == ['Average', 'Weighted average']
        assert overall['x'] == [pytest.approx(2.42105, 0.001), pytest.approx(2.62)]

        for c in ['question_categories', 'evaluation_areas']:
            r_cat = r[c]
            assert len(r_cat) == 2
            assert r_cat[0]['name'] == 'Weighted average'
            assert r_cat[1]['name'] == 'Average'

        assert [qc['y'] == qcs for qc in r['question_categories']]
        assert [ea['y'] == eas for ea in r['evaluation_areas']]

        # TODO: Think of better way to do test these calculations
        assert r['question_categories'][0]['x'][0] == 2.125  # 'Technology' weighted average for SG 0-1
        assert r['question_categories'][1]['x'][0] == 2  # 'Technology' average for SG 0-1
        assert r['question_categories'][0]['x'][-1] == pytest.approx(3.14286)  # 'Innovation' weighted avg for SG 0-1
        assert r['question_categories'][1]['x'][-1] == 3.0  # 'Innovation' average for SG 0-1

        # assert r['question_categories'][0]['x'] == [
        #     pytest.approx(2),
        #     pytest.approx(4),
        #     pytest.approx(1),
        #     pytest.approx(1),
        #     pytest.approx(1),
        #     pytest.approx(4),
        #     pytest.approx(4),
        #     pytest.approx(4),
        #     pytest.approx(4),
        #     pytest.approx(1),
        #     pytest.approx(2.5),
        #     pytest.approx(3)
        # ]
        # assert r['question_categories'][1]['x'] == [
        #     pytest.approx(2),
        #     pytest.approx(4),
        #     pytest.approx(1),
        #     pytest.approx(1),
        #     pytest.approx(1),
        #     pytest.approx(4),
        #     pytest.approx(4),
        #     pytest.approx(4),
        #     pytest.approx(4),
        #     pytest.approx(1),
        #     pytest.approx(2.5),
        #     pytest.approx(2.5)
        # ]
        # assert r['evaluation_areas'][0]['x'] == [pytest.approx(3), 0]
        # assert r['evaluation_areas'][1]['x'] == [3, 0]
