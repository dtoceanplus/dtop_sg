from dtop_stagegate.business.improvement_areas import ImprovementAreas, ChecklistPercentages,\
    MetricPercentages, AssessorPercentages
from test.business.test_sg_studies_business import init_stage_gate_study
from dtop_stagegate.business.models import *


def test_checklist_percentage(app):
    with app.app_context():
        init_stage_gate_study()

        cp = ChecklistPercentages('1', 0)
        ea_names = [
            'Acceptability',
            'Installability',
            'Maintainability',
            'Reliability',
            'Survivability',
            'Affordability',
            'Power Capture',
            'Power Conversion',
            'Controllability'
        ]
        assert cp.checklist_percentages == [{'name': n, 'checklist_score': 0} for n in ea_names]

        ca = set_checklist_complete(1)
        db.session.commit()
        cp = ChecklistPercentages('1', 0)
        full_marks = [{'name': n, 'checklist_score': 100} for n in ea_names]
        assert cp.checklist_percentages == full_marks

        ca[24].complete = 0
        db.session.commit()
        cp = ChecklistPercentages('1', 0)
        bad_acceptability = full_marks.copy()
        bad_acceptability[0]['checklist_score'] = 0
        assert cp.checklist_percentages == bad_acceptability


def test_metric_percentage(app):
    with app.app_context():
        init_stage_gate_study()

        sg_assessment = StageGateAssessment.query.get('1')
        ft = MetricPercentages(sg_assessment)
        assert ft.metric_percentages == []

        qm = QuestionMetric.query.get('1')
        qm.threshold_bool = True
        qm.threshold = 40
        aa = ApplicantAnswer.query.get('25')
        aa.result = 30
        db.session.commit()
        sg_assessment = StageGateAssessment.query.get('2')
        ft = MetricPercentages(sg_assessment)
        assert ft.metric_percentages == [{'name': 'Power Capture', 'metric_score': 0}]

        qm = QuestionMetric.query.get('2')
        qm.threshold_bool = True
        qm.threshold = 30
        aa = ApplicantAnswer.query.get('26')
        aa.result = 29
        db.session.commit()
        sg_assessment = StageGateAssessment.query.get('2')
        ft = MetricPercentages(sg_assessment)
        assert ft.metric_percentages == [
            {'name': 'Power Capture', 'metric_score': 0},
            {'name': 'Power Conversion', 'metric_score': 0}
        ]

        ApplicantAnswer.query.get('25').result = 50
        ApplicantAnswer.query.get('26').result = 40
        db.session.commit()
        sg_assessment = StageGateAssessment.query.get('2')
        ft = MetricPercentages(sg_assessment)
        assert ft.metric_percentages == [
            {'name': 'Power Capture', 'metric_score': 100},
            {'name': 'Power Conversion', 'metric_score': 100}
        ]


def test_assessor_percentage(app):
    with app.app_context():
        init_stage_gate_study()

        sg2_scores = AssessorScore.query.filter(AssessorScore.stage_gate_assessment_id == '2')
        for sg in sg2_scores:
            sg.score = 4
        db.session.commit()

        sg_assessment = StageGateAssessment.query.get('2')
        asc = AssessorPercentages(sg_assessment)
        ea_names = [
            'Survivability',
            'Reliability',
            'Power Conversion',
            'Power Capture',
            'Maintainability',
            'Installability',
            'Availability',
            'Affordability',
            'Acceptability',
        ]
        full_marks = [{'name': n, 'assessor_score': 100} for n in ea_names]
        assert asc.assessor_percentages == full_marks

        s = AssessorScore.query.get('25')
        s.score = 1
        db.session.commit()
        sg_assessment = StageGateAssessment.query.get('2')
        asc = AssessorPercentages(sg_assessment)
        bad_power_capture = full_marks.copy()
        bad_power_capture[3]['assessor_score'] = 75
        assert asc.assessor_percentages == bad_power_capture

        s = AssessorScore.query.get('31')
        s.score = 1
        db.session.commit()
        sg_assessment = StageGateAssessment.query.get('2')
        asc = AssessorPercentages(sg_assessment).assessor_percentages
        bad_install = bad_power_capture.copy()
        bad_install[5]['assessor_score'] = 62
        assert asc == bad_install


def set_checklist_complete(stage):

    ca = ChecklistActivity.query.\
        join(Activity).\
        join(ActivityCategory).\
        join(Stage).\
        filter(Stage.id == stage)

    for c in ca:
        c.complete = 1
    db.session.commit()

    return ca


def test_ia_init(app):
    with app.app_context():
        init_stage_gate_study()

        sg_study = StageGateStudy.query.get('1')
        ia = ImprovementAreas(sg_study)

        assert ia._stage_idx == 0
        assert ia._sg_idx == None
        assert ia._sg_name == 'N/A'
        assert ia.sg_assessment is None

        set_checklist_complete('1')
        ia = ImprovementAreas(sg_study)
        assert ia._stage_idx == 1
        assert ia._sg_idx == 0
        assert ia._sg_name == ia.sg_assessment.stage_gate.name == 'Stage Gate 0 - 1'

        set_checklist_complete('2')
        set_checklist_complete('3')
        set_checklist_complete('4')
        set_checklist_complete('5')
        set_checklist_complete('6')
        ia = ImprovementAreas(sg_study)
        assert ia._stage_idx == 5
        assert ia._sg_idx == None
        assert ia._sg_name == 'N/A'
        assert ia.sg_assessment is None


def test_ia_init_with_stage_index(app):
    with app.app_context():

        init_stage_gate_study()

        sg_study = StageGateStudy.query.get('1')
        ia = ImprovementAreas(sg_study, 0)

        assert ia._stage_idx == 0
        assert ia._sg_idx is None
        assert ia._sg_name == "N/A"

        sg_study = StageGateStudy.query.get('1')
        ia = ImprovementAreas(sg_study, 1)

        assert ia._stage_idx == 1
        assert ia._sg_idx == 0
        assert ia._sg_name == ia.sg_assessment.stage_gate.name == "Stage Gate 0 - 1"

        sg_study = StageGateStudy.query.get('1')
        ia = ImprovementAreas(sg_study, 2)

        assert ia._stage_idx == 2
        assert ia._sg_idx == 1
        assert ia._sg_name == ia.sg_assessment.stage_gate.name == "Stage Gate 1 - 2"


def test_ia_assess_checklist(app):
    with app.app_context():
        init_stage_gate_study()

        ca = set_checklist_complete('1')
        ca[24].complete = 0
        db.session.commit()
        sg_study = StageGateStudy.query.get('1')
        ia = ImprovementAreas(sg_study)
        ia.assess()
        data_out = ia.get_data()
        assert data_out['stage_assessed'] == 0
        assert data_out['stage_gate_assessed'] == 'N/A'
        assert data_out['improvement_areas'] == [{
            'name': 'Acceptability',
            'assessor_score': 'N/A',
            'metric_score': 'N/A',
            'checklist_score': 0,
            'average_score': 0
        }]


def configure_study_and_metrics():
    init_stage_gate_study()
    set_checklist_complete('1')
    set_checklist_complete('2')
    sga = StageGateAssessment.query.get('2')
    sga.applicant_complete = True
    qm = QuestionMetric.query.get('7')
    qm.threshold_bool = True
    qm.threshold = 40
    aa = ApplicantAnswer.query.get('31')
    aa.result = 50
    qm = QuestionMetric.query.get('8')
    qm.threshold_bool = True
    qm.threshold = 40
    aa = ApplicantAnswer.query.get('32')
    aa.result = 50
    db.session.commit()
    sg_study = StageGateStudy.query.get('1')

    return sg_study, sga


def test_ia_assess_metrics(app):
    with app.app_context():

        sg_study, sga = configure_study_and_metrics()
        ia = ImprovementAreas(sg_study)
        ia.assess()
        data_out = ia.get_data()
        assert data_out['stage_assessed'] == 2
        assert data_out['stage_gate_assessed'] == 'Stage Gate 1 - 2'
        assert data_out['improvement_areas'] == [
            {
                'name': 'Acceptability',
                'assessor_score': 'N/A',
                'metric_score': 'N/A',
                'checklist_score': 0,
                'average_score': 0.0
            },
            {
                'assessor_score': 'N/A',
                'average_score': 0.0,
                'checklist_score': 0,
                'metric_score': 0.0,
                'name': 'Installability'
            },
            {
                'assessor_score': 'N/A',
                'average_score': 0.0,
                'checklist_score': 0,
                'metric_score': 'N/A',
                'name': 'Maintainability'
            },
            {
                'assessor_score': 'N/A',
                'average_score': 0.0,
                'checklist_score': 0,
                'metric_score': 'N/A',
                'name': 'Reliability'
            },
            {
                'assessor_score': 'N/A',
                'average_score': 0.0,
                'checklist_score': 0,
                'metric_score': 'N/A',
                'name': 'Survivability'
            },
            {
                'assessor_score': 'N/A',
                'average_score': 0.0,
                'checklist_score': 0,
                'metric_score': 'N/A',
                'name': 'Affordability'
            },
            {
                'assessor_score': 'N/A',
                'average_score': 0.0,
                'checklist_score': 0,
                'metric_score': 'N/A',
                'name': 'Power Capture'
            },
            {
                'assessor_score': 'N/A',
                'average_score': 0.0,
                'checklist_score': 0,
                'metric_score': 'N/A',
                'name': 'Power Conversion'
            },
            {
                'assessor_score': 'N/A',
                'average_score': 0.0,
                'checklist_score': 0,
                'metric_score': 'N/A',
                'name': 'Controllability'
            }
        ]


def test_ia_assess_assessor(app):
    with app.app_context():
        sg_study, sga = configure_study_and_metrics()

        sg2_scores = AssessorScore.query.filter(AssessorScore.stage_gate_assessment_id == '2')
        for sg in sg2_scores:
            sg.score = 4
        s = AssessorScore.query.get('31')
        s.score = 1
        sga.assessor_complete = 1
        db.session.commit()
        ia = ImprovementAreas(sg_study)
        ia.assess()
        data_out = ia.get_data()
        assert data_out['stage_assessed'] == 2
        assert data_out['stage_gate_assessed'] == 'Stage Gate 1 - 2'
        print(data_out['improvement_areas'])
        assert data_out['improvement_areas'] == [
            {
                'name': 'Controllability',
                'checklist_score': 0.0,
                'metric_score': 'N/A',
                'assessor_score': 'N/A',
                'average_score': 0.0
            },
            {
                'name': 'Installability',
                'checklist_score': 0.0,
                'metric_score': 0.0,
                'assessor_score': 62.0,
                'average_score': 21.0
            },
            {
                'name': 'Acceptability',
                'checklist_score': 0.0,
                'metric_score': 'N/A',
                'assessor_score': 100.0,
                'average_score': 50.0
            },
            {
                'name': 'Maintainability',
                'checklist_score': 0.0,
                'metric_score': 'N/A',
                'assessor_score': 100.0,
                'average_score': 50.0
            },
            {
                'name': 'Reliability',
                'checklist_score': 0.0,
                'metric_score': 'N/A',
                'assessor_score': 100.0,
                'average_score': 50.0
            },
            {
                'name': 'Survivability',
                'checklist_score': 0.0,
                'metric_score': 'N/A',
                'assessor_score': 100.0,
                'average_score': 50.0
            },
            {
                'name': 'Affordability',
                'checklist_score': 0.0,
                'metric_score': 'N/A',
                'assessor_score': 100.0,
                'average_score': 50.0
            },
            {
                'name': 'Power Capture',
                'checklist_score': 0.0,
                'metric_score': 'N/A',
                'assessor_score': 100.0,
                'average_score': 50.0
            },
            {
                'name': 'Power Conversion',
                'checklist_score': 0.0,
                'metric_score': 'N/A',
                'assessor_score': 100.0,
                'average_score': 50.0
            }
        ]


def test_ia_assess_threshold(app):
    with app.app_context():
        sg_study, sga = configure_study_and_metrics()

        sg2_scores = AssessorScore.query.filter(AssessorScore.stage_gate_assessment_id == '2')
        for sg in sg2_scores:
            sg.score = 4
        s = AssessorScore.query.get('31')
        s.score = 1
        sga.assessor_complete = 1
        db.session.commit()
        ia = ImprovementAreas(sg_study, None, 40)
        ia.assess()
        data_out = ia.get_data()
        assert data_out['stage_assessed'] == 2
        assert data_out['stage_gate_assessed'] == 'Stage Gate 1 - 2'
        print(data_out['improvement_areas'])
        assert data_out['improvement_areas'] == [
            {
                'name': 'Controllability',
                'checklist_score': 0.0,
                'metric_score': 'N/A',
                'assessor_score': 'N/A',
                'average_score': 0.0
            },
            {
                'name': 'Installability',
                'checklist_score': 0.0,
                'metric_score': 0.0,
                'assessor_score': 62.0,
                'average_score': 21.0
            }
        ]
