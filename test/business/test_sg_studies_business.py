from dtop_stagegate.business.db_utils import install_db
from dtop_stagegate.business.models import StageGateStudy, ChecklistActivity, Activity, StageGateAssessment, StageGate,\
    ApplicantAnswer, Question, AssessorScore, AssessorComment, ScoringCriterion
from dtop_stagegate.business.schemas import StageGateStudySchema
from dtop_stagegate.business.stage_gate_studies import create_study, update_study, delete_study
from dtop_stagegate.service.api.provider_states import add_sg_study


def init_stage_gate_study(f_id=1):
    install_db()
    add_sg_study(f_id)


def get_study_list():
    studies_db = StageGateStudy.query.all()
    sg_studies_schema = StageGateStudySchema(many=True)
    studies = sg_studies_schema.dump(studies_db)

    return studies


def get_study(study_id):
    study = StageGateStudy.query.get(study_id)
    study_schema = StageGateStudySchema()
    study_result = study_schema.dump(study)

    return study_result


def check_study_in_list(sg_study_id):
    studies = get_study_list()
    assert type(studies) is list
    assert len(studies) == sg_study_id
    assert type(studies[sg_study_id-1]) is dict

    study = [s for s in studies if s['id'] == sg_study_id][0]
    assert study.get('id') == sg_study_id
    assert study.get('name') == 'test study {}'.format(sg_study_id)
    assert study.get('description') == 'description for test stage gate study {}'.format(sg_study_id)
    assert study.get('_links').get('self') == 'http://localhost.dev/api/stage-gate-studies/{}'.format(sg_study_id)


def test_get_study_list(app):
    with app.app_context():
        init_stage_gate_study()
        check_study_in_list(1)


def test_create_delete_study(app):
    with app.app_context():
        init_stage_gate_study()
        check_stage_gate_family()

        data = dict(name='test study 2', description='description for test stage gate study 2', framework_id=1)
        create_study(data)
        check_study_in_list(2)

        delete_study('2')
        check_study_in_list(1)
        check_stage_gate_family()

        err = delete_study('2')
        assert 'does not exist' in err

        delete_study('1')
        studies = get_study_list()
        assert type(studies) is list
        assert len(studies) == 0


def check_stage_gate_family():

    check_checklist_activities()
    check_stage_gate_assessments()
    check_applicant_answer()
    check_assessor_score()
    check_assessor_comment()


def check_checklist_activities():

    ca = ChecklistActivity.query.all()
    a = Activity.query.all()

    assert type(ca) == list
    assert len(ca) == len(a)
    assert ca[0].stage_gate_study_id == 1
    assert ca[0].activity_id == a[0].id


def check_stage_gate_assessments():

    sg = StageGate.query.all()
    asg = StageGateAssessment.query.all()

    assert type(asg) == list
    assert len(asg) == len(sg)
    assert not asg[0].applicant_complete
    assert not asg[0].assessor_complete
    assert asg[0].stage_gate_id == sg[0].id
    assert asg[0].stage_gate_study_id == 1


def check_applicant_answer():

    q = Question.query.all()
    aa = ApplicantAnswer.query.all()

    assert type(aa) == list
    assert len(aa) == len(q)
    assert aa[0].result is None
    assert aa[0].question_id == q[0].id
    assert aa[0].stage_gate_assessment_id == 1


def check_assessor_score():

    q = Question.query.all()
    assessor_scores = AssessorScore.query.all()

    assert type(assessor_scores) == list
    assert len(assessor_scores) == len(q)
    assert assessor_scores[0].score is None
    assert assessor_scores[0].stage_gate_assessment_id == 1


def check_assessor_comment():

    sc = ScoringCriterion.query.all()
    ac = AssessorComment.query.all()

    assert type(ac) == list
    assert len(ac) == len(sc)
    assert ac[0].comment is None
    assert ac[0].stage_gate_assessment_id == 1


def test_update_study(app):
    with app.app_context():
        init_stage_gate_study()
        data = dict(
            name='test study 1 modified',
            description='modified description for stage gate study 1',
            checklist_study_complete=True
        )
        update_study(data, '1')

        studies = get_study_list()
        assert len(studies) == 1

        study = get_study('1')
        assert study.get('id') == 1
        assert 'modified' in study.get('name')
        assert 'modified description' in study.get('description')
        assert study.get('checklist_study_complete')

        data = dict(name='breaking test', description='this should lead to an error')
        no_study_msg = update_study(data, '2')
        assert 'does not exist' in no_study_msg
