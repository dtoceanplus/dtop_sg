from dtop_stagegate.business.db_utils import load_json_params, add_json_collection_to_db, add_json_object_to_db, \
    install_db
from dtop_stagegate.business.schemas import RubricSchema, MetricSchema, EvaluationAreaSchema
from dtop_stagegate.business.models import Framework, Metric, Rubric, EvaluationArea


def test_init_db_command(runner, monkeypatch):
    class Recorder(object):
        called = False

    def fake_init_db():
        Recorder.called = True

    monkeypatch.setattr('dtop_stagegate.business.db_utils.init_db', fake_init_db)
    result = runner.invoke(args=['init-db'])
    assert 'Initialized' in result.output
    assert Recorder.called


def test_install_db_command(runner, monkeypatch):
    class Recorder(object):
        called = False

    def fake_init_db():
        Recorder.called = True

    monkeypatch.setattr('dtop_stagegate.business.db_utils.install_db', fake_init_db)
    result = runner.invoke(args=['install-db'])
    assert 'Installed' in result.output
    assert Recorder.called


def test_load_json_params(app):
    with app.app_context():
        json_params = load_json_params('rubrics')
        assert json_params.get('name') == "WES 5-point rubric"
        assert "description of the rubric" in json_params.get('description')
        assert len(json_params.get('grades')) == 5


def test_add_eval_area_and_metric_json_to_db(app):
    with app.app_context():
        add_json_collection_to_db(EvaluationAreaSchema, 'evaluation_areas')
        ea = EvaluationArea.query.all()
        assert len(ea) == 11
        assert ea[0].id == 1
        assert ea[0].name == "Acceptability"

        add_json_collection_to_db(MetricSchema, 'metrics')
        m = Metric.query.all()
        assert len(m) == 27
        assert m[0].name == "Annual captured energy"
        assert m[1].name == "Annual transformed energy"
        assert m[0].evaluation_area_id == 9

        assert m[0].evaluation_area.name == "Power Capture"
        expected_desc = "Power Capture is the process of extracting energy from the natural resource by the " \
                        "interaction with a device and making it available as an input to a power take-off (PTO)."
        assert m[0].evaluation_area.description == expected_desc


def test_add_rubric_json_to_db(app):
    with app.app_context():
        add_json_object_to_db(RubricSchema, 'rubrics')
        r = Rubric.query.all()
        assert len(r) == 1
        assert r[0].name == "WES 5-point rubric"


def test_install_db(app):
    with app.app_context():
        install_db()
        f = Framework.query.all()
        assert len(f) == 1
        assert f[0].name == "Default (no metric thresholds)"

        # Check nested activity metric
        stage_1 = f[0].stages[0]
        assert stage_1.name == "Stage 0"
        act_cat_a = stage_1.activity_categories[0]
        assert act_cat_a.name == "Requirements"
        act_a1 = act_cat_a.activities[0]
        assert act_a1.name == "Requirement definition (Affordability)"
        act_eval_area = act_a1.activity_eval_areas[0]
        assert act_eval_area.evaluation_area_id == 7
        assert act_eval_area.evaluation_area.name == "Affordability"
        assert act_eval_area.evaluation_area.description == "Affordability relates to the cost of electricity " \
                                                            "generated from the wave or tidal stream resource."

        # Check nested question metric
        stage_gate_1_2 = f[0].stage_gates[1]
        assert stage_gate_1_2.name == "Stage Gate 1 - 2"
        q_cat_a = stage_gate_1_2.question_categories[0]
        assert q_cat_a.name == "Technology"
        q = stage_gate_1_2.question_categories[2].questions[0]
        assert q.name == "Annual captured energy"
        q_metric = q.question_metric
        assert not q_metric.threshold_bool
        assert q_metric.threshold is None
        assert q_metric.metric.name == "Annual captured energy"

        # Check nested rubric
        assert q.rubric.name == "WES 5-point rubric"
