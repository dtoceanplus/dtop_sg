"""
Test the integration to the RAMS module.
"""
from dtop_stagegate.business.integration import rams_metrics


EXPECTED_RAMS_METRICS = {
  "availability": {
    "device_id": ["Id1", "Id2", "Id3"],
    "availability_tb": [0.987, 0.976, 0.998],
    "availability_array": 0.92
  },
  "maintainability": {
    "probability_maintenance": [0.987],
    "critical_component": "Id1"
  },
  "reliability_system": {
    "max_annual_pof_ed": 0.890,
    "max_annual_pof_et": 0.890,
    "max_annual_pof_sk": 0.890,
    "max_annual_pof_array": 0.890,
    "max_ttf_ed": 1237629.0,
    "max_ttf_et": 1237629.0,
    "max_ttf_sk": 1237629.0,
    "max_ttf_array": 1237629.0,
    "mttf_ed": 110909.0,
    "mttf_et": 110909.0,
    "mttf_sk": 110909.0,
    "mttf_array": 110909.0,
    "std_ttf_ed": 4628.0,
    "std_ttf_et": 4628.0,
    "std_ttf_sk": 4628.0,
    "std_ttf_array": 4628.0
  },
  "survivability_uls": {
      "ET_Subsystem": {
          "device_id": "Device0",
          "survival_uls": [0.987]
      },
      "SK_Subsystem": {
          "device_id": "Device2",
          "survival_uls": [0.998]
      }
  }
}


def test_rams_metrics():

    metrics = rams_metrics(
        "/rams/results_summary/1"
    )
    assert metrics == EXPECTED_RAMS_METRICS


def test_rams_metrics_500():

    metrics = rams_metrics(
        "/rams/results_summary/42"
    )
    assert metrics == ("Error", 500)


def test_rams_metrics_404():

    metrics = rams_metrics(
        "/rams/results_summary/43"
    )
    assert metrics == ("Error", 404)
