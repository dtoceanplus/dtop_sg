"""
Test the integration to the ESA module.
"""
from dtop_stagegate.business.integration import esa_carbon_footprint_metrics, esa_eia_metrics,\
    esa_social_acceptance_metrics


def test_esa_carbon_footprint_metrics():

    cfp_metrics = esa_carbon_footprint_metrics(
        "/esa/1/carbon_footprint/CFP_global"
    )
    assert cfp_metrics == {
        'gwp': 0.01648182091586897,
        'ced': 0.21426071736369878,
        'epp': 243
    }

    cfp_metrics = esa_carbon_footprint_metrics(
        "/esa/43/carbon_footprint/CFP_global"
    )
    assert cfp_metrics == ("Error", 404)

    cfp_metrics = esa_carbon_footprint_metrics(
        "/esa/44/carbon_footprint/CFP_global"
    )
    assert cfp_metrics == ("Error", 500)


def test_esa_eia_metrics():

    eia_metrics = esa_eia_metrics(
        "/esa/1/environmental_impact_assessment/eia_global"
    )
    assert eia_metrics == {
        'negative_impact': -10.0,
        'positive_impact': 10.0
    }

    eia_metrics = esa_eia_metrics(
        "/esa/43/environmental_impact_assessment/eia_global"
    )
    assert eia_metrics == ("Error", 404)

    eia_metrics = esa_eia_metrics(
        "/esa/44/environmental_impact_assessment/eia_global"
    )
    assert eia_metrics == ("Error", 500)


def test_social_acceptance_metrics():

    sa_metrics = esa_social_acceptance_metrics(
        "/esa/1/social_acceptance"
    )
    assert sa_metrics == {
        'nb_vessel_crew': 434.5,
        'cost_of_consenting': 0.0
    }

    sa_metrics = esa_social_acceptance_metrics(
        "/esa/43/social_acceptance"
    )
    assert sa_metrics == ("Error", 404)

    sa_metrics = esa_social_acceptance_metrics(
        "/esa/44/social_acceptance"
    )
    assert sa_metrics == ("Error", 500)
