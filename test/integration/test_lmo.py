"""
Test the integration to the LMO module.
"""
from dtop_stagegate.business.integration import lmo_metric


def test_lmo_installation_cost():

    installation_cost = lmo_metric(
        "/api/1/phases/installation/cost"
    )
    assert installation_cost == 19188188.02

    installation_cost = lmo_metric(
        "/api/43/phases/installation/cost"
    )
    assert installation_cost == ("Error", 404)

    installation_cost = lmo_metric(
        "/api/44/phases/installation/cost"
    )
    assert installation_cost == ("Error", 500)


def test_lmo_installation_duration():

    installation_duration = lmo_metric(
        "/api/1/phases/installation/duration"
    )
    assert installation_duration == 6588.1

    installation_duration = lmo_metric(
        "/api/43/phases/installation/duration"
    )
    assert installation_duration == ("Error", 404)

    installation_duration = lmo_metric(
        "/api/44/phases/installation/duration"
    )
    assert installation_duration == ("Error", 500)


def test_lmo_maintenance_duration():

    maintenance_duration = lmo_metric(
        "/api/1/phases/maintenance/duration"
    )
    assert maintenance_duration == 7654.3

    maintenance_duration = lmo_metric(
        "/api/43/phases/maintenance/duration"
    )
    assert maintenance_duration == ("Error", 404)

    maintenance_duration = lmo_metric(
        "/api/44/phases/maintenance/duration"
    )
    assert maintenance_duration == ("Error", 500)
