"""
Test the integration to the SLC module.
"""
from dtop_stagegate.business.integration import slc_capex, slc_irr, slc_lcoe, slc_cost_metrics_per_kw, slc_opex


def test_slc_capex():

    capex = slc_capex(
        "/api/projects/1/economical/capex"
    )
    assert capex == 5000000

    capex = slc_capex(
        "/api/projects/43/economical/capex"
    )
    assert capex == ("Error", 404)


def test_slc_irr():

    irr = slc_irr(
        "/api/projects/1/financial/irr"
    )
    assert irr == 24.3

    irr = slc_irr(
        "/api/projects/43/financial/irr"
    )
    assert irr == ("Error", 404)


def test_slc_lcoe():

    lcoe = slc_lcoe(
        "/api/projects/1/economical/lcoe"
    )
    assert lcoe == 7.345

    lcoe = slc_lcoe(
        "/api/projects/43/economical/lcoe"
    )
    assert lcoe == ("Error", 404)


def test_slc_cost_metrics_per_kw():

    cost_metrics = slc_cost_metrics_per_kw(
        "/api/projects/1/benchmark/metrics"
    )
    assert cost_metrics == {
        "capex_kw": 15945.24,
        "opex_kw": 6782.4
    }

    cost_metrics = slc_cost_metrics_per_kw(
        "/api/projects/43/benchmark/metrics"
    )
    assert cost_metrics == ("Error", 404)


def test_slc_opex():

    opex = slc_opex(
        "/api/projects/1/economical/opex"
    )
    assert opex == 5000000

    opex = slc_opex(
        "/api/projects/43/economical/opex"
    )
    assert opex == ("Error", 404)
