"""
Test the integration to the SPEY module.
"""
from dtop_stagegate.business.integration import spey_efficiency_metrics

EXPECTED_SPEY_METRICS = {
    "annual_captured_energy": 100,
    "annual_transformed_energy": 100,
    "annual_delivered_energy": 100,
    "array_capt_eff": 0.5,
    "array_rel_transf_eff": 0.5,
    "array_rel_deliv_eff": 0.5
}


def test_spey_efficiency_metrics():

    spey_metrics = spey_efficiency_metrics(
        "/spey/1/efficiency"
    )
    assert spey_metrics == EXPECTED_SPEY_METRICS

    spey_metrics = spey_efficiency_metrics(
        "/spey/43/efficiency"
    )
    assert spey_metrics == ("Error", 404)

    spey_metrics = spey_efficiency_metrics(
        "/spey/44/efficiency"
    )
    assert spey_metrics == ("Error", 500)
