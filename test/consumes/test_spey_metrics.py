import atexit
import requests

from pact import Consumer, Like, Provider, Term


SPEY_OUTPUTS = {
    "Inputs_EFF": {
        "annual_captured_energy": {
            "value": 100,
            "Label": "aaa",
            "Unit": "m",
            "Description": "ccc"
        },
        "annual_transformed_energy": {
            "value": 100,
            "Label": "aaa",
            "Unit": "m",
            "Description": "ccc"
        },
        "annual_delivered_energy": {
            "value": 100,
            "Label": "aaa",
            "Unit": "m",
            "Description": "ccc"
        }
    },
    "Outputs_EFF": {
        "array_capt_eff": {
            "value": 0.5,
            "Aggregation-Level": "array",
            "Label": "aaa",
            "Unit": "m",
            "Tag": "aaa",
            "Description": "ccc"
        },
        "array_rel_transf_eff": {
            "value": 0.5,
            "Aggregation-Level": "array",
            "Label": "aaa",
            "Unit": "m",
            "Tag": "aaa",
            "Description": "ccc"
        },
        "array_rel_deliv_eff": {
            "value": 0.5,
            "Aggregation-Level": "array",
            "Label": "aaa",
            "Unit": "m",
            "Tag": "aaa",
            "Description": "ccc"
        }
    }
}

pact = Consumer('sg').has_pact_with(Provider('spey'), port=1234)
pact.start_service()
atexit.register(pact.stop_service)


def test_spey_metrics():

    pact.given('spey 1 exists and efficiency results have been saved') \
        .upon_receiving('a request for efficiency') \
        .with_request('GET', Term(r'/spey/\d+/efficiency', '/spey/1/efficiency')) \
        .will_respond_with(200, body=Like(SPEY_OUTPUTS))

    with pact:
        spey_metrics = requests.get(
            f'{pact.uri}/spey/1/efficiency'
        )
        assert spey_metrics.json() == SPEY_OUTPUTS
