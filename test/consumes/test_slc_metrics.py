import atexit
import requests

from pact import Consumer, Like, Provider, Term


pact = Consumer('sg').has_pact_with(Provider('slc'), port=1239)
pact.start_service()
atexit.register(pact.stop_service)


def test_slc_metrics_capex():

    pact.given('slc 1 exists and has results') \
        .upon_receiving('a request for capex') \
        .with_request('GET', Term(r'/api/projects/\d+/economical/capex', '/api/projects/1/economical/capex')) \
        .will_respond_with(200, body=Like({'capex': 5000000}))

    with pact:
        slc_metrics = requests.get(
            f'{pact.uri}/api/projects/1/economical/capex'
        )
        assert slc_metrics.json() == {'capex': 5000000}


def test_slc_metrics_irr():
    pact.given('slc 1 exists and has results') \
        .upon_receiving('a request for irr') \
        .with_request('GET', Term(r'/api/projects/\d+/financial/irr', '/api/projects/1/financial/irr')) \
        .will_respond_with(200, body=Like({'irr': 24.3}))

    with pact:
        slc_metrics = requests.get(
            f'{pact.uri}/api/projects/1/financial/irr'
        )
        assert slc_metrics.json() == {'irr': 24.3}


def test_slc_metrics_lcoe():
    pact.given('slc 1 exists and has results') \
        .upon_receiving('a request for lcoe') \
        .with_request('GET', Term(r'/api/projects/\d+/economical/lcoe', '/api/projects/1/economical/lcoe')) \
        .will_respond_with(200, body=Like({'lcoe': 7.345}))

    with pact:
        slc_metrics = requests.get(
            f'{pact.uri}/api/projects/1/economical/lcoe'
        )
        assert slc_metrics.json() == {'lcoe': 7.345}


def test_slc_metrics_cost_metrics_per_kw():
    pact.given('slc 1 exists and has results') \
        .upon_receiving('a request for benchmark metrics') \
        .with_request('GET', Term(r'/api/projects/\d+/benchmark/metrics', '/api/projects/1/benchmark/metrics')) \
        .will_respond_with(200, body=Like({'capex_kw': 15945.24, 'opex_kw': 6782.4}))

    with pact:
        slc_metrics = requests.get(
            f'{pact.uri}/api/projects/1/benchmark/metrics'
        )
        assert slc_metrics.json() == {
            "capex_kw": 15945.24,
            "opex_kw": 6782.4
        }


def test_slc_metrics_opex():
    pact.given('slc 1 exists and has results') \
        .upon_receiving('a request for opex') \
        .with_request('GET', Term(r'/api/projects/\d+/economical/opex', '/api/projects/1/economical/opex')) \
        .will_respond_with(200, body=Like({'opex': {
            'total': 5000000,
            'per_year': 250000
        }}))

    with pact:
        slc_metrics = requests.get(
            f'{pact.uri}/api/projects/1/economical/opex'
        )
        assert slc_metrics.json() == {'opex': {
            'total': 5000000,
            'per_year': 250000
        }}
