import atexit
import requests

from pact import Consumer, Like, Provider, Term


pact = Consumer('sg').has_pact_with(Provider('lmo'), port=1238)
pact.start_service()
atexit.register(pact.stop_service)


def test_lmo_metrics_installation_cost():

    pact.given('lmo 1 exists and has installation') \
        .upon_receiving('a request for installation cost') \
        .with_request('GET', Term(r'/api/\d+/phases/installation/cost', '/api/1/phases/installation/cost')) \
        .will_respond_with(200, body=Like({'value': 19188188.02, 'unit': "euro"}))

    with pact:
        lmo_metrics = requests.get(
            f'{pact.uri}/api/1/phases/installation/cost'
        )
        assert lmo_metrics.json() == {'value': 19188188.02, 'unit': "euro"}


def test_lmo_metrics_installation_duration():

    pact.given('lmo 1 exists and has installation') \
        .upon_receiving('a request for installation duration') \
        .with_request('GET', Term(r'/api/\d+/phases/installation/duration', '/api/1/phases/installation/duration')) \
        .will_respond_with(200, body=Like({'value': 6588.1, 'unit': "hour"}))

    with pact:
        lmo_metrics = requests.get(
            f'{pact.uri}/api/1/phases/installation/duration'
        )
        assert lmo_metrics.json() == {'value': 6588.1, 'unit': "hour"}


def test_lmo_metrics_maintenance_duration():

    pact.given('lmo 1 exists and has maintenance') \
        .upon_receiving('a request for maintenance duration') \
        .with_request('GET', Term(r'/api/\d+/phases/maintenance/duration', '/api/1/phases/maintenance/duration')) \
        .will_respond_with(200, body=Like({'value': 7654.3, 'unit': "hour"}))

    with pact:
        lmo_metrics = requests.get(
            f'{pact.uri}/api/1/phases/maintenance/duration'
        )
        assert lmo_metrics.json() == {'value': 7654.3, 'unit': "hour"}
