import atexit
import requests

from pact import Consumer, Like, Provider, Term

from dtop_stagegate.business.integration import (
    esa_carbon_footprint_metrics,
    esa_social_acceptance_metrics,
    esa_eia_metrics
)

pact = Consumer('sg').has_pact_with(Provider('esa'), port=1237)
pact.start_service()
atexit.register(pact.stop_service)


def test_esa_carbon_footprint():

    pact.given('esa 1 exists and has global carbon footprint results') \
        .upon_receiving('a request for global carbon footprint') \
        .with_request('GET', Term(r'/esa/\d+/carbon_footprint/CFP_global', '/esa/1/carbon_footprint/CFP_global')) \
        .will_respond_with(200, body=Like({'GWP': 0.0165, 'CED': 0.214, 'EPP': 243}))

    with pact:
        esa_metrics = requests.get(
            f'{pact.uri}/esa/1/carbon_footprint/CFP_global'
        )
        assert esa_metrics.json() == {'GWP': 0.0165, 'CED': 0.214, 'EPP': 243}


def test_esa_eia():

    pact.given('esa 1 exists and has global eia results') \
        .upon_receiving('a request for eia') \
        .with_request('GET', Term(r'/esa/\d+/environmental_impact_assessment/eia_global',
                                  '/esa/1/environmental_impact_assessment/eia_global')) \
        .will_respond_with(200, body=Like({'negative_impact': -10.0, 'positive_impact': 10.0}))

    with pact:
        esa_metrics = requests.get(
            f'{pact.uri}/esa/1/environmental_impact_assessment/eia_global'
        )
        assert esa_metrics.json() == {
            'negative_impact': -10.0,
            'positive_impact': 10.0
        }


def test_social_acceptance():

    pact.given('esa 1 exists and has social acceptance results') \
        .upon_receiving('a request for social acceptance') \
        .with_request('GET', Term(r'/esa/\d+/social_acceptance', '/esa/1/social_acceptance')) \
        .will_respond_with(200, body=Like({'nb_vessel_crew': 434.5, 'cost_of_consenting': 0.0}))

    with pact:
        esa_metrics = requests.get(
            f'{pact.uri}/esa/1/social_acceptance'
        )
        assert esa_metrics.json() == {
            'nb_vessel_crew': 434.5,
            'cost_of_consenting': 0.0
        }
