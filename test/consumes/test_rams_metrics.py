# import atexit
# import requests
#
# from pact import Consumer, Like, Provider, Term
#
#
# RAMS_RESULTS_SUMMARY = {
#   "availability": {
#     "device_id": ["Id1", "Id2", "Id3"],
#     "availability_tb": [0.987, 0.976, 0.998],
#     "availability_array": 0.92
#   },
#   "maintainability": {
#     "probability_maintenance": [0.987],
#     "critical_component": "Id1"
#   },
#   "reliability_system": {
#     "max_annual_pof_ed": 0.890,
#     "max_annual_pof_et": 0.890,
#     "max_annual_pof_sk": 0.890,
#     "max_annual_pof_array": 0.890,
#     "max_ttf_ed": 1237629.0,
#     "max_ttf_et": 1237629.0,
#     "max_ttf_sk": 1237629.0,
#     "max_ttf_array": 1237629.0,
#     "mttf_ed": 110909.0,
#     "mttf_et": 110909.0,
#     "mttf_sk": 110909.0,
#     "mttf_array": 110909.0,
#     "std_ttf_ed": 4628.0,
#     "std_ttf_et": 4628.0,
#     "std_ttf_sk": 4628.0,
#     "std_ttf_array": 4628.0
#   },
#   "survivability_uls": {
#       "ET_Subsystem": {
#           "device_id": "Device0",
#           "survival_uls": 0.987
#       },
#       "SK_Subsystem": {
#           "device_id": "Device2",
#           "survival_uls": 0.998
#       }
#   }
# }
#
# pact = Consumer('sg').has_pact_with(Provider('rams'), port=1236)
# pact.start_service()
# atexit.register(pact.stop_service)
#
#
# def test_rams_metrics():
#
#     pact.given('rams 1 exists and has results summary') \
#         .upon_receiving('a request for results summary') \
#         .with_request('GET', Term(r'/rams/results_summary/\d+', '/rams/results_summary/1')) \
#         .will_respond_with(200, body=Like(RAMS_RESULTS_SUMMARY))
#
#     with pact:
#         metrics = requests.get(
#             f'{pact.uri}/rams/results_summary/1'
#         )
#         assert metrics.json() == RAMS_RESULTS_SUMMARY
