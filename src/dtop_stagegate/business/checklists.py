# This is the Stage Gate module of the DTOceanPlus suite of tools
# Copyright (C) 2021 Wave Energy Scotland - Ben Hudson
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
import itertools

from dtop_stagegate.service import db
from .frameworks import get_eval_areas
from .models import ChecklistActivity, Activity
from .schemas import ActivitySchema
from .stage_gate_studies import update_study


def update_checklist_activity_answers(study_id, data):
    """
    Update the Checklist Activity database table based on the request sent from the frontend.

    Sets the *checklist_study_complete* entry to true for the specified Stage Gate study.
    Get the checklist activities associated with the specified study. Set the 'complete' attribute to true for any of
    the activities who's ID (*activity_id* not primary key ID) is in the request body data.

    :param str study_id: the ID of the Stage Gate study that needs to be updated
    :param list data: the IDs of the selected activities that have been marked as complete
    """
    update_study(dict(checklist_study_complete=True), study_id)
    checklist_activities = ChecklistActivity.query.filter(ChecklistActivity.stage_gate_study_id == study_id)
    ca_activity_ids = [ca.activity_id for ca in checklist_activities]
    if bool(set(data).difference(ca_activity_ids)):
        return "Request body contains checklist activities with invalid activity IDs"
    for ca in checklist_activities:
        if ca.activity_id in data:
            ca.complete = True
        else:
            ca.complete = False
    db.session.commit()


def group_checklist_activities(checklist_activity_list, grouping_function):
    """
    Categorise a list of ChecklistActivity instances using the grouping function passed as an input argument.

    Uses the *itertools* groupby method in combination with the grouping_function.

    :param list checklist_activity_list: list of instances of \
    :class:`~dtop_stagegate.business.models.ChecklistActivity()`
    :param function grouping_function: function that returns the desired nested ChecklistActivity property with which \
    to perform the grouping
    :return: the grouped data with the keys equal to the parameter returned by the grouping_function and the values \
    equal to the list of activities that can be categorised by that parameter.
    :rtype: dict
    """
    grouped_data = {
        k: list(g) for k, g in itertools.groupby(
            checklist_activity_list,
            grouping_function
        )
    }

    return grouped_data


def group_checklist_activities_by_eval_area(eval_areas, checklist_activity_list):
    """
    Sort the checklist activity data by evaluation area name.

    A separate grouping procedure is required because an activity can be linked to more than one evaluation area.
    For each checklist activity in the list passed as an argument, loops through the evaluation areas associated with
    that activity and categorises based on the name of the evaluation area.

    :param list eval_areas: the list of evaluation areas originally retrieved using the \
    :meth:`~dtop_stagegate.business.frameworks.get_eval_areas()` method
    :param list checklist_activity_list: list of instances of \
    :class:`~dtop_stagegate.business.models.ChecklistActivity()`
    :return: the checklist activities categorised by evaluation area
    :rtype: dict
    """
    evaluation_area_dict = {ea['name']: [] for ea in eval_areas}
    for a in checklist_activity_list:
        for ea in a.activity.activity_eval_areas:
            evaluation_area_dict[ea.evaluation_area.name].append(a)

    return evaluation_area_dict


class ChecklistResults(object):
    """
    The main class for calculating the results of a Checklist Study.

    The only required input argument is the ID of the Stage Gate study to be assessed.
    """

    def __init__(self, stage_gate_study_id):
        """
        Class is initialised as follows:

        - the :class:`~dtop_stagegate.business.schemas.ActivitySchema()` schema is initialised,

        - the list of evaluation areas is retrieved by calling the \
         :meth:`~dtop_stagegate.business.frameworks.get_eval_areas()` method,

        - the checklist activities for the study ID are read and joined to the Activity database table,

        - the activities are sorted according to their stage using the :meth:`group_checklist_activities()` method and

        - the class loops through each of the stages, using the :class:`StageResults()` class to calculate the results \
        for each stage.

        :param str stage_gate_study_id: the ID of the stage gate study to be assessed.
        """
        self._study_id = stage_gate_study_id
        self._eval_areas = []
        self._activity_schema = ActivitySchema(many=True)
        self._checklist_activities = []
        self._stage_data = None
        self._results = []

        self._eval_areas = get_eval_areas()
        self._get_checklist_activities()
        self._stage_data = group_checklist_activities(self._checklist_activities, self._grouping_function)
        self._assess_stages()

    def _get_checklist_activities(self):
        """
        Query the ChecklistActivity database table for the specified stage gate study

        Joins to the Activity data model in order to access all the activity information.
        """
        self._checklist_activities = db.session.query(ChecklistActivity).join(Activity).\
            filter(ChecklistActivity.stage_gate_study_id == self._study_id).all()

    @staticmethod
    def _grouping_function(checklist_activity_instance):
        """Function for returning stage name when performing group-by operation"""
        return checklist_activity_instance.activity.activity_category.stage.name

    def _assess_stages(self):
        """ Calculate the results for each *stage*"""
        for stage_name, act_list in self._stage_data.items():
            self._results.append(StageResults(self, stage_name, act_list).data)

    @property
    def results(self):
        """
        The list of calculated checklist results for the stage gate study

        :rtype: list
        """
        return self._results

    @property
    def eval_areas(self):
        """
        List of all of the evaluation areas defined in the local database

        :rtype: list
        """
        return self._eval_areas

    @property
    def activity_schema(self):
        """
        The initialised marshmallow schema for the Activity model

        :rtype: dtop_stagegate.business.schemas.ActivitySchema()
        """
        return self._activity_schema


def calculate_activity_totals(activity_list):
    """
    Function for calculating the activity totals and percentages complete based on a given list of checklist activities.

    The number of activities that have been completed is divided by the total number of activities in the list.
    This value is multiplied by 100 and rounded to the nearest integer.

    If the total number of activities is equal to zero, returns a dictionary with 'total', 'complete' and 'percent'
    values all set to 0. During development when a dummy set of activities were being used, there were several
    evaluation areas that had no corresponding activities. This if-block that returns a dictionary of zeros is not
    expected to be used when the final stage gate metrics framework is implemented.

    :param list activity_list: list of instances of \
    :class:`~dtop_stagegate.business.models.ChecklistActivity()`
    :return: the summary totals dictionary with the total number of activities, completed number of activities and \
    the percentage of activities completed for the particular categorisation of activities.
    :rtype: dict
    """
    total_activities = len(activity_list)
    if total_activities == 0:
        return dict(total=0, complete=0, percent=0)
    complete_activities = len([ca for ca in activity_list if ca.complete])
    percent_complete = round(100 * (complete_activities/total_activities))

    return dict(total=total_activities, complete=complete_activities, percent=percent_complete)


class StageResults(object):
    """
    Calculate the results for a stage of a Stage Gate study based on the current state of the \
    :class:`~dtop_stagegate.business.models.ChecklistActivity` table.

    For each category (activity category and evaluation area), creates an instance of \
    :class:`StageCategoryAssessment()` class.
    """

    def __init__(self, checklist_results, stage_name, activity_list):
        """
        Class is initialised as follows:

        - the summary results for the stage are calculated, based on the total number of activities and activities \
        completed in the stage using the :meth:`calculate_activity_totals()` method,

        - the checklist result data are assessed by activity category,

        - the checklist result data are assessed by evaluation area and

        - the results data dictionary that summarises the above data is created.

        :param ChecklistResults checklist_results: instance of the main :class:`ChecklistResults()` class
        :param str stage_name: the name of the stage being assessed
        :param list activity_list: list of instances of \
        :class:`~dtop_stagegate.business.models.ChecklistActivity()`
        """
        self._checklist_results = checklist_results
        self._name = stage_name
        self._activity_list = activity_list
        self._percent_complete = None
        self._activity_category_summary = []
        self._evaluation_area_summary = []
        self._data = None

        # Constructor methods
        self._calculate_stage_summary()
        self._assess_activity_category()
        self._assess_evaluation_area()
        self._create_data_dict()

    def _calculate_stage_summary(self):
        """
        Calculate the summary results for the stage.

        Calculation based on the total number of activities and the number of activities completed for the specified
        stage. Uses the :meth:`calculate_activity_totals()` method. Sets the *_percent_complete* variable to be equal
        to the percent parameter of that method.
        """
        data = calculate_activity_totals(self._activity_list)
        self._percent_complete = data['percent']

    def _assess_activity_category(self):
        """
        Assess the checklist results by activity category.

        Creates an instance of :class:`StageCategoryAssessment()` class and uses the \
        :meth:`StageCategoryAssessment.assess()` method.

        Appends the summary results dictionary to *_activity_category_summary* for each activity category.
        """
        activity_category_assessment = StageCategoryAssessment(
            self._checklist_results,
            self._activity_list,
            'activity_category'
        )
        self._activity_category_summary = activity_category_assessment.assess()

    def _assess_evaluation_area(self):
        """
        Assess the checklist results by evaluation area.

        Creates an instance of :class:`StageCategoryAssessment()` class and uses the \
        :meth:`StageCategoryAssessment.assess()` method.

        Appends the summary results dictionary to *_evaluation_area_summary* for each activity category.
        """
        evaluation_area_assessment = StageCategoryAssessment(
            self._checklist_results,
            self._activity_list,
            'evaluation_area'
        )
        self._evaluation_area_summary = evaluation_area_assessment.assess()

    def _create_data_dict(self):
        """Create the dictionary that stores the results for the stage"""
        self._data = dict(
            name=self._name,
            percent_complete=self._percent_complete,
            activity_categories=self._activity_category_summary,
            evaluation_areas=self._evaluation_area_summary,
        )

    @property
    def data(self):
        """
        The results data dictionary for the stage

        :rtype: dict
        """
        return self._data


class StageCategoryAssessment(object):
    """
    A class for assessing the checklist results based on a stage *category*.

    The category can be either *activity category* or *evaluation area*.

    **Public methods**

    - :meth:`assess()` - perform the selected categorisation assessment procedure
    """

    def __init__(self, checklist_results, checklist_activities_list, stage_cat_type):
        """
        The class first groups the checklist activity data using either the :meth:`group_checklist_activities()` \
        method or the :meth:`group_checklist_activities_by_eval_area()`, depending on the chosen stage category.

        :param ChecklistResults checklist_results: instance of the main :class:`ChecklistResults()` class
        :param list checklist_activities_list: list of instances of \
        :class:`~dtop_stagegate.business.models.ChecklistActivity()`
        :param str stage_cat_type: selection of whether the stage category to be assess is *activity category* or \
         *evaluation area*
        """
        self._activity_schema = checklist_results.activity_schema
        self._eval_areas = checklist_results.eval_areas
        self._checklist_activities_list = checklist_activities_list
        self._type = stage_cat_type
        self._stage_cat_dict = {}
        self._summary = []

        self._group()

    def _group(self):
        """Group by activity category or evaluation area"""
        if self._type == 'activity_category':
            self._stage_cat_dict = group_checklist_activities(
                self._checklist_activities_list,
                self._activity_category_grouping_function
            )
        elif self._type == 'evaluation_area':
            self._stage_cat_dict = group_checklist_activities_by_eval_area(
                self._eval_areas,
                self._checklist_activities_list
            )

    @staticmethod
    def _activity_category_grouping_function(checklist_activity_instance):
        """Function for returning activity category name when performing group by operation"""
        return checklist_activity_instance.activity.activity_category.name

    def assess(self):
        """
        For each stage category; calculate the total number of activities and completed activities, percentages \
        and list of outstanding activities.

        :return: list of dictionaries, with each item corresponding to an individual stage category and containing the \
        summary results data
        :rtype: list
        """
        for cat_name, cat_list in self._stage_cat_dict.items():
            data = calculate_activity_totals(cat_list)
            data['name'] = cat_name
            outstanding_activities = [ca.activity for ca in cat_list if not ca.complete]
            data['outstanding'] = self._activity_schema.dump(outstanding_activities)
            self._summary.append(data)

        return self._summary
