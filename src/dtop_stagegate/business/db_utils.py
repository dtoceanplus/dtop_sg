# This is the Stage Gate module of the DTOceanPlus suite of tools
# Copyright (C) 2021 Wave Energy Scotland - Ben Hudson
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
import json

import click
from flask import current_app
from flask.cli import with_appcontext

from .models import StageGateStudy, Framework, Stage, ActivityCategory, Activity
from .schemas import LoadFrameworkSchema, RubricSchema, MetricSchema, EvaluationAreaSchema
from ..service import db


def init_db():
    """Initialise the database; drop all existing tables then create all tables from imported models"""
    db.drop_all()
    db.create_all()


def install_db():
    """Populate the sqlite database using the data stored in 'module parameters'."""

    # TODO: create an issue for beta version; no need for using json as input for db installation. Better to use CSV.
    #  Also need to edit models so that only the metric questions are duplicated when 'creating' a new framework.
    #  Everything else redundant
    # TODO: Can also clean up framework and children models by removing redundant 'further_details' property
    # TODO: Refactor to 'populate-db' (install is not quite right)

    add_json_collection_to_db(EvaluationAreaSchema, 'evaluation_areas')
    add_json_collection_to_db(MetricSchema, 'metrics')
    add_json_object_to_db(RubricSchema, 'rubrics')
    add_json_object_to_db(LoadFrameworkSchema, 'frameworks')


def add_json_object_to_db(schema, json_filename):
    """
    Add a JSON object to the database.

    :param schema: the marshmallow schema to use when loading (de-serialising) the raw JSON data
    :param str json_filename: the name of the JSON file stored in dtop_stagegate/business/module_parameters
    """
    model_schema = schema()
    json_data = load_json_params(json_filename)
    new_instance = model_schema.load(json_data)
    db.session.add(new_instance)
    db.session.commit()


def add_json_collection_to_db(schema, json_filename):
    """
    Add a list (collection) of JSON objects to the database.

    :param schema: the marshmallow schema to use when loading (de-serialising) the raw JSON data
    :param str json_filename: the name of the JSON file stored in dtop_stagegate/business/module_parameters
    """
    models_schema = schema(many=True)
    json_data = load_json_params(json_filename)
    new_instances = models_schema.load(json_data)
    db.session.bulk_save_objects(new_instances)
    db.session.commit()


def load_json_params(json_filename):
    """
    Load the JSON data stored in the *json_filename* file.

    :param str json_filename: the name of the JSON file to be loaded
    :return: the JSON data converted to a Python dictionary or list of Python dictionaries
    :rtype: dict or list
    """
    with current_app.open_resource("static/module_parameters/{}.json".format(json_filename)) as read_file:
        return json.load(read_file)


@click.command('init-db')
@with_appcontext
def init_db_command():
    """Clear the existing data and create new tables."""
    init_db()
    click.echo('Initialized the database.')


@click.command('install-db')
@with_appcontext
def install_db_command():
    """Clear the existing data, create new tables and populate the database with module parameters."""
    init_db()
    install_db()
    click.echo('Installed the database')


def init_app(app):
    """Add init_db_command to app.cli"""
    app.cli.add_command(init_db_command)
    app.cli.add_command(install_db_command)
