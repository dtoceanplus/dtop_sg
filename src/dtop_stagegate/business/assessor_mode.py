# This is the Stage Gate module of the DTOceanPlus suite of tools
# Copyright (C) 2021 Wave Energy Scotland - Ben Hudson
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
from dtop_stagegate.service import db
from .applicant_mode import ApplicantResults
from .models import AssessorScore, AssessorComment, StageGateAssessment, Question, QuestionMetric, QuestionCategory, \
    EvaluationArea, Metric

import pandas as pd


class AssessorStageGateData(object):
    """
    Join assessor scores and comments to the stage gate data, sorting by the question ID.

    Uses the :class:`~dtop_stagegate.business.applicant_mode.ApplicantResults` class.
    Specifically, calls the :meth:`~dtop_stagegate.business.applicant_mode.ApplicantResults.calculate` and
    :meth:`~dtop_stagegate.business.applicant_mode.ApplicantResults.get_results` methods.
    This yields the stage gate data with the calculated applicant results inserted for each question.
    Class then loops through this stage gate data and joins the corresponding
    :class:`~dtop_stagegate.business.models.AssessorScore` and :class:`~dtop_stagegate.business.models.AssessorComment`
    entries to the relevant questions.

    An instance of the :class:`~dtop_stagegate.business.models.StageGateAssessment` model is the only required input
    argument.

    **Public methods**

    - :meth:`join_data()` - join the stage gate data to the assessor scores and comments.
    """

    def __init__(self, stage_gate_assessment):
        """
        Class is initialised as follows:

        - the :class:`~dtop_stagegate.business.applicant_mode.ApplicantResults` class is initialised and the \
        :meth:`~dtop_stagegate.business.applicant_mode.ApplicantResults.calculate` and \
        :meth:`~dtop_stagegate.business.applicant_mode.ApplicantResults.get_results` methods are used to join the \
        stage gate data to the applicant results. The *responses* property is extracted from the applicant results.

        - the assessor scores are obtained for the specified Stage Gate Assessment ID \
        (queries the :class:`~dtop_stagegate.business.models.AssessorScore` database table)

        - the assessor comments are obtained for the specified Stage Gate Assessment ID \
        (queries the :class:`~dtop_stagegate.business.models.AssessorComment` database table)

        :param StageGateAssessment stage_gate_assessment: instance of the \
        :class:`~dtop_stagegate.business.models.StageGateAssessment` model
        """
        self._sg_assessment = stage_gate_assessment
        self._applicant_results = None
        self._assessor_score_list = []
        self._assessor_comment_list = []
        self.applicant_responses = None

        self._get_applicant_results()
        self._get_assessor_scores_by_sg_assessment_id()
        self._get_assessor_comments_by_sg_assessment_id()

    def _get_applicant_results(self):
        """Initialise the applicant results class, calculate the results and extract the responses from the results"""
        ar = ApplicantResults(self._sg_assessment)
        ar.calculate()
        self._applicant_results = ar.get_results()
        self.applicant_responses = self._applicant_results['responses']

    def _get_assessor_scores_by_sg_assessment_id(self):
        """Get a list of the assessor scores for the specified stage gate assessment ID"""
        self._assessor_score_list = AssessorScore.query.filter_by(
            stage_gate_assessment_id=self._sg_assessment.id
        ).all()

    def _get_assessor_comments_by_sg_assessment_id(self):
        """Get a list of the assessor comments for the specified stage gate assessment ID"""
        self._assessor_comment_list = AssessorComment.query.filter_by(
            stage_gate_assessment_id=self._sg_assessment.id
        ).all()

    def join_data(self):
        """
        Join the stage gate data to the assessor scores and comments.

        Loops through the applicant responses (which are in the same format as the stage gate data) and inserts the
        corresponding assessor scores and the list of assessor comments to the appropriate question in the nested
        dictionary.

        Adds a *score* property to the appropriate question.
        Adds a *comment* property to the appropriate scoring criterion.

        :return: the stage gate data, categorised by 'Question' and 'Question Category' for the specified \
        stage gate, and including the assessor scores and comments
        :rtype: dict
        """
        for q_cat in self.applicant_responses['question_categories']:
            for q in q_cat['questions']:
                a_score = next((x for x in self._assessor_score_list if x.question_id == q['id']), None)
                a_score_dict = {
                    'id': a_score.id,
                    'score': a_score.score,
                }
                q['score'] = a_score_dict
                for sc in q['scoring_criteria']:
                    a_comment = next(
                        (x for x in self._assessor_comment_list if x.scoring_criterion_id == sc['id']),
                        None
                    )
                    a_comment_dict = {
                        'id': a_comment.id,
                        'comment': a_comment.comment
                    }
                    sc['comment'] = a_comment_dict

        return self.applicant_responses


def update_assessor_data_in_db(sg_assessment, score_data, comment_data):
    """
    Update the assessor scores and comments in the local database for the specified Stage Gate Assessment ID.

    First attempts to update the assessor scores using the :meth:`update_assessor_scores_in_db` method.
    Breaks and returns an error message if this method fails.
    Then tries to update the assessor comments using the :meth:`update_assessor_comments_in_db` method.
    Also breaks and returns an error message if this method fails.
    Finally commits all the changes to the database.

    :param StageGateAssessment sg_assessment: instance of the \
    :class:`~dtop_stagegate.business.models.StageGateAssessment` model
    :param list score_data: instances of *assessor_scores* component of request body; containing *ID* and *score* of \
    the Assessor Scores to be updated
    :param list comment_data: instances of *assessor_comments* component of request body; containing the *ID* and \
    *comment* of the Assessor Comments to be updated
    """
    scores_err = update_assessor_scores_in_db(sg_assessment, score_data)
    if type(scores_err) is str:
        return scores_err
    comments_err = update_assessor_comments_in_db(sg_assessment, comment_data)
    if type(comments_err) is str:
        return comments_err
    db.session.commit()


def update_assessor_scores_in_db(sg_assessment, assessor_score_data):
    """
    Update the :class:`~dtop_stagegate.business.models.AssessorScore` database table.

    Update the appropriate entries in the :class:`~dtop_stagegate.business.models.AssessorScore` database table for the
    specified Stage Gate Assessment and using the *assessor_scores* component of the request body.
    Returns an error if any of the assessor score IDs in the request body data do not correspond to the list
    of scores that are linked to the specified stage gate assessment entry.
    Then loops through the *assessor_scores* in the request body data and updates the
    assessor scores in the corresponding database tables.

    :param StageGateAssessment sg_assessment: instance of the \
    :class:`~dtop_stagegate.business.models.StageGateAssessment` model
    :param list assessor_score_data: instances of *assessor_scores* component of request body; containing *ID* and \
    *score* of the Assessor Scores to be updated
    """
    assessor_scores_list = AssessorScore.query.filter_by(stage_gate_assessment_id=sg_assessment.id).all()
    as_ids = [a_score.id for a_score in assessor_scores_list]
    requested_ids = [score['id'] for score in assessor_score_data]
    if bool(set(requested_ids).difference(as_ids)):
        return "Request body contains assessor scores with invalid IDs"
    for a_score in assessor_score_data:
        as_id = a_score['id']
        as_db = AssessorScore.query.get(as_id)
        score = a_score.get('score', None)
        if score is not None:
            as_db.score = score


def update_assessor_comments_in_db(sg_assessment, assessor_comment_data):
    """
    Update the :class:`~dtop_stagegate.business.models.AssessorComment` database table.

    Update the appropriate entries in the :class:`~dtop_stagegate.business.models.AssessorComment` database table for
    the specified Stage Gate Assessment and using the *assessor_comments* component of the request body.
    Returns an error if any of the assessor comment IDs in the request body data do not correspond to the list
    of scores that are linked to the specified stage gate assessment entry.
    Then loops through the *assessor_comments* in the request body data and updates the
    assessor comments in the corresponding database tables.

    :param StageGateAssessment sg_assessment: instance of the \
    :class:`~dtop_stagegate.business.models.StageGateAssessment` model
    :param list assessor_comment_data: instances of *assessor_comments* component of request body; containing *ID* and \
    *comment* of the Assessor Scores to be updated
    """
    assessor_comments_list = AssessorComment.query.filter_by(stage_gate_assessment_id=sg_assessment.id).all()
    ac_ids = [ac.id for ac in assessor_comments_list]
    requested_ids = [c['id'] for c in assessor_comment_data]
    if bool(set(requested_ids).difference(ac_ids)):
        return "Request body contains assessor comments with invalid IDs"
    for a_comment in assessor_comment_data:
        ac_id = a_comment['id']
        ac_db = AssessorComment.query.get(ac_id)
        comment = a_comment.get('comment', None)
        if comment is not None:
            ac_db.comment = comment


def update_assessor_complete_status(sg_assessment):
    """
    Update the *assessor_complete* property of the :class:`~dtop_stagegate.business.models.StageGateAssessment`
    database entity for the specified Stage Gate Assessment.

    Sets the status to *True*.

    :param StageGateAssessment sg_assessment: instance of the \
    :class:`~dtop_stagegate.business.models.StageGateAssessment` model
    """
    sg_assessment.assessor_complete = True
    db.session.commit()


class AssessorResults(object):
    """
    Class for calculating the results of an Assessor Mode study.

    Summary

    - Retrieves the Assessor Score data from the local database, joining the assessor score data to other tables to \
    get the Question ID, the Question weighting, theQuestion Category name and the Evaluation Area name
    
    - Converts the joined SQL query to a Pandas DataFrame to be used as the basis of the calculation
    
    - Calculates the weighted score by multiplying the weighting by the assessor scorer for each question
    
    - Calculates the summary score for the entire stage gate, both a weighted average and the standard average value
    
    - Groups the questions by both Question Category and Evaluation Area and calculates the weighted averages and \
    averages for these groupings
    
    - Creates an instance of the :class:`AssessorStageGateData` class and calls the \
    :meth:`AssessorStageGateData.join_data` method to get the assessor stage gate data formatted by question category \
    and question
    
    Requires an instance of :class:`~dtop_stagegate.business.models.StageGateAssessment` object for initialising.
    
    **Public methods**
    
    - :meth:`calculate()` - perform the assessor results calculation
    - :meth:`get_results()` - return the assessor results in the format required for the front-end
    """

    def __init__(self, sg_assessment):
        """
        Class is initialised by querying the SQLite database for the AssessorScores, joining to the Question, 
        QuestionCategory and EvaluationArea tables and converting the result to a pandas DataFrame.

        For the specified Stage Gate assessment, the final table that is produced contains the following parameters:

        - AssessorScore
            - All columns

        - Question
            - ID
            - Weighting

        - Question Category
            - Name

        - Evaluation Area
            - Name

        :param StageGateAssessment sg_assessment: instance of the \
        :class:`~dtop_stagegate.business.models.StageGateAssessment` model
        """
        self._sg_assessment = sg_assessment
        self._sg_assessment_id = sg_assessment.id
        self._df = None
        self._overall_average = None
        self._overall_weighted_average = None
        self._q_cat_avg_df = None
        self._eval_area_avg_df = None
        self._question_category_results = None
        self._evaluation_area_results = None
        self._assessor_sg_data = None
        self._qa_ea_idx = 1

        self._get_assessor_score_data()

    def _get_assessor_score_data(self):
        """Query the AssessorScore and related database tables and convert executed SQL query to pandas DataFrame"""
        q = db.session.query(AssessorScore).\
            join(Question).\
            join(QuestionMetric, isouter=True).\
            join(Metric, isouter=True).\
            join(EvaluationArea, isouter=True).\
            join(QuestionCategory).\
            join(StageGateAssessment).\
            filter(StageGateAssessment.id == self._sg_assessment_id).\
            with_entities(
                Question.id.label('question_id'),
                AssessorScore.score,
                Question.weighting,
                QuestionCategory.name.label('question_category'),
                EvaluationArea.name.label('evaluation_area')
            )
        self._df = pd.read_sql(q.statement, db.engine)

    def calculate(self):
        """
        Calculation is composed of five methods:

        - The weighted scores are calculated by multiplying the weighting of each question by the assessor score and \
        dividing by 100 (weightings are in percent)

        - The overall average is calculated as the mean of the raw assessor scores

        - The overall weighted average is calculated as the sum of the weighted scores (this is possible because the \
        weightings are expressed as a decimal number between 0 and 1---see e.g. 'Convex combination example' section \
        on https://en.wikipedia.org/wiki/Weighted_arithmetic_mean)

        - The results for each question category and evaluation area are calculated by grouping by the appropriate \
        category and calculating the mean of the raw assessor score and the sum of the weighted scores. The weighted \
        average for each category is then calculated by dividing the total weighted score by the sum of the weightings \
        for that category. This is equivalent to dividing the total weighted score by the total possible weighted \
        score for that category and multiplying the resulting ratio by the highest score in the rubric.

        - For each category, the grouping and calculation method also involves formatting the results data in the \
        appropriate format for the front-end

        - Finally, the Assessor Stage Gate data is retrieved by calling the \
        :meth:`AssessorStageGateData.join_data()` method.
        """
        self._calculate_weighted_scores()
        self._calculate_total_averages()
        self._question_category_results = self._groupby_and_calculate_averages('question_category')
        self._evaluation_area_results = self._groupby_and_calculate_averages('evaluation_area')
        self._assessor_sg_data = AssessorStageGateData(self._sg_assessment).join_data()

    def _calculate_weighted_scores(self):
        """Calculate weighted score by multiplying the weighting by the score of each question and dividing by 100"""
        self._df['weighted_score'] = self._df['score'] * self._df['weighting'] / 100.

    def _calculate_total_averages(self):
        """Calculate the average as the mean of the raw scores and the sum of the weighted scores"""
        self._overall_average = self._df['score'].mean()
        self._overall_weighted_average = self._df['weighted_score'].sum()

    def _groupby_and_calculate_averages(self, category):
        """
        Groupby the specified category and calculate the average and weighted average for that category.

        :param str category: the grouping category; either 'question_category' or 'evaluation_area'
        :return: the formatted results for the specified grouping category
        :rtype: list
        """
        df = self._df.groupby(category).agg({
            'score': 'mean',
            'weighting': 'sum',
            'weighted_score': 'sum'
        })
        df['Weighted average'] = df['weighted_score']/(df['weighting']/100.)
        df = df.rename(columns={'score': 'Average'})
        if category == 'question_category':
            df = df[~df.index.str.contains('Metrics - ')]
        averages = self._format_bar_chart_data(df)

        return averages

    def _format_bar_chart_data(self, df):
        """
        Format the average and weighted average data for the front-end.

        :param pd.DataFrame df:
        :return: the assessor scores for a group formatted for the front-end
        :rtype: list
        """
        formatted_results = []
        for title in ['Weighted average', 'Average']:
            d = {
                'id': self._qa_ea_idx,
                'name': title,
                'orientation': "h",
                "type": "bar",
                'x': list(df[title])[::-1],  # TODO: this is losing precision. FIX during beta development
                'y': list(df.index)[::-1]
            }
            formatted_results.append(d)
            self._qa_ea_idx += 1

        return formatted_results

    def get_results(self):
        """
        Format and retrieve the Assessor Results data.

        Output dictionary contains:

        - *overall* - the summary average and weighted average results for the entire stage gate

        - *question_categories* - the average and weighted average results for the data as grouped by Question Category

        - *evaluation_areas* - the average and weighted average results for the data as grouped by Evaluation Area

        - *responses* - the Assessor Stage Gate data ordered by Question Category

        :return: the formatted Assessor Results
        :rtype: dict
        """
        r = {
            'overall': [{
                'id': 0,
                'x': [self._overall_average, self._overall_weighted_average],
                'y': ['Average', 'Weighted average'],
                'name': 'Stage Gate score',
                'orientation': 'h'
            }],
            'question_categories': self._question_category_results,
            'evaluation_areas': self._evaluation_area_results,
            'responses': self._assessor_sg_data
        }

        return r

    @property
    def overall_average(self):
        """
        The overall average assessor score for the stage gate assessment.

        :rtype: float
        """
        return self._overall_average

    @property
    def overall_weighted_average(self):
        """
        The overall weighted average assessor score for the stage gate assessment.

        :rtype: float
        """
        return self._overall_weighted_average

    @property
    def question_category_results(self):
        """
        The average and weighted average assessor scores categorised by question category.

        :rtype: list
        """
        return self._question_category_results

    @property
    def evaluation_area_results(self):
        """
        The average and weighted average assessor scores categorised by evaluation area.

        :rtype: list
        """
        return self._evaluation_area_results
