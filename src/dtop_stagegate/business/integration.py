# This is the Stage Gate module of the DTOceanPlus suite of tools
# Copyright (C) 2021 Wave Energy Scotland - Ben Hudson
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
import os
import requests

from dtop_stagegate.business.models import ApplicantAnswer, Question, QuestionMetric
from dtop_stagegate.service import db


def extract_entity_id(entity_list, module_id):
    """
    Find the entity ID in the list of registered entities for a specified module ID

    :param list entity_list: the list of entities registered from the Main Module
    :param int module_id: the ID of the module
    :return: the entity ID for the specified module
    :rtype: int
    """
    return next((item['entityId'] for item in entity_list if item["moduleId"] == module_id), None)


def get_url_and_headers(module):
    """
    Common function provided by OCC to get DTOP basic module URL and headers

    :param str module: the DTOP module nickname (e.g. 'mm')
    :return: the module API url and the headers instance
    :rtype: tuple
    """
    protocol = os.getenv('DTOP_PROTOCOL', 'http')
    domain = os.environ['DTOP_DOMAIN']
    auth = os.getenv('DTOP_AUTH', '')

    print()
    print("Authorization header value :")
    header_auth = f'{auth}'
    print(header_auth)

    print()
    print("Headers - auth :")
    headers = {'Authorization': header_auth}
    print(headers)

    print()
    print("Host header value :")
    header_host = f'{module + "." + domain}'
    print(header_host)

    server_url = f'{protocol + "://" + header_host}'

    print()
    print("Check if DTOP is deployed on DNS resolved server or on workstation :")

    try:
        response = requests.get(server_url, headers=headers)
        print('DTOP appears to be deployed on DNS resolved server')
        module_api_url = f'{server_url}'
    except requests.ConnectionError as exception:
        print('DTOP appears to be deployed on workstation')
        docker_ws_ip = "http://172.17.0.1:80"
        module_api_url = f'{docker_ws_ip}'
        print()
        print("Headers - auth and localhost :")
        headers['Host'] = header_host
        print(headers)

    print()
    print("Basic Module Open API URL :")
    print(module_api_url)
    print()

    return module_api_url, headers


def call_intermodule_service(module, uri):
    """
    Generic function for communicating directly with the BE of other modules.

    Uses the *get_url_and_headers* function.
    Leads to a 400 error if the module server is not available.
    If the request is unsuccessful, a HTTP error is returned.

    :param str module: the nickname for the module that is being communicated with
    :param str uri: the URI specifying the appropriate route to call for the metric
    :return: the request response if successful, tuple of error and status code otherwise
    :rtype: requests.Response or tuple
    """
    print()
    print("Starting intermodule service call")

    print()
    api_url, headers = get_url_and_headers(module)
    print("API URL:")
    users_url = f'{api_url + uri}'
    print(users_url)

    print()
    print(f"Making request from SG to {module.upper()}:")
    print()

    try:
        resp = requests.get(users_url, headers=headers)
        resp.raise_for_status()
        print()
        return resp
    except requests.exceptions.ConnectionError as errc:
        print("Error Connecting:", errc)
        print("The module server is not available")
        return errc, 400
    except requests.exceptions.HTTPError as errh:
        print("HTTP Error:", errh)
        return errh, errh.response.status_code


class MetricExtractor:
    """
    Find the entities from other modules that are registered to the same Project and Study and extract metric results

    Get the appropriate URL for each metric and use integration business logic to make requests, get metrics and
    store results in DB.

    Module IDs as defined in the main module:

        1 = SC,
        2 = MC,
        3 = EC,
        4 = ET,
        5 = ED,
        6 = SK,
        7 = LMO,
        8 = SPEY,
        9 = RAMS,
        10 = SLC,
        11 = ESA,
        12 = SI,
        13 = SG.

    **Public methods**

    - :meth:`extract()` - issue the GET requests to other modules and extract metric results

    :param list entity_list: the list of entities registered from the Main Module
    :param str sg_assess_id: the ID of the Stage Gate Assessment database entity
    """

    def __init__(self, sg_assess_id, entity_list):
        """
        As part of class initialisation, the ApplicantAnswer database table is searched for the appropriate applicant
        answers for the specified Stage Gate Assessment ID.

        :param list entity_list: the list of entities registered from the Main Module
        :param str sg_assess_id: the ID of the Stage Gate Assessment database entity
        """
        self._sg_assess_id = sg_assess_id
        self._extractor_functions = (
            get_lmo_metrics,
            get_spey_metrics,
            get_slc_metrics,
            get_esa_metrics,
            get_rams_metrics
        )
        self.status = {
            'lmo': [],
            'spey': [],
            'slc': [],
            'esa': [],
            'rams': []
        }
        self.entity_list = entity_list
        self.app_answer = db.session.query(ApplicantAnswer).join(Question).join(QuestionMetric).filter(
            ApplicantAnswer.stage_gate_assessment_id == self._sg_assess_id
        )

    def extract(self):
        """
        Loop through the metric extractor functions, issue the GET requests to other modules and extract metric results

        :return: the status of the extraction process for each metric
        :rtype: dict
        """
        for f in self._extractor_functions:
            f(self)

        db.session.commit()

        return {'metric_status': self.status}


class JsonExtractor(object):
    """
    Extract required data from the JSON response obtained from a provider module.

    Only extracts the JSON data if a status code of 200 is returned.

    Requires the appropriate URL for the provider API request to be initialised.
    Also accepts an optional *metric* argument that specifies the specific metric to extract from the JSON response.

    **Public methods**

    - :meth:`extract_data()` - extract the required metric from the JSON response data
    """

    def __init__(self, uri, module, metric=None):
        """
        Class is initialised by

        - making the get request,

        - checking the status code of the response and,

        - if a 200 success code is returned, then extracts the JSON data from the response.

        :param str uri: the API route for a provider module
        :param str metric: the name of the metric to extract from the JSON data
        """
        self._uri = uri
        self._module = module
        self._metric = metric
        self._response = None
        self._json = None
        self._successful_response = False

        self._make_get_request()

    def _make_get_request(self):
        """Make the get request and check the status code of the response. If equal to 200 then extract the JSON data"""
        self._response = call_intermodule_service(self._module, self._uri)
        if type(self._response) is tuple:
            self._code = self._response[1]
        else:
            self._code = self._response.status_code
            if self._code == 200:
                self._successful_response = True
                self._get_json()

    def _get_json(self):
        """Extract the JSON data from the response"""
        self._json = self._response.json()

    def extract_data(self):
        """
        Extract the value of the specified metric from the JSON data.

        :return: int or float
        """
        return self._json.get(self._metric, 'None')

    @property
    def successful_response(self):
        """
        Boolean value for whether a successful response with status code 200 was received in the API response

        :rtype: bool
        """
        return self._successful_response

    @property
    def json(self):
        """
        The response JSON data de-serialised to a Python dictionary

        :rtype: dict
        """
        return self._json

    @property
    def code(self):
        """
        The status code of the API response.

        :rtype: int
        """
        return self._code


def get_metric(uri, module, metric):
    """
    Extract a specified metric from the response of a provider API call.

    Uses the :class:`JsonExtractor` class to perform the extraction.
    Returns the requested metric value if a successful response is returned.
    Otherwise returns an error message and the status code of the unsuccessful response

    :param str uri: the URI for a provider module
    :param str module: the DTOP module nickname (e.g. 'mm')
    :param str metric: the name of the metric to extract from the JSON data
    :return: the request metric value or a tuple containing an error message and the unsuccessful status code
    :rtype: int or float or tuple
    """
    json_extractor = JsonExtractor(uri, module, metric)
    if json_extractor.successful_response:
        metric = json_extractor.extract_data()
        return metric
    return 'Error', json_extractor.code


def get_multiple_metrics(url, module, metric_list):
    """
    Extract multiple metrics from the response of a provider API call.

    Uses the :class:`JsonExtractor` class to load the response data and check the status code.
    Returns the requested metric values if a successful response is returned.
    Otherwise returns an error message and the status code of the unsuccessful response

    :param str url: the API route for a provider module
    :param str module: the DTOP module nickname (e.g. 'mm')
    :param list metric_list: the list of metrics to extract from the JSON data
    :return: the requested metric values or a tuple containing an error message and the unsuccessful status code
    :rtype: dict or tuple
    """
    json_extractor = JsonExtractor(url, module)
    if json_extractor.successful_response:
        json = json_extractor.json
        metrics = {m.lower(): json.get(m) for m in metric_list}
        return metrics

    return 'Error', json_extractor.code


def update_metric_result(app_answer, metric_id, metric_result):
    """
    Update the result for a specified metric and set of applicant answers in the database

    Filters the set of applicant answers to get the appropriate QuestionMetric and sets the result as specified in the
    input arguments.
    Some metrics are only available at later Stages (and complexity levels), so there is an exception catcher that
    checks for an IndexError in the case that the specified metric is not request at the Stage in question.

    :param dtop_stagegate.business.models.ApplicantAnswer app_answer: list of Applicant Answer entities for the \
    relevant Stage Gate Assessment instance
    :param int metric_id: the ID of the metric to update
    :param float metric_result: the extracted metric result
    """
    qm = app_answer.filter(QuestionMetric.metric_id == metric_id).all()
    try:
        qm[0].result = metric_result
    except IndexError:
        # Some metrics only available at later Stages (complexity levels)
        raise


def update_single_metric(app_answer, extractor_function, uri, metric_id):
    """
    Update a single metric in the database

    Use the specified extractor function and call it with the url argument to make the get request to the appropriate
    module.
    As long as the extractor function does not return an error message (a tuple), then update the metric result in the
    database.

    :param dtop_stagegate.business.models.ApplicantAnswer app_answer: list of Applicant Answer entities for the \
    relevant Stage Gate Assessment instance
    :param function extractor_function: the appropriate extractor function to call the get request for a metric
    :param string uri: the URI for the metric
    :param int metric_id: the ID of the metric
    """
    metric = extractor_function(uri)
    if type(metric) is tuple:
        return 'Error', 'Failed in GET request'
    else:
        try:
            update_metric_result(app_answer, metric_id, metric)
            return 'Success', 'Metric updated'
        except IndexError:
            return 'Warning', 'Metric not required for this Stage Gate'


def update_multiple_metrics(app_answer, extractor_function, url, metric_map):
    """
    Update multiple metrics in the database.

    For the case where multiple metrics are extracted from a single GET request to another module.
    Loops through the metric_map supplied as an argument and updates the metric result for each metric detailed in the
    metric map.

    :param dtop_stagegate.business.models.ApplicantAnswer app_answer: list of Applicant Answer entities for the \
    relevant Stage Gate Assessment instance
    :param function extractor_function: the appropriate extractor function to call the get request for a metric
    :param string url: the URL route for the metric
    :param list metric_map: list of mappings between metric IDs and metric names
    """
    metric = extractor_function(url)
    if type(metric) is tuple:
        return [{
            'metric_id': m['id'],
            'status': 'Error',
            'message': 'Failed in GET request'
        } for m in metric_map]
    else:
        messages = []
        for m in metric_map:
            try:
                update_metric_result(app_answer, m['id'], metric[m['metric']])
                messages.append(dict(metric_id=m['id'], status='Success', message='Metric updated'))
            except IndexError:
                messages.append(dict(
                    metric_id=m['id'],
                    status='Warning',
                    message='Metric not required for this Stage Gate'
                ))

        return messages


def get_lmo_metrics(metric_extractor):
    """Make requests to LMO module, extract the required metrics and save these to the database"""
    lmo_id = extract_entity_id(metric_extractor.entity_list, 7)
    if lmo_id:
        for m in [
            # (url, metric_id)
            (f"/api/{lmo_id}/phases/installation/cost", 8),
            (f"/api/{lmo_id}/phases/installation/duration", 7),
            (f"/api/{lmo_id}/phases/maintenance/duration", 13)
        ]:
            s, msg = update_single_metric(metric_extractor.app_answer, lmo_metric, *m)
            metric_extractor.status['lmo'].append(dict(metric_id=m[1], status=s, message=msg))
    else:
        metric_extractor.status['lmo'] = [
            {'metric_id': m_id, 'status': 'Error', 'message': 'No LMO entity has been registered'}
            for m_id in (8, 7, 13)
        ]


def lmo_metric(uri):
    """
    Extract metric from LMO module

    Stage Gate expects to consume three metrics from LMO

    - installation cost: "/api/1/phases/installation/cost"

    - installation duration: "/api/1/phases/installation/duration"

    - maintenance duration: "/api/1/phases/maintenance/duration"

    :param str uri: the URI for the appropriate LMO API route
    :return: the extracted value of the metric
    :rtype: float
    """
    metric = get_metric(uri, 'lmo', 'value')

    return metric


class SpeyDataHandler(object):
    """
    Data-handler class for extracting the required metrics from the SPEY *efficiency* API response

    Extracts the annual captured energy, annual transformed energy and annual delivered energy parameters from the
    inputs component of the SPEY *efficiency* API response.
    Extracts the array captured efficiency, array relative transformed efficiency and array relative delivered
    efficiency from the outputs component of the SPEY *efficiency* API response.

    Requires the JSON response data from the API request to be initialised.

    **Public methods**

    - :meth:`extract_data()` - extract the required metric from the JSON response data
    """

    EFFICIENCY_INPUTS = [
        "annual_captured_energy",
        "annual_transformed_energy",
        "annual_delivered_energy"
    ]
    EFFICIENCY_OUTPUTS = [
        "array_capt_eff",
        "array_rel_transf_eff",
        "array_rel_deliv_eff"
    ]

    def __init__(self, json):
        """
        Initialised with the JSON response data from the API request

        :param dict json: JSON response data from the API request
        """
        self._json = json
        self._spey_metrics = {}

    def _extract_metric_values(self, io_key, io_list):
        """Use the class constants to extract metrics from the input and output components of the response"""
        metric_dict = self._json.get(io_key)
        for m in io_list:
            self._spey_metrics[m] = metric_dict.get(m).get('value')

    def extract_data(self):
        """Perform the extraction of the input and output data from the SPEY *efficiency* API response"""
        self._extract_metric_values('Inputs_EFF', self.EFFICIENCY_INPUTS)
        self._extract_metric_values('Outputs_EFF', self.EFFICIENCY_OUTPUTS)

        return self._spey_metrics


def get_spey_metrics(metric_extractor):
    """Make requests to SPEY module, extract the required metrics and save these to the database"""
    spey_id = extract_entity_id(metric_extractor.entity_list, 8)
    spey_metric_ids = range(1, 7)
    if spey_id:
        spey = spey_efficiency_metrics(f"/spey/{spey_id}/efficiency")
        if type(spey) is tuple:
            # Failed GET request
            metric_extractor.status['spey'] = [{
                'metric_id': m,
                'status': 'Error',
                'message': 'Failed in GET request'
            } for m in spey_metric_ids]
        else:
            # Add SPEY metrics to DB
            for m in [
                (1, spey['annual_captured_energy']),
                (2, spey['annual_transformed_energy']),
                (3, spey['annual_delivered_energy']),
                (4, spey['array_capt_eff']),
                (5, spey['array_rel_transf_eff']),
                (6, spey['array_rel_deliv_eff'])
            ]:
                try:
                    update_metric_result(metric_extractor.app_answer, *m)
                    metric_extractor.status['spey'].append(dict(
                        metric_id=m[0],
                        status='Success',
                        message='Metric updated'
                    ))
                except IndexError:
                    metric_extractor.status['spey'].append(dict(
                        metric_id=m[0],
                        status='Warning',
                        message='Metric not required for this Stage Gate')
                    )
    else:
        metric_extractor.status['spey'] = [
            {'metric_id': m_id, 'status': 'Error', 'message': 'No SPEY entity has been registered'}
            for m_id in spey_metric_ids
        ]


def spey_efficiency_metrics(url):
    """
    Extract the SPEY efficiency metrics from the SPEY.

    Loads the JSON response data using the :class:`JsonExtractor` class.
    If a successful response is provided, then uses the :class:`SpeyDataHandler` class to perform the extraction.
    Otherwise returns an error message and the status code of the unsuccessful response.

    :param str url: the API route for a provider module
    :return: the requested metric values or a tuple containing an error message and the unsuccessful status code
    :rtype: dict or tuple
    """
    json_extractor = JsonExtractor(url, 'spey')
    if json_extractor.successful_response:
        spey_metrics = SpeyDataHandler(json_extractor.json).extract_data()
        return spey_metrics

    return 'Error', json_extractor.code


def get_slc_metrics(metric_extractor):
    """Make requests to SLC module, extract the required metrics and save these to the database"""
    slc_id = extract_entity_id(metric_extractor.entity_list, 10)
    if slc_id:
        for m in [
            # (func, url, metric_id)
            (slc_capex, f"/api/projects/{slc_id}/economical/capex", 16),
            (slc_irr, f"/api/projects/{slc_id}/financial/irr", 20),
            (slc_lcoe, f"/api/projects/{slc_id}/economical/lcoe", 15),
            (slc_opex, f"/api/projects/{slc_id}/economical/opex", 17)
        ]:
            s, msg = update_single_metric(metric_extractor.app_answer, *m)
            metric_extractor.status['slc'].append(dict(metric_id=m[2], status=s, message=msg))

        messages = update_multiple_metrics(
            metric_extractor.app_answer,
            slc_cost_metrics_per_kw,
            f"/api/projects/{slc_id}/benchmark/metrics",
            [
                {
                    'id': 18,
                    'metric': 'capex_kw'
                },
                {
                    'id': 19,
                    'metric': 'opex_kw'
                }
            ]
        )
        metric_extractor.status['slc'] += messages
    else:
        metric_extractor.status['slc'] = [
            {'metric_id': m_id, 'status': 'Error', 'message': 'No SLC entity has been registered'}
            for m_id in (16, 20, 15, 17, 18, 19)
        ]


def slc_capex(url):
    """Extract the CapEx metric from the SLC module"""
    capex = get_metric(url, 'slc', 'capex')

    return capex


def slc_irr(url):
    """Extract the internal rate of return metric from the SLC module"""
    irr = get_metric(url, 'slc', 'irr')

    return irr


def slc_lcoe(url):
    """Extract the levelised cost of energy metric from the SLC module"""
    lcoe = get_metric(url, 'slc', 'lcoe')

    return lcoe


def slc_cost_metrics_per_kw(url):
    """Extract the CapEx per kW and OpEx per kW per year metrics from the SLC module"""
    cost_metric_labels = [
        'capex_kw',
        'opex_kw'
    ]
    cost_metrics_per_kw = get_multiple_metrics(url, 'slc', cost_metric_labels)

    return cost_metrics_per_kw


def slc_opex(url):
    """
    Extract the total OpEx metric from the SLC module.

    Loads the JSON response data using the :class:`JsonExtractor` class.
    If a successful response is provided, then returns the **total** OpEx value from the nested JSON response.
    Otherwise returns an error message and the status code of the unsuccessful response.

    :param str url: the API route for a provider module
    :return: the requested metric value or a tuple containing an error message and the unsuccessful status code
    :rtype: int or float or tuple
    """
    json_extractor = JsonExtractor(url, 'slc')
    if json_extractor.successful_response:
        opex = json_extractor.json.get('opex').get('total')
        return opex

    return 'Error', json_extractor.code


def get_esa_metrics(metric_extractor):
    """Make requests to ESA module, extract the required metrics and save these to the database"""
    esa_id = extract_entity_id(metric_extractor.entity_list, 11)
    if esa_id:
        for m in [
            # (
            #   func,
            #   url,
            #   metric_mapping_list
            # )
            (
                esa_carbon_footprint_metrics,
                f"/esa/{esa_id}/carbon_footprint/CFP_global",
                [dict(id=21, metric='gwp'), dict(id=22, metric='ced'), dict(id=23, metric='epp')]
            ),
            (
                esa_eia_metrics,
                f"/esa/{esa_id}/environmental_impact_assessment/eia_global",
                [dict(id=25, metric='negative_impact'), dict(id=26, metric='positive_impact')]
            ),
            (
                esa_social_acceptance_metrics,
                f"/esa/{esa_id}/social_acceptance",
                [dict(id=24, metric='nb_vessel_crew'), dict(id=27, metric='cost_of_consenting')]
            )
        ]:
            messages = update_multiple_metrics(metric_extractor.app_answer, *m)
            metric_extractor.status['esa'] += messages
    else:
        metric_extractor.status['esa'] = [
            {'metric_id': m_id, 'status': 'Error', 'message': 'No ESA entity has been registered'}
            for m_id in (21, 22, 23, 25, 26, 24, 27)
        ]


def esa_carbon_footprint_metrics(url):
    """Extract the Global Warming Participation and Cumulative Energy Demand metrics from the ESA module"""
    cfp_metric_labels = [
        'GWP',
        'CED',
        'EPP'
    ]
    cfp_metrics = get_multiple_metrics(url, 'esa', cfp_metric_labels)

    return cfp_metrics


def esa_eia_metrics(url):
    """Extract the Environmental Impact Assessment scores from the ESA module"""
    eia_metric_labels = [
        'negative_impact',
        'positive_impact'
    ]
    eia_metrics = get_multiple_metrics(url, 'esa', eia_metric_labels)

    return eia_metrics


def esa_social_acceptance_metrics(url):
    """Extract the social acceptance metrics from the ESA module"""
    sa_metric_labels = [
        'nb_vessel_crew',
        'cost_of_consenting'
    ]
    sa_metrics = get_multiple_metrics(url, 'esa', sa_metric_labels)

    return sa_metrics


def get_rams_metrics(metric_extractor):
    """Make requests to RAMS module, extract the required metrics and save these to the database"""
    rams_id = extract_entity_id(metric_extractor.entity_list, 9)
    rams_metric_ids = [9, 10, 11, 12, 14]
    if rams_id:
        rams = rams_metrics(f"/rams/results_summary/{rams_id}")
        if type(rams) is tuple:
            # Failed GET request
            metric_extractor.status['rams'] = [{
                'metric_id': m,
                'status': 'Error',
                'message': 'Failed in GET request'
            } for m in rams_metric_ids]
        else:
            # Extract RAMS metrics
            rams_data = extract_rams_data(rams)
            # Add RAMS metrics to DB
            for rd in rams_data:
                if rd[1]:
                    update_rams_metric(metric_extractor, rd[0], rd[1])
    else:
        metric_extractor.status['rams'] = [
            {'metric_id': m_id, 'status': 'Error', 'message': 'No RAMS entity has been registered'}
            for m_id in rams_metric_ids
        ]


def rams_metrics(url):
    """Extract JSON data from the summary of results service from the RAMS module"""
    json_extractor = JsonExtractor(url, 'rams')
    if json_extractor.successful_response:
        data = json_extractor.json
        return data

    return 'Error', json_extractor.code


def extract_rams_data(rams_json):
    """
    Extract relevant rams metrics from RAMS API response, in preparation for adding metric results to database

    There is the possibility of the *results-summary* JSON response only being partially filled, so each extraction is
    wrapped in a try block that catches potential key errors.

    :param dict rams_json: the JSON response from RAMS *results-summary* service
    :return: list of tuples, each containing the metric result and the metric ID, metric result is False if a KeyError \
    is raised, in which case that metric will not be updated in the database, but a warning message will be added \
    to the metric extractor status for that metric.
    :rtype: list
    """
    metrics = []
    try:
        mttf_array = rams_json['reliability_system']['mttf_array']
        metrics.append((mttf_array, 9))
    except KeyError:
        metrics.append((False, 9))
    try:
        prob_failure_array = rams_json['reliability_system']['max_annual_pof_array']
        metrics.append((prob_failure_array, 10))
    except KeyError:
        metrics.append((False, 10))
    try:
        availability_array = rams_json['availability']['availability_array']
        metrics.append((availability_array, 11))
    except KeyError:
        metrics.append((False, 11))
    try: 
        maintenance_probabilities = rams_json['maintainability']['probability_maintenance'][0]
        metrics.append((maintenance_probabilities, 12))
    except KeyError:
        metrics.append((False, 12))
    try:
        survival_probabilities = calculate_min_uls(rams_json)
        metrics.append((survival_probabilities, 14))
    except KeyError:
        metrics.append((False, 14))

    return metrics


def calculate_min_uls(data):
    """
    Calculate the minimum value of the survival probability from the ET and SK sub-systems

    :param dict data: extracted JSON response containing RAMS metrics
    :return: minimum of probabilities for ET and SK sub-systems
    :rtype: float or None
    """
    try:
        uls = data['survivability_uls']
        et = uls['ET_Subsystem']['survival_uls'][0]
        sk = uls['SK_Subsystem']['survival_uls'][0]
        return min(et, sk)
    except TypeError:
        return None


def update_rams_metric(metric_extractor, metric_result, metric_id):
    """
    Update a RAMS metric result in the DB and the status in the metric extractor instance

    If the metric result is False, adds a warning message saying that the metric has not been computed.
    Otherwise, tries to update the metric result in the DB.
    Also adds a successful status message to the MetricExtractor instance if successful.
    In case of an IndexError, adds a warning message to the MetricExtractor instance informing that the requested
    metric is not required for the Stage Gate (only the case for SG 0 - 1).

    :param MetricExtractor metric_extractor: instance of the metric extractor class, containing the status dictionary
    :param float metric_result: the result of the metric obtained from the RAMS module
    :param int metric_id: the ID of the metric from the DB
    :return:
    """
    if metric_result is False:
        metric_extractor.status['rams'].append(dict(
            metric_id=metric_id,
            status='Warning',
            message='Metric result has not been computed in RAMS')
        )
    else:
        try:
            update_metric_result(metric_extractor.app_answer, metric_id, metric_result)
            metric_extractor.status['rams'].append(
                dict(metric_id=metric_id, status='Success', message='Metric updated')
            )
        except IndexError:
            metric_extractor.status['rams'].append(dict(
                metric_id=metric_id,
                status='Warning',
                message='Metric not required for this Stage Gate')
            )
