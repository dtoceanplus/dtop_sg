# This is the Stage Gate module of the DTOceanPlus suite of tools
# Copyright (C) 2021 Wave Energy Scotland - Ben Hudson
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
from ..service import db

from sqlalchemy.orm import backref


class StageGateStudy(db.Model):
    """
    An SQLAlchemy database model class for a Stage Gate study.
    """

    id = db.Column(db.Integer, primary_key=True)
    """The ID of the Stage Gate study"""
    name = db.Column(db.String(64), index=True, nullable=False)
    """The name of the Stage Gate study"""
    description = db.Column(db.String(120), index=True)
    """A description of the Stage Gate study"""
    framework_id = db.Column(db.Integer, db.ForeignKey("framework.id"), nullable=False)
    """The ID of the framework that is linked to the Stage Gate study"""
    framework = db.relationship("Framework", uselist=False)
    """The relationship to the framework database table; a Stage Gate study can be linked to only one framework"""
    checklist_study_complete = db.Column(db.Boolean, default=False)
    """Indication of whether the checklist study has been performed or not"""


class Framework(db.Model):
    """SQLAlchemy database model for a Stage Gate framework"""

    id = db.Column(db.Integer, primary_key=True)
    """The ID of the Stage Gate metric framework"""
    name = db.Column(db.String(64), index=True, unique=True, nullable=False)
    """The name of the Stage Gate metric framework"""
    description = db.Column(db.String(120), index=True)
    """A description of the Stage Gate metric framework"""


class Stage(db.Model):
    """SQLAlchemy database model for the Stages in a Stage Gate framework"""

    id = db.Column(db.Integer, primary_key=True)
    """The ID of the Stage Gate stage"""
    name = db.Column(db.String(64), index=True, nullable=False)
    """The name of the Stage Gate stage"""
    description = db.Column(db.String(120), index=True)
    """A description of the Stage Gate stage"""
    number = db.Column(db.Integer)
    """The number of the Stage; required for order of stages"""
    framework_id = db.Column(db.Integer, db.ForeignKey("framework.id"))
    """The ID of the framework to which the Stage belongs"""
    framework = db.relationship("Framework", backref=backref('stages', cascade="all,delete"))
    """The relationship to the framework database table"""


class ActivityCategory(db.Model):
    """SQLAlchemy database model for the Activity Categories in a Stage Gate framework"""

    id = db.Column(db.Integer, primary_key=True)
    """The ID of the Stage Gate activity category"""
    name = db.Column(db.String(64), index=True, nullable=False)
    """The name of the Stage Gate activity category"""
    description = db.Column(db.String(120), index=True)
    """A description of the Stage Gate activity category"""
    stage_id = db.Column(db.Integer, db.ForeignKey("stage.id"))
    """The ID of the Stage to which the activity category belongs"""
    stage = db.relationship("Stage", backref=backref("activity_categories", cascade="all,delete"))
    """The relationship to the stage database table"""


class Activity(db.Model):
    """SQLAlchemy database model for the Activities in a Stage Gate framework"""

    id = db.Column(db.Integer, primary_key=True)
    """The ID of the Stage Gate activity"""
    name = db.Column(db.String(64), index=True, nullable=False)
    """The name of the Stage Gate activity"""
    description = db.Column(db.String(1000), index=True)
    """A description of the Stage Gate activity"""
    further_details = db.Column(db.String(255))
    """Further details of the Stage Gate activity"""
    activity_category_id = db.Column(db.Integer, db.ForeignKey("activity_category.id"))
    """The ID of the activity category to which the activity belongs"""
    activity_category = db.relationship("ActivityCategory", backref=backref("activities", cascade="all,delete"))
    """The relationship to the activity category database table"""
    activity_eval_areas = db.relationship("ActivityEvalArea", cascade="all,delete")
    """The relationship between an activity and its 'tagged metrics'"""


class ChecklistActivity(db.Model):
    """SQLAlchemy database model for the Checklist Activities associated with a Stage Gate study"""

    id = db.Column(db.Integer, primary_key=True)
    """The ID of the Checklist Activity"""
    activity_id = db.Column(db.Integer, db.ForeignKey("activity.id"))
    """The ID of the activity to which the checklist activity corresponds"""
    activity = db.relationship("Activity", uselist=False)
    """The one-to-one relationship between a checklist activity and an activity"""
    complete = db.Column(db.Boolean, default=False)
    """The status of the checklist activity; the default is false, meaning incomplete"""
    stage_gate_study_id = db.Column(db.Integer, db.ForeignKey("stage_gate_study.id"))
    """The ID of the Stage Gate study to which the checklist activity belongs"""
    stage_gate_study = db.relationship(
        "StageGateStudy",
        uselist=False,
        backref=backref("checklist_activities", cascade="all,delete")
    )
    """The relationship to a Stage Gate study; a Checklist Activity cannot exist without a Stage Gate study"""


class EvaluationArea(db.Model):
    """SQLAlchemy database model for the Evaluation Areas in a Stage Gate framework"""

    id = db.Column(db.Integer, primary_key=True)
    """The ID of the Stage Gate evaluation area"""
    name = db.Column(db.String(64), index=True, nullable=False)
    """The name of the Stage Gate evaluation area"""
    description = db.Column(db.String(600), index=True)
    """A description of the Stage Gate activity"""


class Metric(db.Model):
    """SQLAlchemy database model for the Metrics in a Stage Gate framework"""

    id = db.Column(db.Integer, primary_key=True)
    """The ID of the Stage Gate metric"""
    name = db.Column(db.String(64), index=True, nullable=False)
    """The name of the Stage Gate metric"""
    unit = db.Column(db.String(64))
    """The units of the metric, if applicable"""
    threshold_type = db.Column(db.String(64))
    """The type of the threshold; 
    'upper' limit implies a maximum allowable value, 
    'lower' limit implies a minimum allowable value"""
    evaluation_area_id = db.Column(db.Integer, db.ForeignKey("evaluation_area.id"))
    """The ID of the evaluation area linked to a certain metric"""
    evaluation_area = db.relationship("EvaluationArea")
    """Defines the relationship between a metric and the corresponding evaluation area"""


class ActivityEvalArea(db.Model):
    """SQLAlchemy database model for the ActivityEvalArea table that links Stage Gate activities and evaluation areas"""

    id = db.Column(db.Integer, primary_key=True)
    """The ID of the activity eval area that links activities to evaluation areas"""
    activity_id = db.Column(db.Integer, db.ForeignKey("activity.id"))
    """The ID of the activity---eval_area link"""
    evaluation_area_id = db.Column(db.Integer, db.ForeignKey("evaluation_area.id"))
    """The ID of the metric linked to a certain activity"""
    evaluation_area = db.relationship("EvaluationArea")
    """Defines the relationship between an activity eval area and the 'tagged' evaluation areas"""


class StageGate(db.Model):
    """SQLAlchemy database model for the Stage Gates in a Stage Gate framework"""

    id = db.Column(db.Integer, primary_key=True)
    """The ID of the Stage Gate"""
    name = db.Column(db.String(64), index=True, nullable=False)
    """The name of the Stage Gate"""
    description = db.Column(db.String(120), index=True)
    """A description of the Stage Gate"""
    entry_stage = db.Column(db.Integer, nullable=False)
    """The number of the stage preceding the stage gate"""
    exit_stage = db.Column(db.Integer, nullable=False)
    """The number of the stage succeeding the stage gate"""
    framework_id = db.Column(db.Integer, db.ForeignKey("framework.id"))
    """The ID of the framework to which the Stage Gate belongs"""
    framework = db.relationship("Framework", backref=backref("stage_gates", cascade="all,delete"))
    """The relationship to the framework database table"""


class QuestionCategory(db.Model):
    """SQLAlchemy database model for the Question Categories in a Stage Gate framework"""

    id = db.Column(db.Integer, primary_key=True)
    """The ID of the Stage Gate question category"""
    name = db.Column(db.String(64), index=True, nullable=False)
    """The name of the Stage Gate question category"""
    description = db.Column(db.String(1000), index=True)
    """A description of the Stage Gate question category"""
    stage_gate_id = db.Column(db.Integer, db.ForeignKey("stage_gate.id"))
    """The ID of the Stage Gate to which the question category belongs"""
    stage_gate = db.relationship("StageGate", backref=backref("question_categories", cascade="all,delete"))
    """The relationship to the stage gate database table"""


class Question(db.Model):
    """SQLAlchemy database model for the Questions in a Stage Gate framework"""

    id = db.Column(db.Integer, primary_key=True)
    """The ID of the Stage Gate question"""
    name = db.Column(db.String(100), index=True, nullable=False)
    """The name of the Stage Gate question"""
    number = db.Column(db.Integer, index=True)
    """The number of the question; required for ordering"""
    description = db.Column(db.String(1000), index=True)
    """A description of the Stage Gate question"""
    further_details = db.Column(db.String(255))
    """Further details of the Stage Gate question"""
    weighting = db.Column(db.Float, nullable=False)
    """The weighting of the question as part of a Stage Gate study"""
    question_category_id = db.Column(db.Integer, db.ForeignKey("question_category.id"))
    """The ID of the question category to which the activity belongs"""
    question_category = db.relationship("QuestionCategory", backref=backref("questions", cascade="all,delete"))
    """The relationship to the question category database table"""
    rubric_id = db.Column(db.Integer, db.ForeignKey("rubric.id"))
    """The ID of the rubric associated with the question"""
    rubric = db.relationship("Rubric", uselist=False)
    """The one-to-one relationship between the rubric and the question"""
    question_metric = db.relationship("QuestionMetric", uselist=False, cascade="all,delete")
    """The relationship to the question metric database table; a question can be associated with only one metric"""


class QuestionMetric(db.Model):
    """SQLAlchemy database model for the QuestionMetric table that links Stage Gate questions and metrics"""

    id = db.Column(db.Integer, primary_key=True)
    """The ID of the question metric that links a question to a metric"""
    question_id = db.Column(db.Integer, db.ForeignKey("question.id"))
    """The ID of the question--metric link"""
    metric_id = db.Column(db.Integer, db.ForeignKey("metric.id"))
    """The ID of the metric linked to a certain question"""
    metric = db.relationship("Metric")
    """Defines the relationship between a question metric and the associated metric"""
    threshold_bool = db.Column(db.Boolean, default=False)
    """Boolean value defining whether to apply threshold or not; True means apply threshold, False means do not apply"""
    threshold = db.Column(db.Float)
    """The threshold value for the metric if associated with a Stage Gate question"""


class ScoringCriterion(db.Model):
    """SQLAlchemy database model for the Scoring Criteria in a Stage Gate framework"""

    id = db.Column(db.Integer, primary_key=True)
    """The ID of the scoring criterion"""
    name = db.Column(db.String(64), index=True)
    """The name of the scoring criterion"""
    description = db.Column(db.String(500))
    """A description of the scoring criterion; what is being assessed by the Stage Gate question"""
    question_id = db.Column(db.Integer, db.ForeignKey("question.id"))
    """The ID of the question to which the scoring criterion corresponds"""
    question = db.relationship("Question", backref=backref("scoring_criteria", cascade="all,delete"))
    """The relationship to the question database table"""


class Rubric(db.Model):
    """SQLAlchemy database model for the Rubrics in a Stage Gate framework"""

    id = db.Column(db.Integer, primary_key=True)
    """The ID of the Stage Gate question rubric"""
    name = db.Column(db.String(64), unique=True, index=True)
    """The name of the Stage Gate question rubric"""
    description = db.Column(db.String(255))
    """The description of the Stage Gate question rubric"""


class Grade(db.Model):
    """SQLAlchemy database model for the Grades that comprise a Stage Gate rubric"""

    id = db.Column(db.Integer, primary_key=True)
    """The ID of a grade from a Stage Gate rubric"""
    score = db.Column(db.Integer)
    """The score for a grade in a Stage Gate rubric"""
    label = db.Column(db.String(64))
    """The label for a grade in a Stage Gate rubric"""
    description = db.Column(db.String(255))
    """The description of a grade in a Stage Gate rubric"""
    rubric_id = db.Column(db.Integer, db.ForeignKey("rubric.id"))
    """The ID of the rubric to which the grade belongs"""
    rubric = db.relationship("Rubric", backref="grades")
    """The relationship to the rubric database table"""


class StageGateAssessment(db.Model):
    """SQLAlchemy database model for the Stage Gate assessment entries for a Stage Gate study"""

    id = db.Column(db.Integer, primary_key=True)
    """The ID of the Stage Gate Assessment entry"""
    applicant_complete = db.Column(db.Boolean, default=False)
    """Status of the applicant Stage Gate assessment; true if the assessment for this Stage Gate has been submitted"""
    assessor_complete = db.Column(db.Boolean, default=False)
    """Status of the assessor Stage Gate assessment; true if the assessor mode for this Stage Gate has been submitted"""
    stage_gate_id = db.Column(db.Integer, db.ForeignKey("stage_gate.id"))
    """The ID of the Stage Gate that the Stage Gate Assessment is linked to"""
    stage_gate = db.relationship("StageGate", uselist=False)
    """The relationship between the Stage Gate Assessment and the Stage Gate"""
    stage_gate_study_id = db.Column(db.Integer, db.ForeignKey("stage_gate_study.id"))
    """The ID of the Stage Gate study to which the stage gate assessment belongs"""
    stage_gate_study = db.relationship(
        "StageGateStudy",
        uselist=False,
        backref=backref("stage_gate_assessment", cascade="all,delete")
    )
    """The relationship to a Stage Gate study; an Applicant Answer cannot exist without a Stage Gate study"""


class ApplicantAnswer(db.Model):
    """SQLAlchemy database model for the Applicant responses to a Stage Gate assessment"""

    id = db.Column(db.Integer, primary_key=True)
    """The ID of the Applicant Answer"""
    question_id = db.Column(db.Integer, db.ForeignKey("question.id"))
    """The ID of the question to which the Applicant Answer corresponds"""
    question = db.relationship("Question", uselist=False)
    """The one-to-one relationship between the Stage Gate assessment question and the applicant answer"""
    result = db.Column(db.Float)
    """The metric result for the question; only available for questions with an associated question metric"""
    justification = db.Column(db.String(5000))
    """The justification for the metric result; only available for questions with an associated question metric"""
    response = db.Column(db.String(5000))
    """The response to the question; only available for questions without an associated question metric"""
    stage_gate_assessment_id = db.Column(db.Integer, db.ForeignKey("stage_gate_assessment.id"))
    """The ID of the Stage Gate Assessment entry that the Applicant Answer is related to"""
    stage_gate_assessment = db.relationship(
        "StageGateAssessment",
        uselist=False,
        backref=backref("applicant_answer", cascade="all, delete")
    )
    """The relationship to the Stage Gate Assessment model"""


class AssessorScore(db.Model):
    """SQLAlchemy database model for the Assessor Scores for a question within a Stage Gate assessment"""

    id = db.Column(db.Integer, primary_key=True)
    """The ID of the Assessor Score"""
    question_id = db.Column(db.Integer, db.ForeignKey("question.id"))
    """The ID of the question to which the Assessor Score corresponds"""
    question = db.relationship("Question", uselist=False)
    """The one-to-one relationship between the Stage Gate assessment question and the assessor score"""
    score = db.Column(db.Float)
    """The score that the assessor has given to the specific stage gate question"""
    stage_gate_assessment_id = db.Column(db.Integer, db.ForeignKey("stage_gate_assessment.id"))
    """The ID of the Stage Gate Assessment entry that the Assessor Score is related to"""
    stage_gate_assessment = db.relationship(
        "StageGateAssessment",
        uselist=False,
        backref=backref("assessor_score", cascade="all, delete")
    )
    """The relationship to the Stage Gate Assessment model"""


class AssessorComment(db.Model):
    """SQLAlchemy database model for the Assessor Comment for the scoring criterion of a stage gate question"""

    id = db.Column(db.Integer, primary_key=True)
    """The ID of the Assessor Comment"""
    scoring_criterion_id = db.Column(db.Integer, db.ForeignKey("scoring_criterion.id"))
    """The ID of the scoring criterion to which the Assessor Comment corresponds"""
    scoring_criterion = db.relationship("ScoringCriterion", uselist=False)
    """The one-to-one relationship between the scoring criterion and the assessor comment"""
    comment = db.Column(db.String(1500))
    """The assessor's comment on the applicant's response to a question, based on the specific scoring criterion"""
    stage_gate_assessment_id = db.Column(db.Integer, db.ForeignKey("stage_gate_assessment.id"))
    """The ID of the Stage Gate Assessment entry that the Assessor Comment is related to"""
    stage_gate_assessment = db.relationship(
        "StageGateAssessment",
        uselist=False,
        backref=backref("assessor_comment", cascade="all, delete")
    )
    """The relationship to the Stage Gate Assessment model"""
