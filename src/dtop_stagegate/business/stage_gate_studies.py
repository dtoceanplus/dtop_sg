# This is the Stage Gate module of the DTOceanPlus suite of tools
# Copyright (C) 2021 Wave Energy Scotland - Ben Hudson
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
from sqlalchemy.exc import IntegrityError

from dtop_stagegate.service import db
from .frameworks import get_activities_by_framework, get_stage_gates_by_framework, get_questions_by_framework
from .models import StageGateStudy
from .schemas import StageGateStudySchema, ChecklistActivitySchema, StageGateAssessmentSchema, ApplicantAnswerSchema, \
    AssessorScoreSchema, AssessorCommentSchema
from .applicant_mode import get_stage_gate_assessments


def create_study(data):
    """
    Create a new Stage Gate study and add it to the local storage database.

    Returns an error message and rolls back the session if the new Stage Gate study has the same name as an existing
    study in the database.

    When a new study is created, a family of additional SQL database tables is created that is required for the various
    modes of the Stage Gate Design tool. Namely, the **Checklist Mode**, **Applicant Mode** and **Assessor Mode**.
    The :class:`StageGateStudyFamily` class is used to create these additional database tables.
    Specifically, the :meth:`StageGateStudyFamily.create_additional_study_db_tables` method is used.

    :param dict data: dictionary required to create the Stage Gate study object.
    :return: the newly created Stage Gate study instance
    :rtype: dtop_stagegate.business.models.StageGateStudy
    """
    sg_study_schema = StageGateStudySchema(exclude=['checklist_activities'])
    sg_study = sg_study_schema.load(data)
    db.session.add(sg_study)
    db.session.commit()  # line added to fix bug introduced in sqlalchemy=1.3.14; Query-invoked auto-flush

    StageGateStudyFamily(sg_study).create_additional_study_db_tables()
    db.session.commit()

    return sg_study


class StageGateStudyFamily(object):
    """
    Add supporting SQLAlchemy database tables that need to be created when a new Stage Gate study is created.

    Specifically, the following database tables are also populated with entries:

    - :class:`~dtop_stagegate.business.models.ChecklistActivity`

    - :class:`~dtop_stagegate.business.models.StageGateAssessment`

    - :class:`~dtop_stagegate.business.models.ApplicantAnswer`

    - :class:`~dtop_stagegate.business.models.AssessorScore`

    - :class:`~dtop_stagegate.business.models.AssessorComment`

    Requires an instance of :class:`~dtop_stagegate.business.models.StageGateStudy` class to be initialised.

    **Public methods**

    - :meth:`create_additional_study_db_tables()` - create the required additional database tables
    """

    def __init__(self, sg_study):
        """
        Class is initialised as follows:

        - The list of activities associated with the specified framework ID are obtained using the \
        :meth:`~dtop_stagegate.business.frameworks.get_activities_by_framework` method

        - The list of stage gates associated with the specified framework ID are obtained using the \
        :meth:`~dtop_stagegate.business.frameworks.get_stage_gates_by_framework` method

        - The list of questions associated with the specified framework ID are obtained using the \
        :meth:`~dtop_stagegate.business.frameworks.get_questions_by_framework` method

        Empty lists are initialised for the applicant answers, assessor scores and assessor comments.
        The relevant marshmallow schemas are also initialised.

        :param dtop_stagegate.business.models.StageGateStudy sg_study: instance of the \
        :class:`~dtop_stagegate.business.models.StageGateStudy` class
        """
        self._sg_study = sg_study
        self._framework_id = sg_study.framework_id
        self._applicant_answers = []
        self._assessor_scores = []
        self._assessor_comments = []

        # Initialise framework data
        self._activities = get_activities_by_framework(self._framework_id)
        self._stage_gates = get_stage_gates_by_framework(self._framework_id)
        self._questions = get_questions_by_framework(self._framework_id)

        # Schemas
        self._checklist_activities_schema = ChecklistActivitySchema(many=True)
        self._stage_gate_assessments_schema = StageGateAssessmentSchema(many=True)
        self._applicant_answers_schema = ApplicantAnswerSchema(many=True, exclude=['id'])
        self._assessor_scores_schema = AssessorScoreSchema(many=True, exclude=['id'])
        self._assessor_comments_schema = AssessorCommentSchema(many=True, exclude=['id'])

    def create_additional_study_db_tables(self):
        """
        Creates all of the additional database tables that are related to the Stage Gate study.

        - First adds the new checklist activity entries

        - Then creates the entries for a new Stage Gate assessment

        - Finally adds the required applicant and assessor database tables by looping through the questions and \
        scoring criteria associated with the framework and stage gate assessment in question.
        """
        self._add_checklist_activities()
        self._add_stage_gate_assessments()
        self._add_applicant_and_assessor_data()

    def _add_checklist_activities(self):
        """
        Add new checklist activity entries to the local sqlite database when a new Stage Gate study is created.

        Create a new collection of instances in the ChecklistActivity database table; one entry for each activity
        associated with the framework that has been linked to the specified Stage Gate study.
        """
        checklist_activities = [
            {
                'activity_id': act.id,
                'complete': False,
                'stage_gate_study_id': self._sg_study.id
            }
            for act in self._activities
        ]
        new_instances = self._checklist_activities_schema.load(checklist_activities)
        db.session.bulk_save_objects(new_instances)

    def _add_stage_gate_assessments(self):
        """
        Add entries for a Stage Gate assessment to the SQLite database.

        Insert rows to the :class:`~dtop_stagegate.business.models.StageGateAssessment` class for the newly created
        Stage Gate study.
        """
        stage_gate_assessments = [
            {
                'stage_gate_id': sg.id,
                'applicant_complete': False,
                'assessor_complete': False,
                'stage_gate_study_id': self._sg_study.id
            }
            for sg in self._stage_gates
        ]
        new_instances = self._stage_gate_assessments_schema.load(stage_gate_assessments)
        db.session.bulk_save_objects(new_instances)

    def _add_applicant_and_assessor_data(self):
        """
        Initialise the database tables for the applicant answers, assessor scores and assessor comments associated with
        a stage gate assessment.
        """
        self._sg_assessments = get_stage_gate_assessments(self._sg_study.id)
        for q in self._questions:
            sg_id = q.question_category.stage_gate.id
            sg_assessment_id = self._sg_assessments.filter_by(stage_gate_id=sg_id).first().id
            init_dict = dict(
                question_id=q.id,
                stage_gate_assessment_id=sg_assessment_id
            )
            self._applicant_answers.append(init_dict)
            self._assessor_scores.append(init_dict)
            for sc in q.scoring_criteria:
                self._assessor_comments.append(dict(
                    scoring_criterion_id=sc.id,
                    stage_gate_assessment_id=sg_assessment_id
                ))
        self._save_applicant_assessor_data()

    def _save_applicant_assessor_data(self):
        """
        Perform a bulk save of the newly created database entries for applicant answers, assessor scores and
        assessor comments.
        """
        new_instances = [
            self._applicant_answers_schema.load(self._applicant_answers),
            self._assessor_scores_schema.load(self._assessor_scores),
            self._assessor_comments_schema.load(self._assessor_comments)
        ]
        [db.session.bulk_save_objects(ni) for ni in new_instances]


def update_study(data, study_id):
    """
    Update a Stage Gate study entry in the local storage database.

    Returns an error message if;

    - the Stage Gate study ID that is trying to be updated does not exist or

    - a Stage Gate study with the same name already exists.

    :param dict data: the Stage Gate study data required to update a study in the local database.
    :param str study_id: the ID of the Stage Gate study in the local storage database
    :return: the updated Stage Gate instance
    :rtype: dtop_stagegate.business.models.StageGateStudy
    """
    s = StageGateStudy.query.get(study_id)
    if s is None:
        return "Stage Gate study with that ID does not exist."
    name = data.get('name', None)
    if name is not None:
        s.name = name
    desc = data.get('description', None)
    if desc is not None:
        s.description = desc
    checklist_study_complete = data.get('checklist_study_complete')
    if checklist_study_complete is not None:
        s.checklist_study_complete = checklist_study_complete
    db.session.commit()
    return s


def delete_study(study_id):
    """
    Delete a Stage Gate from the local storage database.

    If study with the specified ID does not exist, returns an error message that leads to a HTTP 404 error.

    :param str study_id: the ID of the Stage Gate study in local storage database to be deleted
    :return: error message if ID does not exist, dictionary with deleted status set to True if deleted successfully
    :rtype: dict
    """
    s = StageGateStudy.query.get(study_id)
    if s is None:
        return "Stage Gate study with that ID does not exist."
    else:
        db.session.delete(s)
        db.session.commit()
        return "Study deleted"
