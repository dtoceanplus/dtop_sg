# This is the Stage Gate module of the DTOceanPlus suite of tools
# Copyright (C) 2021 Wave Energy Scotland - Ben Hudson
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
from sqlalchemy.exc import IntegrityError

from dtop_stagegate.service import db
from .db_utils import load_json_params
from .models import Framework, StageGateStudy, EvaluationArea, \
    Stage, ActivityCategory, Activity, ActivityEvalArea, \
    StageGate, QuestionCategory, Question, QuestionMetric
from .schemas import LoadFrameworkSchema, EvaluationAreaSchema, ActivitySchema


def create_framework(data):
    """
    Create a new Stage Gate framework.

    Creates a copy of the framework template (see default module parameters).
    Sets the name and description as provided by data input argument.
    The framework name must be unique.

    :param dict data: the required name and optional description of the new Stage Gate framework
    :return: an instance of the newly created Framework database object
    :rtype: dtop_stagegate.business.models.Framework
    :except IntegrityError: if framework name already exists in database, returns error message
    """
    json_data = load_json_params('frameworks')
    framework_schema = LoadFrameworkSchema()
    new_instance = framework_schema.load(json_data)
    new_instance.name = data.get('name')
    new_instance.description = data.get('description')
    try:
        db.session.add(new_instance)
        db.session.commit()
        return new_instance
    except IntegrityError:
        db.session.rollback()
        return "Framework with that name already exists"


def update_framework(data, framework_id):
    """
    Update a Stage Gate framework in the local storage database.

    If framework with the specified ID does not exist, returns an error message.
    Returns an error message if the default framework template is attempted to be updated.

    :param dict data: the data from the request body containing values for *name* and *description* of framework.
    :param str framework_id: the ID of the Stage Gate framework to be updated in local storage database
    :return: instance of the newly updated framework object or error message as string if unsuccessful
    :rtype: dtop_stagegate.business.models.Framework
    """
    f = Framework.query.get(framework_id)
    if f is None:
        return "Framework with that ID does not exist"
    if f.name == 'Default (no metric thresholds)':
        return "Cannot update default framework template"
    name = data.get('name', None)
    if name is not None:
        f.name = name
    desc = data.get('description', None)
    if desc is not None:
        f.description = desc
    try:
        db.session.commit()
        return f
    except IntegrityError:
        db.session.rollback()
        return "Framework with that name already exists"


def delete_framework(framework_id):
    """
    Delete a Stage Gate framework from the local storage database.

    If framework with the specified ID does not exist, returns an error message.
    Returns an error message if the default framework template is attempted to be deleted.
    Returns an error message if the framework is assigned to any active Stage Gate study.

    :param str framework_id: the ID of the Stage Gate framework to be deleted from local storage database
    :return: message confirming successful deletion or showing the appropriate error if unsuccessful
    :rtype: str
    """
    f = Framework.query.get(framework_id)
    if f is None:
        return "Framework with that ID does not exist"
    elif f.name == 'Default (no metric thresholds)':
        return "Cannot delete default framework template"
    elif StageGateStudy.query.filter(StageGateStudy.framework_id == framework_id).all():
        return "This Framework is being used in a Stage Gate study. Cannot delete."
    else:
        db.session.delete(f)
        db.session.commit()
        return "Framework deleted"


def get_activities_by_framework(framework_id):
    """
    Select all activities associated with a specific Stage Gate framework.

    Queries the Activity database table, filtering the nested tables by the ID of the Stage Gate framework.

    :param str framework_id: the ID of the Stage Gate framework
    :return: all the activities associated with the specified framework ID
    :rtype: list
    """
    activities = Activity.query.\
        join(ActivityCategory).\
        join(Stage).\
        join(Framework).\
        filter(Framework.id == framework_id)

    return activities.all()


def update_question_metric(data, qm_id):
    """
    Update a question metric in the local sqlite database.

    Checks if the Question Metric with the specified ID exists in the database.
    Updates the values for either of the two provided parameters; *threshold_bool* and *threshold*.

    :param dict data: the data from the request body containing values for *threshold_bool* and *threshold* keys.
    :param str qm_id: the ID of the question metric in the local database
    :return: instance of the newly updated question metric object or error message as string if unsuccessful
    :rtype: dtop_stagegate.business.models.QuestionMetric
    """
    qm = QuestionMetric.query.get(qm_id)
    if qm is None:
        return "Question Metric with that ID does not exist"
    threshold_bool = data.get('threshold_bool', None)
    if threshold_bool is not None:
        qm.threshold_bool = threshold_bool
    threshold = data.get('threshold', None)
    if threshold is not None:
        qm.threshold = threshold
    db.session.commit()

    return qm


def update_question_metric_list(data):
    """
    Update a group of question metrics in the local sqlite database.

    Loops through each threshold set in the request body, finds the appropriate Question Metric and sets the threshold
    and threshold_bool parameters.
    Then performs a bulk save in the database.
    The validation of the request body items takes place in the corresponding Flask route.

    :param list data: list of dicts containing id, threshold_bool and threshold fields
    """
    new_instances = []
    for d in data:
        qm = QuestionMetric.query.get(d['id'])
        qm.threshold_bool = d['threshold_bool']
        qm.threshold = d['threshold']
        new_instances.append(qm)
    db.session.bulk_save_objects(new_instances)
    db.session.commit()


def get_list_applied_metric_thresholds(framework_id):
    """
    Get a list of the metric thresholds that have been set for a specific Stage Gate framework.

    Queries the QuestionMetric database table, filtering the nested tables by the ID of the Stage Gate framework
    as well as filtering by question metrics with a *threshold_bool* value set to *True*.

    :param str framework_id: the ID of the Stage Gate framework
    :return: the metric thresholds that have been applied for the requested framework ID
    :rtype: flask_sqlalchemy.BaseQuery
    """
    metric_thresholds = QuestionMetric.query.\
        join(Question).\
        join(QuestionCategory).\
        join(StageGate).\
        join(Framework).\
        filter(Framework.id == framework_id, QuestionMetric.threshold_bool.is_(True))

    return metric_thresholds


def get_metric_thresholds_by_stage_gate_id(stage_gate_id):
    """
    Get a list of thresholds for the framework applied to a specific study and inferred for latest completed Stage Gate.

    Queries the QuestionMetric database table, filtering the nested tables by the ID of the Stage Gate
    as well as filtering by question metrics with a *threshold_bool* value set to *True*.

    :param str stage_gate_id: the ID of the Stage Gate that has been identified
    :return: the metric thresholds that have been applied for the requested Stage Gate
    :rtype: flask_sqlalchemy.BaseQuery
    """
    metric_thresholds = QuestionMetric.query.\
        join(Question).\
        join(QuestionCategory).\
        join(StageGate).\
        join(Framework).\
        filter(
            StageGate.id == stage_gate_id,
            QuestionMetric.threshold_bool.is_(True)
        )

    return metric_thresholds


def get_stage_by_evaluation_area(stage_id):
    """
    Categorise the activities in a specified Stage by their Evaluation Area, rather than by Activity Category.

    The procedure is as follows:

    - Get a list of all evaluation areas using :meth:`get_eval_areas` function
    - Query the database for all the ActivityEvalArea entries for the specified Stage using the \
    :meth:`get_activity_eval_area_query` function
    - Categorise the stage information by Evaluation Area using :meth:`categorise_by_eval_area`
    - Add the Stage information to the eval_stages return parameter using :meth:`add_stage_info_to_eval_stages`

    :param str stage_id: the ID of the Stage in the Stage Gate framework
    :return: the Stage information; comprising the list of activities categorised by Evaluation Area
    :rtype: dict
    """
    act_eval_area_query = get_activity_eval_area_query(stage_id)
    if not act_eval_area_query.all():
        return None
    eval_areas = get_eval_areas()
    eval_areas = categorise_by_eval_area(eval_areas, act_eval_area_query)
    eval_stages = add_stage_info_to_eval_stages(stage_id, eval_areas)

    return eval_stages


def get_activity_eval_area_query(stage_id):
    """
    Query the local database for all the ActivityEvalArea entries that exist for the specified Stage ID.

    The query returns a tuple-like response comprising the evaluation_area_id of the ActivityEvalArea and the
    corresponding Activity.
    Query joins on ActivityCategory and Stage and then filters the results for the appropriate Stage ID.

    :param str stage_id: the ID of the Stage in the Stage Gate framework
    :return: the SQLAlchemy query for the ActivityEvalArea.evaluation_area_id and Activity db model
    :rtype: flask_sqlalchemy.BaseQuery
    """
    act_eval_area_query = db.session.query(ActivityEvalArea.evaluation_area_id, Activity).\
        join(Activity).\
        join(ActivityCategory).\
        join(Stage).\
        filter(Stage.id == stage_id)

    return act_eval_area_query


def get_eval_areas():
    """
    Get a list of all the evaluation areas in the database.

    :return: the evaluation areas stored in the local database
    :rtype: list
    """
    eval_area_query = EvaluationArea.query.all()
    eval_area_schema = EvaluationAreaSchema(many=True)
    eval_areas = eval_area_schema.dump(eval_area_query)

    return eval_areas


def categorise_by_eval_area(eval_areas, act_eval_area_query):
    """
    Categorise the stage information by Evaluation Area

    Loops through the evaluation areas and adds any of the activities that have "tagged" that evaluation area.
    Boolean comparator is based on the evaluation_area_id.
    Removes any evaluation areas that do not have any linked activities.

    :param list eval_areas: the evaluation areas stored in the local database
    :param flask_sqlalchemy.BaseQuery act_eval_area_query: the SQLAlchemy query for the \
    ActivityEvalArea.evaluation_area_id and Activity db model
    :return: the evaluation areas with added "activities" dictionary item, filtering for those evaluation areas that \
    have linked activities
    :rtype: list
    """
    activity_schema = ActivitySchema()
    for ea in eval_areas:
        ea['activities'] = [activity_schema.dump(act[1]) for act in act_eval_area_query if act[0] == ea.get('id')]
    eval_areas = [ea for ea in eval_areas if ea['activities']]

    return eval_areas


def add_stage_info_to_eval_stages(stage_id, eval_areas):
    """
    Add the Stage information to the eval_stages return parameter

    Queries the Stage database model to use the same data used for a standard call for a *Stage*

    :param str stage_id: the ID of the Stage in the Stage Gate framework
    :param list eval_areas: the filtered evaluation areas with added "activities" dictionary item
    :return: the activities in the specified Stage categorised by Evaluation Area
    :rtype: dict
    """
    stage = Stage.query.get(stage_id)
    eval_stages = dict(
        evaluation_areas=eval_areas,
        name=stage.name,
        description=stage.description,
        number=stage.number,
    )

    return eval_stages


def get_stage_gates_by_framework(f_id):
    """
    Select all stage gates associated with a specific Stage Gate framework.

    Queries the Stage Gate database table, filtering by the ID of the Stage Gate framework.

    :param str f_id: the ID of the Stage Gate framework
    :return: all the stage gates associated with the specified framework ID
    :rtype: list
    """
    stage_gates = StageGate.query.filter_by(framework_id=f_id).all()

    return stage_gates


def get_questions_by_framework(f_id):
    """
    Select all questions associated with a specific Stage Gate framework.

    Queries the Questions database table, filtering by the ID of the Stage Gate framework.

    :param str f_id: the ID of the Stage Gate framework
    :return: all the questions associated with the specified framework ID
    :rtype: list
    """
    questions = Question.query.\
        join(QuestionCategory).\
        join(StageGate).\
        join(Framework).\
        filter(Framework.id == f_id).all()

    return questions
