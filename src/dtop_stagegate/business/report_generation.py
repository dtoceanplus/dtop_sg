# This is the Stage Gate module of the DTOceanPlus suite of tools
# Copyright (C) 2021 Wave Energy Scotland - Ben Hudson
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
import datetime

from .applicant_mode import ApplicantResults
from .assessor_mode import AssessorResults
from .checklists import ChecklistResults
from .improvement_areas import ImprovementAreas

# Module constants
DTO_COLOURS = {
    'blue': '#022c5f'
}
X_AXIS = {
    'percentage': {
        'title': "Percentage activities complete (%)",
        'tickmode': 'array',
        'tickvals': [0, 25, 50, 75, 100],
        "range": [
            -5,
            105
        ],
    },
    'assessor_score': {
        'title': "Assessor score (5-point scale)",
        'tickmode': 'array',
        'tickvals': list(range(6)),
        'ticktext': ['Unacceptable', 'Poor', 'Acceptable', 'Good', 'Excellent'],
        "range": [
            -0.2,
            4.2
        ],
    }
}


class StudyData(object):
    """
    Get the general study data for the report.

    Generates the current date and formats the study content that includes the study name, the date, the selected Stage 
    and Stage Gate, the description of the study, the threshold settings that are applied and the the selected 
    stage index.

    An instance of :class:`ReportCompiler` is required to initialise the class.
    """

    def __init__(self, report_compiler):
        """
        Class is initialised by extracting the relevant study details from the :class:`ReportCompiler` class.

        :param ReportCompiler report_compiler: instance of the :class:`ReportCompiler` class.
        """
        self._sg_study = report_compiler.sg_study
        self._stage_index = report_compiler.stage_index
        self._stage_gate_index = report_compiler.stage_gate_index
        self._selected_stage = report_compiler.selected_stage
        self._selected_stage_gate = report_compiler.selected_stage_gate
        self._description = 'N/A'
        if self._sg_study.description is not None:
            self._description = self._sg_study.description
        self._date = None
        self._content = []

        self._get_date()
        self._format_data()

    def _get_date(self):
        """Get the current datetime and format it as DD/MM/YYYY"""
        now = datetime.datetime.now()
        self._date = now.strftime("%d/%m/%Y")

    def _format_data(self):
        """Format the study details and add data to the StudyData content list"""
        self._content = {
            'study_name': self._sg_study.name,
            'date': self._date,
            'selected_stage': self._selected_stage,
            'selected_stage_gate': self._selected_stage_gate,
            'description': self._description,
            'threshold_settings': self._sg_study.framework.name,
            'stage_index': self._stage_index
        }

    @property
    def content(self):
        """
        The general study data content.

        :rtype: dict
        """
        return self._content


class PlotlyBarChartData(object):
    """
    Generate the data required to create Plotly bar-charts in the front-end. 

    Requires a data item containing the lists of x and y values, as well as the title for the graph.
    Also required is a tag for the x-axis, which can be either *percentage* or *assessor_score*, this helps select the
    x-axis formatting options as specified using the module constant *X_AXIS*'
    Two further optional arguments are required. 
    The first is the desired height of the figure, with a default value of 500.
    The last is a boolean value for whether to plot the figure title or not. 
    """

    def __init__(self, data, x_axis, fig_height=500, show_title=True):
        """
        Class is initialised as follows;

        - extract the x and y values from the data,

        - extract the title of the chart,

        - use the *x_axis* tag to select the appropriate x-axis formatting options.

        :param dict data: the input data used to make the bar-chart, must contain 'x', 'y' and 'title' parameters
        :param str x_axis: enum for what type of x-axis to use, one of *percentage* or *assessor_score*
        :param int fig_height: the desired height of the figure
        :param bool show_title: decision on whether to include the figure title
        """
        self._x = data['x']
        self._y = data['y']
        self._title = data['name']
        self._fig_height = fig_height
        self._show_title = show_title
        self._x_axis = X_AXIS[x_axis]
        self.data = {}
        self.layout = {}

        self._get_plot_data()

    def _get_plot_data(self):
        """
        Generate the data required to create the bar-chart in the front-end using JavaScript. 

        Copies the majority of formatting used in the front-end version of the bar-charts.
        """
        self.data = [dict(
            x=self._x,
            y=self._y,
            orientation='h',
            type='bar',
            name=self._title
        )]
        if self._show_title:
            title = self._title
        else:
            title = ''
        self.layout = dict(
            title={
                'text': title,
                'x': 0.5,
                'xanchor': 'center'
            },
            xaxis=self._x_axis,
            template='none',
            yaxis={
                'automargin': True
            },
            margin={
                'pad': 6
            },
            autosize=False,
            width=500,
            height=self._fig_height,
            font={
                'color': 'black',
            }
        )


class ChecklistData(object):
    """
    Format the Activity Checklist section of the report.

    Includes sub-sections for

    - *SUMMARY* (checklist); table giving breakdown of percentage activities completed for each stage,

    - *DETAILED RESULTS FOR STAGE XXX*; bar charts of the further breakdown of completed activities, one for \
    activity category breakdown and one for evaluation area breakdown and

    - *OUTSTANDING ACTIVITIES*; the bullet-point list of the outstanding activities yet to be completed.

    Requires an instance of :class:`ReportCompiler` class as an input argument.
    """

    def __init__(self, report_compiler):
        """
        Class is initialised by extracting the Stage Gate Study instance and Stage index from :class:`ReportCompiler`

        Depending on the sections specified in the Report Generation request body, generates the *SUMMARY*,
        *DETAILED STAGE RESULTS* and *OUTSTANDING ACTIVITIES* sections and updates the **content** property of 
        :class:`ReportCompiler` with the appropriate data. 

        Uses the :class:`~dtop_stagegate.business.checklists.ChecklistResults` class to calculate and format the
        Checklist results.

        The detailed breakdown of Stage results requires the :class:`PlotlyBarChartData` class to generate the
        necessary bar chart data for the report.

        :param ReportCompiler report_compiler: instance of the :class:`ReportCompiler` class.
        """
        self.report_compiler = report_compiler
        self._sg_study = report_compiler.sg_study
        self._stage_index = report_compiler.stage_index
        sr = report_compiler.section_request
        self._stage_data = None
        self._checklist_results = None
        self._outstanding_activities = []
        self._bullet_points = []

        self.checklist_summary = []

        self._get_checklist_results()
        if sr['checklist_summary']:
            self._format_summary()
        if sr['checklist_stage_results']:
            self._format_stage_results()
        if sr['checklist_outstanding_activities']:
            self._format_outstanding_activities()

    def _get_checklist_results(self):
        """
        Use :class:`~dtop_stagegate.business.checklists.ChecklistResults` class to calculate checklist results.

        If the Stage index has been specified then extracts the detailed Stage data for that Stage.
        :return:
        """
        checklist_results = ChecklistResults(self._sg_study.id)
        self._checklist_results = checklist_results.results
        if self._stage_index is not None:
            self._stage_data = self._checklist_results[self._stage_index]

    def _format_summary(self):
        """Format the percentage scores for each stage and add to :class:`ReportCompiler` **content** property"""
        self.report_compiler.content['checklist_summary'] = [
            f"{stg['percent_complete']}%" for stg in self._checklist_results
        ]

    def _format_stage_results(self):
        """Format the checklist bar chart data and add to :class:`ReportCompiler` **content** property"""
        self._make_checklist_bar_charts()
        self.report_compiler.content['checklist_bar_charts'] = {
            'activity_category': {
                'data': self._activity_category_bar_chart.data,
                'layout': self._activity_category_bar_chart.layout
            },
            'evaluation_area': {
                'data': self._evaluation_area_bar_chart.data,
                'layout': self._evaluation_area_bar_chart.layout
            }
        }

    def _make_checklist_bar_charts(self):
        """Extract the results for both Activity Categories and Evaluation Areas and generates the bar chart data"""
        ac_x, ac_y = self._extract_bar_data('activity_categories')
        ea_x, ea_y = self._extract_bar_data('evaluation_areas')
        self._activity_category_bar_chart = PlotlyBarChartData(
            {
                'x': ac_x,
                'y': ac_y,
                'name': 'Activity Categories'
            },
            'percentage'
        )
        self._evaluation_area_bar_chart = PlotlyBarChartData(
            {
                'x': ea_x,
                'y': ea_y,
                'name': 'Evaluation Areas'
            },
            'percentage'
        )

    def _extract_bar_data(self, category):
        """Extract and re-format the ChecklistResults data into x and y data points"""
        data = self._stage_data[category]
        x = [i['percent'] for i in data]
        y = [i['name'] for i in data]

        return x, y

    def _format_outstanding_activities(self):
        """Format bullet points for outstanding activities and add to :class:`ReportCompiler` **content** property"""
        self._format_bullet_points()
        self._outstanding_activities = [
            {
                'text': 'OUTSTANDING ACTIVITIES',
                'style': 'subheader'
            },
            '\n',
            {
                'text': f"The following is a list of the outstanding activities for Stage {self._stage_index}, "
                        f"sorted by activity category:",
                'alignment': 'justify'
            },
            '\n',
            {
                'ul': self._bullet_points,
                'alignment': 'justify'
            }
        ]
        self.report_compiler.content['outstanding_activities'] = self._outstanding_activities

    def _format_bullet_points(self):
        """Extract the outstanding activity data from the checklist results and re-format for the summary report"""
        for ac in self._stage_data['activity_categories']:
            activity_list = ac['outstanding']
            if activity_list:
                self._bullet_points.append(ac['name'])
                activities = [self._format_activity(a) for a in activity_list]
                activities[-1]['text'].append('\n\n')
                self._bullet_points.append({
                    'type': 'circle',
                    'ul': activities
                })

    @staticmethod
    def _format_activity(activity):
        """Re-format an individual activity to the structure required for the summary report"""
        d = {
            'text': [
                {
                    'text': '{}: '.format(activity['name']),
                    'style': 'bold'
                },
                activity['description'].replace(' <br> ', '\n'),
            ]
        }

        return d


class ApplicantData(object):
    """
    Format the Applicant Mode results section of the report.

    Generates sub-sections for

    - *SUMMARY* (applicant); short text section giving success rate percentage and threshold success rate if applicable,

    - *METRIC RESULTS*; tabular summary of metric results for the Applicant Mode; table will be inserted as a portrait \
    or landscape page depending on whether thresholds have been applied or not.

    - *APPLICANT ANSWERS*; the bullet-point list of the questions of a Stage Gate and the applicant's responses.

    Requires an instance of :class:`ReportCompiler` class as an input argument.
    """

    def __init__(self, report_compiler):
        """
        Class is initialised as follows;

        - extracts the Stage Gate Study instance, Stage Gate index and selected Stage Gate string from \
        :class:`ReportCompiler`,

        - loads the appropriate stage gate assessment instance using the provided Stage Gate study and Stage Gate \
        index and

        - uses the :class:`~dtop_stagegate.business.applicant_mode.ApplicantResults` class to calculate and format the \
        Applicant Mode results.

        Depending on the sections specified in the Report Generation request body, generates the *SUMMARY*,
        *METRIC RESULTS* and *APPLICANT ANSWERS* sections and updates the **content** property of
        :class:`ReportCompiler` with the appropriate data.

        The Applicant Answers section requires the :class:`QuestionFormatter` class to format the applicant answers for
        the applicant responses.

        :param ReportCompiler report_compiler: instance of the :class:`ReportCompiler` class.
        """
        self.report_compiler = report_compiler
        self._sg_study = report_compiler.sg_study
        self._stage_gate_index = report_compiler.stage_gate_index
        self._selected_stage_gate = report_compiler.selected_stage_gate
        sr = report_compiler.section_request
        self._sg_assessment = self._sg_study.stage_gate_assessment[self._stage_gate_index]
        self._applicant_results = None
        self._response_rate = None
        self._threshold_success_rate = None
        self._summary = []
        self._metric_results = [
            {
                'text': 'METRIC RESULTS',
                'style': 'subheader'
            },
            '\n'
        ]
        self._responses = [
            {
                'text': 'APPLICANT ANSWERS',
                'style': 'subheader'
            },
            '\n'
        ]

        self._get_applicant_results()
        if sr['applicant_summary']:
            self._format_summary()
        if sr['metric_results']:
            self._format_metric_data_table()
        if sr['applicant_answers'] and sr['assessor_scores_comments']:
            self._add_reference_to_assessor_section()
        if sr['applicant_answers'] and not sr['assessor_scores_comments']:
            self._format_responses()

    def _get_applicant_results(self):
        """
        Use the :class:`~dtop_stagegate.business.applicant_mode.ApplicantResults` class to calculate and format the
        Applicant Mode results.

        Extracts the *summary_data*, *response_rate* and *threshold_success_rate* parameters from the results and
        stores these values locally.
        """
        ar = ApplicantResults(self._sg_assessment)
        ar.calculate()
        self._applicant_results = ar.get_results()
        summary_data = self._applicant_results['summary_data']
        self._response_rate = summary_data['response_rate']
        self._threshold_success_rate = summary_data['threshold_success_rate']

    def _format_summary(self):
        """
        Format the summary of the Applicant Mode results and add to :class:`ReportCompiler` **content** property

        Sentence describing the response rate is always added.
        If the study is using a framework that has thresholds applied, then adds the threshold success rate in addition
        to the response rate.
        """
        self._summary = [
            {
                'text': 'SUMMARY',
                'style': 'subheader'
            },
            '\n',
            {
                'text': f"The response rate for {self._selected_stage_gate} was {self._response_rate}%.",
                        # 'The response rate is the percentage of questions that an applicant has answered fully.
                'alignment': 'justify'
            },
            '\n',
        ]
        if self._threshold_success_rate is not None:
            threshold_summary = {
                'text': f"The threshold success rate for {self._selected_stage_gate} was "
                        f"{self._threshold_success_rate}%.\n\n",
                        # 'The threshold success rate is the number of metric results that have passed a specified '
                        # 'threshold, divided by the total number of metrics with thresholds applied, expressed as a'
                        # ' percentage.'
                'alignment': 'justify'
            }
            self._summary.append(threshold_summary)
        self.report_compiler.content['applicant_summary'] = self._summary

    def _format_metric_data_table(self):
        """
        Add the metric results section to :class:`ReportCompiler` **content** property

        If thresholds have been specified, then adds the metric results table as a landscape page.
        Otherwise adds the standard metric results table in portrait orientation.
        """
        if self._threshold_success_rate is not None:
            self._append_metric_results_with_thresholds()
        else:
            self._append_metric_results()

    def _append_metric_results(self):
        """Format and add metric results table to ApplicantData content list"""
        formatted_metric_results = [
            [
                mr['metric'],
                mr['unit'],
                mr['result']
            ]
            for mr in self._applicant_results['metric_results']
        ]
        headers = [
            {'fillColor': DTO_COLOURS['blue'], 'text': 'Metric', 'style': 'tableHeader'},
            {'fillColor': DTO_COLOURS['blue'], 'text': 'Unit', 'style': 'tableHeader'},
            {'fillColor': DTO_COLOURS['blue'], 'text': 'Result', 'style': 'tableHeader'},
        ]
        col_widths = ['auto', '*', '*']
        self._metric_results += [
            {
                'text': 'The table below shows the metric results for {}.'.format(self._selected_stage_gate),
                'alignment': 'justify',
            },
            '\n',
            {
                'style': 'tableExample',
                'table': {
                    'headerRows': 1,
                    'widths': col_widths,
                    'body': [headers] + formatted_metric_results,
                }
            },
            {
                'text': ' ',
                'pageBreak': 'after',
            }
        ]
        self.report_compiler.content['metric_results'] = self._metric_results

    def _append_metric_results_with_thresholds(self):
        """Format and add landscape metric results table with thresholds to ApplicantData content list"""
        formatted_metric_results = [
            [
                mr['metric'],
                mr['unit'],
                mr['result'],
                mr['threshold'],
                mr['absolute_distance'],
                mr['percent_distance']
            ]
            for mr in self._applicant_results['metric_results']
        ]
        headers = [
            {'fillColor': DTO_COLOURS['blue'], 'text': 'Metric', 'style': 'tableHeader'},
            {'fillColor': DTO_COLOURS['blue'], 'text': 'Unit', 'style': 'tableHeader'},
            {'fillColor': DTO_COLOURS['blue'], 'text': 'Result', 'style': 'tableHeader'},
            {'fillColor': DTO_COLOURS['blue'], 'text': 'Threshold', 'style': 'tableHeader'},
            {'fillColor': DTO_COLOURS['blue'], 'text': 'Absolute distance (unit)', 'style': 'tableHeader'},
            {'fillColor': DTO_COLOURS['blue'], 'text': 'Percentage distance (%)', 'style': 'tableHeader'}
        ]
        col_widths = ['auto', '*', '*', '*', '*', '*']
        self._metric_results += [
            {
                'text': 'The table below shows the metric results for {}.'.format(self._selected_stage_gate),
                'alignment': 'justify',
            },
            '\n',
            {
                'style': 'tableExample',
                'table': {
                    'headerRows': 1,
                    'widths': col_widths,
                    'body': [headers] + formatted_metric_results,
                },
                'pageBreak': 'before',
                'pageOrientation': 'landscape'
            },
            {
                'text': ' ',
                'pageBreak': 'after',
                'pageOrientation': 'portrait'
            }
        ]
        self.report_compiler.content['metric_results'] = self._metric_results

    def _add_reference_to_assessor_section(self):
        """Add a reference to assessor section in the case that both applicant and assessor responses are requested"""
        self._responses += [{
            'text': [
                f"The applicant responses for the questions in {self._selected_stage_gate} are provided in the ",
                {
                    'text': 'Assessor Mode',
                    'style': 'bold'
                },
                " section, together with the assessor scores and comments."
            ],
            'alignment': 'justify'
        }]
        self.report_compiler.content['applicant_answers'] = self._responses

    def _format_responses(self):
        """
        Format the applicant answer responses to the questions and add to :class:`ReportCompiler` **content** property

        Loop through the question categories from the applicant results.
        Uses the :class:`QuestionFormatter` class to format the individual questions.
        Adds the formatted list to the content list.
        """
        self._responses += [
            {
                'text': 'The applicant responses for the questions in {} are shown below, '
                        'ordered by question category.'.format(self._selected_stage_gate),
                'alignment': 'justify'
            },
            '\n'
        ]
        for qc in self._applicant_results['responses']['question_categories']:
            qc_header = {
                'text': qc['name'],
                'style': 'subsubheader'
            }
            qc_content = [
                qc_header,
                qc['description'].replace(' <br> ', '\n'),
                '\n'
            ]
            q_list = []
            for q in qc['questions']:
                q_format = QuestionFormatter(q).format_applicant_response()
                q_list.append(q_format)
            qc_content.append({
                'ul': q_list
            })
            self._responses += qc_content

        self.report_compiler.content['applicant_answers'] = self._responses


class QuestionFormatter(object):
    """
    Re-format a question entity for the stage gate summary report.

    Question can be categorised as quantitative or qualitative.
    Questions can be formatted as an applicant or assessor response.

    Requires the serialised version of a nested question to be initialised.

    **Public methods**

    - :meth:`format_applicant_response()` - re-format the input question data as an applicant response

    - :meth:`format_assessor_response()` - re-format the input question data as an assessor response

    """

    def __init__(self, question):
        """
        Class is initialised as follows

        - extracts the required information from the serialised Question data,

        - replaces any instances of '<br>' in the question description with a new-line command,

        - determines whether the question is quantitative depending on whether the quesion_metric field is null and

        - immediately adds the question description and weighting to the bullet point list.

        :param dict question: serialised version of the Question data (nested schema)
        """
        self._q = question
        self._name = self._q['name']
        self._desc = self._q['description'].replace('<br>', '\n')
        self._weighting = self._q['weighting']
        self._scoring_criteria = self._q['scoring_criteria']
        self._quantitative_question = self._q['question_metric']
        self._unit = None
        if self._quantitative_question:
            self._unit = self._q['question_metric']['metric']['unit']
        app_res = self._q['applicant_results']
        self._response = app_res['response']
        self._result = app_res['result']
        self._justification = app_res['justification']
        self._bullets = [
            f"{self._desc}",
            f"Weighting (%): {self._weighting}"
        ]
        self._response_box = {'text': 'test'}
        self._sc_box = []

    def _add_response_or_result(self):
        """
        Add the response or the metric result and justification to the bullet point list.

        Applicant answers are outlined in a black box for clarity (i.e. a table with one cell)
        The applicant response will be added for Qualitative questions.
        The metric result and applicant's justification will be added for Quantitative questions.
        """
        if not self._quantitative_question:
            self._response_box = {
                'style': 'tableExample',
                'table': {
                    'body': [
                        [
                            {
                                 'text': [
                                     {'text': "Applicant's response: \n\n", 'bold': True},
                                     self._response
                                 ]
                            }
                        ],
                    ],
                    'widths': ['*']
                },
            }
        else:
            self._response_box = {
                'style': 'tableExample',
                'table': {
                    'body': [
                        [
                            {
                                'text': [
                                    {'text': f"Applicant's metric result ({self._unit}): ", 'bold': True},
                                    f"{self._result}\n\n",
                                    {'text': "Applicant's justification: ", 'bold': True},
                                    self._justification
                                ]
                            }
                        ],
                    ],
                    'widths': ['*']
                },
            }

    def _add_scoring_criteria_contents(self):
        """Add a scoring criteria main bullet and the individual scoring criterion as sub-bullets"""
        sc = [
            'Scoring criteria:',
            {
                'ul': [sc['description'] for sc in self._scoring_criteria],
                'type': 'square'
            }
        ]
        self._bullets.append(sc)

    def _add_assessor_comments(self):
        """
        Add scoring criteria along with the assessor comment relevant for each individual criterion

        Assessor scores and comments are outlined in a black box for clarity (i.e. a table with one cell)
        """
        self._sc_box = {
            'style': 'tableExample',
            'table': {
                'body': [
                    [
                        [
                            {
                                'text': [
                                    {'text': f"Assessor's score: ", 'bold': True},
                                    f"{self._q['score']['score']}\n\n",
                                    {'text': "Assessor's comments:\n\n", "bold": True},
                                ]
                            },
                            {
                                'ol': [
                                    [
                                        {
                                            "text": f"{sc['description']}\n\n",
                                            "italics": True
                                        },
                                        {
                                            "text": f"Comment:",
                                            'decoration': 'underline'
                                        },
                                        f" {sc['comment']['comment']}\n\n"
                                    ]
                                    for sc in self._scoring_criteria
                                ]
                            }
                        ]
                    ],
                ],
                'widths': ['*']
            }
        }

    def _get_applicant_contents(self):
        """Performing the final formatting operations for the applicant content"""
        c = [
            self._name,
            {
                'ul': self._bullets,
                'type': 'circle',
                'alignment': 'justify'
            },
            '\n',
            self._response_box,
            '\n'
        ]

        return c

    def _get_assessor_contents(self):
        """Performing the final formatting operations for the assessor content"""
        c = [
            self._name,
            {
                'ul': self._bullets,
                'type': 'circle',
                'alignment': 'justify'
            },
            '\n',
            self._response_box,
            '\n',
            self._sc_box
        ]

        return c

    def format_applicant_response(self):
        """
        Format the question as a response for the Applicant Mode section.

        The order of the bullet points for each question in this case is

        - description,

        - weighting,

        - scoring criteria (if applicable) and

        - response or result and justification.

        :return: question data formatted as an Applicant Response for the stage gate summary report
        :rtype: list
        """
        if self._scoring_criteria:
            self._add_scoring_criteria_contents()
        self._add_response_or_result()
        c = self._get_applicant_contents()

        return c

    def format_assessor_response(self):
        """
        Format the question as a response for the Assessor Mode section.

        The order of the bullet points for each question in this case is

        - description,

        - weighting,

        - response or result and justification,

        - assessor score and

        - scoring criteria with assessor comments (if applicable).

        :return: question data formatted as an Assessor Response for the stage gate summary report
        :rtype: list
        """
        self._add_response_or_result()
        if self._scoring_criteria:
            self._add_assessor_comments()
        c = self._get_assessor_contents()

        return c


class AssessorData(object):
    """
    Format the Assessor Mode results section of the report.

    Includes sub-sections for

    - *OVERALL SCORES* (assessor); summary bar-chart showing the overall average and weighted average assessor scores \
    for the entire Stage Gate,

    - *QUESTION CATEGORY SCORES*; additional bar-charts showing the breakdown of assessor scores by Question Category,

    - *ASSESSOR SCORES AND COMMENTS ANSWERS*; bullet-point list of the questions of a Stage Gate, the applicant's \
    responses and the assessor's scores and comments.

    Requires an instance of :class:`ReportCompiler` class as an input argument.
    """

    def __init__(self, report_compiler):
        """
        Class is initialised as follows;

        - extracts the Stage Gate Study instance, Stage Gate index and selected Stage Gate string from \
        :class:`ReportCompiler`,

        - loads the appropriate stage gate assessment instance using the provided Stage Gate study and Stage Gate \
        index and

        - uses the :class:`~dtop_stagegate.business.assessor_mode.AssessorResults` class to calculate and format the \
        Assessor Mode results.

        Depending on the sections specified in the Report Generation request body, generates the *OVERALL SCORES*,
        *QUESTION CATEGORY SCORES* and *ASSESSOR SCORES AND COMMENTS ANSWERS* sections and updates the **content**
        property of :class:`ReportCompiler` with the appropriate data.

        The Assessor Answers section requires the :class:`QuestionFormatter` class to format the applicant answers for
        the assessor responses.

        :param ReportCompiler report_compiler: instance of the :class:`ReportCompiler` class.
        """
        self.report_compiler = report_compiler
        self._sg_study = report_compiler.sg_study
        self._stage_gate_index = report_compiler.stage_gate_index
        self._selected_stage_gate = report_compiler.selected_stage_gate
        sr = report_compiler.section_request
        self._sg_assessment = self._sg_study.stage_gate_assessment[self._stage_gate_index]
        self._assessor_results = None
        self._content = []
        self._summary = []
        self._summary_bar_chart = None
        self._weighted_q_cat_bar_chart = None
        self._q_cat_bar_chart = None
        self._weighted_avg_eval_bar_chart = None
        self._avg_eval_bar_chart = None
        self._question_category_scores = []
        self._responses = []

        self._get_assessor_results()
        if sr['assessor_overall_scores']:
            self._format_summary_bar_chart()
        if sr['assessor_question_categories']:
            self._format_question_cat_bar_charts()
        if sr['assessor_scores_comments']:
            self._format_responses()

    def _get_assessor_results(self):
        """
        Use the :class:`~dtop_stagegate.business.assessor.AssessorResults` class to calculate and format the
        Assessor Mode results.
        """
        ar = AssessorResults(self._sg_assessment)
        ar.calculate()
        self._assessor_results = ar.get_results()

    def _format_summary_bar_chart(self):
        """Format the OVERALL SCORES bar chart using :class:`PlotlyBarChartData` and add to ReportCompiler content"""
        self._summary_bar_chart = PlotlyBarChartData(
            self._assessor_results['overall'][0],
            'assessor_score',
            300,
            show_title=False
        )
        self.report_compiler.content['assessor_overall_scores'] = {
            'data': self._summary_bar_chart.data,
            'layout': self._summary_bar_chart.layout
        }

    def _format_question_cat_bar_charts(self):
        """Format the Question Category bar-chart using :class:`PlotlyBarChartData` and add to ReportCompiler content"""
        self._weighted_q_cat_bar_chart = PlotlyBarChartData(
            self._assessor_results['question_categories'][0],
            'assessor_score',
            show_title=False
        )
        self._q_cat_bar_chart = PlotlyBarChartData(
            self._assessor_results['question_categories'][1],
            'assessor_score',
            show_title=False
        )
        self.report_compiler.content['assessor_question_categories'] = {
            'data': [
                self._weighted_q_cat_bar_chart.data[0],
                self._q_cat_bar_chart.data[0]
            ],
            'layout': self._weighted_q_cat_bar_chart.layout
        }

    def _format_responses(self):
        """
        Format the assessor scores and comments and add :class:`ReportCompiler` **content** property

        Loop through the question categories from the assessor results.
        Uses the :class:`QuestionFormatter` class to format the individual questions.
        Adds the formatted list to :class:`ReportCompiler` **content** property
        """
        self._responses = [
            {
                'pageBreak': 'before',
                'text': 'ASSESSOR SCORES AND COMMENTS',
                'style': 'subheader'
            },
            '\n',
            {
                'text': 'The assessor scores and comments for each of the applicant answers in {} are shown below, '
                        'ordered by question category.'.format(self._selected_stage_gate),
                'alignment': 'justify'
            },
            '\n'
        ]
        for qc in self._assessor_results['responses']['question_categories']:
            qc_header = {
                'text': qc['name'],
                'style': 'subsubheader'
            }
            qc_content = [
                qc_header,
                qc['description'].replace(' <br> ', '\n'),
                '\n'
            ]
            q_list = []
            for q in qc['questions']:
                q_format = QuestionFormatter(q).format_assessor_response()
                q_list.append(q_format)
            qc_content.append({
                'ul': q_list
            })
            self._responses += qc_content

        self.report_compiler.content['assessor_scores_comments'] = self._responses


class ImprovementAreaData(object):
    """
    Format the improvement areas to be included in a Stage Gate summary report.

    Acts as a wrapper around the :class:`~dtop_stagegate.business.improvement_areas.ImprovementAreas` class.
    Initialises and assesses the improvement area results and re-formats these for use in the report export feature.

    Requires an instance of :class:`ReportCompiler` class as an input argument.
    """

    def __init__(self, report_compiler):
        """
        Class is initialised as follows

        - creates an instance of :class:`~dtop_stagegate.business.improvement_areas.ImprovementAreas` class,

        - uses this instance to identify the stage and assess the improvement areas and

        - extracts the improvement area results and stores these data locally.

        :param ReportCompiler report_compiler: instance of the :class:`ReportCompiler` class.
        """
        self.report_compiler = report_compiler
        self._improvement_areas = ImprovementAreas(
            report_compiler.sg_study,
            report_compiler.stage_index,
            report_compiler.ia_threshold
        )
        self._improvement_areas.assess()
        self._ia_data = self._improvement_areas.get_data()['improvement_areas']
        self._content = []
        self._bullet_points = []

        self._format_summary()
        self._format_ia_table()

    def _format_summary(self):
        """Format the improvement area header and intro text and append to the improvement area content list"""
        self._summary = [
            {
                'pageBreak': 'before',
                'text': 'IMPROVEMENT AREAS',
                'style': 'header'
            },
            '\n',
            {
                'text': [
                    'Below is the list of improvement areas that have been identified for this Stage Gate study. '
                    'The improvement areas are those evaluation areas that have been highlighted as weaknesses of '
                    'the technology or device. Improvement areas are defined by the ',
                    {'text': 'average improvement area score', 'italics': True},
                    ', which is the average of the ',
                    {'text': 'checklist', 'italics': True},
                    ', ',
                    {'text': 'metric', 'italics': True},
                    ' and ',
                    {'text': 'weighted average assessor', 'italics': True},
                    ' scores.\n\n'
                ],
                'alignment': 'justify'
            },
            {
                'ul': [
                    'The checklist score is the result of the Activity Checklist for each evaluation area.',
                    'The metric score is calculated for each evaluation area as the number of metrics that have passed '
                    'their metric thresholds, divided by the number of metrics for that evaluation area that have '
                    'thresholds applied, and expressed as a percentage.',
                    'The assessor score is calculated as the weighted average assessor score for each evaluation area, '
                    'expressed as a percentage with respect to the maximum possible score (4)'
                ],
                'alignment': 'justify'
            },
            '\n',
            {
                'text': f"The average improvement area score is based only on those scores that are available. "
                        f"For instance, in the case where the metric and assessor scores are not applicable (e.g. "
                        f"Applicant Mode and Assessor Mode have not been completed), then the average improvement "
                        f"area score is just equal to the checklist score for each evaluation area. If only one "
                        f"column is unavailable, the average improvement area score is the average of the two "
                        f"remaining columns. \n\n The table of results is sorted by the average improvement area score "
                        f"in ascending order. The threshold level for what is considered an improvement area has been "
                        f"set as {self.report_compiler.ia_threshold}%.\n\n",
                'alignment': 'justify'
            }
        ]
        self._content += self._summary

    def _format_ia_table(self):
        """Format the improvement area bullet points and add them to :class:`ReportCompiler` **content** property"""
        formatted_improv_areas = [
            [
                ia['name'],
                ia['checklist_score'],
                ia['metric_score'],
                ia['assessor_score'],
                ia['average_score']
            ]
            for ia in self._ia_data
        ]
        headers = [
            {'fillColor': DTO_COLOURS['blue'], 'text': 'Evaluation Area', 'style': 'tableHeader'},
            {'fillColor': DTO_COLOURS['blue'], 'text': 'Checklist score (%)', 'style': 'tableHeader'},
            {'fillColor': DTO_COLOURS['blue'], 'text': 'Metric score (%)', 'style': 'tableHeader'},
            {'fillColor': DTO_COLOURS['blue'], 'text': 'Weighted average assessor score (%)', 'style': 'tableHeader'},
            {'fillColor': DTO_COLOURS['blue'], 'text': 'Average improvement area score (%)', 'style': 'tableHeader'},
        ]
        col_widths = ['auto', '*', '*', '*', '*']
        self._content += [
            {
                'style': 'tableExample',
                'table': {
                    'headerRows': 1,
                    'widths': col_widths,
                    'body': [headers] + formatted_improv_areas,
                }
            }
        ]
        self.report_compiler.content['improvement_areas'] = self._content


class ReportCompiler(object):
    """
    Main class for compiling the Stage Gate summary report.

    Organises the structure of the report and formats the appropriate data sections.

    An instance of the :class:`~dtop_stagegate.business.models.StageGateStudy` model is required as an input argument,
    as well as the request body from the FE, which should contain:

    - a *sections* field listing the sections to include in the report,

    - the index of the selected Stage to use in the analysis,

    - the index of the selected Stage Gate to use in the analysis and

    - the threshold to use for the identification of Improvement Areas

    **Public methods**

    - :meth:`construct()` - construct the report using the various classes in the *report_generation.py* module
    """

    def __init__(self, sg_study, report_export_request):
        """
        Class is initialised as follows:

        - adds the logo and header of the front page to the _content field,

        - stores local copy of the Stage Gate Study instance,

        - extracts the sections, stage_index, stage_gate_index and Improvement Area threshold properties from \
        the request body,

        - validates that the sections that have been requested align with whether the Stage and/or Stage Gate indices \
        have been provided and

        - checks if the Activity Checklist or a Stage Gate assessment in Applicant or Assessor Mode is required but \
        has not been completed.

        :param dtop_stagegate.business.models.StageGateStudy sg_study: an instance of the \
        :class:`~dtop_stagegate.business.models.StageGateStudy` model
        :param dict report_export_request: the request body specifying the sections to include in the report and the \
        indices of the selected Stage and Stage Gate to use in the analysis and report
        """
        # Private attributes
        self.content = {}
        self._stage_index = report_export_request['stage_index']
        self._stage_gate_index = report_export_request['stage_gate_index']
        self._ia_threshold = report_export_request['ia_threshold']
        if self._stage_gate_index is not None:
            self._sg_assessment = sg_study.stage_gate_assessment[self._stage_gate_index]
        else:
            self._sg_assessment = None
        self._study_data = None
        self._checklist_stage_required = False
        self._checklist_required = False
        self._improvement_areas_required = False
        self._stage_index_required = False
        self._applicant_required = False
        self._assessor_required = False
        self._stage_gate_index_required = False
        self._validation_errors = []
        self._toc_sections = [
            'Introduction',
            'Study details'
        ]

        # Public attributes
        self.sg_study = sg_study
        self.section_request = report_export_request['sections']

        # Constructor methods
        self._validate_sections()
        self._validate_checklist_complete()
        if self._sg_assessment is not None:
            self._validate_sg_assessments_complete()

    def _validate_sections(self):
        """
        Additional validation for the request body

        Ensure that the sections that have been requested align with whether the Stage and/or Stage Gate indices
        have been provided.
        Uses the :meth:`_check_main_section` method to assess the request sections input.
        The *..._required* fields are used in the logic of the :meth:`construct` method when deciding which
        sections to include.
        If the Stage index is required and not provided and/or if the Stage Gate index is required and not provided
        then sets a value for the *error* property, which will stop any further analysis.
        """
        self._checklist_stage_required = self._check_main_section([
            'checklist_stage_results',
            'checklist_outstanding_activities'
        ])
        self._checklist_required = self._checklist_stage_required or self.section_request['checklist_summary']
        self._improvement_areas_required = self.section_request['improvement_areas']
        self._stage_index_required = self._checklist_stage_required or self._improvement_areas_required
        self._applicant_required = self._check_main_section([
            'applicant_summary',
            'metric_results',
            'applicant_answers',
        ])
        self._assessor_required = self._check_main_section([
            'assessor_overall_scores',
            'assessor_question_categories',
            'assessor_scores_comments',
        ])
        self._stage_gate_index_required = self._applicant_required or self._assessor_required
        if self._stage_index is None and self._stage_index_required:
            self._validation_errors.append('Stage index required but was not provided.')
        if self._stage_gate_index is None and self._stage_gate_index_required:
            self._validation_errors.append('Stage Gate index required but was not provided.')

    def _check_main_section(self, sections):
        """
        Check if any of the sections passed as an input argument have been set to True in the request body

        :param list sections: the list of sections to check for in the request body
        :return: True or False result for whether any of the specified sections are present in the request body
        :rtype: bool
        """
        return any([self.section_request[s] for s in sections])

    def _validate_checklist_complete(self):
        """Validate the checklist_study_complete status for the requested Stage Gate Study."""
        if self._checklist_required and not self.sg_study.checklist_study_complete:
            self._validation_errors.append('The activity checklist for the requested study has not been completed.')

    def _validate_sg_assessments_complete(self):
        """Validate the SG assessment complete status of Applicant and Assessor Mode for the requested assessment"""
        if self._applicant_required and not self._sg_assessment.applicant_complete:
            self._validation_errors.append(
                'The Applicant Mode for the requested Stage Gate Assessment has not been completed.'
            )
        if self._assessor_required and not self._sg_assessment.assessor_complete:
            self._validation_errors.append(
                'The Assessor Mode for the requested Stage Gate Assessment has not been completed.'
            )

    def construct(self):
        """
        Construct the Stage Gate summary report based on the sections requested by the user.

        Implements the outer layer of logic for whether to include the main sections of the report or not.
        For example if no checklist sub-sections are request, then the ChecklistData class is not initialised and
        this main *Activity Checklist* section is not included.

        Each class that is called adds the relevant sections to the **content** property.

        Also generates a table of contents, based on the sections requested by the user.
        The study data, table of contents and section request are added regardless of the specified inputs.
        """
        # Get study content and table of contents
        self._study_data = StudyData(self)
        self._format_table_of_contents()
        self.content = {
            'study_params': self._study_data.content,
            'table_of_contents': self._toc,
            'section_request': self.section_request
        }
        # Get checklist content
        if self._checklist_required:
            ChecklistData(self)
        # Get applicant content
        if self._applicant_required:
            ApplicantData(self)
        # Get assessor content
        if self._assessor_required:
            AssessorData(self)
        # Get improvement areas
        if self._improvement_areas_required:
            ImprovementAreaData(self)

    def _format_table_of_contents(self):
        """Generate a table of contents based on the sections requested by the user"""
        # Checklist sections
        checklist_map = [
            ('Summary', 'checklist_summary'),
            ('Detailed breakdown of Stage results', 'checklist_stage_results'),
            ('Outstanding activities', 'checklist_outstanding_activities')
        ]
        self._toc_formatter(checklist_map, 'Activity Checklist')

        # Applicant section
        applicant_map = [
            ('Summary', 'applicant_summary'),
            ('Metric Results', 'metric_results'),
            ('Applicant answers', 'applicant_answers')
        ]
        self._toc_formatter(applicant_map, 'Applicant Mode')

        # Assessor section
        assessor_map = [
            ('Overall scores', 'assessor_overall_scores'),
            ('Question Category scores', 'assessor_question_categories'),
            ('Assessor scores and comments', 'assessor_scores_comments')
        ]
        self._toc_formatter(assessor_map, 'Assessor Mode')

        # Improvement areas section
        if self.section_request['improvement_areas']:
            self._toc_sections.append('Improvement Areas')

        self._toc = {
            'ul': self._toc_sections,
            'alignment': 'justify'
        }

    def _toc_formatter(self, mapping, section_name):
        """Format sub-sections for a main section in the table of contents"""
        sub_section = []
        for mp in mapping:
            if self.section_request[mp[1]]:
                sub_section.append(mp[0])
        if sub_section:
            self._toc_sections += [
                section_name,
                {
                    'type': 'circle',
                    'ul': sub_section
                }
            ]

    @property
    def errors(self):
        """
        Additional validation errors for request body.

        These errors arise if

        - there is a mismatch between the requested sections and whether the Stage and/or Stage Gate \
        indices have been provided or

        - if the Activity Checklist or a Stage Gate assessment in Applicant or Assessor Mode is required but has not \
        been completed.

        :rtype: str or None
        """
        if self._validation_errors:
            return ' '.join(self._validation_errors)
        else:
            return None

    @property
    def study_data(self):
        """
        Instance of the :class:`StudyData` class

        :rtype: StudyData
        """
        return self._study_data

    @property
    def stage_index(self):
        """
        The index of the requested Stage to include in the summary report.

        :rtype: integer
        """
        return self._stage_index

    @property
    def stage_gate_index(self):
        """
        The index of the requested Stage Gate to include in the summary report.

        :rtype: integer
        """
        return self._stage_gate_index

    @property
    def selected_stage(self):
        """
        The name of the requested Stage to include in the summary report.

        If no Stage index is specified, returns 'N/A'.

        :rtype: str
        """
        if self._stage_index is not None:
            return f"Stage {self._stage_index}"
        else:
            return 'N/A'

    @property
    def selected_stage_gate(self):
        """
        The name of the requested Stage Gate to include in the summary report.

        If no Stage Gate index is specified, returns 'N/A'.

        :rtype: str
        """
        if self._stage_gate_index is not None:
            return f"Stage Gate {self._stage_gate_index} - {self._stage_gate_index + 1}"
        else:
            return 'N/A'

    @property
    def ia_threshold(self):
        """
        The improvement area threshold.

        Default threshold is 50%, if none provided in the POST request.
        Otherwise returns the specified threshold.

        :rtype: int
        """
        if self._ia_threshold is None:
            return 50
        else:
            return self._ia_threshold
