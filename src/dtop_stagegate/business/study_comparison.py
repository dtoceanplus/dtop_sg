# This is the Stage Gate module of the DTOceanPlus suite of tools
# Copyright (C) 2021 Wave Energy Scotland - Ben Hudson
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
from .applicant_mode import ApplicantResults
from .assessor_mode import AssessorResults
from .checklists import ChecklistResults
from .models import StageGateStudy, StageGateAssessment


class StudyComparison(object):
    """
    Base class for performing the comparative analysis of multiple Stage Gate studies.

    Used as the entry-point for performing the comparative analysis of the activity checklist results.
    Accepts a list of the Study IDs to be compared and gets an instance of :class:`StageGateStudy` for each study.
    Sets the **study_error** property to *True* if any of the provided Study IDs do not exist in the database.

    **Public methods**

    - :meth:`get_checklist_comparison()` - get the results for the activity checklist comparison
    """

    def __init__(self, study_ids):
        """
        Class is initialised as follows

        - creates an instance of :class:`StageGateStudy` for each of the study IDs passed as an input argument,

        - by default sets the the **study_error** property fo *False*, if any of the study IDs do not exist in the \
        database then sets this property to *True*

        :param list study_ids: list of the study IDs to be compared
        """
        self._study_ids = study_ids
        self._sg_studies = [StageGateStudy.query.get(s) for s in study_ids]
        self.study_error = False
        if None in self._sg_studies:
            self.study_error = True

    def get_checklist_comparison(self):
        """
        Get the checklist comparison results.

        Uses the :class:`ChecklistComparison` class to retrieve the results and returns the **data** property of that
        class.
        """
        cc = ChecklistComparison(self._sg_studies)
        cc.get_checklist_comparison_data()

        return cc.data


class ChecklistComparison(object):
    """
    Retrieves the activity checklist results for each of the specified studies and formats the data for front-end.

    Loops through each study in the comparison and creates an instance of the :class:`ChecklistResults` class.
    Re-structures the checklist results data to the appropriate format required for the GUI layer.

    **Public methods**

    - :meth:`get_checklist_comparison_data` - retrieve and format the Checklist Results data
    """

    def __init__(self, sg_studies):
        """
        Class is initialised as follows:

        - initialises the main **_data** dictionary to be populated after calling :meth:`get_checklist_comparison_data`,

        - also sets empty lists for the activity category and evaluation area arrays.

        :param list sg_studies: list of the Stage Gate study instances
        """
        self._sg_studies = sg_studies
        self._data = {
            'summary': [],
            'bar_chart_data': [
                {
                    'idx': i,
                    'name': 'Stage {}'.format(i),
                    'activity_categories': [],
                    'evaluation_areas': []
                }
                for i in range(6)
            ]
        }
        self._activity_category_data = []
        self._evaluation_area_data = []

    def get_checklist_comparison_data(self):
        """
        Loop through the Stage Gate studies, get the ChecklistResults data and format.

        Creates an instance of :class:`ChecklistResults` for each study.
        First formats the comparison summary results.
        Then formats the data required for the bar-charts in the front-end
        """
        for study in self._sg_studies:
            checklist_results = ChecklistResults(study.id).results
            self._format_checklist_comparison_summary(study, checklist_results)
            for stg in self._data['bar_chart_data']:
                self._format_checklist_comparison_bar_chart_data(study, checklist_results, stg, 'activity_categories')
                self._format_checklist_comparison_bar_chart_data(study, checklist_results, stg, 'evaluation_areas')

    def _format_checklist_comparison_summary(self, s, checklist_results):
        """
        Format the summary data for a single Stage Gate study and append it to the Checklist Comparison data.

        :param StageGateStudy s: the instance of :class:`StageGateStudy`
        :param list checklist_results: the *results* property of the :class:`ChecklistResults` class
        """
        study_name = s.name
        d = {
            cr['name'].lower().replace(" ", "_"): '{}%'.format(cr['percent_complete'])
            for cr in checklist_results
        }
        d['name'] = study_name
        self._data['summary'].append(d)

    @staticmethod
    def _format_checklist_comparison_bar_chart_data(study, checklist_results, stage, category):
        """
        Format the checklist comparison data for the bar-charts in the front-end.

        :param StageGateStudy study: the instance of :class:`StageGateStudy`
        :param list checklist_results: the *results* property of the :class:`ChecklistResults` class
        :param dict stage: the appropriate dictionary in 'bar_chart_data' of ChecklistComparison for the specified stage
        :param string category: the category to analyse, 'activity_categories' or 'evaluation_areas'
        """
        cr_stage = next((x for x in checklist_results if x['name'] == stage['name']), None)
        data = cr_stage[category]
        d_out = {
            'y': [],
            'x': [],
            'name': study.name,
            'type': 'bar',
            'orientation': 'h'
        }
        for cat in data:
            d_out['y'].append(cat['name'])
            d_out['x'].append(cat['percent'])
        stage[category].append(d_out)

    @property
    def data(self):
        """
        The comparison results data for the activity checklist

        :rtype: dict
        """
        return self._data


class StageGateAssessmentComparison(object):
    """
    Base class for performing the comparative analysis of multiple Stage Gate assessments.

    Used as the entry-point for performing the comparative analysis of the applicant mode and assessor mode results.
    Accepts a list of the StageGateAssessment IDs to be compared and gets an instance of
    :class:`StageGateAssessment` for each Stage Gate assessment.
    Sets the **sg_assess_error** property to *True* if any of the provided Stage Gate assessment IDs do not exist in the
    database.

    **Public methods**

    - :meth:`get_applicant_comparison()` - get the results for the applicant mode comparison

    - :meth:`get_assessor_comparison()` - get the results for the assessor mode comparison
    """

    def __init__(self, sg_assess_ids):
        """
        Class is initialised as follows

        - creates an instance of :class:`StageGateAssessment` for each of the stage gate assessment IDs passed as an \
        input argument,

        - by default sets the the **sg_assess_error** property fo *False*, if any of the stage gate assessment IDs do \
        not exist in the database then sets this property to *True*

        :param list sg_assess_ids: list of the stage gate assessment IDs to be compared
        """
        self._sg_assess_ids = sg_assess_ids
        self._sg_assessments = [StageGateAssessment.query.get(sgi) for sgi in sg_assess_ids]
        self.sg_assess_error = False
        if None in self._sg_assessments:
            self.sg_assess_error = True

    def get_applicant_comparison(self):
        """
        Get the applicant mode comparison results.

        Uses the :class:`ApplicantComparison` class to retrieve the results and returns the **data** property of that
        class.
        """
        ac = ApplicantComparison(self._sg_assessments)
        ac.get_applicant_comparison_data()

        return ac.data

    def get_assessor_comparison(self):
        """
        Get the assessor mode comparison results.

        Uses the :class:`AssessorComparison` class to retrieve the results and returns the **data** property of that
        class.
        """
        ac = AssessorComparison(self._sg_assessments)
        ac.get_assessor_comparison_data()

        return ac.data


class ApplicantComparison(object):
    """
    Retrieves the applicant mode results for each of the specified assessments and formats the data for front-end.

    Loops through each stage gate assessment in the comparison and creates an instance of the :class:`ApplicantResults`
    class.
    Re-structures the applicant results data to the appropriate format required for the GUI layer.

    **Public methods**

    - :meth:`get_applicant_comparison_data` - retrieve and format the Applicant Comparison data
    """

    def __init__(self, sg_assessments):
        """
        Class requires a list of instances of :class:`StageGateAssessment` class to be initialised.

        Also initialises the three main output data entities; *study columns*, *summary data* and *metric data*.

        :param list sg_assessments: list of stage gate assessment instances
        """
        self._sg_assessments = sg_assessments
        self._study_label = ''
        self._study_name = ''
        self._applicant_results = None
        self._study_columns = []
        self._summary_data = []
        self._metric_data = []

    def get_applicant_comparison_data(self):
        """
        Loop through the Stage Gate assessments, get the ApplicantResults data and format the output.

        Extracts the label of the Stage Gate study corresponding to each of the stage gate assessments in turn.
        Also creates a *_study_name* property for each study which is in the format *study_{id}*.
        Uses the study names and labels to create the *study columns* property.
        Then creates an instance of :class:`ApplicantResults` for each stage gate assessment.
        Uses the calculated data to format both the summary data and the metric data from each assessment.
        """
        for i, sga in enumerate(self._sg_assessments):
            self._study_label = sga.stage_gate_study.name
            self._study_name = 'study_{}'.format(sga.stage_gate_study.id)
            self._format_study_column_data()
            self._applicant_results = ApplicantResults(sga)
            self._applicant_results.calculate()
            self._format_summary_data(sga)
            self._format_metric_data(i)

    def _format_study_column_data(self):
        """Append the name and label of each study to the *study columns* property."""
        self._study_columns.append(
            {
                'prop': self._study_name,
                'label': self._study_label
            }
        )

    def _format_summary_data(self, sga):
        """
        Extract the response and threshold success rates for an assessment and append data to *summary data* property.

        :param StageGateAssessment sga: instance of the Stage Gate Assessment being analysed
        """
        self._summary_data.append(
            {
                'study_label': self._study_label,
                'sg_name': sga.stage_gate.name,
                'sg_assess_id': sga.id,
                'response_rate': self._applicant_results.response_rate,
                'threshold_success_rate': self._applicant_results.threshold_success_rate
            }
        )

    def _format_metric_data(self, i):
        """
        Format the metric results data for each stage gate assessment.

        For the first stage gate assessment being analysed, creates the *data* property for each of the items in the
        *metric data* field.
        This *data* property is a list of dictionaries, which each dictionary containing the name of the appropriate
        study (this corresponds to the study label in the *study columns* property) and the appropriate metric result.
        For subsequent stage gate assessments, appends the appropriate study name and metric result to this *data*
        property.

        :param int i: the enumerated value of the stage gate assessments being analysed
        """
        if i == 0:
            keys = ['metric', 'unit', 'threshold_type', 'threshold']
            for mr in self._applicant_results.metric_results:
                metric_data = {k: mr[k] for k in keys}
                metric_data['data'] = [{
                    'name': self._study_name,
                    'result': mr['result']
                }]
                self._metric_data.append(metric_data)
        else:
            metric_results = self._applicant_results.metric_results
            for md in self._metric_data:
                mr = next((x for x in metric_results if x['metric'] == md['metric']), None)
                md['data'].append({
                    'name': self._study_name,
                    'result': mr['result']
                })

    @property
    def data(self):
        """
        Contains data entities for

        - study_columns; the names and labels for the studies associated with each of the stage gate assessments,

        - summary_data; the summary response rate and threshold success rate variables for each assessment and

        - metric_data; the metric results for each of the stage gate assessments, in the format required for the \
        front-end.

        :rtype: dict
        """
        d = {
            'study_columns': self._study_columns,
            'summary_data': self._summary_data,
            'metric_data': self._metric_data
        }

        return d


class AssessorComparison(object):
    """
    Retrieves the assessor mode results for each of the specified assessments and formats the data for front-end.

    Loops through each stage gate assessment in the comparison and creates an instance of the :class:`AssessorResults`
    class.
    Re-structures the assessor results data to the appropriate format required for the GUI layer.

    **Public methods**

    - :meth:`get_assessor_comparison_data` - retrieve the Assessor Results for each of the stage gate assessments \
    being compared
    """

    def __init__(self, sg_assessments):
        """
        Class requires a list of instances of :class:`StageGateAssessment` class to be initialised.

        Also initialises the three main output data entities; *summary data*, *question categories* and
        *evaluation areas*.

        :param list sg_assessments: list of stage gate assessment instances
        """
        self._sg_assessments = sg_assessments
        self._study_name = ''
        self._assessor_results = None
        self._summary_data = []
        self._question_categories = {
            'weighted_average': [],
            'average': []
        }
        self._evaluation_areas = {
            'weighted_average': [],
            'average': []
        }

    def get_assessor_comparison_data(self):
        """
        Loop through the Stage Gate assessments, get the AssessorResults data and format the output.

        First extracts the name of the Stage Gate study corresponding to each of the stage gate assessments in turn.
        Then creates an instance of :class:`AssessorResults` for each stage gate assessment.
        Then formats the summary data for each stage gate assessment.
        Finally, it formats the Assessor Results data for both question categories and evaluation areas.
        """
        for sga in self._sg_assessments:
            if sga.assessor_complete:
                self._study_name = sga.stage_gate_study.name
                self._assessor_results = AssessorResults(sga)
                self._assessor_results.calculate()
                self._format_summary_data()
                self._format_category_data(self._question_categories, 'question_category_results')
                self._format_category_data(self._evaluation_areas, 'evaluation_area_results')

    def _format_summary_data(self):
        """Extract the study name, weighted average score and average score for each stage gate assessment"""
        self._summary_data.append(
            {
                "name": self._study_name,
                "orientation": "h",
                "type": "bar",
                "x": [
                    self._assessor_results.overall_average,
                    self._assessor_results.overall_weighted_average
                ],
                "y": [
                    "Average",
                    "Weighted average"
                ]
            }
        )

    def _format_category_data(self, category_dict, category_prop):
        """
        Format the question category or evaluation area applicant results for a specific stage gate assessment.

        :param dict category_dict: the appropriate dictionary to append the applicant results, either \
        *self._question_categories* or *self._evaluation_areas*
        :param str category_prop: the name of the category corresponding to the property to extract from the applicant \
        results, either *question_category_results* or *evaluation_area_results*
        :return:
        """
        cat_data = getattr(self._assessor_results, category_prop)
        for d in cat_data:
            d['name'] = self._study_name
            d['orientation'] = "h"
            d['type'] = "bar"
        category_dict['weighted_average'].append(cat_data[0])
        category_dict['average'].append(cat_data[1])

    @property
    def data(self):
        """
        Contains data entities for

        - summary_data; the bar chart data for the weighted average and average assessor scores for the overall score \
        of each stage gate assessment being compared,

        - question_categories; the bar chart data for the weighted average and average assessor scores categorised by \
        question category, for each stage gate assessment being compared and

        - evaluation_areas; the bar chart data for the weighted average and average assessor scores categorised by \
        evaluation area, for each stage gate assessment being compared.

        :rtype: dict
        """
        d = {
            'summary_data': self._summary_data,
            'question_categories': self._question_categories,
            'evaluation_areas': self._evaluation_areas
        }

        return d
