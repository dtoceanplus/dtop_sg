# This is the Stage Gate module of the DTOceanPlus suite of tools
# Copyright (C) 2021 Wave Energy Scotland - Ben Hudson
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
from .applicant_mode import ApplicantResults
from .checklists import ChecklistResults
from .complexity_level import ComplexityLevelMap
from .models import StageGateAssessment
from .assessor_mode import AssessorResults

import pandas as pd


class ImprovementAreas(object):
    """
    Get the improvement areas for a Stage Gate study.

    Implements the service for the Structured Innovation (SI) module that identifies the evaluation areas that have
    been identified as weaknesses by the Stage Gate module.
    The "average improvement area score" is a percentage number that is based on three components:

    1.  **Checklist percentages** - the percentage number of activities completed for an evaluation area in a specific \
    stage, taken from the results of the Activity Checklist feature (see :class:`ChecklistPercentages`).

    2.  **Metric percentages** - for each Stage Gate, there are a number of metrics associated with each \
    Evaluation Area. The metric percentage for each Evaluation Area is the number of these metrics that have "passed" \
    any metric thresholds that have been set expressed as a percentage of the total number of metrics that are \
    "tagged" to that Evaluation Area. For instance, if metric thresholds were set for two metrics "tagged" to the \
    Power Conversion Evaluation Area, and the metric result for one passes while the other fails, the \
    metric percentage for Power Conversion would be 50%. Remember that no metric thresholds are set by default. See \
    :class:`MetricPercentages` for more details.

    3. **Assessor percentages** - the weighted average assessor score for each evaluation area is calculated and \
    expressed as a percentage (i.e. dividing the weighted average by 4 and expressing the figure as a percentage). \
    See :class:`AssessorPercentages` for more details.

    The class accepts an optional input argument for the *stage index* which specifies the Stage to use in the analysis.
    If Stage 1, 2, 3 or 4 is selected, the Stage Gate is set to be the one preceding the selected stage, otherwise
    the improvement area analysis for Stage Gates is not performed (see numbered bullets below).
    If no *stage index* is specified, then the class can infer the stage and stage gate to use in the assessment by
    assessing the activity checklist results.
    In this case, the :class:`~dtop_stagegate.business.complexity_level.ComplexityLevelMap` class is used to identify
    the stage that the user has reached.
    The stage that the user has reached is defined as the latest consecutive stage, starting from
    0, that has reached 100% completion of activities.
    The stage that the improvement area analysis will be performed on is the next stage after the stage that has been
    fully completed.
    Then there are three ways that the analysis will proceed:

    1.  If the user has not achieved 100% completion of Stage 0, then the improvement area analysis is performed on \
    Stage 0. In this case, the improvement area analysis does not consider the failed threshold or assessor score \
    causes.

    2.  If the user has completed 100% of activities for all of the Stages, then no improvement area analysis will be
    performed. In this case, the back-end will return an emtpy list for the improvement areas.

    3.  Otherwise, the analysis will consist of all three potential causes. The Stage Gate analyses will be performed \
    on the Stage Gate preceding the stage that is being assessed.

    For example, if the user has completed all activities in Stage 2, then the checklist percentage analysis will be
    performed on Stage 3, while the failed threshold and assessor score analyses will be performed on Stage Gate 2 - 3.

    The three individual scores are calculated and then the average improvement area score is calculated as the average
    of the three numbers.
    The results are filtered for those with a score less than the specified 'improvement area score threshold', which
    has a default value of 50%.
    Finally, the results are ordered by the average improvement area score in ascending order.

    An instance of the :class:`~dtop_stagegate.business.models.StageGateStudy` model is required as an input argument.
    As explained above, there is also an optional input argument for the index of the Stage to use as the basis for the
    improvement area analysis.

    **Public methods**

    - :meth:`assess()` - perform the improvement area assessment for each of the three components, calculate the \
    average score, select average scores less than improvement area threshold and rank the improvement areas by \
    average score in ascending order

    - :meth:`get_data()` - retrieve the improvement area data
    """

    def __init__(self, sg_study, stage_index=None, ia_threshold=50):
        """
        Class is initialised by identifying the selected stage and stage gate.

        If the *stage index* argument is provided and is greater than 0 and less than 1, it identifies the Stage Gate
        as that one immediately preceding the specified stage.
        Otherwise, the stage gate index and name fields are not initialised.
        If no *stage index* argument is provided, then the class infers which stage and stage-gate to use.

        :param dtop_stagegate.business.models.StageGateStudy sg_study: an instance of the \
        :class:`~dtop_stagegate.business.models.StageGateStudy` model
        :param int stage_index: the index of the Stage to use as the basis of the improvement area assessment
        """
        # Private attributes
        self._sg_study = sg_study
        self._sg_study_id = sg_study.id
        self._stage_idx = stage_index
        self._ia_threshold = ia_threshold
        self._sg_idx = None
        self._sg_name = 'N/A'
        self.sg_assessment = None
        self._checklist_percentages = []
        self._metric_percentages = []
        self._assessor_percentages = []
        self._improvement_areas = []

        if self._stage_idx is not None and 0 < self._stage_idx < 5:
            self._load_stage_gate_details()
            self._get_sg_assessment()
        elif self._stage_idx is None:
            self._infer_stage_and_stage_gate()

    def _load_stage_gate_details(self):
        """
        If the selected stage is greater than 0 and less than 5, set the Stage Gate index to be the one preceding
        the selected stage index and then infer Stage Gate name.
        """
        self._sg_idx = self._stage_idx - 1
        self._sg_name = f"Stage Gate {self._sg_idx} - {self._stage_idx}"

    def _get_sg_assessment(self):
        """Get the instance of the specific Stage Gate assessment being assessed."""
        self.sg_assessment = StageGateAssessment.query.filter(
            StageGateAssessment.stage_gate_study_id == self._sg_study_id
        )[self._sg_idx]

    def _infer_stage_and_stage_gate(self):
        """
        Identify the stage and stage gate to use as the basis for the improvement area analysis.

        Gets the latest consecutive stage that has been reached for the stage gate study.

        If the user has not yet fully completed Stage 0, sets the stage to be assessed to Stage 0.
        In this case, the stage gate criteria for improvement areas will not be assessed.

        Otherwise, as long as Stage 5 has not yet been fully completed, then the stage to be assessed is set as the
        stage after the latest stage that has been reached, and the stage gate to be assessed is set as the immediately
        after the latest stage that has been reached.

        If Stage 5 has been fully completed, the improvement area analysis for Stage Gates will not be performed.
        """
        cpx_levels = ComplexityLevelMap(self._sg_study).get_complexity_levels()
        if cpx_levels.get('message', None) is not None:
            self._stage_idx = 0
        elif cpx_levels['stage'] < 5:
            cpx_stage = cpx_levels['stage']
            self._stage_idx = cpx_stage + 1
            self._load_stage_gate_details()
            self._get_sg_assessment()
        elif cpx_levels['stage'] == 5:
            self._stage_idx = 5

    def _calculate_avg_score_and_sort(self):
        """
        Calculate the average improvement area scores and sort the resulting data.

        - Converts the three component parts to pandas DataFrames and concatenates them together (joining on the \
        evaluation area name).
        - Calculates the average score for each row of the df.
        - Sorts by average score in ascending order.
        - Filters for average score values less than or equal to the improvement area score threshold.
        - Fills null values with "N/A"
        - Converts to dictionary
        - In the case where the metric or assessor percentages consist of empty lists, then retrospectively adds "N/A" \
        values for the appropriate columns.
        """
        score_dfs = [pd.DataFrame(i).set_index('name') for i in [
                self._checklist_percentages,
                self._metric_percentages,
                self._assessor_percentages
        ] if i != []]
        df = pd.concat(score_dfs, axis=1, join='outer')
        df['average_score'] = df.mean(axis=1)
        df.sort_values('average_score', inplace=True)
        df.index.name = 'name'
        df.reset_index(inplace=True)
        df = df[df['average_score'] <= self._ia_threshold]
        df.fillna('N/A', inplace=True)
        self._improvement_areas = df.round(0).to_dict(orient="records")
        if not self._metric_percentages:
            for ia in self._improvement_areas:
                ia['metric_score'] = 'N/A'
        if not self._assessor_percentages:
            for ia in self._improvement_areas:
                ia['assessor_score'] = 'N/A'

    def assess(self):
        """
        Assess and identify the improvement areas

        - Performs the improvement area assessment for each of the three components
        - Calculates the average score
        - Selects average scores less than improvement area threshold and
        - Ranks the improvement areas by average score in ascending order

        This method calls each of the :class:`ChecklistPercentages`, :class:`MetricPercentages` and
        :class:`AssessorPercentages` classes as appropriate.
        """
        self._checklist_percentages = ChecklistPercentages(self._sg_study.id, self._stage_idx).checklist_percentages
        if self.sg_assessment is not None and self.sg_assessment.applicant_complete:
            self._metric_percentages = MetricPercentages(self.sg_assessment).metric_percentages
            if self.sg_assessment.assessor_complete:
                self._assessor_percentages = AssessorPercentages(self.sg_assessment).assessor_percentages
        self._calculate_avg_score_and_sort()

    def get_data(self):
        """
        Get the output data of the improvement area analysis.

        Rank the causes, order the improvement areas by the number of causes for each evaluation area and return the
        output data in the correct format.

        :return: the output data of the improvement area analysis
        :rtype: dict
        """
        data = {
            'stage_assessed': self._stage_idx,
            'stage_gate_assessed': self._sg_name,
            'improvement_areas': self._improvement_areas
        }

        return data


class ChecklistPercentages(object):
    """
    Get the Checklist Percentages from the Checklist Results.

    A wrapper around the :class:`~dtop_stagegate.business.checklists.ChecklistResults` class.
    Loops through the activity checklist results that are categorised by evaluation area.
    Ignores evaluation areas that do not have any associated activities (e.g. the 'Other' evaluation area)

    The ID of the stage gate study and the index of the stage are both required to initialise the class.
    """

    def __init__(self, sg_study_id, stage_idx):
        """
        Class is initialised as follows:

        - the checklist results are obtained using :class:`~dtop_stagegate.business.checklists.ChecklistResults` and

        - the checklist percentages are obtained.

        :param str sg_study_id: the ID of the stage gate study
        :param int stage_idx: the index of the stage
        """
        self._checklist_results = ChecklistResults(sg_study_id)
        self._stage_idx = stage_idx
        self._eval_areas = None
        self._checklist_percentages = None

        self._get_checklist_percentages()

    def _get_checklist_percentages(self):
        """Get the checklist percentages by extracting from the checklist results"""
        self._eval_areas = self._checklist_results.results[self._stage_idx].get('evaluation_areas')
        self._checklist_percentages = [
            {
                "name": ea['name'],
                "checklist_score": ea['percent']
            }
            for ea in self._eval_areas if (ea['total'] != 0)
        ]

    @property
    def checklist_percentages(self):
        """
        The Checklist Percentages for each Evaluation Area, based on the Activity Checklist results.

        :rtype: list
        """
        return self._checklist_percentages


class MetricPercentages(object):
    """
    Get the Metric Percentages using the Applicant Mode results.

    A wrapper around the :class:`~dtop_stagegate.business.applicant_mode.ApplicantResults` class.
    Initialises and calculates the applicant results and extracts the metric results component.
    Calculates the Metric Percentage by looping through the metric results and, for each evaluation area, calculating
    the total number of tagged metrics, as well as the number of metrics that have "passed" any metric thresholds.
    The metric percentage is then the "passed" metrics divided by the total number of metrics, for each evaluation area,
    expressed as a percentage.
    Calculation ignores metrics that do not have thresholds specified.

    An instance of the :class:`~dtop_stagegate.business.models.StageGateAssessment` database model is required to
    initialise the class.
    """

    def __init__(self, sg_assessment):
        """
        Class is initialised as follows:

        - the applicant results are obtained using the \
        :class:`~dtop_stagegate.business.applicant_mode.ApplicantResults` class and

        - the metric percentages are calculated.

        :param StageGateAssessment sg_assessment: instance of the \
        :class:`~dtop_stagegate.business.models.StageGateAssessment` model
        """
        self._sg_assessment = sg_assessment
        self._metric_results = None
        self._metric_percentages = []

        self._calculate()

    def _calculate(self):
        """Get the applicant results and calculate the metric percentages, as described above"""
        app_results = ApplicantResults(self._sg_assessment)
        app_results.calculate()
        self._metric_results = app_results.metric_results
        eval_area_record = []
        metric_percentage_calc = []
        for mr in self._metric_results:
            ea = mr['evaluation_area']
            if (ea not in eval_area_record) and mr['threshold_bool']:
                eval_area_record.append(ea)
                metric_percentage_calc.append({
                    'name': ea,
                    'total': 1,
                    'passed': int(mr['threshold_passed'])
                })
            elif mr['threshold_bool']:
                metric_calc_ea = next(item for item in metric_percentage_calc if item["name"] == ea)
                metric_calc_ea['total'] += 1
                metric_calc_ea['passed'] += int(mr['threshold_passed'])
        self._metric_percentages = [{
            'name': d['name'],
            'metric_score': calculate_percentage(d['passed'], d['total'])
        } for d in metric_percentage_calc]

    @property
    def metric_percentages(self):
        """
        The Metric Percentages for each Evaluation Area, calculated using Applicant Mode results.

        :rtype: list
        """
        return self._metric_percentages


class AssessorPercentages(object):
    """
    Get the Assessor Percentages using the Assessor Mode results.

    A wrapper around the :class:`~dtop_stagegate.business.assessor_mode.AssessorResults` class.
    Gets the assessor scores as categorised by evaluation area.
    Then calculates the Assessor Percentages by taking the weighted average assessor score for each evaluation area
    and expressing this as a percentage (dividing by the maximum possible score, 4, and multiplying by 100).

    An instance of the :class:`~dtop_stagegate.business.models.StageGateAssessment` database model is required to
    initialise the class.
    """

    def __init__(self, sg_assessment):
        """
        Class is initialised as follows:

        - the assessor results are obtained using the \
        :class:`~dtop_stagegate.business.assessor_mode.AssessorResults` class and

        - the Assessor Percentages are calculated.

        :param StageGateAssessment sg_assessment: instance of the \
        :class:`~dtop_stagegate.business.models.StageGateAssessment` model
        """
        self._sg_assessment = sg_assessment
        self._assessor_results = None
        self._ea_scores = None
        self._assessor_percentages = None

        self._get_assessor_scores_by_eval_area()
        self._get_assessor_percentages()

    def _get_assessor_scores_by_eval_area(self):
        """Get the assessor results as categorised by evaluation area"""
        assessor_results = AssessorResults(self._sg_assessment)
        assessor_results.calculate()
        self._ea_scores = assessor_results.evaluation_area_results

    def _get_assessor_percentages(self):
        """Calculates the assessor percentages based on the weighted average assessor score"""
        ea_dict = self._ea_scores[0]  # weighted average, index 0
        self._assessor_percentages = [
            {
                "name": ea_dict['y'][idx],
                "assessor_score": calculate_percentage(val, 4.0)
            }
            for idx, val in enumerate(ea_dict['x'])
        ]

    @property
    def assessor_percentages(self):
        """
        The Assessor Percentages for each Evaluation Area, based on the Assessor Mode results.

        :rtype: list
        """
        return self._assessor_percentages


def calculate_percentage(num, denom):
    """
    Calculate percentage based on numerator and denominator.

    Divides the numerator by the denominator, multiplies by 100 and rounds to the nearest integer.

    :param float num: numerator
    :param float denom: denominator
    :return: percentage result
    :rtype: int
    """
    p = round(num*100/denom)
    return p
