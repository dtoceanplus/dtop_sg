# This is the Stage Gate module of the DTOceanPlus suite of tools
# Copyright (C) 2021 Wave Energy Scotland - Ben Hudson
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
from dtop_stagegate.service import db
from dtop_stagegate.service.api.frameworks import get_sg_stage_gate
from .models import StageGateAssessment, ApplicantAnswer


def get_stage_gate_assessments(study_id):
    """
    Get list of Stage Gate Assessments for the specified Stage Gate study ID.

    :param str study_id: the ID of the Stage Gate study
    :return: the SQLAlchemy query of :class:`~dtop_stagegate.business.models.StageGateAssessment` class, filtered by \
    study ID
    :rtype: flask_sqlalchemy.BaseQuery
    """
    stage_gate_assessments = StageGateAssessment.query.filter_by(stage_gate_study_id=study_id)

    return stage_gate_assessments


def get_applicant_answers_by_stage_gate_assessment_id(sg_assessment_id):
    """
    Get a list of all the applicant answers for the specified stage gate assessment id.

    Searches the SQL database for applicant answers whose stage_gate_assessment_id property is equal to the passed
    input argument.
    Reads the :class:`~dtop_stagegate.business.models.ApplicantAnswer` database table.

    :param str sg_assessment_id: the ID of the stage gate assessment
    :return: the applicant answers for that stage gate assessment
    :rtype: list
    """
    applicant_answer_list = ApplicantAnswer.query.filter_by(stage_gate_assessment_id=sg_assessment_id).all()

    return applicant_answer_list


class ApplicantStageGateData(object):
    """
    Join applicant answers or results to stage gate data, sorting by the question ID.

    Loads the requested stage gate entity from the database and inserts the corresponding list of applicant answers or
    applicant results for each question.

    For **applicant answers**, extracts the request applicant answers from the
    :class:`~dtop_stagegate.business.models.ApplicantAnswer` database table.
    Then loops through the stage gate data and adds the applicant answers to the appropriate question in the nested
    dictionary.

    For **applicant results**, it first calculates the results using the :class:`ApplicantResults` class and performs
    the same loop using the applicant results rather than the applicant answers.

    An instance of the :class:`~dtop_stagegate.business.models.StageGateAssessment` model is the only required input
    argument.
    In the case of analysing *applicant results*, the optional *applicant_analysis_list* is required.

    **Public methods**

    - :meth:`join_data()` - join the stage gate data to the applicant answers or results.
    """

    def __init__(self, stage_gate_assessment, applicant_analysis_list=None):
        """
        Class is initialised as follows:

        - the stage gate data is extracted using :meth:`~dtop_stagegate.service.api.frameworks.get_sg_stage_gate`,

        - if the *applicant_analysis_list* argument is *not* provided;

          - retrieves the applicant answers using the :meth:`get_applicant_answers_by_stage_gate_assessment_id` method,

          - assigns the *_add_applicant_func* method to the *_add_applicant_answers* method.

        - if the *applicant_analysis_list* argument is provided;

          - sets the *_app_answer_list* attribute to be equal to the provided *applicant_analysis_list* argument and

          - assigns the *_add_applicant_func* method to the *_add_applicant_results* method.

        :param StageGateAssessment stage_gate_assessment: instance of the \
        :class:`~dtop_stagegate.business.models.StageGateAssessment` model
        :param list applicant_analysis_list: optional input argument; list of instances of \
        :class:`ApplicantAnswerAnalysis` object
        """
        self._sg_assessment = stage_gate_assessment
        self._sg_assessment_id = stage_gate_assessment.id
        self._stage_gate_id = stage_gate_assessment.stage_gate_id
        self._stage_gate = get_sg_stage_gate(self._stage_gate_id)
        if applicant_analysis_list is None:
            self._app_answer_list = get_applicant_answers_by_stage_gate_assessment_id(self._sg_assessment_id)
            self._add_applicant_func = self._add_applicant_answers
        else:
            self._app_answer_list = applicant_analysis_list
            self._add_applicant_func = self._add_applicant_results

    @staticmethod
    def _add_applicant_answers(app_answer):
        """Create a dictionary containing the id, result, response and justification of the ApplicationAnswer input."""
        answer_dict = {
            'id': app_answer.id,
            'result': app_answer.result,
            'response': app_answer.response,
            'justification': app_answer.justification
        }

        return 'applicant_answers', answer_dict

    def _add_applicant_results(self, app_answer):
        """Create a dictionary containing the applicant answers and results for metric questions"""
        results_dict = self._add_applicant_answers(app_answer)[1]
        if app_answer.metric_question:
            ta = app_answer.threshold_analysis
            results_dict['threshold_passed'] = ta.threshold_passed
            results_dict['absolute_distance'] = ta.absolute_distance
            results_dict['percent_distance'] = ta.percent_distance
        else:
            results_dict['threshold_passed'] = None
            results_dict['absolute_distance'] = None
            results_dict['percent_distance'] = None

        return 'applicant_results', results_dict

    def join_data(self):
        """
        Join the stage gate data to the applicant answers or results.

        Loops through the requested stage gate entity from the database and inserts the corresponding list of applicant
        answers or applicant results to the appropriate question in the nested dictionary.

        Calls the *_add_applicant_func* method which is assigned to either *_add_applicant_answers* or
        *_add_applicant_results* depending on whether the applicant answers or results are being analysed.

        :return: the applicant answers or results, categorised by 'Question' and 'Question Category' for the specified \
        stage gate
        :rtype: dict
        """
        for q_cat in self._stage_gate['question_categories']:
            for q in q_cat['questions']:
                aa = next((x for x in self._app_answer_list if x.question_id == q['id']), None)
                lab, app_dict = self._add_applicant_func(aa)
                q[lab] = app_dict

        return self._stage_gate


def update_applicant_complete_status(sg_assessment):
    """
    Update the *applicant_complete* property of the :class:`~dtop_stagegate.business.models.StageGateAssessment`
    database entity for the specified Stage Gate Assessment.

    Sets the status to *True*.

    :param StageGateAssessment sg_assessment: instance of the \
    :class:`~dtop_stagegate.business.models.StageGateAssessment` model
    """
    sg_assessment.applicant_complete = True
    db.session.commit()


def update_applicant_answers_in_db(sg_assessment, data):
    """
    Update the applicant answers in the local database for the specified Stage Gate Assessment ID.

    Loops through the request body data and updates the applicant answers in the database table.
    Returns an error if any of the applicant answer IDs in the request body data do not correspond to the list of
    applicant answers that are linked to the specified stage gate assessment entry.

    :param StageGateAssessment sg_assessment: instance of the \
    :class:`~dtop_stagegate.business.models.StageGateAssessment` model
    :param list data: the request body containing the applicant answers to be updated
    """
    app_answer_list = ApplicantAnswer.query.filter_by(stage_gate_assessment_id=sg_assessment.id).all()
    aa_ids = [aa.id for aa in app_answer_list]
    requested_ids = [answer['id'] for answer in data]
    if bool(set(requested_ids).difference(aa_ids)):
        return "Request body contains applicant answers with invalid IDs"
    for app_answer in data:
        app_answer_id = app_answer['id']
        aa = ApplicantAnswer.query.get(app_answer_id)
        result = app_answer.get('result', None)
        if result is not None:
            aa.result = result
        response = app_answer.get('response', None)
        if response is not None:
            aa.response = response
        justification = app_answer.get('justification', None)
        if justification is not None:
            aa.justification = justification
    db.session.commit()


class ApplicantResults(object):
    """
    The main class for calculating the results of an Applicant Mode study.

    Summary:

    - Uses the :class:`ApplicantAnswerAnalysis` class to perform the analysis on each applicant answer.

    - Calculates the summary parameters for the applicant mode results.

    - Extracts the metric results for any metric questions in the analysis.

    - Joins the applicant results to the stage gate data.

    Requires an instance of :class:`~dtop_stagegate.business.models.StageGateAssessment` object for initialising.

    **Public methods**

    - :meth:`calculate()` - perform the assessment of the applicant answers
    - :meth:`get_results()` - update and return the applicant mode results data
    """

    def __init__(self, stage_gate_assessment):
        """
        Class is initialised as follows:

        - the list of applicant answers for the stage gate assessment in question are extracted from the SQL database \
        using the :meth:`get_applicant_answers_by_stage_gate_assessment_id` function and

        - an instance of the :class:`ApplicantAnswerAnalysis` class is created for each applicant answer in this list.

        :param StageGateAssessment stage_gate_assessment: instance of the \
        :class:`~dtop_stagegate.business.models.StageGateAssessment` model
        """
        self._stage_gate_assessment = stage_gate_assessment
        self._response_rate = None
        self._threshold_success_rate = None
        self._results = None
        self._stage_data = None
        self._metric_results = None

        self._applicant_answer_list = get_applicant_answers_by_stage_gate_assessment_id(stage_gate_assessment.id)
        self._applicant_analysis_list = [ApplicantAnswerAnalysis(a) for a in self._applicant_answer_list]
        self._app_stage_gate_data = None

    def _analyse_applicant_answers(self):
        """Call the *analyse* method of each applicant analysis instance"""
        [app_analysis.analyse() for app_analysis in self._applicant_analysis_list]

    def _calculate_summary_params(self):
        """
        Calculate the summary parameters for the applicant mode results.

        - The response rate is the number of answered questions divided by the total number of questions, multiplied \
        by 100 and rounded to the nearest integer.

        - The threshold success rate is the number of metric questions with applied thresholds that have passed \
        divided by the number of metric questions with applied thresholds, multiplied by 100 and rounded to the \
        nearest integer.

        Only calculates the threshold success rate if the user has set a threshold (no default thresholds)
        """
        num_answered = len([app_answer for app_answer in self._applicant_analysis_list if app_answer.answered])
        self._response_rate = round(100 * num_answered / len(self._applicant_analysis_list))
        answers_with_thresholds = [
            app_answer for app_answer in self._applicant_analysis_list if app_answer.threshold_bool
        ]
        num_passed_thresholds = len([at for at in answers_with_thresholds if at.threshold_analysis.threshold_passed])
        if len(answers_with_thresholds) > 0:
            self._threshold_success_rate = round(100*num_passed_thresholds/len(answers_with_thresholds))

    def calculate(self):
        """
        The calculation procedure is as follows:

        - each instance of :class:`ApplicantAnswerAnalysis` in the *_applicant_analysis_list* is analysed by calling \
        the :meth:`ApplicantAnswerAnalysis.analyse()` method,

        - the summary parameters (response rate and threshold success rate) are calculated,

        - the metric results are extracted for each applicant answer that is a metric question and

        - the stage gate data is joined to the applicant results using :meth:`ApplicantStageGateData.join_data()`.
        """
        self._analyse_applicant_answers()
        self._calculate_summary_params()
        self._metric_results = [
            app_answer.threshold_analysis.data for app_answer in self._applicant_analysis_list
            if app_answer.metric_question
        ]
        self._app_stage_gate_data = ApplicantStageGateData(self._stage_gate_assessment, self._applicant_analysis_list)
        self._stage_data = self._app_stage_gate_data.join_data()

    def get_results(self):
        """
        The applicant results are assigned to the *_results* attribute and returned.

        :return: the results of the applicant mode analysis including summary data, metric results and responses to \
        the stage gate questions including the calculated applicant results
        :rtype: dict
        """
        self._results = {
            'summary_data': {
                'response_rate': self._response_rate,
                'threshold_success_rate': self._threshold_success_rate
            },
            'metric_results': self._metric_results,
            'responses': self._stage_data
        }

        return self._results

    @property
    def response_rate(self):
        """
        The number of answered questions divided by the total number of questions, multiplied by 100 and rounded
        to the nearest integer.

        :rtype: int
        """
        return self._response_rate

    @property
    def threshold_success_rate(self):
        """
        The number of metric questions with applied thresholds that have passed divided by the number of metric
        questions with applied thresholds, multiplied by 100 and rounded to the nearest integer.

        :rtype: int
        """
        return self._threshold_success_rate

    @property
    def metric_results(self):
        """
        The metric results for the applicant mode assessment.

        :rtype: list
        """
        return self._metric_results


class ApplicantAnswerAnalysis(object):
    """
    Perform the analysis for a single applicant answer.

    Summary:

    - Determines whether the question is a *metric question* or not.

    - Assesses whether the question, either qualitative or quantitative, has been answered.

    - Initialises and performs the threshold analysis using the :class:`ThresholdAnalysis` class.

    Requires an instance of :class:`~dtop_stagegate.business.models.ApplicantAnswer` to be initialised.

    **Public methods**

    - :meth:`analyse()` - perform the analysis of the applicant answer
    """

    def __init__(self, applicant_answer_instance):
        """
        The class is initialised by asserting whether the question is a metric question or not.

        A *metric question*, also referred to as a *quantitative question* is a question that is directly associated
        with a metric.

        :param ApplicantAnswer applicant_answer_instance: an instance of the ApplicantAnswer class
        """
        self._app_answer = applicant_answer_instance
        self._metric_question = self._assert_metric_question()
        self._answered = False
        self._threshold_bool = False
        self._id = applicant_answer_instance.id
        self._result = applicant_answer_instance.result
        self._justification = applicant_answer_instance.justification
        self._response = applicant_answer_instance.response
        self._question_id = self._app_answer.question_id
        self._threshold_analysis = None

    def _assert_metric_question(self):
        """Return True if the *question.question_metric* attribute of the applicant answer is not None"""
        if self._app_answer.question.question_metric:
            return True
        else:
            return False

    def _assert_qualitative_answered_status(self):
        """Return True if the *response* attribute of the applicant answer is not None"""
        if self._app_answer.response:
            self._answered = True

    def _assert_quantitative_answered_status(self):
        """Return True if both the *result* and *justification* attributes of the applicant answer are not None"""
        if self._app_answer.result and self._app_answer.justification:
            self._answered = True

    def analyse(self):
        """
        Analyse the applicant answer.

        If the question is a *metric question*:

        - Asserts whether the quantitative question has been answered.

        - Initialises an instance of :class:`ThresholdAnalysis`.

        - If the threshold of the metric question has been applied then performs the :class:`ThresholdAnalysis`.

        If the question is **not** a *metric question*:

        - Asserts whether the qualitative question has been answered.
        """
        if self._metric_question:
            self._assert_quantitative_answered_status()
            self._threshold_analysis = ThresholdAnalysis(self._app_answer)
            if self._threshold_analysis.threshold_bool:
                self._threshold_bool = True
                self._threshold_analysis.calculate()
        else:
            self._assert_qualitative_answered_status()

    @property
    def answered(self):
        """
        Boolean value for whether the applicant question has been answered or not.

        *True* if the question has been answered, *False* otherwise.

        :rtype: bool
        """
        return self._answered

    @property
    def threshold_bool(self):
        """
        Boolean for whether the threshold has been applied for the metric question or not.

        Set to be the same as the *question_metric.threshold_bool* attribute of the corresponding question.

        :rtype: bool
        """
        return self._threshold_bool

    @property
    def metric_question(self):
        """
        Boolean value for whether the question is a *metric question* or not.

        *True* if the question is a *metric question*, *False* otherwise.

        :rtype: bool
        """
        return self._metric_question

    @property
    def id(self):
        """
        The ID of the corresponding :class:`~dtop_stagegate.business.models.ApplicantAnswer` object.

        :rtype: int
        """
        return self._id

    @property
    def result(self):
        """
        The result property of the corresponding :class:`~dtop_stagegate.business.models.ApplicantAnswer` object.

        :rtype: float
        """
        return self._result

    @property
    def justification(self):
        """
        The justification property of the corresponding :class:`~dtop_stagegate.business.models.ApplicantAnswer` object.

        :rtype: str
        """
        return self._justification

    @property
    def response(self):
        """
        The response property of the corresponding :class:`~dtop_stagegate.business.models.ApplicantAnswer` object.

        :rtype: str
        """
        return self._response

    @property
    def question_id(self):
        """
        The question_id property of the corresponding :class:`~dtop_stagegate.business.models.ApplicantAnswer` object.

        :rtype: int
        """
        return self._question_id

    @property
    def threshold_analysis(self):
        """
        An instance of the corresponding :class:`ThresholdAnalysis` class.

        :rtype: ThresholdAnalysis
        """
        return self._threshold_analysis


class ThresholdAnalysis(object):
    """
    Evaluate whether the threshold has been passed and calculate absolute and percentage distance if not.

    Summary:

    - Evaluates the threshold by comparing the result to the threshold, taking account of whether the threshold is of \
    type *upper* or *lower*.

    - If the result fails the threshold, i.e. if the threshold was not achieved, then calculates the absolute and \
    percentage distance between the threshold and the metric result achieved.

    Requires an instance of :class:`~dtop_stagegate.business.models.ApplicantAnswer` to be initialised.

    **Public methods**

    - :meth:`calculate()` - compare threshold to result, calculate distances if necessary and update the data property
    """

    def __init__(self, app_answer):
        """
        Class is initialised by extracting the relevant properties from the *question metric* property of the question.

        A private *_threshold_type_map* attribute is also defined that relates the strict inequality symbol to the \
        type of threshold (*upper* or *lower*).
        This is required for the threshold inequality evaluation.
        The threshold inequalities are evaluated as such:

        - **upper**: the threshold has passed if the result is less than or equal to the threshold \
        (i.e. *result <= threshold*)

        - **lower**: the threshold has passed if the result is greater than or equal to the threshold \
        (i.e. *result >= threshold*)

        :param ApplicantAnswer app_answer: an instance of the ApplicantAnswer class
        """
        self._description = app_answer.question.description
        self._result = app_answer.result
        self._justification = app_answer.justification
        qm = app_answer.question.question_metric
        self._metric = qm.metric
        self._metric_name = self._metric.name
        self._unit = self._metric.unit
        self._threshold_type = self._metric.threshold_type
        self._threshold_bool = qm.threshold_bool
        self._threshold = qm.threshold
        self._evaluation_area = self._metric.evaluation_area.name
        self._threshold_type_map = {
            'lower': '>=',
            'upper': '<='
        }

        self._threshold_passed = None
        self._absolute_distance = None
        self._percent_distance = None
        self._set_output_data()

    def _evaluate_threshold(self):
        """Perform the evaluation of the threshold inequality"""
        self._threshold_passed = eval('{} {} {}'.format(
            self._result,
            self._threshold_type_map[self._threshold_type],
            self._threshold
        ))

    def _calculate_absolute_distance(self):
        """Calculate the absolute difference between the metric result and the threshold"""
        self._absolute_distance = abs(self._result - self._threshold)

    def _calculate_percent_distance(self):
        """Calculate the percentage distance between the result and the threshold; returns the percent value
        rounded to the nearest integer"""
        self._percent_distance = round(100 * self._absolute_distance / self._threshold)

    def _set_output_data(self):
        """Compiles the relevant attributes into the output data dictionary format"""
        self._data = {
            'metric': self._metric_name,
            'unit': self._unit,
            'threshold': self._threshold,
            'result': self._result,
            'justification': self._justification,
            'absolute_distance': self._absolute_distance,
            'percent_distance': self._percent_distance,
            'threshold_bool': self._threshold_bool,
            'threshold_passed': self._threshold_passed,
            'threshold_type': self._threshold_type,
            'evaluation_area': self._evaluation_area,
            'description': self._description
        }

    def calculate(self):
        """
        Perform the evaluation and calculations required for the threshold analysis.

        If a result for the metric has not been provided, sets the *threshold_passed* property as *False* and does \
        not evaluate the threshold or calculate distances between metrics and thresholds.

        If a result for the the metric has been provided then the calculation proceeds as follows:

        - Evaluates the threshold.

        - If the threshold has not passed:

          - Calculates the absolute distance between the metric result and threshold and

          - Calculates the percent distance between the metric result and threshold.

        - Updates the local *_data* attribute based on the threshold evaluation and calculations.
        """
        if self._result is not None:
            self._evaluate_threshold()
            if not self._threshold_passed:
                self._calculate_absolute_distance()
                self._calculate_percent_distance()
        else:
            self._threshold_passed = False
        self._set_output_data()

    @property
    def threshold_bool(self):
        """
        Boolean for whether the threshold has been applied for the metric question or not.

        Set to be the same as the *question_metric.threshold_bool* attribute of the corresponding question.

        :rtype: bool
        """
        return self._threshold_bool

    @property
    def threshold_passed(self):
        """
        Boolean value of whether the threshold has been passed or not.

        *True* if the threshold has been passed.

        :rtype: bool
        """
        return self._threshold_passed

    @property
    def absolute_distance(self):
        """
        The absolute distance between the threshold and the metric result.

        :rtype: float
        """
        return self._absolute_distance

    @property
    def percent_distance(self):
        """
        The percentage distance between the threshold and the metric result.

        Defined as the absolute distance divided by the threshold value, multiplied by 100 and rounded to the nearest
        integer.

        :rtype: int
        """
        return self._percent_distance

    @property
    def data(self):
        """
        Output data of the threshold analysis.

        A required public attribute for use in :class:`ApplicantResults` class.

        :rtype: dict
        """
        return self._data
