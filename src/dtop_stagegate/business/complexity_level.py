# This is the Stage Gate module of the DTOceanPlus suite of tools
# Copyright (C) 2021 Wave Energy Scotland - Ben Hudson
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
from .checklists import ChecklistResults


class ComplexityLevelMap(object):
    """
    Get the appropriate complexity level for each of the Deployment and Assessment tools.

    Contains the *combination matrix* that links stages of the framework to the complexity levels of the D&A tools.
    Also returns the stage that the user has reached and the subsequent stage gate that is suggested.
    Only possible to get the complexity level if a user has completed the Activity Checklist.
    Also returns a 404 error if the user has not completed all of the activities in *any* of the stages.

    Uses the :class:`~dtop_stagegate.business.checklist.ChecklistResults` class to get the percent complete status of
    each stage.

    An instance of the :class:`~dtop_stagegate.business.models.StageGateStudy` model is required as an input argument.

    **Public methods**

    - :meth:`get_complexity_levels()` - return the stage and stage gate that the user has reached, as well as the \
    appropriate complexity level to be used for each Deployment and Assessment tool.
    """

    def __init__(self, stage_gate_study):
        """
        Class is initialised as follows:

        - the **checklist complete** property is set to *False* by default, this property is set to *True* if the \
        Stage Gate study **checklist_complete_status** property is *True*,

        - the *combination matrix* is defined, mapping complexity levels and stages.

        :param dtop_stagegate.business.models.StageGateStudy stage_gate_study: an instance of the \
        :class:`~dtop_stagegate.business.models.StageGateStudy` model
        """
        self._sg_study = stage_gate_study
        self._checklist_complete = False
        self._checklist_results = None
        self._combination_matrix = {
            0:
                {
                    'stage': 0,
                    'stage_gate': 'Stage Gate 0 - 1',
                    'complexity_levels': {
                        "site_characterisation": 1,
                        "machine_characterisation": 1,
                        "energy_capture": 1,
                        "energy_transformation": 1,
                        "energy_delivery": 1,
                        "station_keeping": 1,
                        "logistics_marine_ops": 1,
                        "system_performance_energy_yield": 1,
                        "rams": 1,
                        "system_lifetime_cost": 1,
                        "environmental_social_acceptance": 1
                    }
                },
            1:
                {
                    'stage': 1,
                    'stage_gate': 'Stage Gate 1 - 2',
                    'complexity_levels': {
                        "site_characterisation": 1,
                        "machine_characterisation": 1,
                        "energy_capture": 1,
                        "energy_transformation": 1,
                        "energy_delivery": 1,
                        "station_keeping": 1,
                        "logistics_marine_ops": 1,
                        "system_performance_energy_yield": 1,
                        "rams": 1,
                        "system_lifetime_cost": 1,
                        "environmental_social_acceptance": 1
                    }
                },
            2:
                {
                    'stage': 2,
                    'stage_gate': 'Stage Gate 2 - 3',
                    'complexity_levels': {
                        "site_characterisation": 2,
                        "machine_characterisation": 2,
                        "energy_capture": 2,
                        "energy_transformation": 2,
                        "energy_delivery": 1,
                        "station_keeping": 1,
                        "logistics_marine_ops": 1,
                        "system_performance_energy_yield": 1,
                        "rams": 1,
                        "system_lifetime_cost": 1,
                        "environmental_social_acceptance": 2
                    }
                },
            3:
                {
                    'stage': 3,
                    'stage_gate': 'Stage Gate 3 - 4',
                    'complexity_levels': {
                        "site_characterisation": 2,
                        "machine_characterisation": 2,
                        "energy_capture": 2,
                        "energy_transformation": 2,
                        "energy_delivery": 2,
                        "station_keeping": 2,
                        "logistics_marine_ops": 2,
                        "system_performance_energy_yield": 1,
                        "rams": 2,
                        "system_lifetime_cost": 2,
                        "environmental_social_acceptance": 2
                    }
                },
            4:
                {
                    'stage': 4,
                    'stage_gate': 'Stage Gate 4 - 5',
                    'complexity_levels': {
                        "site_characterisation": 3,
                        "machine_characterisation": 3,
                        "energy_capture": 3,
                        "energy_transformation": 3,
                        "energy_delivery": 3,
                        "station_keeping": 3,
                        "logistics_marine_ops": 3,
                        "system_performance_energy_yield": 1,
                        "rams": 3,
                        "system_lifetime_cost": 3,
                        "environmental_social_acceptance": 3
                    }
                },
            5:
                {
                    'stage': 5,
                    'stage_gate': 'NA',
                    'complexity_levels': {
                        "site_characterisation": 3,
                        "machine_characterisation": 3,
                        "energy_capture": 3,
                        "energy_transformation": 3,
                        "energy_delivery": 3,
                        "station_keeping": 3,
                        "logistics_marine_ops": 3,
                        "system_performance_energy_yield": 1,
                        "rams": 3,
                        "system_lifetime_cost": 3,
                        "environmental_social_acceptance": 3
                    }
                }
        }

        self._assess_checklist_complete_status()

    def _assess_checklist_complete_status(self):
        """Assess the whether the activity checklist for the study has been completed"""
        if self._sg_study.checklist_study_complete:
            self._checklist_complete = True

    def _get_checklist_results(self):
        """Get the results of the checklist using :class:`~dtop_stagegate.business.checklist.ChecklistResults` class"""
        self._checklist_results = ChecklistResults(self._sg_study.id).results

    def get_complexity_levels(self):
        """
        Get the complexity levels for the latest stage that has reached 100% completion.

        Starting at Stage 0, finds the latest consecutive stage that has a 100% of activities marked as complete.
        If the user has not completed all of the activities in Stage 0, returns an error message.

        :return: the complexity level map if a user has completed 100% of activities in a stage, otherwise an error \
        message
        :rtype: dict
        """
        self._get_checklist_results()
        stg_idx = -1
        for s in self._checklist_results:
            if s['percent_complete'] == 100:
                stg_idx += 1
            else:
                break
        if stg_idx > -1:
            return self._combination_matrix[stg_idx]
        else:
            return {'message': 'The technology/device has not completed all of the activities in any of the stages'}

    @property
    def checklist_complete(self):
        """
        The *checklist_study_complete* status of the Stage Gate study.

        :rtype: bool
        """
        return self._checklist_complete
