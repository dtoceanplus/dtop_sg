# This is the Stage Gate module of the DTOceanPlus suite of tools
# Copyright (C) 2021 Wave Energy Scotland - Ben Hudson
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
from marshmallow import fields, validates, ValidationError

from .models import Framework, Stage, ActivityCategory, Activity, ActivityEvalArea, StageGate, QuestionCategory, \
    Question, QuestionMetric, ScoringCriterion, Rubric, Grade, Metric, EvaluationArea, ChecklistActivity, \
    StageGateStudy, StageGateAssessment, ApplicantAnswer, AssessorScore, AssessorComment
from ..service import ma, db


class EvaluationAreaSchema(ma.ModelSchema):
    """Marshmallow schema for Evaluation Area database model"""

    class Meta:
        model = EvaluationArea
        sqla_session = db.session


class MetricSchema(ma.ModelSchema):
    """Marshmallow schema for Metric database model"""

    evaluation_area = fields.Nested(EvaluationAreaSchema)
    evaluation_area_id = fields.Integer(load_only=True)

    class Meta:
        model = Metric
        sqla_session = db.session


class ActivityEvalAreaSchema(ma.ModelSchema):
    """Marshmallow schema for Activity Metric database model"""

    class Meta:
        model = ActivityEvalArea
        sqla_session = db.session


class ActivitySchema(ma.ModelSchema):
    """Marshmallow schema for Activity database model"""

    class Meta:
        model = Activity
        sqla_session = db.session


class ChecklistActivitySchema(ma.ModelSchema):
    """Marshmallow schema for Checklist Activity database model"""

    activity_id = fields.Integer()
    stage_gate_study_id = fields.Integer()

    class Meta:
        model = ChecklistActivity
        sqla_session = db.session


class ActivityCategorySchema(ma.ModelSchema):
    """Marshmallow schema for Activity Category database model"""

    activities = fields.Nested(ActivitySchema, many=True)

    class Meta:
        model = ActivityCategory
        sqla_session = db.session


class StageSchema(ma.ModelSchema):
    """Marshmallow schema for Stage database model"""

    activity_categories = fields.Nested(ActivityCategorySchema, many=True)

    class Meta:
        model = Stage
        sqla_session = db.session


class GradeSchema(ma.ModelSchema):
    """Marshmallow schema for Grade database model"""

    class Meta:
        model = Grade
        sqla_session = db.session


class RubricSchema(ma.ModelSchema):
    """Marshmallow schema for Rubric database model"""

    grades = fields.Nested(GradeSchema, many=True)

    class Meta:
        model = Rubric
        sqla_session = db.session


class ScoringCriterionSchema(ma.ModelSchema):
    """Marshmallow schema for Scoring Criterion database model"""

    class Meta:
        model = ScoringCriterion
        sqla_session = db.session


class QuestionMetricSchema(ma.ModelSchema):
    """Marshmallow schema for Question Metric database model"""

    metric = fields.Nested(MetricSchema)
    metric_id = fields.Integer(load_only=True)

    class Meta:
        model = QuestionMetric
        sqla_session = db.session


class QuestionSchema(ma.ModelSchema):
    """Marshmallow schema for Question database model"""

    scoring_criteria = fields.Nested(ScoringCriterionSchema, many=True)
    question_metric = fields.Nested(QuestionMetricSchema)

    class Meta:
        model = Question
        sqla_session = db.session


class ApplicantAnswerSchema(ma.ModelSchema):
    """Marshmallow schema for Applicant Answer database model"""

    id = fields.Integer(required=True)
    question_id = fields.Integer()
    stage_gate_assessment_id = fields.Integer()

    class Meta:
        model = ApplicantAnswer
        sqla_session = db.session


class AssessorScoreSchema(ma.ModelSchema):
    """Marshmallow schema for Assessor Score database model"""

    id = fields.Integer(required=True)
    question_id = fields.Integer()
    stage_gate_assessment_id = fields.Integer()

    class Meta:
        model = AssessorScore
        sqla_session = db.session


class AssessorCommentSchema(ma.ModelSchema):
    """Marshmallow schema for Assessor Comment database model"""

    id = fields.Integer(required=True)
    scoring_criterion_id = fields.Integer()
    stage_gate_assessment_id = fields.Integer()

    class Meta:
        model = AssessorComment
        sqla_session = db.session


class QuestionCategorySchema(ma.ModelSchema):
    """Marshmallow schema for Question Category database model"""

    questions = fields.Nested(QuestionSchema, many=True)

    class Meta:
        model = QuestionCategory
        sqla_session = db.session


class StageGateSchema(ma.ModelSchema):
    """Marshmallow schema for Stage Gate database model"""

    question_categories = fields.Nested(QuestionCategorySchema, many=True)

    class Meta:
        model = StageGate
        sqla_session = db.session


class StageGateAssessmentSchema(ma.ModelSchema):
    """Marshmallow schema for Stage Gate Assessment database model"""

    stage_gate = fields.Pluck(StageGateSchema, "name")
    stage_gate_id = fields.Integer()
    stage_gate_study_id = fields.Integer()

    class Meta:
        model = StageGateAssessment
        sqla_session = db.session


class LoadFrameworkSchema(ma.ModelSchema):
    """Marshmallow model schema for de-serialising Framework data stored in a nested JSON file"""

    stages = fields.Nested(StageSchema, many=True)
    stage_gates = fields.Nested(StageGateSchema, many=True)

    class Meta:
        model = Framework
        sqla_session = db.session


class FrameworkSchema(ma.ModelSchema):
    """Marshmallow model schema for serialising Framework data. Includes hyperlinks to stages and stage-gates"""

    class Meta:
        model = Framework
        sqla_session = db.session

    _links = ma.Hyperlinks(
        {
            "self": ma.URLFor("api_frameworks.get_sg_framework", framework_id="<id>"),
            "collection": ma.URLFor("api_frameworks.get_sg_framework_list")}
    )
    stages = ma.List(ma.HyperlinkRelated("api_stages.get_sg_stage", url_key="stage_id"))
    stage_gates = ma.List(ma.HyperlinkRelated("api_stage_gates.get_sg_stage_gate", url_key="stage_gate_id"))


class NameDescriptionSchema(ma.Schema):
    """
    Marshmallow schema for loading a request body that contains a *name* and a *description*.

    Name field is required and cannot be empty. Schema validates the provided data against these criteria.
    """

    name = fields.String(
        required=True,
        error_messages={"required": "Name is required"}
    )
    description = fields.String()

    @validates("name")
    def validate_name(self, value):
        """Validate name field is not empty"""
        if len(value) == 0:
            raise ValidationError("Name cannot be empty")


class UpdateQuestionMetricSchema(ma.Schema):
    """
    Marshmallow schema for updating a Question Metric entry in the framework database.

    One or both of the *threshold* and *threshold_bool* parameters can be updated. The schema ensures that a boolean
    value provided for *threshold_bool* and that the data provided for the *threshold* parameter can be converted to a
    float.
    """

    threshold_bool = fields.Boolean()
    threshold = fields.Float()


class UpdateListQuestionMetricSchema(ma.Schema):
    """
    Marshmallow schema for updating a list of Question Metrics.

    Schema ensures that request body consists of a list of dicts with id, threshold and threshold_bool fields.
    """

    id = fields.Integer(required=True)
    threshold_bool = fields.Boolean(required=True)
    threshold = fields.Float(required=True)


class StageGateStudySchema(ma.ModelSchema):
    """Marshmallow schema for Stage Gate Study database model"""

    framework_id = fields.Integer()
    framework = fields.Pluck(FrameworkSchema, "name")
    checklist_study_complete = fields.Boolean(required=False, default=False)

    class Meta:
        model = StageGateStudy
        sqla_session = db.session

    _links = ma.Hyperlinks(
        {
            "self": ma.URLFor("api_stage_gate_studies.get_sg_study", study_id="<id>"),
            "collection": ma.URLFor("api_stage_gate_studies.get_sg_study_list")
        }
    )


class LoadStageGateStudySchema(ma.Schema):
    """
    Marshmallow schema for loading the request body for creation of a Stage Gate study.

    Name field is required and cannot be empty. Schema validates the provided data against these criteria.
    framework_id field is also required and must be an integer.
    """

    name = fields.String(
        required=True,
        error_messages={"required": "Name is required."}
    )
    description = fields.String()
    framework_id = fields.Integer(
        required=True,
        error_messages={"required": "Framework needs to be specified."}
    )

    @validates("name")
    def validate_name(self, value):
        """Validate name field is not empty"""
        if len(value) == 0:
            raise ValidationError("Name cannot be empty.")

    @validates("framework_id")
    def validate_framework_id(self, framework_id):
        """Validate that the framework ID exists"""
        f = Framework.query.get(framework_id)
        if f is None:
            raise ValidationError("Framework with that ID does not exist.")


class UpdateChecklistInputsSchema(ma.Schema):
    """
    Marshmallow schema for loading the request body for updating the Checklist Activities for a Stage Gate study.

    *study_id* and *completed_activities* are required inputs. Schema validates that study_id is a string and
    completed activities is a list.
    """

    completed_activities = fields.List(
        fields.Integer(),
        required=True,
        error_messages={"required": "List of completed activities required"}
    )


class ImprovementAreaSchema(ma.Schema):
    """
    Marshmallow schema for loading the request body for retrieving the Improvement Area analysis.

    Must contain the single *stage_index* field but this is nullable.
    """
    stage_index = fields.Integer(
        allow_none=True,
        required=True,
        error_messages={"required": "The Stage index is required"}
    )
    threshold = fields.Integer(
        allow_none=True,
        required=True,
        error_messages={"required": "The threshold for improvement areas is required"}
    )


class ReportSectionSchema(ma.Schema):
    """
    Marshmallow schema for the section field of the LoadReportExportSchema.

    Each field in the request body must be a Boolean and corresponds to a section of the summary report.
    """

    checklist_summary = fields.Boolean(
        required=True,
        error_messages={"required": "checklist_summary field is required."}
    )
    checklist_stage_results = fields.Boolean(
        required=True,
        error_messages={"required": "checklist_stage_results field is required."}
    )
    checklist_outstanding_activities = fields.Boolean(
        required=True,
        error_messages={"required": "checklist_outstanding_activities field is required."}
    )
    applicant_summary = fields.Boolean(
        required=True,
        error_messages={"required": "applicant_summary field is required."}
    )
    metric_results = fields.Boolean(
        required=True,
        error_messages={"required": "metric_results field is required."}
    )
    applicant_answers = fields.Boolean(
        required=True,
        error_messages={"required": "applicant_answers field is required."}
    )
    assessor_overall_scores = fields.Boolean(
        required=True,
        error_messages={"required": "assessor_overall_scores field is required."}
    )
    assessor_question_categories = fields.Boolean(
        required=True,
        error_messages={"required": "assessor_question_categories field is required."}
    )
    assessor_scores_comments = fields.Boolean(
        required=True,
        error_messages={"required": "assessor_scores_comments field is required."}
    )
    improvement_areas = fields.Boolean(
        required=True,
        error_messages={"required": "improvement_areas field is required."}
    )


class LoadReportExportSchema(ma.Schema):
    """
    Marshmallow schema for loading the request body for generating the Summary Stage Gate report.

    Comprises the list of sections to include in the report, which is a nested field using ReportSectionSchema, as well
    as the indices for both the Stage and Stage Gate.
    The Stage and Stage Gate indices are required but they are nullable, i.e. a None value can be returned for these
    two fields.
    """

    sections = fields.Nested(
        ReportSectionSchema,
        required=True,
        error_messages={"required": "The nested sections field is required"}
    )
    stage_index = fields.Integer(
        allow_none=True,
        required=True,
        error_messages={"required": "The Stage index is required"}
    )
    stage_gate_index = fields.Integer(
        allow_none=True,
        required=True,
        error_messages={"required": "The Stage Gate index is required"}
    )
    ia_threshold = fields.Integer(
        allow_none=True,
        required=True,
        error_messages={"required": "The improvement area threshold is required"}
    )
