# This is the Stage Gate module of the DTOceanPlus suite of tools
# Copyright (C) 2021 Wave Energy Scotland - Ben Hudson
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
import os

from flask import Flask
from flask_babel import Babel
from flask_cors import CORS
from flask_marshmallow import Marshmallow
from flask_sqlalchemy import SQLAlchemy

basedir = os.path.abspath(os.path.dirname(__file__))
babel = Babel()
db = SQLAlchemy()
ma = Marshmallow()


def create_app(test_config=None):

    # create and configure the app
    app = Flask(__name__, instance_relative_config=True)
    app.config.from_mapping(
        SECRET_KEY='dev',
        SQLALCHEMY_DATABASE_URI=os.environ.get('DATABASE_URL') or
        'sqlite:///' + os.path.join(app.instance_path, 'stagegate.db'),
        SQLALCHEMY_TRACK_MODIFICATIONS=False
    )

    # enable CORS
    CORS(app, resources={r'/*': {'origins': '*'}})

    babel.init_app(app)

    if test_config is None:
        # load the instance config, if it exists, when not testing
        app.config.from_pyfile('config.py', silent=True)
    else:
        # load the test config if passed in
        app.config.from_mapping(test_config)

    # ensure the instance folder exists
    try:
        os.makedirs(app.instance_path)
    except OSError:
        pass

    db.init_app(app)
    ma.init_app(app)

    from dtop_stagegate.business import db_utils
    db_utils.init_app(app)

    # Registering Blueprints
    from .api import doc as doc_api
    app.register_blueprint(doc_api.bp, url_prefix='/api')

    from .api import stage_gate_studies as sg_studies
    app.register_blueprint(sg_studies.bp, url_prefix='/api/stage-gate-studies')

    from .api import frameworks as sg_frameworks
    app.register_blueprint(sg_frameworks.bp, url_prefix='/api/frameworks')
    app.register_blueprint(sg_frameworks.bp_stages, url_prefix='/api/stages')
    app.register_blueprint(sg_frameworks.bp_stage_gates, url_prefix='/api/stage-gates')
    app.register_blueprint(sg_frameworks.bp_question_metric, url_prefix='/api/question-metrics')

    from .api import applicant_mode as app_mode
    app.register_blueprint(app_mode.bp, url_prefix='/api/stage-gate-assessments')

    from .api import assessor_mode as assessor_mode
    app.register_blueprint(assessor_mode.bp, url_prefix='/api/stage-gate-assessments')

    if os.environ.get("FLASK_ENV") == "development":
        from dtop_stagegate.service.api import provider_states

        app.register_blueprint(provider_states.bp)

    return app


# Uncomment to be able to use DB in flask shell context;
# from dtop_stagegate.business.models import ChecklistActivity, Activity, ActivityCategory, Stage, AssessorScore, \
#     Question, QuestionCategory, QuestionMetric, Metric, EvaluationArea, StageGateAssessment
# from dtop_stagegate.business.checklists import ChecklistResults
#
#
# app = create_app()
#
#
# @app.shell_context_processor
# def make_shell_context():
#     d = {
#         'db': db,
#         'ChecklistActivity': ChecklistActivity,
#         'Activity': Activity,
#         'ActivityCategory': ActivityCategory,
#         'Stage': Stage,
#         'ChecklistResults': ChecklistResults,
#         'AssessorScore': AssessorScore,
#         'Question': Question,
#         'QuestionCategory': QuestionCategory,
#         'QuestionMetric': QuestionMetric,
#         'Metric': Metric,
#         'EvaluationArea': EvaluationArea,
#         'StageGateAssessment': StageGateAssessment
#     }
#     return d
