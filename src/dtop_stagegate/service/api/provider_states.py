# This is the Stage Gate module of the DTOceanPlus suite of tools
# Copyright (C) 2021 Wave Energy Scotland - Ben Hudson
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
from flask import Blueprint, request, jsonify

from dtop_stagegate.business.db_utils import init_db, install_db
from dtop_stagegate.business.applicant_mode import get_applicant_answers_by_stage_gate_assessment_id
from dtop_stagegate.business.frameworks import create_framework, update_question_metric
from dtop_stagegate.business.models import Activity, ActivityCategory, Stage, ChecklistActivity, StageGateStudy
from dtop_stagegate.business.stage_gate_studies import create_study
from dtop_stagegate.service import db
from dtop_stagegate.service.api.errors import bad_request


bp = Blueprint("provider_states", __name__)


@bp.route("/provider_states/setup", methods=["POST"])
def provider_states_setup():
    """
    Reset database, install default framework and create data necessary for contract testing.
    Route only available when `FLASK_ENV=development`.
    """
    init_db()
    install_db()
    state = request.json["state"]

    if state == 'sg 1 exists and has checklist results':
        configure_checklist_complete_state()
    elif state == 'sg 1 exists and has improvement areas':
        configure_improvement_areas_state()
    elif state == 'sg 1 exists and has metric results':
        configure_metric_results()
    elif state == 'sg 1 exists and has metric thresholds':
        configure_metric_thresholds()
    else:
        return bad_request("No matching state")

    return format_response(state)


def format_response(state):
    """Return JSON response for successful configuration of provider state"""
    response = jsonify({'message': f"State configured for: {state}"})
    response.status_code = 201

    return response


def configure_checklist_complete_state():
    """
    Configure state so that a study exists and the checklist has been completed

    - Adds the Stage Gate study

    - Sets the *checklist_study_complete* property to true

    - Marks all activities in the first stage as *Complete*
    """
    add_sg_study(1)
    study = StageGateStudy.query.get('1')
    study.checklist_study_complete = True
    ca = set_complete_status_for_stage('1')

    return ca


def add_sg_study(f_id):
    """Add Stage Gate study to db"""
    study_data = dict(
        name='test study 1',
        description='description for test stage gate study 1',
        framework_id=f_id
    )
    create_study(study_data)


def set_complete_status_for_stage(stg_id):
    """Mark all the activities in the specified Stage as complete"""
    ca = db.session.query(ChecklistActivity). \
        join(Activity). \
        join(ActivityCategory). \
        join(Stage). \
        filter(Stage.id == stg_id).all()
    for a in ca:
        a.complete = True
    db.session.commit()

    return ca


def configure_improvement_areas_state():
    """
    Configure state for improvement areas service

    - Configures checklist complete status

    - Marks one of the checklist activities as incomplete
    """
    ca = configure_checklist_complete_state()
    ca[24].complete = 0
    db.session.commit()


def configure_metric_results():
    """Add some dummy metric results to be passed to SI as part of PACT for metric results route"""
    add_sg_study(1)
    set_complete_status_for_stage(1)
    set_complete_status_for_stage(2)
    app_answer_list = get_applicant_answers_by_stage_gate_assessment_id('2')
    app_answer_list[5].result = 147.
    app_answer_list[5].justification = 'a present for SI'
    app_answer_list[11].result = 18
    app_answer_list[11].justification = 'YOU SHALL NOT PACT!!!!!'
    db.session.commit()


def configure_metric_thresholds():
    """Add new framework and set some example thresholds for SI to use as part of metric thresholds PACT"""
    f = {
        "name": "test framework name",
        "description": "test framework description"
    }
    create_framework(f)
    threshold = {
        "threshold_bool": True,
        "threshold": 150.
    }
    update_question_metric(threshold, "109")
    add_sg_study(2)
    set_complete_status_for_stage(7)
    set_complete_status_for_stage(8)
    db.session.commit()
