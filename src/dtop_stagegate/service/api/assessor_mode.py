# This is the Stage Gate module of the DTOceanPlus suite of tools
# Copyright (C) 2021 Wave Energy Scotland - Ben Hudson
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
from flask import Blueprint, request, jsonify
from marshmallow import ValidationError

from dtop_stagegate.business.assessor_mode import AssessorStageGateData, update_assessor_data_in_db, AssessorResults, \
    update_assessor_complete_status
from dtop_stagegate.business.models import StageGateAssessment, AssessorScore
from dtop_stagegate.business.schemas import AssessorScoreSchema, AssessorCommentSchema
from dtop_stagegate.business.study_comparison import StageGateAssessmentComparison
from dtop_stagegate.service.api.errors import not_found, bad_request
from dtop_stagegate.service.api.utils import join_validation_error_messages

bp = Blueprint('api_assessor_mode', __name__)


@bp.route('/<sg_assess_id>/assessor-scores', methods=['GET'])
def get_assessor_scores(sg_assess_id):
    """
    Get the assessor scores for a specified Stage Gate.

    Extracts the Stage Gate Assessment entity for the specified ID, returns a Not Found error if this ID does not exist.
    Calls the :meth:`~dtop_stagegate.business.assessor_mode.AssessorStageGateData.join_data()` function to combine
    the assessor scores and comments to the Stage Gate data.

    :param str sg_assess_id: the ID of the Stage Gate Assessment database entity
    :return: the stage gate data, categorised by 'Question' and 'Question Category' for the specified stage gate, and \
    including the assessor scores and comments
    :rtype: dict
    """
    sg_assessment = StageGateAssessment.query.get(sg_assess_id)
    if sg_assessment is None:
        return not_found('Stage Gate Assessment with that ID could not be found')
    assessor_sg_data = AssessorStageGateData(sg_assessment)

    return assessor_sg_data.join_data()


@bp.route('/<sg_assess_id>/assessor-scores', methods=['PUT'])
def update_assessor_data(sg_assess_id):
    """
    Update the assessor data for the specified Stage Gate Assessment ID.

    Loads the JSON request body. Returns a Bad Request error if no request body is provided.
    Returns a Not Found error if the specific Stage Gate Assessment ID does not exist.
    Validates the *assessor_scores* component of the request body using
    :class:`~dtop_stagegate.business.schemas.AssessorScoreSchema()` class.
    Returns a Bad Request error if this validation fails.
    Validates the *assessor_comments* component of the request body using
    :class:`~dtop_stagegate.business.schemas.AssessorCommentSchema()` class.
    Returns a Bad Request error if this validation fails.
    Updates both the *scores* and *comments* components using the
    :meth:`~dtop_stagegate.business.assessor_mode.update_assessor_data_in_db` method.
    Returns a Bad Request error if the request body contains assessor score or comment IDs that are not compatible with
    the specified stage gate assessment.

    :param str sg_assess_id: the ID of the Stage Gate Assessment database entity
    :return: serialised response with the ID of the updated Stage Gate assessment if successful, error message otherwise
    :rtype: flask.wrappers.Response
    """
    request_body = request.get_json()
    if not request_body:
        return bad_request('No request body provided')
    sg_assessment = StageGateAssessment.query.get(sg_assess_id)
    if sg_assessment is None:
        return not_found('Stage Gate Assessment with that ID could not be found')
    assessor_scores = request_body.get('assessor_scores', None)
    assessor_comments = request_body.get('assessor_comments', None)
    if assessor_scores is not None:
        try:
            AssessorScoreSchema(many=True).load(assessor_scores)
        except ValidationError as err:
            return bad_request(join_validation_error_messages(err))
    if assessor_comments is not None:
        try:
            AssessorCommentSchema(many=True).load(assessor_comments)
        except ValidationError as err:
            return bad_request(join_validation_error_messages(err))
    assessor_update = update_assessor_data_in_db(sg_assessment, assessor_scores, assessor_comments)
    if type(assessor_update) is str:
        return bad_request(assessor_update)
    response = {
        'updated_stage_gate_assessment_id': sg_assess_id,
        'message': 'Assessor Scores updated'
    }

    return jsonify(response)


@bp.route('/<sg_assess_id>/assessor-status', methods=['PUT'])
def update_assessor_study_status(sg_assess_id):
    """
    Update the status of the assessor study.

    Sets the *assessor_complete* status of the Stage Gate assessment to *True*.

    :param str sg_assess_id: the ID of the Stage Gate Assessment database entity
    :return: success message or error message
    :rtype: dict
    """
    sg_assessment = StageGateAssessment.query.get(sg_assess_id)
    if sg_assessment is None:
        return not_found('Stage Gate Assessment with that ID could not be found')
    update_assessor_complete_status(sg_assessment)

    return {'message': 'Assessor Study complete status set to True'}


@bp.route('/<sg_assess_id>/assessor-results', methods=['GET'])
def get_assessor_results(sg_assess_id):
    """
    Get the assessor results for a specified Stage Gate assessment.

    Extracts the Stage Gate Assessment entity for the specified ID, returns a Not Found error if this ID does not exist.
    Uses the :class:`~dtop_stagegate.business.assessor_mode.AssessorResults` class to calculate the assessor results.
    Returns the output of the :meth:`~dtop_stagegate.business.assessor_mode.AssessorResults.get_results` method of
    this class.

    :param str sg_assess_id: the ID of the Stage Gate Assessment database entity
    :return: the assessor results including overall average and weighted average results, average and weighted average \
    results for both question category and evaluation area groups and the responses to the stage gate questions \
    including the assessor scores and comments.
    :rtype: dict
    """
    sg_assessment = StageGateAssessment.query.get(sg_assess_id)
    if sg_assessment is None:
        return not_found('Stage Gate Assessment with that ID could not be found')
    assessor_scores = AssessorScore.query.filter(AssessorScore.stage_gate_assessment_id == sg_assess_id).all()
    if any([a_s.score is None for a_s in assessor_scores]):
        return bad_request('Not all Assessor Scores have been filled')
    ar = AssessorResults(sg_assessment)
    ar.calculate()

    return ar.get_results()


@bp.route('/assessor-comparison', methods=['GET'])
def get_assessor_comparison_results():
    """
    Get the assessor comparison results for the specified stage gate assessment IDs.

    The **ids** of the Stage Gate assessments should be specified as query string parameters separated by ampersands,
    e.g. `/api/stage-gate-assessments/assessor-comparison?id=1&id=6&id=11`.
    If the number of Stage Gate assessment IDs provided in the GET request is less than or equal to 1, returns a Bad
    Request error.
    If a Stage Gate assessment ID is specified that does not exist in the database (the **sg_assess_error** property of
    the :class:`StageGateAssessmentComparison` class is equal to *True*), returns a Not Found error.
    Otherwise calculates and retrieves the AssessorComparison results using
    :class:`~dtop_stagegate.business.study_comparison.AssessorComparison`

    :return: the results of the assessor comparison analysis (i.e. the **data** property of the \
    :class:`~dtop_stagegate.business.study_comparison.AssessorComparison` class)
    :rtype: dict
    """
    sg_assess_ids = request.args.getlist('id')
    if len(sg_assess_ids) <= 1:
        return bad_request('An insufficient number of Stage Gate assessment IDs were provided for the comparison')
    sga_comparison = StageGateAssessmentComparison(sg_assess_ids)
    if sga_comparison.sg_assess_error:
        return not_found('A Stage Gate assessment for one of the requested IDs does not exist')
    assessor_comparison = sga_comparison.get_assessor_comparison()

    return assessor_comparison
