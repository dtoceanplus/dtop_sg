# This is the Stage Gate module of the DTOceanPlus suite of tools
# Copyright (C) 2021 Wave Energy Scotland - Ben Hudson
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
from flask import Blueprint, request, jsonify
from marshmallow import ValidationError

from dtop_stagegate.business.applicant_mode import update_applicant_answers_in_db, ApplicantStageGateData,\
    ApplicantResults, update_applicant_complete_status
from dtop_stagegate.business.integration import MetricExtractor
from dtop_stagegate.business.models import StageGateAssessment
from dtop_stagegate.business.schemas import ApplicantAnswerSchema
from dtop_stagegate.business.study_comparison import StageGateAssessmentComparison
from dtop_stagegate.service.api.errors import bad_request, not_found
from dtop_stagegate.service.api.utils import join_validation_error_messages

bp = Blueprint('api_applicant_mode', __name__)


@bp.route('/<sg_assess_id>/applicant-answers', methods=['GET'])
def get_applicant_answers(sg_assess_id):
    """
    Get the applicant answers to the questions for a specified Stage Gate.

    Extracts the Stage Gate Assessment entity for the specified ID, returns a Not Found error if this ID does not exist.
    Calls the :meth:`~dtop_stagegate.business.applicant_mode.ApplicantStageGateData.join_data()` function to combine
    the applicant answers and the Stage Gate data.

    :param str sg_assess_id: the ID of the Stage Gate Assessment database entity
    :return: the applicant answers, categorised by 'Question' and 'Question Category' for the specified stage gate
    :rtype: dict
    """
    sg_assessment = StageGateAssessment.query.get(sg_assess_id)
    if sg_assessment is None:
        return not_found('Stage Gate Assessment with that ID could not be found')
    app_stage_gate_data = ApplicantStageGateData(sg_assessment)

    return app_stage_gate_data.join_data()


@bp.route('/<sg_assess_id>/applicant-answers', methods=['PUT'])
def update_applicant_answers(sg_assess_id):
    """
    Update the applicant answers for the specified Stage Gate Assessment ID.

    Loads the JSON request body. Returns a Bad Request error if no request body is provided.
    Returns a Not Found error if the specific Stage Gate Assessment ID does not exist.
    Validates the request body using :class:`~dtop_stagegate.business.schemas.ApplicantAnswerSchema()` class.
    Calls the :meth:`~dtop_stagegate.business.applicant_mode.update_applicant_answers_in_db()` method.
    Returns a Bad Request error if the request body contains applicant answer IDs that are not compatible with the
    specified stage gate assessment.

    :param str sg_assess_id: the ID of the Stage Gate Assessment database entity
    :return: serialised response with the ID of the updated Stage Gate assessment if successful, error message otherwise
    :rtype: flask.wrappers.Response
    """
    request_body = request.get_json()
    if not request_body:
        return bad_request("No request body provided")
    sg_assessment = StageGateAssessment.query.get(sg_assess_id)
    if sg_assessment is None:
        return not_found('Stage Gate Assessment with that ID could not be found')
    try:
        ApplicantAnswerSchema(many=True).load(request_body)
    except ValidationError as err:
        return bad_request(join_validation_error_messages(err))
    aa = update_applicant_answers_in_db(sg_assessment, request_body)
    if type(aa) is str:
        return bad_request(aa)
    response = {
        'updated_stage_gate_assessment_id': sg_assess_id,
        'message': 'Applicant Answers updated'
    }

    return jsonify(response)


@bp.route('/<sg_assess_id>/applicant-answers-integrated', methods=['PUT'])
def update_applicant_answers_integrated(sg_assess_id):
    """
    Load the metric results from the Deployment and Assessment modules.

    Returns a bad request if no request body is provided.
    If a Stage Gate Assessment for the specified ID cannot be found, returns a Not Found error.
    Then it tries to extract the metrics from the assessment tools and return a list of the modules that have had
    metrics successfully extracted.
    It catches a KeyError exception in the case where it cannot access the DTOP_DOMAIN environment variable, in which
    case it returns a Bad Request error.

    :param str sg_assess_id: the ID of the Stage Gate Assessment database entity
    :return: serialised response of the list of modules that have had metrics successfully extracted
    :rtype: flask.wrappers.Response
    """
    request_body = request.get_json()
    if not request_body:
        return bad_request("No request body provided")
    sg_assessment = StageGateAssessment.query.get(sg_assess_id)
    if sg_assessment is None:
        return not_found('Stage Gate Assessment with that ID could not be found')
    try:
        metric_extractor = MetricExtractor(sg_assess_id, request_body)
        metric_results = metric_extractor.extract()
        return jsonify(metric_results)
    except KeyError:
        return bad_request("Error in accessing assessment modules")
    

@bp.route('/<sg_assess_id>/applicant-status', methods=['PUT'])
def update_applicant_study_status(sg_assess_id):
    """
    Update the status of the applicant study.

    Sets the *applicant_complete* status of the Stage Gate assessment to *True*.

    :param str sg_assess_id: the ID of the Stage Gate Assessment database entity
    :return: success message or error message
    :rtype: dict
    """
    sg_assessment = StageGateAssessment.query.get(sg_assess_id)
    if sg_assessment is None:
        return not_found('Stage Gate Assessment with that ID could not be found')
    update_applicant_complete_status(sg_assessment)

    return {'message': 'Applicant Study complete status set to True'}


@bp.route('/<sg_assess_id>/applicant-results', methods=['GET'])
def get_applicant_results(sg_assess_id):
    """
    Get the applicant results for a specified Stage Gate.

    Extracts the Stage Gate Assessment entity for the specified ID, returns a Not Found error if this ID does not exist.
    Uses the :class:`~dtop_stagegate.business.applicant_mode.ApplicantResults` class to calculate the applicant results.
    Returns the output of the :meth:`~dtop_stagegate.business.applicant_mode.ApplicantResults.get_results` method of
    this class.

    :param str sg_assess_id: the ID of the Stage Gate Assessment database entity
    :return: the applicant results including summary data, metric results and responses to the stage gate questions \
    including the calculated applicant results
    :rtype: dict
    """
    sg_assessment = StageGateAssessment.query.get(sg_assess_id)
    if sg_assessment is None:
        return not_found('Stage Gate Assessment with that ID could not be found')
    applicant_results = ApplicantResults(sg_assessment)
    applicant_results.calculate()

    return applicant_results.get_results()


@bp.route('/applicant-comparison', methods=['GET'])
def get_applicant_comparison_results():
    """
    Get the applicant comparison results for the specified stage gate assessment IDs.

    The **ids** of the Stage Gate assessments should be specified as query string parameters separated by ampersands,
    e.g. `/api/stage-gate-assessments/applicant-comparison?id=1&id=6&id=11`.
    If the number of Stage Gate assessment IDs provided in the GET request is less than or equal to 1, returns a Bad
    Request error.
    If a Stage Gate assessment ID is specified that does not exist in the database (the **sg_assess_error** property of
    the :class:`StageGateAssessmentComparison` class is equal to *True*), returns a Not Found error.
    Otherwise calculates and retrieves the ApplicantComparison results using
    :class:`~dtop_stagegate.business.study_comparison.ApplicantComparison`

    :return: the results of the applicant comparison analysis (i.e. the **data** property of the \
    :class:`~dtop_stagegate.business.study_comparison.ApplicantComparison` class)
    :rtype: dict
    """
    sg_assess_ids = request.args.getlist('id')
    if len(sg_assess_ids) <= 1:
        return bad_request('An insufficient number of Stage Gate assessment IDs were provided for the comparison')
    sga_comparison = StageGateAssessmentComparison(sg_assess_ids)
    if sga_comparison.sg_assess_error:
        return not_found('A Stage Gate assessment for one of the requested IDs does not exist')
    applicant_comparison = sga_comparison.get_applicant_comparison()

    return applicant_comparison
