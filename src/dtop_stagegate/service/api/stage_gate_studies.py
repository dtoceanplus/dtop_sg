# This is the Stage Gate module of the DTOceanPlus suite of tools
# Copyright (C) 2021 Wave Energy Scotland - Ben Hudson
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
from flask import Blueprint, request, jsonify, url_for
from marshmallow import ValidationError

from dtop_stagegate.business.applicant_mode import get_stage_gate_assessments, ApplicantResults
from dtop_stagegate.business.stage_gate_studies import create_study, update_study, delete_study
from dtop_stagegate.business.complexity_level import ComplexityLevelMap
from dtop_stagegate.business.improvement_areas import ImprovementAreas
from dtop_stagegate.business.frameworks import get_metric_thresholds_by_stage_gate_id
from dtop_stagegate.business.report_generation import ReportCompiler
from dtop_stagegate.business.study_comparison import StudyComparison
from dtop_stagegate.service.api.errors import bad_request, not_found
from dtop_stagegate.business.models import ChecklistActivity, StageGateStudy
from dtop_stagegate.business.schemas import *
from dtop_stagegate.business.checklists import update_checklist_activity_answers, ChecklistResults
from dtop_stagegate.service.api.utils import join_validation_error_messages

bp = Blueprint('api_stage_gate_studies', __name__)


@bp.route('/', methods=['GET'], strict_slashes=False)
def get_sg_study_list():
    """
    Flask blueprint route for getting a list of Stage Gate studies.

    :return: the json response containing the list of stage gate studies and link to stage gate studies URI
    :rtype: dict
    """
    sg_studies = StageGateStudy.query.all()
    sg_studies_schema = StageGateStudySchema(many=True, exclude=['checklist_activities'])
    result = sg_studies_schema.dump(sg_studies)

    return {"stage_gate_studies": result}


@bp.route('/', methods=['POST'], strict_slashes=False)
def create_sg_study():
    """
    Flask blueprint route for creating a Stage Gate study.

    Validates the request body, ensuring non-empty name is provided.
    Calls the :meth:`~dtop_stagegate.business.stage_gate_studies.create_study()` method. If creation of study is
    successful, then sets HTTP status code as 201. If the study name is not provided or already exists for another study
    in the database, then returns a bad request with a HTTP status code of 400.

    :return: the json response containing the created study object if successful, a HTTP 400 error if not
    :rtype: flask.wrappers.Response
    :except ValidationError: if empty name or no name parameter is provided
    """
    request_body = request.get_json()
    if not request_body:
        return bad_request("No request body provided")
    try:
        LoadStageGateStudySchema().load(request_body)
    except ValidationError as err:
        msg = join_validation_error_messages(err)
        return bad_request(msg)
    sg_study = create_study(request_body)
    sg_study_schema = StageGateStudySchema(exclude=['checklist_activities'])
    result = sg_study_schema.dump(sg_study)
    response = jsonify(result)
    response.status_code = 201
    response.headers['Location'] = url_for('api_stage_gate_studies.get_sg_study', study_id=sg_study.id)

    return response


@bp.route('/<study_id>', methods=['GET'])
def get_sg_study(study_id):
    """
    Flask blueprint route for getting a single Stage Gate study.

    If study_id provided does not exist in the database, returns a 404 error.

    :param str study_id: the ID of the Stage Gate study
    :return: the serialised response containing the selected stage gate study or a HTTP 404 error
    :rtype: dict
    """
    study = StageGateStudy.query.get(study_id)
    if study is None:
        return not_found('Stage Gate study with that ID could not be found')
    study_schema = StageGateStudySchema(exclude=['checklist_activities'])
    study_result = study_schema.dump(study)

    return study_result


@bp.route('/<study_id>', methods=['PUT'])
def update_sg_study(study_id):
    """
    Flask blueprint route for updating a Stage Gate study.

    Calls the :meth:`~dtop_stagegate.business.stage_gate_studies.update_study()` method.
    Returns HTTP 400 error if no request body provided; if study name not provided; or if stage gate study with same
    name already exists in database.
    Returns 404 error if study ID does not exist.

    :return: the json response containing a successful 'study updated' message and the new study data, if successful
    :rtype: flask.wrappers.Response
    """
    request_body = request.get_json()
    if not request_body:
        return bad_request("No request body provided")
    try:
        NameDescriptionSchema().load(request_body)
    except ValidationError as err:
        return bad_request(err.messages['name'][0])
    s = update_study(request_body, study_id)
    if type(s) is str:
        return not_found(s)
    s_schema = StageGateStudySchema(exclude=['checklist_activities'])

    response = {
        'updated_study': s_schema.dump(s),
        'message': 'Study updated.'
    }

    return jsonify(response)


@bp.route('/<study_id>', methods=['DELETE'])
def delete_sg_study(study_id):
    """
    Flask blueprint route for deleting a Stage Gate study.

    Calls the :meth:`~dtop_stagegate.business.stage_gate_studies.delete_study()` method.
    Returns HTTP 400 error if deletion of stage gate study is unsuccessful.

    :param str study_id: the ID of the Stage Gate study to be deleted
    :return: serialised response containing 'successfully deleted' message; bad request error otherwise
    :rtype: flask.wrappers.Response
    """
    data = delete_study(study_id)
    if data == "Study deleted":
        response = {
            'deleted_study_id': study_id,
            'message': 'Study deleted.'
        }
        return jsonify(response)
    else:
        return bad_request(data)


@bp.route('/<study_id>/checklist-inputs', methods=['GET'])
def get_completed_checklist_activities(study_id):
    """
    Flask blueprint route for getting the list of completed activities for a specified Stage Gate study.

    Queries the ChecklistActivity and filters by Stage Gate study ID; then extracts the activity_ids for those checklist
    activities that have been 'completed' (whose *complete* property has been set to true.

    :param str study_id:
    :return: a dictionary containing the list of completed activity IDs
    :rtype: dict
    """
    study = StageGateStudy.query.get(study_id)
    if study is None:
        return not_found('Stage Gate study with that ID could not be found')
    checklist_activities = ChecklistActivity.query.filter(ChecklistActivity.stage_gate_study_id == study_id)
    stage_activities = {k: [] for k in range(0, 6)}
    completed_activities = []
    for act in checklist_activities:
        stage_activities[act.activity.activity_category.stage.number].append(act.activity_id)
        if act.complete:
            completed_activities.append(act.activity_id)
    checklist_inputs = {
        'completed_activities': completed_activities,
        'stage_activities': stage_activities
    }

    return checklist_inputs


@bp.route('/<study_id>/checklist-inputs', methods=['PUT'])
def update_checklist_answers(study_id):
    """
    Flask blueprint route for updating the answers to the Activity Checklist section of a Stage Gate study.

    Extracts the list of completed activities from the request body from the frontend and updates the Checklist Activity
    database table.
    Calls the :meth:`~dtop_stagegate.business.checklists.update_checklist_activity_answers()` method to update db table.

    :param str study_id: the ID of the Stage Gate study to be updated
    :return: serialised response containing the updated study ID and a success message
    :rtype: flask.wrappers.Response
    """
    request_body = request.get_json()
    if not request_body:
        return bad_request("No request body provided")
    study = StageGateStudy.query.get(study_id)
    if study is None:
        return not_found('Stage Gate study with that ID could not be found')
    try:
        UpdateChecklistInputsSchema().load(request_body)
    except ValidationError as err:
        return bad_request(join_validation_error_messages(err))
    u = update_checklist_activity_answers(study_id, request_body['completed_activities'])
    if type(u) is str:
        return bad_request(u)
    response = {
        'study_id': study_id,
        'message': 'Checklist activities updated.'
    }

    return jsonify(response)


@bp.route('/<study_id>/checklist-outputs', methods=['GET'])
def get_checklist_results(study_id):
    """
    Flask blueprint route for getting the results of a checklist study for the specified Study ID.

    Creates an instance of the :class:`~dtop_stagegate.business.checklists.ChecklistResults()` class.

    :param str study_id: the ID of the Stage Gate study to be updated
    :return: serialised response containing the checklist results for the requested Study ID.
    :rtype: flask.wrappers.Response
    """
    study = StageGateStudy.query.get(study_id)
    if study is None:
        return not_found('Stage Gate study with that ID could not be found')
    checklist_results = ChecklistResults(study_id)
    response = {
        'study_id': study_id,
        'results': checklist_results.results
    }

    return jsonify(response)


@bp.route('/<study_id>/stage-gate-assessments', methods=['GET'])
def get_list_stage_gate_assessments(study_id):
    """
    Get the list of the stage gate assessments associated with the specified stage gate study ID.

    Calls the :meth:`~dtop_stagegate.business.applicant_mode.get_stage_gate_assessments()` method to return this list.
    Returns a Not Found error if the specified stage gate study ID does not exist in the database.

    :param study_id: the ID of the Stage Gate study to be updated
    :return: serialised response containing the list of stage gate assessments for the specified stage gate study
    :rtype: dict
    """
    study = StageGateStudy.query.get(study_id)
    if study is None:
        return not_found('Stage Gate study with that ID could not be found')
    stage_gate_assessments_query = get_stage_gate_assessments(study_id)
    stage_gate_assessment_schema = StageGateAssessmentSchema(
        many=True,
        exclude=[
            'applicant_answer',
            'assessor_score',
            'assessor_comment'
        ]
    )
    stage_gate_assessments = stage_gate_assessment_schema.dump(stage_gate_assessments_query)

    return {"stage_gate_assessments": stage_gate_assessments}


@bp.route('/<study_id>/complexity-levels', methods=['GET'])
def get_complexity_levels(study_id):
    """
    Get the complexity levels of Deployment and Assessment tools appropriate for a Stage Gate study.

    Also returns the Stage and Stage Gate that have been reached by the user.
    Uses the :class:`~dtop_stagegate.business.complexity_levels.ComplexityLevelMap` class to get the complexity levels.
    Returns a Not Found error if the specified stage gate study ID does not exist in the database.
    Returns a Bad Request error if the activity checklist for the specified Stage Gate study has not been completed.
    Returns a Not Found error if the user has not completed all of the activities in any of the stages.

    :param str study_id: the ID of the Stage Gate study to be updated
    :return: serialised response containing the complexity level map for the specified stage gate study
    :rtype: dict
    """
    study = StageGateStudy.query.get(study_id)
    if study is None:
        return not_found('Stage Gate study with that ID could not be found')
    if not study.checklist_study_complete:
        return bad_request('Activity checklist for that Stage Gate study has not been completed')
    cm = ComplexityLevelMap(study)
    cpx_level = cm.get_complexity_levels()
    if cpx_level.get('message', None) is not None:
        return not_found(cpx_level['message'])

    return cpx_level


@bp.route('/<study_id>/improvement-areas', methods=['GET'])
def get_improvement_areas(study_id):
    """
    Gets the improvement areas for a specified Stage Gate study.

    Uses the :class:`~dtop_stagegate.business.improvement_areas.ImprovementAreas` class to get the improvement areas.
    Returns a Not Found error if the Stage Gate study does not exist.
    Returns a Not Found error if the activity checklist for the Stage Gate study has not been completed.
    If all of the activities have been marked as complete, returns a dictionary with the improvement areas set to be
    an empty lists.

    :param str study_id: the ID of the Stage Gate study
    :return: serialised response containing the improvement areas (if any) for the specified stage gate study
    :rtype: dict
    """
    study = StageGateStudy.query.get(study_id)
    if study is None:
        return not_found('Stage Gate study with that ID could not be found')
    if not study.checklist_study_complete:
        return bad_request('Activity checklist for that Stage Gate study has not been completed')
    ias = ImprovementAreas(study)
    ias.assess()

    return ias.get_data()


@bp.route('/<study_id>/improvement-areas', methods=['POST'])
def get_improvement_areas_for_specified_stage(study_id):
    """
    Gets the improvement areas for a specified Stage Gate study.

    Uses the :class:`~dtop_stagegate.business.improvement_areas.ImprovementAreas` class to get the improvement areas.
    Returns a Not Found error if the Stage Gate study does not exist.
    Returns a Not Found error if the activity checklist for the Stage Gate study has not been completed.
    If all of the activities have been marked as complete, returns a dictionary with the improvement areas set to be
    an empty lists.

    :param str study_id: the ID of the Stage Gate study
    :return: serialised response containing the improvement areas (if any) for the specified stage gate study
    :rtype: dict
    """
    request_body = request.get_json()
    if not request_body:
        return bad_request("No request body provided")
    try:
        ImprovementAreaSchema().load(request_body)
    except ValidationError as err:
        msg = join_validation_error_messages(err)
        return bad_request(msg)
    study = StageGateStudy.query.get(study_id)
    if study is None:
        return not_found('Stage Gate study with that ID could not be found')
    if not study.checklist_study_complete:
        return bad_request('Activity checklist for that Stage Gate study has not been completed')
    ias = ImprovementAreas(study, request_body['stage_index'], request_body['threshold'])
    ias.assess()

    return ias.get_data()


@bp.route('/checklist-comparison', methods=['GET'])
def get_checklist_comparison_results():
    """
    Get the checklist comparison results for the specified study IDs.

    The **ids** of the Stage Gate studies should be specified as query string parameters separated by ampersands, e.g.
    `/api/stage-gate-studies/checklist-comparison?id=1&id=2&id=3`.
    If the number of study IDs provided in the GET request is less than or equal to 1, returns a Bad Request error.
    If a Study ID is specified that does not exist in the database (the **study_error** property of the
    :class:`StudyComparison` class is equal to *True*), returns a Not Found error.
    Otherwise calculates and retrieves the ChecklistComparison results using
    :class:`~dtop_stagegate.business.study_comparison.ChecklistComparison`

    :return: the results of the checklist comparison analysis (i.e. the **data** property of the \
    :class:`~dtop_stagegate.business.study_comparison.ChecklistComparison` class)
    :rtype: dict
    """
    study_ids = request.args.getlist('id')
    if len(study_ids) <= 1:
        return bad_request('An insufficient number of study IDs were provided for the comparision')
    study_comparison = StudyComparison(study_ids)
    if study_comparison.study_error:
        return not_found('A Stage Gate study for one of the requested IDs does not exist')
    comparison_results = study_comparison.get_checklist_comparison()

    return comparison_results


@bp.route('/<study_id>/report-data', methods=['POST'])
def get_report_pdf(study_id):
    """
    Generate the content for the report export.

    If no request body is provided returns a Bad Request error.
    Otherwise, validates the request body to make sure it has the list of sections to include in the report.
    Also ensures that there are fields for both the *stage_index* and the *stage_gate_index*, though these fields can
    be set to None.
    If a Study ID is specified that does not exist in the database, returns a Not Found error.
    Returns a Bad Request error if the validation that is performed when the
    :class`~dtop_stagegate.business.report_generation.ReportCompiler` is initialised leads to any validation errors.
    Uses the :class`~dtop_stagegate.business.report_generation.ReportCompiler` to generate the report content.

    :param str study_id: the ID of the Stage Gate study
    :return: the *content* and *styles* component required for the PDF generation package in the front-end
    :rtype: dict
    """
    request_body = request.get_json()
    if not request_body:
        return bad_request("No request body provided")
    try:
        LoadReportExportSchema().load(request_body)
    except ValidationError as err:
        msg = join_validation_error_messages(err)
        return bad_request(msg)
    study = StageGateStudy.query.get(study_id)
    if study is None:
        return not_found('Stage Gate study with that ID could not be found')
    report_data = ReportCompiler(study, request_body)
    errors = report_data.errors
    if errors is not None:
        return bad_request(errors)
    report_data.construct()

    return report_data.content


@bp.route('/<study_id>/metric-thresholds', methods=['GET'])
def get_metric_thresholds_by_study(study_id):
    """
    Get the list of thresholds from a framework applied to a specific study and the latest completed Stage Gate.

    Queries the Stage Gate study instance to find the framework that was applied.
    Then extracts the metric thresholds from this framework instance.
    Returns a Not Found error if the Stage Gate study does not exist.
    Returns a Bad Request error if no suitable Stage Gate can be found (if the user hasn't completed the activity
    checklist, if they haven't completed the first Stage or if they have completed all activities).

    :param str study_id: the ID of the Stage Gate study
    :return: the metric that have been applied for the specified Stage Gate study
    :rtype: dict
    """
    study = StageGateStudy.query.get(study_id)
    if study is None:
        return not_found('Stage Gate study with that ID could not be found')
    ia = ImprovementAreas(study)
    if ia.sg_assessment is None:
        return bad_request('Suitable Stage Gate could not be found')
    stage_gate_id = ia.sg_assessment.stage_gate_id
    metric_threshold_list = get_metric_thresholds_by_stage_gate_id(stage_gate_id)
    qm_schema = QuestionMetricSchema(many=True)
    metric_thresholds = qm_schema.dump(metric_threshold_list)

    return {"metric_thresholds": metric_thresholds}


@bp.route('/<study_id>/metric-results', methods=['GET'])
def get_metric_results(study_id):
    """
    Get the metric results for the Stage Gate that has been selected based on the Activity Checklist.

    Finds the latest consecutive Stage that has been fully completed and extracts the metric results for the Stage Gate
    immediately following this latest Stage that has been reached.
    Then uses the :class:`~dtop_stagegate.business.applicant_mode.ApplicantResults` class to calculate the Applicant
    Results and extract tge *metric results* component.
    A service provided for the SI tool.
    Returns a Not Found error if the Stage Gate study ID does not exist.
    Returns a Bad Request error if the Activity Checklist or the selected Stage Gate in Applicant Mode have not been
    completed.

    :param str study_id: the ID of the Stage Gate study
    :return: the metric results for the latest Stage Gate results
    :rtype: dict
    """
    study = StageGateStudy.query.get(study_id)
    if study is None:
        return not_found('Stage Gate study with that ID could not be found')
    ia = ImprovementAreas(study)
    sga = ia.sg_assessment
    if sga is None:
        return bad_request('Suitable Stage Gate could not be found')
    applicant_results = ApplicantResults(sga)
    applicant_results.calculate()

    return {"metric_results": applicant_results.metric_results}
