# This is the Stage Gate module of the DTOceanPlus suite of tools
# Copyright (C) 2021 Wave Energy Scotland - Ben Hudson
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
from flask import Blueprint, request, jsonify, url_for
from marshmallow import ValidationError

from dtop_stagegate.business.frameworks import create_framework, delete_framework, update_framework, \
    update_question_metric, get_list_applied_metric_thresholds, get_stage_by_evaluation_area, \
    update_question_metric_list
from dtop_stagegate.business.models import Framework, Stage, StageGate
from dtop_stagegate.business.schemas import FrameworkSchema, NameDescriptionSchema, StageSchema, \
    StageGateSchema, QuestionMetricSchema, UpdateQuestionMetricSchema, UpdateListQuestionMetricSchema
from dtop_stagegate.service.api.errors import bad_request, not_found
from dtop_stagegate.service.api.utils import join_validation_error_messages

bp = Blueprint('api_frameworks', __name__)
bp_stages = Blueprint('api_stages', __name__)
bp_stage_gates = Blueprint('api_stage_gates', __name__)
bp_question_metric = Blueprint('api_question_metric', __name__)


@bp.route('/', methods=['GET'], strict_slashes=False)
def get_sg_framework_list():
    """
    Flask blueprint route for getting a list of all the available Stage Gate frameworks.

    :return: dictionary with the 'frameworks' key containing the list of frameworks in JSON format
    :rtype: dict
    """
    frameworks = Framework.query.all()
    frameworks_schema = FrameworkSchema(many=True)
    result = frameworks_schema.dump(frameworks)

    return {"frameworks": result}


@bp.route('/', methods=['POST'], strict_slashes=False)
def create_sg_framework():
    """
    Flask blueprint route for creating a new Stage Gate framework.

    Validates the request body, ensuring non-empty name is provided.
    Calls the :meth:`~dtop_stagegate.business.frameworks.create_framework()` method.
    Returns HTTP 400 error if the name of the framework already exists in the database.
    If creation of framework is successful, then sets HTTP status code as 201.

    :return: the JSON response containing the created framework if successful, a HTTP 400 error if not
    :rtype: flask.wrappers.Response
    :except ValidationError: if empty name or no name parameter is provided
    """
    request_body = request.get_json()
    if not request_body:
        return bad_request("No request body provided")
    try:
        NameDescriptionSchema().load(request_body)
    except ValidationError as err:
        return bad_request(err.messages['name'][0])
    f = create_framework(request_body)
    if type(f) is str:
        return bad_request(f)
    framework_schema = FrameworkSchema()
    result = framework_schema.dump(f)
    response = jsonify(result)
    response.status_code = 201
    response.headers['Location'] = url_for('api_frameworks.get_sg_framework', framework_id=f.id)

    return response


@bp.route('/<framework_id>', methods=['GET'])
def get_sg_framework(framework_id):
    """
    Flask blueprint route for getting a single Stage Gate framework.

    Returns a HTTP 404 error if the framework_id does not exists in the database.

    :param str framework_id: the ID of the Stage Gate framework
    :return: the serialised response for a single Stage Gate framework
    :rtype: dict
    """
    framework = Framework.query.get(framework_id)
    if framework is None:
        return not_found("Framework could not be found")
    framework_schema = FrameworkSchema()
    framework_result = framework_schema.dump(framework)

    return framework_result


@bp.route('/<framework_id>', methods=['PUT'])
def update_sg_framework(framework_id):
    """
    Flask blueprint route for updating a Stage Gate framework.

    Calls the :meth:`~dtop_stagegate.business.frameworks.update_framework()` method.
    Returns HTTP 400 error if no request body provided; if framework name not provided; or if framework with same name
    already exists in database.
    Returns 404 error if framework ID does not exist.

    :param str framework_id: the ID of the framework in the local database
    :return: serialised response containing the updated framework data if successful; error message otherwise
    :rtype: flask.wrappers.Response
    """
    request_body = request.get_json()
    if not request_body:
        return bad_request("No request body provided")
    try:
        NameDescriptionSchema().load(request_body)
    except ValidationError as err:
        return bad_request(err.messages['name'][0])
    f = update_framework(request_body, framework_id)
    if type(f) is str:
        if "does not exist" in f:
            return not_found(f)
        else:
            return bad_request(f)
    f_schema = FrameworkSchema()
    response = {
        'updated_framework': f_schema.dump(f),
        'message': 'Framework updated.'
    }

    return jsonify(response)


@bp.route('/<framework_id>', methods=['DELETE'])
def delete_sg_framework(framework_id):
    """
    Flask blueprint route for deleting a Stage Gate framework.

    Calls the :meth:`~dtop_stagegate.business.frameworks.delete_framework()` method.
    Returns HTTP 400 error if deletion of framework is unsuccessful.

    :param str framework_id: tge ID of the Stage Gate framework to be deleted
    :return: serialised response for the newly-created Stage Gate framework if successful; bad request error otherwise.
    :rtype: flask.wrappers.Response
    """
    data = delete_framework(framework_id)
    if data == "Framework deleted":
        response = {
            'deleted_framework_id': framework_id,
            'message': data
        }
        return jsonify(response)
    else:
        return bad_request(data)


@bp_stages.route('/<stage_id>', methods=['GET'])
def get_sg_stage(stage_id):
    """
    Flask blueprint route for getting a single Stage from a Stage Gate framework.

    Returns 404 'Not Found' error if requested id does not exist.

    :param str stage_id: the ID of the Stage in the local database
    :return: serialised response for the selected Stage
    :rtype: dict
    """
    stage = Stage.query.get(stage_id)
    if stage is None:
        return not_found("Stage could not be found")
    stage_schema = StageSchema()
    stage_result = stage_schema.dump(stage)

    return stage_result


@bp_stages.route('/<stage_id>/eval', methods=['GET'])
def get_eval_stage(stage_id):
    """
    Flask blueprint route for getting a single Stage from a Stage Gate framework and categorising it by Evaluation Area

    Returns 404 'Not Found' error if the requested stage ID does not exist.

    :param str stage_id: the ID of the Stage in the local database
    :return: serialised response for the Stage categorised by evaluation area if successful; not found error otherwise
    :rtype: flask.wrappers.Response
    """
    eval_stages = get_stage_by_evaluation_area(stage_id)
    if eval_stages is None:
        return not_found("Stage could not be found")

    return jsonify(eval_stages)


@bp_stage_gates.route('/<stage_gate_id>', methods=['GET'])
def get_sg_stage_gate(stage_gate_id):
    """
    Flask blueprint route for getting a single Stage Gate from a Stage Gate framework.

    Returns 404 'Not Found' error if requested id does not exist.

    :param str stage_gate_id: the ID of the Stage Gate in the local database
    :return: serialised response for the selected Stage Gate
    :rtype: dict
    """
    stage_gate = StageGate.query.get(stage_gate_id)
    if stage_gate is None:
        return not_found("Stage Gate could not be found")
    stage_gate_schema = StageGateSchema()
    stage_gate_result = stage_gate_schema.dump(stage_gate)

    return stage_gate_result


@bp_question_metric.route('/<question_metric_id>', methods=['PUT'])
def update_sg_question_metric(question_metric_id):
    """
    Flask blueprint route for updating a Question Metric entry in a Stage Gate framework.

    Validates the request body using :class:`~dtop_stagegate.business.model_schemas.UpdateQuestionMetricSchema()`.
    Returns validation error if threshold boolean is not a valid boolean or if the threshold value can not be converted
    to a float.
    Also returns an error if the question metric for the specified ID does not exist.

    :param str question_metric_id: the ID of the question metric in the local database
    :return: serialised response containing the updated question metric data if successful; error message otherwise
    :rtype: flask.wrappers.Response
    """
    request_body = request.get_json()
    try:
        qm_update_data = UpdateQuestionMetricSchema().load(request_body)
    except ValidationError as err:
        msg = join_validation_error_messages(err)
        return bad_request(msg)
    qm = update_question_metric(qm_update_data, question_metric_id)
    if type(qm) is str:
        return not_found(qm)
    qm_schema = QuestionMetricSchema()
    response = {
        'updated_question_metric': qm_schema.dump(qm),
        'message': 'Question Metric updated.'
    }

    return jsonify(response)


@bp_question_metric.route('/', methods=['PUT'], strict_slashes=False)
def update_sg_question_metric_list():
    """
    Flask route for updating a list of question metrics.

    Validates the request body schema looking for id, threshold and threshold_bool.
    Returns a Bad Request error if this validation fails.
    Otherwise calls the BL logic function that loops through each item in the request body list and updated the
    corresponding item in the database.

    :return: serialised response containing a successful message
    :rtype: flask.wrappers.Response
    """
    request_body = request.get_json()
    try:
        qm_data = UpdateListQuestionMetricSchema(many=True).load(request_body)
    except ValidationError as err:
        msg = join_validation_error_messages(err)
        return bad_request(msg)
    qm = update_question_metric_list(qm_data)

    response = {
        'message': 'Question Metrics updated.'
    }

    return jsonify(response)

# TODO: PACT with Luis, Yi and Nicolas


@bp.route('/<framework_id>/metric-thresholds', methods=['GET'])
def get_metric_thresholds(framework_id):
    """
    Flask blueprint route for getting the list of metric thresholds that have been applied for a Stage Gate framework.

    Calls the :meth:`~dtop_stagegate.business.frameworks.get_list_applied_metric_thresholds()` method.
    This is a service that will be used by the Structured Innovation module.

    :param str framework_id: the ID of the Stage Gate framework in the local database
    :return: a dictionary that contains the list of metric thresholds that have been applied
    :rtype: dict
    """
    metric_threshold_list = get_list_applied_metric_thresholds(framework_id)
    qm_schema = QuestionMetricSchema(many=True)
    metric_thresholds = qm_schema.dump(metric_threshold_list)

    return {"metric_thresholds": metric_thresholds}
