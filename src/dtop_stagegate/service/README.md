This package is for service layer.

See:
- [__init__.py](__init__.py) creates application instance for Flask.
- [static](static) folder contains static resources.
- [foo](foo) is a Blueprint (an application component).


It uses [Flask](http://flask.pocoo.org) framework.
