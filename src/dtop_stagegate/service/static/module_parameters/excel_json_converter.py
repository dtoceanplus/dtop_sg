# This is the Stage Gate module of the DTOceanPlus suite of tools
# Copyright (C) 2021 Wave Energy Scotland - Ben Hudson
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
import json

import pandas as pd


class ExcelToJsonFrameworkConverter(object):
    """
    Class for converting Framework data from Excel format to the JSON format required for installation of Alpha version
    of Stage Gate tool.

    Requires xlrd package; optional dependency of pandas package for reading excel files.
    Conda environment 'stagegate_utils' has been created to run this class.

    Class is commented out for alpha version because it is not used by the code, only used in preparing the
    module parameters. Will eventually be modified to convert the source Excel data to the required CSV files that will
    replace the json files in module parameters.

    # TODO: Once framework models is fixed for beta, this function should convert excel to the separate CSV files in
    #  module parameters
    """

    file_in = r'C:\Users\DTOceanPlusUser\Documents\DTOceanPlus_Docs\framework_data_v3.xlsx'
    framework_name = r'Default (no metric thresholds)'
    framework_description = r'The default setting does not apply any metric thresholds in the Framework'

    def __init__(self, selection=None):

        self._data = {
            'name': self.framework_name,
            'description': self.framework_description,
            'stages': [],
            'stage_gates': []
        }

        self.stages_df = None
        self.act_cats_df = None
        self.acts_df = None
        self.stage_gates_df = None
        self.q_cats_df = None
        self.qs_df = None
        self.sc_df = None
        self.eas_df = None
        self.metrics_df = None

        self._extract_data()
        if selection == 'metrics':
            self._convert_metrics()
        else:
            self._convert_stages()
            self._convert_stage_gates()
            self._write_output()

    def _extract_data(self):

        self.stages_df = self._get_excel_sheet('stages')
        self.act_cats_df = self._get_excel_sheet('activity_categories')
        self.acts_df = self._get_excel_sheet('activities')
        self.stage_gates_df = self._get_excel_sheet('stage_gates')
        self.q_cats_df = self._get_excel_sheet('question_categories')
        self.qs_df = self._get_excel_sheet('questions')
        self.sc_df = self._get_excel_sheet('scoring_criteria')
        self.eas_df = self._get_excel_sheet('evaluation_areas')
        self.metrics_df = self._get_excel_sheet('metrics')

    def _get_excel_sheet(self, sheet_name):

        df = pd.read_excel(
            self.file_in,
            sheet_name=sheet_name
        )

        return df

    def _convert_stages(self):
        for i, r in self.stages_df.iterrows():
            stage = Stage(self, r)
            self._data['stages'].append(stage.data)

    def _convert_stage_gates(self):
        for i, r in self.stage_gates_df.iterrows():
            stage_gate = StageGate(self, r)
            self._data['stage_gates'].append(stage_gate.data)

    def _write_output(self):
        with open('frameworks.json', 'w') as outfile:
            json.dump(self._data, outfile)

    def _convert_metrics(self):
        mdf = self.metrics_df.to_dict(orient='records')
        with open('metrics.json', 'w') as outfile:
            json.dump(mdf, outfile)


class Stage(object):

    def __init__(self, f_inst, r):

        self.data = {
            'name': r['name'],
            'description': r['description'].replace('\n', ' <br> '),
            'number': int(r['number']),
            'activity_categories': []
        }
        self._stage = r['number']
        self._f_inst = f_inst
        self.ac_df = f_inst.act_cats_df[f_inst.act_cats_df['stage'] == r['number']]
        self._convert_acs()

    def _convert_acs(self):
        for i, r in self.ac_df.iterrows():
            ac = ActCat(self._f_inst, r, self._stage)
            self.data['activity_categories'].append(ac.data)


class ActCat(object):

    def __init__(self, f_inst, r, stage):

        self.data = {
            'name': r['name'],
            'description': r['description'].replace('\n', ' <br> '),
            'activities': []
        }
        self._ac_name = r['name']
        self._f_inst = f_inst
        self._a_df = f_inst.acts_df[
            (f_inst.acts_df['stage'] == stage) & (f_inst.acts_df['activity_category'] == r['name'])
        ]
        self._convert_acts()

    def _convert_acts(self):
        for i, r in self._a_df.iterrows():
            a = Act(self._f_inst, r)
            self.data['activities'].append(a.data)


class Act(object):

    def __init__(self, f_inst, r):

        self.data = {
            'name': r['activity_name'],
            'description': r['description'].replace('\n', ' <br> '),
            'activity_eval_areas': []
        }
        self._f_inst = f_inst
        self._ea_str = r['evaluation_area']
        self._assign_eas()

    def _assign_eas(self):

        eas = self._ea_str.split(',')
        for ea in eas:
            ea_id = self._f_inst.eas_df[self._f_inst.eas_df['name'] == ea.strip()]['id'].values[0]
            self.data['activity_eval_areas'].append(dict(evaluation_area_id=int(ea_id)))


class StageGate(object):

    def __init__(self, f_inst, r):

        self.data = {
            'name': r['name'],
            'entry_stage': int(r['entry_stage']),
            'exit_stage': int(r['exit_stage']),
            'question_categories': []
        }
        self._entry_stage = r['entry_stage']
        self._exit_stage = r['exit_stage']
        self._f_inst = f_inst
        self._convert_qcs()

    def _convert_qcs(self):
        for i, r in self._f_inst.q_cats_df.iterrows():
            qc = QCat(self._f_inst, r, (self._entry_stage, self._exit_stage))
            if len(qc.data['questions']) > 0:
                self.data['question_categories'].append(qc.data)


class QCat(object):

    def __init__(self, f_inst, r, sg_tuple):

        self.data = {
            'name': r['name'],
            'description': r['description'].replace('\n', ' <br> ').replace('\t', ''),
            'questions': []
        }
        self._sg = '{} - {}'.format(int(sg_tuple[0]), int(sg_tuple[1]))
        self._ac_name = r['name']
        self._f_inst = f_inst
        self._q_df = f_inst.qs_df[
            (f_inst.qs_df['stage_gate'] == self._sg) & (f_inst.qs_df['question_category'] == r['name'])
        ]
        self._convert_acts()

    def _convert_acts(self):
        for i, r in self._q_df.iterrows():
            a = Quest(self._f_inst, r)
            self.data['questions'].append(a.data)


class Quest(object):

    def __init__(self, f_inst, r):

        self.data = {
            'name': r['name'],
            'number': str(r['id']),
            'description': r['description'].replace('\n', ' <br> ').replace('\t', ''),
            'weighting': r['weighting'],
            'rubric': 1,
            'scoring_criteria': [],
        }
        self._q_id = r['id']
        self._m = str(r['metric'])
        self._f_inst = f_inst
        self._assign_metrics()
        self._assign_scoring_criteria()

    def _assign_metrics(self):

        if self._m != 'nan':
            m_id = self._f_inst.metrics_df[self._f_inst.metrics_df['name'] == self._m]['id'].values[0]
            self.data['question_metric'] = dict(metric_id=int(m_id))


    def _assign_scoring_criteria(self):

        sc_df = self._f_inst.sc_df[self._f_inst.sc_df['question'] == self._q_id]
        for i, r in sc_df.iterrows():
            self.data['scoring_criteria'].append(dict(description=r['description'].replace('\t', '')))


if __name__ == '__main__':
    ExcelToJsonFrameworkConverter()
