# DTOceanPlus GUI template

Template for a DTOceanPlus module's GUI layer implemented using [Vue.js](https://vuejs.org/) and [Element-UI](https://element.eleme.io/#/en-US). 

Based on [vue-admin-template](http://panjiachen.github.io/vue-admin-template). Author log-in and authorisation components removed by [Francesco [AAU]](https://gitlab.com/ffro). Dummy table data also added from Element-UI component library.

## Description of vue-admin-template

> A minimal vue admin template with Element UI & axios & iconfont & permission control & lint


## Build Setup
The following instructions can be used to configure a version of the GUI template in your main module repository. It assumes that you have previously downloaded [Node.js](https://nodejs.org/en/) and thus have access to `npm` commands. 

```bash
# clone the project
git clone https://gitlab.com/wave-energy-scotland/dtoceanplus/gui-template.git

# copy the project to a new sub-folder of your module
# e.g. /dtop_stagegate/src/dtop_stagegate/gui/

# enter the project directory
cd src/dtop_stagegate/gui

# install dependencies
npm install

# develop
npm run dev
```

This will automatically open http://localhost:8080

## Build

```bash
# build for production environment
npm run build:prod
```

## More examples

To see examples of how to connect to the module's API, see 
*  [dtop_energycapt/frontend](https://gitlab.com/aau-c/dtoceanplus/energycapture/tree/master/src/dtop_energycapt/frontend) or
*  [dtop_stagegate/gui](https://gitlab.com/wave-energy-scotland/dtoceanplus/dtop-stagegate/tree/master/src/dtop_stagegate/gui)
