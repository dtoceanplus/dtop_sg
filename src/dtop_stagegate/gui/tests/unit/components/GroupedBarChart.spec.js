import { shallowMount } from '@vue/test-utils'

import GroupedBarChart from '@/components/GroupedBarChart/index.vue'
import ElementUI from 'element-ui';
import ChecklistComparisonJson from '../json/ChecklistComparisonJson.json'

import Vue from 'vue'

Vue.use(ElementUI);
jest.mock("axios")

describe('index.vue', () => {
    let bar_chart_data = ChecklistComparisonJson.data.bar_chart_data['0'].activity_categories;
    let xAxisProps = {
      tickmode: 'array',
      tickvals: [0, 25, 50, 75, 100],
      range: [-5, 105],
      title: "Percentage activities completed (%)"
    };
    let figureTitle = "test title";
    const wrapper = shallowMount(GroupedBarChart, {
      propsData: {
        bar_chart_data: bar_chart_data,
        xAxisProps: xAxisProps,
        figureTitle: figureTitle
      }
    })

    it('test bar chart data', () => {
      expect(wrapper.vm.layout.barmode).toBe('group');
      expect(wrapper.vm.layout.xaxis).toEqual(xAxisProps)
      expect(wrapper.vm.data[0]).toEqual(bar_chart_data[0])
      expect(wrapper.vm.data[1]).toEqual(bar_chart_data[1])
    })

})
