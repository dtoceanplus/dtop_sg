import { shallowMount } from '@vue/test-utils'

import AssessorComparison from '@/components/AssessorComparison/index.vue'
import ElementUI from 'element-ui';
import AssessorComparisonJson from '../json/AssessorComparisonJson.json'

import Vue from 'vue'
import axios from 'axios'

Vue.use(ElementUI);
jest.mock("axios")

describe('index.vue', () => {
    let sgStudies = [
      {
        "_links": {
            "collection": "/api/stage-gate-studies/",
            "self": "/api/stage-gate-studies/1"
        },
        "checklist_study_complete": false,
        "description": "test",
        "framework": "DTOceanPlus Framework template",
        "framework_id": 1,
        "id": 1,
        "name": "Test",
        "stage_gate_assessment": [
          1,
          2,
          3,
          4,
          5
        ]
      },
      {
        "_links": {
            "collection": "/api/stage-gate-studies/",
            "self": "/api/stage-gate-studies/2"
        },
        "checklist_study_complete": false,
        "description": "second test",
        "framework": "DTOceanPlus Framework template",
        "framework_id": 1,
        "id": 2,
        "name": "Second test",
        "stage_gate_assessment": [
          6,
          7,
          8,
          9,
          10
        ]
      }
    ];
    const wrapper = shallowMount(AssessorComparison, {
      propsData: {
        sgStudies: sgStudies
      }
    })

    it('test init AssessorCopmarison', () => {
      expect(wrapper.vm.queryString).toEqual("none")
      expect(wrapper.vm.summaryData).toEqual([])
      expect(wrapper.vm.questionCategories).toEqual([])
      expect(wrapper.vm.evaluationAreas).toEqual([])
    })

    it('test queryString computed', () => {
      wrapper.setData({ chosenStageGateId: '0' })
      expect(wrapper.vm.queryString).toEqual("1&id=6")
    })

    it('onSelectStageGate and getAssessorComparison', async() => {
      axios.resolveWith(AssessorComparisonJson);
      await wrapper.vm.onSelectStageGate();
      await wrapper.vm.$nextTick();
      expect(wrapper.vm.summaryData).toEqual(AssessorComparisonJson.data.summary_data)
      expect(wrapper.vm.questionCategories).toEqual(AssessorComparisonJson.data.question_categories)
      expect(wrapper.vm.evaluationAreas).toEqual(AssessorComparisonJson.data.evaluation_areas)
    })

    it('onSelectStageGate error', async() => {
      axios.resolveWith(AssessorComparisonJson, true)
      await wrapper.vm.onSelectStageGate();
  })
})
