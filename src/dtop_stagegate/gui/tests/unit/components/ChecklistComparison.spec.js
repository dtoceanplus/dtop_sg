import { shallowMount } from '@vue/test-utils'

import ChecklistComparison from '@/components/ChecklistComparison/index.vue'
import ElementUI from 'element-ui';
import ChecklistComparisonJson from '../json/ChecklistComparisonJson.json'

import Vue from 'vue'
import axios from 'axios'

Vue.use(ElementUI);
jest.mock("axios")

describe('index.vue', () => {
    let queryString = "1&id=2";
    const wrapper = shallowMount(ChecklistComparison, {
      propsData: {
        queryString: queryString
      }
    })

    it('getChecklistComparisonResults', async() => {
      axios.resolveWith(ChecklistComparisonJson);
      await wrapper.vm.getChecklistComparisonResults();
      await wrapper.vm.$nextTick();
      expect(wrapper.vm.checklistComparisonData).toEqual(ChecklistComparisonJson.data)
    })

    it('activityCategoryData', () => {
      wrapper.setData({
        checklistComparisonData: ChecklistComparisonJson.data,
        chosenStageId: '1'
      })
      expect(wrapper.vm.activityCategoryData).toEqual(ChecklistComparisonJson.data.bar_chart_data['1'].activity_categories)
    })

    it('evalAreaData', () => {
      wrapper.setData({
        checklistComparisonData: ChecklistComparisonJson.data,
        chosenStageId: '1'
      })
      expect(wrapper.vm.evalAreaData).toEqual(ChecklistComparisonJson.data.bar_chart_data['1'].evaluation_areas)
    })
})
