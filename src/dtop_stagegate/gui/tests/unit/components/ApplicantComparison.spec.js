import { shallowMount } from '@vue/test-utils'

import ApplicantComparison from '@/components/ApplicantComparison/index.vue'
import ElementUI from 'element-ui';
import ApplicantComparisonJson from '../json/ApplicantComparisonJson.json'

import Vue from 'vue'
import axios from 'axios'

Vue.use(ElementUI);
jest.mock("axios")

describe('index.vue', () => {
    let sgStudies = [
      {
        "_links": {
            "collection": "/api/stage-gate-studies/",
            "self": "/api/stage-gate-studies/1"
        },
        "checklist_study_complete": false,
        "description": "test",
        "framework": "DTOceanPlus Framework template",
        "framework_id": 1,
        "id": 1,
        "name": "Test",
        "stage_gate_assessment": [
          1,
          2,
          3,
          4,
          5
        ]
      },
      {
        "_links": {
            "collection": "/api/stage-gate-studies/",
            "self": "/api/stage-gate-studies/2"
        },
        "checklist_study_complete": false,
        "description": "second test",
        "framework": "DTOceanPlus Framework template",
        "framework_id": 1,
        "id": 2,
        "name": "Second test",
        "stage_gate_assessment": [
          6,
          7,
          8,
          9,
          10
        ]
      }
    ];
    const wrapper = shallowMount(ApplicantComparison, {
      propsData: {
        sgStudies: sgStudies
      }
    })

    it('test init ApplicantCopmarison', () => {
      expect(wrapper.vm.queryString).toEqual("none")
      expect(wrapper.vm.applicantComparisonData).toEqual([])
      expect(wrapper.vm.metricDataIn).toEqual([])
      expect(wrapper.vm.metricData).toEqual([])
    })

    it('test metricData computed', () => {
      wrapper.setData({ metricDataIn: ApplicantComparisonJson.data.metric_data})
      let formattedMetricData = [
        {
          "data": [
            {
              "name": "study_1",
              "result": 0.0
            },
            {
              "name": "study_2",
              "result": null
            }
          ],
          "study_1": 0.0,
          "study_2": null,
          "metric": "Average installation duration",
          "threshold": null,
          "threshold_type": "upper",
          "unit": "hours per kW"
        },
        {
          "data": [
            {
              "name": "study_1",
              "result": 0.0
            },
            {
              "name": "study_2",
              "result": null
            }
          ],
          "study_1": 0.0,
          "study_2": null,
          "metric": "Cost of installation",
          "threshold": null,
          "threshold_type": "upper",
          "unit": "Euro"
        }
      ]
      expect(wrapper.vm.metricData).toEqual(formattedMetricData)
    })

    it('test queryString computed', () => {
      wrapper.setData({ chosenStageGateId: '0' })
      expect(wrapper.vm.queryString).toEqual("1&id=6")
    })

    it('onSelectStageGate and getApplicantComparison', async() => {
      axios.resolveWith(ApplicantComparisonJson);
      await wrapper.vm.onSelectStageGate();
      await wrapper.vm.$nextTick();
      expect(wrapper.vm.applicantComparisonData).toEqual(ApplicantComparisonJson.data)
      expect(wrapper.vm.metricDataIn).toEqual(ApplicantComparisonJson.data.metric_data)
    })

    it('onSelectStageGate error', async() => {
      axios.resolveWith(ApplicantComparisonJson, true)
      await wrapper.vm.onSelectStageGate();
  })
})
