import { shallowMount } from '@vue/test-utils'

import StageGates from '@/components/StageGates/index.vue'
import ElementUI from 'element-ui';
import StageGateDataJson from '../json/StageGateDataJson.json'

import Vue from 'vue'

Vue.use(ElementUI);

describe('index.vue', () => {
  let sgData = StageGateDataJson.data.question_categories
  const wrapper = shallowMount(StageGates, {
    propsData: {
      stageGateData: sgData
    }
  })

  it('check props', () => {
    expect(wrapper.vm.stageGateData).toEqual(StageGateDataJson.data.question_categories);
  })

  it('filterHandler false', () => {
    let test = wrapper.vm.filterHandler(10, { 'test': 7 }, { property: 'test'});
    expect(test).toEqual(false);
  })

  it('filterHandler true', () => {
    let test = wrapper.vm.filterHandler(10, { 'test': 10 }, { property: 'test'});
    expect(test).toEqual(true);
  })

  it('qualitativeQuestions computed value', () => {
    expect(wrapper.vm.qualitativeQuestions).toEqual([
      {
        "description": "Cupidatat dolore irure in sint minim in non.",
        "id": 1,
        "name": "Question Category A",
        "questions": [
          {
            "description": "Cupidatat dolore irure in sint minim in non.",
            "further_details": "Further details describing the Stage Gate question.",
            "id": 1,
            "name": "Question A1",
            "number": 1,
            "question_category": 1,
            "question_metric": null,
            "rubric": 1,
            "scoring_criteria": [
              {
                "description": "Description of the first scoring criterion being assessed in the question",
                "id": 1,
                "name": "First scoring criterion",
                "question": 1
              },
              {
                "description": "Description of the second scoring criterion being assessed in the question",
                "id": 2,
                "name": "Second scoring criterion",
                "question": 1
              }
            ],
            "weighting": 0.25
          },
          {
            "description": "Cupidatat dolore irure in sint minim in non.",
            "further_details": "Further details describing the Stage Gate question.",
            "id": 2,
            "name": "Question A2",
            "number": 2,
            "question_category": 1,
            "question_metric": null,
            "rubric": 1,
            "scoring_criteria": [
              {
                "description": "Description of the first scoring criterion being assessed in the question",
                "id": 3,
                "name": "First scoring criterion",
                "question": 2
              },
              {
                "description": "Description of the second scoring criterion being assessed in the question",
                "id": 4,
                "name": "Second scoring criterion",
                "question": 2
              }
            ],
            "weighting": 0.25
          }
        ],
        "stage_gate": 1
      }
    ]);
  })

  it('quantitativeQuestions computed value', () => {
    expect(wrapper.vm.quantitativeQuestions).toEqual([
      {
        metric: "LCOE",
        unit: "Euro/kWh",
        threshold_type: "upper",
        threshold: 150,
        threshold_bool: true,
        weighting: 0.25,
        evaluation_area: "Affordability",
        description: "Cupidatat dolore irure in sint minim in non."
      },
      {
        metric: "Capacity factor",
        unit: "%",
        threshold_type: "lower",
        threshold: 20,
        threshold_bool: true,
        weighting: 0.25,
        evaluation_area: "Energy Capture",
        description: "Cupidatat dolore irure in sint minim in non."
      }
    ]);
  })

  it('qualitativeEmpty and quantitativeEmpty', () => {
    expect(wrapper.vm.qualitativeEmpty).toEqual(false);
    expect(wrapper.vm.quantitativeEmpty).toEqual(false);
    wrapper.setData({stageGateData: []});
    expect(wrapper.vm.qualitativeEmpty).toEqual(true);
    expect(wrapper.vm.quantitativeEmpty).toEqual(true);
  })

})
