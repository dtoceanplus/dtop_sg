import { shallowMount } from '@vue/test-utils'

import Stages from '@/components/Stages/index.vue'
import ElementUI from 'element-ui';

import Vue from 'vue'

Vue.use(ElementUI);
jest.mock("axios")

describe('index.vue', () => {
  const wrapper = shallowMount(Stages)

  it('checkEmptyActiveActivities', () => {
    expect(wrapper.vm.activeNames).toEqual([])
  })
})
