import { shallowMount } from '@vue/test-utils'

import ImprovementAreas from '@/views/stage_gate_studies/improvement_areas/index.vue'
import ElementUI from 'element-ui';
import ImprovementAreasJson from '../json/ImprovementAreasJson.json'

import Vue from 'vue'
import axios from 'axios'

Vue.use(ElementUI);
jest.mock("axios")

describe('index.vue', () => {
  const $router = {
    push: jest.fn(),
  }
  let study_data = {
    "_links": {
      "collection": "/api/stage-gate-studies/",
      "self": "/api/stage-gate-studies/1"
    },
    "checklist_study_complete": true,
    "description": "test",
    "framework": "DTOceanPlus Framework template",
    "framework_id": 1,
    "id": 1,
    "name": "test"
  }
  let payload = {
    'stage_index': 0
  };
  const wrapper = shallowMount(ImprovementAreas, {
    mocks: {
      $route: {
          params: {
              study_data: study_data
          }
      },
      $router
    }
  })

  it('goBack', async() => {
    await wrapper.vm.goBack()
    await wrapper.vm.$nextTick()
    expect($router.push).toHaveBeenCalledWith({"name": "SgStudiesHome", "params": {"study_id": study_data.id}});
  })

  it('getImprovementAreas', async() => {
    axios.resolveWith(ImprovementAreasJson)
    await wrapper.vm.getImprovementAreas(wrapper.vm.studyData.id, payload);
    expect(wrapper.vm.improvementAreas).toEqual(ImprovementAreasJson.data.improvement_areas);
    expect(wrapper.vm.stageIndex).toEqual(2)
  })

  it('onSelectStage', async() => {
    axios.resolveWith(ImprovementAreasJson)
    await wrapper.vm.onSelectStage(2)
    expect(wrapper.vm.payload).toEqual({'stage_index': 2, 'threshold': 50});
    expect(wrapper.vm.improvementAreas).toEqual(ImprovementAreasJson.data.improvement_areas);
  })

  it('onUpdateThreshold', async() => {
    axios.resolveWith(ImprovementAreasJson)
    await wrapper.vm.onUpdateThreshold(2)
    expect(wrapper.vm.payload).toEqual({'stage_index': 2, 'threshold': 50});
    expect(wrapper.vm.improvementAreas).toEqual(ImprovementAreasJson.data.improvement_areas);
  })
})
