import { shallowMount } from '@vue/test-utils'

import SgStudyComparisonHome from '@/views/stage_gate_studies/comparison/index'
import ElementUI from 'element-ui';
import StageGateStudiesJson from '../json/StageGateStudiesJson.json'
import FrameworksJson from '../json/FrameworksJson.json'

import Vue from 'vue'
import axios from 'axios'

Vue.use(ElementUI);
jest.mock("axios")

describe('index.vue', () => {

  const $router = {
    push: jest.fn(),
  }
  const wrapper = shallowMount(SgStudyComparisonHome, {
    mocks: {
        $router
    }
  })

  it('getFrameworks', async() => {
    axios.resolveWith(FrameworksJson)
    await wrapper.vm.getFrameworks();
    await wrapper.vm.$nextTick()
    expect(wrapper.vm.frameworks).toEqual(FrameworksJson.data.frameworks);
  })

  it('getStudies', async() => {
    axios.resolveWith(StageGateStudiesJson)
    await wrapper.vm.getStudies();
    expect(wrapper.vm.sgStudies).toEqual(StageGateStudiesJson.data.stage_gate_studies);
  })

  it('filterStudies', async() => {
    let studies = [
      {
        "_links": {
          "collection": "/api/stage-gate-studies/", "self": "/api/stage-gate-studies/1"
        },
        "checklist_study_complete": true,
        "description": "test",
        "framework": "DTOceanPlus Framework template",
        "framework_id": 1,
        "id": 1,
        "name": "test"
      },
      {
        "_links": {
          "collection": "/api/stage-gate-studies/", "self": "/api/stage-gate-studies/2"
        },
        "checklist_study_complete": true,
        "description": "test",
        "framework": "DTOceanPlus Framework template",
        "framework_id": 1,
        "id": 2,
        "name": "test 2"
      },
      {
        "_links": {
          "collection": "/api/stage-gate-studies/", "self": "/api/stage-gate-studies/3"
        },
        "checklist_study_complete": true,
        "description": "test",
        "framework": "DTOceanPlus Framework template",
        "framework_id": 2,
        "id": 3,
        "name": "test 3"
      }
    ]
    wrapper.setData({ sgStudies: studies, value: ['1', '2'] })
    await wrapper.vm.filterStudies(1)
    let studiesFiltered = [
      {
        "_links": {
          "collection": "/api/stage-gate-studies/", "self": "/api/stage-gate-studies/1"
        },
        "checklist_study_complete": true,
        "description": "test",
        "framework": "DTOceanPlus Framework template",
        "framework_id": 1,
        "id": 1,
        "key": 1,
        "name": "test",
        "label": "test"
      },
      {
        "_links": {
          "collection": "/api/stage-gate-studies/", "self": "/api/stage-gate-studies/2"
        },
        "checklist_study_complete": true,
        "description": "test",
        "framework": "DTOceanPlus Framework template",
        "framework_id": 1,
        "id": 2,
        "key": 2,
        "name": "test 2",
        "label": "test 2"
      },
    ]
    expect(wrapper.vm.sgStudiesFiltered).toEqual(studiesFiltered)
    expect(wrapper.vm.value).toEqual([])
  })

  it('checkNumStudies', () => {
    wrapper.setData({ value: ['1'] })
    expect(wrapper.vm.checkNumStudies()).toBeTruthy()
    wrapper.setData({ value: ['1', '2'] })
    expect(wrapper.vm.checkNumStudies()).toBeFalsy()
  })

  it('openComparisonHomePage', async() => {
    wrapper.setData({ value: ['1', '2', '3'] })
    await wrapper.vm.openComparisonHomePage()
    await wrapper.vm.$nextTick()
    expect($router.push).toHaveBeenCalledWith({"name": "SgStudyComparisonResults", "params": {"studyIds": ['1', '2', '3']}});
  })
})
