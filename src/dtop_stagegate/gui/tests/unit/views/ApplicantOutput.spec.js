import { shallowMount } from '@vue/test-utils'

import ApplicantOutput from '@/views/stage_gate_studies/applicant/output/index'
import ElementUI from 'element-ui';
import ApplicantResultsJson from '../json/ApplicantResultsJson.json'

import Vue from 'vue'
import axios from 'axios'

Vue.use(ElementUI);
jest.mock("axios")

describe('index.vue', () => {

  let study_data = { "_links": { "collection": "/api/stage-gate-studies/", "self": "/api/stage-gate-studies/1" }, "checklist_study_complete": true, "description": "test", "framework": "DTOceanPlus Framework template", "framework_id": 1, "id": 1, "name": "test" }
  let app_stage_gate_data = {
    "complete": true,
    "id": 1,
    "stage_gate": "Stage Gate 1 - 2",
    "stage_gate_id": 1,
    "stage_gate_study": 1,
    "stage_gate_study_id": 1
  }
  const $router = {
    push: jest.fn(),
  }
  const wrapper = shallowMount(ApplicantOutput, {
    mocks: {
        $route: {
            params: {
                study_data: study_data,
                app_stage_gate_data: app_stage_gate_data
            }
        },
        $router
    }
  })

  it('getApplicantResults', async() => {
    axios.resolveWith(ApplicantResultsJson);
    await wrapper.vm.getApplicantResults(wrapper.vm.appStageGateData.id);
    await wrapper.vm.$nextTick();
    expect(wrapper.vm.applicantResults).toEqual(ApplicantResultsJson.data);
    expect(wrapper.vm.summaryData).toEqual(ApplicantResultsJson.data.summary_data);
    expect(wrapper.vm.responses).toEqual(ApplicantResultsJson.data.responses);
    expect(wrapper.vm.metricResults).toEqual(ApplicantResultsJson.data.metric_results);
    await wrapper.vm.$nextTick();
    expect(wrapper.vm.qualitativeQuestions).toEqual([
      {
        "description": "Cupidatat dolore irure in sint minim in non.",
        "id": 1,
        "name": "Question Category A",
        "questions": [
          {
            "applicant_results": {
              "id": 1,
              "justification": null,
              "response": null,
              "result": null
            },
            "description": "Cupidatat dolore irure in sint minim in non.",
            "further_details": "Further details describing the Stage Gate question.",
            "id": 1,
            "name": "Question A1",
            "number": 1,
            "question_category": 1,
            "question_metric": null,
            "rubric": 1,
            "scoring_criteria": [
              {
                "description": "Description of the first scoring criterion being assessed in the question",
                "id": 1,
                "name": "First scoring criterion",
                "question": 1
              },
              {
                "description": "Description of the second scoring criterion being assessed in the question",
                "id": 2,
                "name": "Second scoring criterion",
                "question": 1
              }
            ],
            "weighting": 0.25
          },
          {
            "applicant_results": {
              "id": 2,
              "justification": null,
              "response": null,
              "result": null
            },
            "description": "Cupidatat dolore irure in sint minim in non.",
            "further_details": "Further details describing the Stage Gate question.",
            "id": 2,
            "name": "Question A2",
            "number": 2,
            "question_category": 1,
            "question_metric": null,
            "rubric": 1,
            "scoring_criteria": [
              {
                "description": "Description of the first scoring criterion being assessed in the question",
                "id": 3,
                "name": "First scoring criterion",
                "question": 2
              },
              {
                "description": "Description of the second scoring criterion being assessed in the question",
                "id": 4,
                "name": "Second scoring criterion",
                "question": 2
              }
            ],
            "weighting": 0.25
          }
        ],
        "stage_gate": 1
      }
    ]);
  })

  it('goBack', async() => {
    await wrapper.vm.goBack();
    await wrapper.vm.$nextTick();
    expect($router.push).toHaveBeenCalledWith({"name": "ApplicantHome", "params": {"study_data": study_data}});
  })

  it('tableRowClassName', () => {
    let no_threshold_row = {
      threshold: false
    }
    let no_threshold = wrapper.vm.tableRowClassName({row: no_threshold_row, rowIndex: 0});
    expect(no_threshold).toEqual('')
    let warning_row = {
      threshold: 50,
      threshold_passed: false
    }
    let warning = wrapper.vm.tableRowClassName({row: warning_row, rowIndex: 0});
    expect(warning).toEqual('warning-row')
    let success_row = {
      threshold: 50,
      threshold_passed: true
    }
    let success = wrapper.vm.tableRowClassName({row: success_row, rowIndex: 0});
    expect(success).toEqual('success-row')
  })

  it('passFailFormatter', () => {
    let no_threshold_row = {
      threshold: false
    }
    let no_threshold = wrapper.vm.passFailFormatter(no_threshold_row);
    expect(no_threshold).toEqual('')
    let warning_row = {
      threshold: 50,
      threshold_passed: false
    }
    let warning = wrapper.vm.passFailFormatter(warning_row);
    expect(warning).toEqual('FAIL')
    let success_row = {
      threshold: 50,
      threshold_passed: true
    }
    let success = wrapper.vm.passFailFormatter(success_row);
    expect(success).toEqual('PASS')
  })

  it('filterHandler false', () => {
    let test = wrapper.vm.filterHandler(10, { 'test': 7 }, { property: 'test'});
    expect(test).toEqual(false);
  })

  it('filterHandler true', () => {
    let test = wrapper.vm.filterHandler(10, { 'test': 10 }, { property: 'test'});
    expect(test).toEqual(true);
  })

  it('isThresholdApplied', () => {
    let q_true = {
      threshold_bool: true
    };
    let q_threshold_true = wrapper.vm.isThresholdApplied(q_true);
    expect(q_threshold_true).toBeTruthy()
    let q_false = {
      threshold_bool: false
    };
    let q_threshold_false = wrapper.vm.isThresholdApplied(q_false);
    expect(q_threshold_false).toBeFalsy()
    let q_no_qm = {
      error: true
    };
    let q_no_qm_false = wrapper.vm.isThresholdApplied(q_no_qm);
    expect(q_no_qm_false).toBeFalsy()
  })

  it('isThresholdFailed', () => {
    let error = {
      error: true
    };
    let error_out = wrapper.vm.isThresholdFailed(error);
    expect(error_out).toBeFalsy()
    let threshold = {
      threshold_bool: true,
      threshold_passed: false
    };
    let threshold_out = wrapper.vm.isThresholdFailed(threshold);
    expect(threshold_out).toBeTruthy()
    let threshold_fail = {
      threshold_bool: true,
      threshold_passed: true
    };
    let threshold_failed_false = wrapper.vm.isThresholdFailed(threshold_fail);
    expect(threshold_failed_false).toBeFalsy()
  })

  it('returnThresholdPassed', () => {
    let q_yes = {
      threshold_bool: true,
      threshold_passed: true
    }
    let q_yes_out = wrapper.vm.returnThresholdPassed(q_yes);
    expect(q_yes_out).toEqual("Yes")
    let q_no = {
      threshold_bool: true,
      threshold_passed: false
    }
    let q_no_out = wrapper.vm.returnThresholdPassed(q_no);
    expect(q_no_out).toEqual("No")
  })

  it('checkThresholdSuccessRate', () => {
    wrapper.setData({ summaryData: { threshold_success_rate: 0 } })
    expect(wrapper.vm.checkThresholdSuccessRate()).toBeTruthy()
    wrapper.setData({ summaryData: { threshold_success_rate: 50 } })
    expect(wrapper.vm.checkThresholdSuccessRate()).toBeTruthy()
    wrapper.setData({ summaryData: { threshold_success_rate: null } })
    expect(wrapper.vm.checkThresholdSuccessRate()).toBeFalsy()
  })
})
