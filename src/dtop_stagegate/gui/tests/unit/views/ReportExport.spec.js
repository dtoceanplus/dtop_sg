import { shallowMount, createLocalVue } from '@vue/test-utils'
import VueRouter from 'vue-router'

import ReportExport from '@/views/stage_gate_studies/report_export/index.vue'
import ElementUI from 'element-ui';
import ReportExampleJson from '../json/ReportExampleJson.json'
import StageGateAssessmentsJson from '../json/StageGateAssessmentsJson.json'

import Vue from 'vue'
import axios from 'axios'

/* Layout */
import Layout from '@/layout'

Vue.use(ElementUI);
jest.mock("axios")
jest.useFakeTimers();


describe('index.vue', () => {
  let study_data = {
    "_links": {
      "collection": "/api/stage-gate-studies/",
      "self": "/api/stage-gate-studies/1"
    },
    "checklist_study_complete": true,
    "description": "test",
    "framework": "DTOceanPlus Framework template",
    "framework_id": 1,
    "id": 1,
    "name": "test"
  }

  const $router = {
    push: jest.fn(),
  }
  const wrapper = shallowMount(ReportExport, {
    mocks: {
        $route: {
            params: {
              study_data: study_data
          }
        },
        $router
    }
  })

  let expectedRequestBody = {
    'sections': {
      'checklist_summary': true,
      'checklist_stage_results': false,
      'checklist_outstanding_activities': false,
      'applicant_summary': false,
      'metric_results': false,
      'applicant_answers': false,
      'assessor_overall_scores': false,
      'assessor_question_categories': false,
      'assessor_scores_comments': false,
      'improvement_areas': false
    },
    'stage_index': null,
    'stage_gate_index': null,
    'ia_threshold': 50
  }

  it('goBack', async() => {
    await wrapper.vm.goBack()
    await wrapper.vm.$nextTick()
    expect($router.push).toHaveBeenCalledWith({"name": "SgStudiesHome", "params": {"study_id": study_data.id}});
  })

  it('getStageGateData', async() => {
    axios.resolveWith(StageGateAssessmentsJson)
    await wrapper.vm.getSgAssessmentData(wrapper.vm.studyData.id);
    await wrapper.vm.$nextTick()
    expect(wrapper.vm.sgAssessmentData).toEqual(StageGateAssessmentsJson.data.stage_gate_assessments)
  })

  it('handleCheckChange', () => {
    wrapper.setData({ stageGateIndex : 0 })
    const mockedMethod = jest.fn(() => [5])
    wrapper.vm.$refs.tree.getCheckedKeys = mockedMethod
    wrapper.vm.handleCheckChange()
    expect(wrapper.vm.stageGateIndex).toEqual(null)
    expect(wrapper.vm.requestedKeys).toEqual([5])
  })

  it('checkStageIndexRequired', () => {
    const mockedMethod = jest.fn(() => [6])
    wrapper.vm.$refs.tree.getCheckedKeys = mockedMethod
    wrapper.vm.handleCheckChange()
    expect(wrapper.vm.stageIndexRequired).toEqual(true)
  })

  it('checkStageGateIndexRequired', () => {
    const mockedMethod = jest.fn(() => [8])
    wrapper.vm.$refs.tree.getCheckedKeys = mockedMethod
    wrapper.vm.handleCheckChange()
    expect(wrapper.vm.stageGateIndexRequired).toEqual(true)
  })

  it('iaThresholdRequired', () => {
    expect(wrapper.vm.iaThresholdRequired).toEqual(false)
    const mockedMethod = jest.fn(() => [14])
    wrapper.vm.$refs.tree.getCheckedKeys = mockedMethod
    wrapper.vm.handleCheckChange()
    expect(wrapper.vm.iaThresholdRequired).toEqual(true)
  })

  it('checkStageGateOptions', () => {
    let expectedOptions = [{
      value: 0,
      label: 'Stage Gate 0 - 1',
      disabled: false
    }, {
      value: 1,
      label: 'Stage Gate 1 - 2',
      disabled: false
    }, {
      value: 2,
      label: 'Stage Gate 2 - 3',
      disabled: false
    }, {
      value: 3,
      label: 'Stage Gate 3 - 4',
      disabled: true
    }, {
      value: 4,
      label: 'Stage Gate 4 - 5',
      disabled: true
    }]
    const mockedMethod = jest.fn(() => [8])
    wrapper.vm.$refs.tree.getCheckedKeys = mockedMethod
    wrapper.vm.handleCheckChange()
    expect(wrapper.vm.stageGateOptions).toEqual(expectedOptions)
  })

  it('check assessor Stage Gate disabled', () => {
    const mockedMethod = jest.fn(() => [11])
    wrapper.vm.$refs.tree.getCheckedKeys = mockedMethod
    let assessorDisabled_idx0 = wrapper.vm.checkStageGateDisabled(0)
    expect(assessorDisabled_idx0).toEqual(false)
    let assessorDisabled_idx2 = wrapper.vm.checkStageGateDisabled(2)
    expect(assessorDisabled_idx2).toEqual(true)
  })

  it('check applicant Stage Gate disabled', () => {
    const mockedMethod = jest.fn(() => [8])
    wrapper.vm.$refs.tree.getCheckedKeys = mockedMethod
    let applicantDisabled_idx0 = wrapper.vm.checkStageGateDisabled(0)
    expect(applicantDisabled_idx0).toEqual(false)
    let applicantDisabled_idx3 = wrapper.vm.checkStageGateDisabled(3)
    expect(applicantDisabled_idx3).toEqual(true)
  })

  it('formatRequestBody', () => {
    const mockedMethod = jest.fn(() => [5])
    wrapper.vm.$refs.tree.getCheckedKeys = mockedMethod
    wrapper.vm.formatRequestBody()
    expect(wrapper.vm.requestBody).toEqual(expectedRequestBody)
  })

  it('getReportData error', async() => {
    axios.resolveWith(ReportExampleJson, true)
    await wrapper.vm.getReportData(wrapper.vm.studyData.id, expectedRequestBody);
    expect(wrapper.vm.reportData).toEqual([])
    expect(wrapper.vm.valid).toEqual(false)
  })

  it('getReportData', async() => {
    axios.resolveWith(ReportExampleJson)
    await wrapper.vm.getReportData(wrapper.vm.studyData.id, expectedRequestBody);
    await wrapper.vm.$nextTick()
    expect(wrapper.vm.reportData).toEqual(ReportExampleJson.data)
    expect(wrapper.vm.valid).toEqual(true)
  })

  it('generateReport', async() => {
    axios.resolveWith(ReportExampleJson)
    await wrapper.vm.generateReport()
    jest.runAllTimers();
    expect(wrapper.vm.reportData).toEqual(ReportExampleJson.data)
    expect(wrapper.vm.valid).toEqual(true)
  })

  it('makeReportTitle', () => {
    expect(wrapper.vm.reportData.study_params).toEqual({
      "date": "24/06/2021",
      "description": "asdfasdf",
      "selected_stage": "Stage 2",
      "selected_stage_gate": "Stage Gate 1 - 2",
      "stage_index": 2,
      "study_name": "fdsa",
      "threshold_settings": "Default (no metric thresholds)"
    });
    wrapper.vm.makeReportTitle();
    expect(wrapper.vm.reportTitle.slice(-2)[0]).toEqual(
      {
        'text': 'fdsa\n\n\n\n',
        'alignment': 'center',
        'fontSize': 18
      },
      {
        'text': 'Stage Gate v1.0.0\n"24/06/2021"',
        'alignment': 'center',
        'fontSize': 12,
        'pageBreak': 'after'
      }
    );
  })

  it('makePreamble', () => {
    wrapper.vm.makePreamble();
    expect(wrapper.vm.preamble).toEqual([
      {
        'text': 'TABLE OF CONTENTS\n',
        'style': 'header'
      },
      '\n',
      {
        'text': 'The following requested sections have been included in this summary report;',
        'alignment': 'justify',
      },
      '\n',
      {
        "alignment": "justify",
        "ul": [
          "Introduction",
          "Study details",
          "Activity Checklist",
          {
            "type": "circle",
            "ul": [
              "Summary",
              "Detailed breakdown of Stage results",
              "Outstanding activities",
            ]
          },
          "Applicant Mode",
          {
            "type": "circle",
            "ul": [
              "Summary",
              "Metric Results",
              "Applicant answers",
            ],
          },
          "Assessor Mode",
          {
            "type": "circle",
            "ul": [
              "Overall scores",
              "Question Category scores",
              "Assessor scores and comments",
            ],
          },
          "Improvement Areas",
        ]
      },
      '\n',
      {
        'text': 'INTRODUCTION\n',
        'style': 'header'
      },
      '\n',
      {
        'alignment': 'justify',
        'text': `This report is an output of the DTOceanPlus Stage Gate design tool. The aim of this report is to assist decision-making through standardised activities, questions and metrics to assess ocean technology development process in objective assessment. For more details on how the Stage DTOceanPlus website or through the Graphical User Interface (GUI) of the software.`
      },
      '\n',
      {
          'text': 'STUDY DETAILS\n',
          'style': 'header'
      },
      '\nThe summary details of the Stage Gate study being assessed are given below:\n\n',
      {
        'ul': [
          'Name: fdsa',
          'Description: asdfasdf',
          'Threshold settings: Default (no metric thresholds)',
          'Selected Stage: Stage 2',
          'Selected Stage Gate: Stage Gate 1 - 2'
        ]
      },
      '\n'
    ]);
  })

  it('makeChecklist', () => {
      expect(wrapper.vm.checklistSection.slice(0, 8)).toEqual([
        {
          'text': 'ACTIVITY CHECKLIST RESULTS',
          'style': 'header'
        },
        '\n',
        {
          'text': 'SUMMARY',
          'style': 'subheader'
        },
        '\n',
        {
          'text': 'The table below summarises the percentage of activities completed in each of the stages in the Stage Gate framework.\n',
          'alignment': 'justify'
        },
        '\n',
        {
          'style': 'tableExample',
          'table': {
            'headerRows': 1,
            'widths': ['auto', '*', '*', '*', '*', '*', '*'],
            'body': [
              [
                {'text': '', 'style': 'tableHeader', 'fillColor': "#022c5f"},
                {'text': 'Stage 0', 'style': 'tableHeader', 'fillColor': "#022c5f"},
                {'text': 'Stage 1', 'style': 'tableHeader', 'fillColor': "#022c5f"},
                {'text': 'Stage 2', 'style': 'tableHeader', 'fillColor': "#022c5f"},
                {'text': 'Stage 3', 'style': 'tableHeader', 'fillColor': "#022c5f"},
                {'text': 'Stage 4', 'style': 'tableHeader', 'fillColor': "#022c5f"},
                {'text': 'Stage 5', 'style': 'tableHeader', 'fillColor': "#022c5f"}
              ],
              [
                {
                  'text': '% activities complete',
                  'italics': true
                },
                "100%",
                "100%",
                "62%",
                "0%",
                "0%",
                "0%"
              ]
            ]
          }
        },
        '\n'
      ]);
      expect(wrapper.vm.checklistSection.slice(8, 11)).toEqual([
        {
          'text': 'DETAILED RESULTS FOR STAGE 2',
          'style': 'subheader'
        },
        '\n',
        {
            'text': "Below are two bar-charts summarising the percentage of completed activities for Stage 2, categorised by both activity category and evaluation area.",
            'alignment': 'justify'
        }
      ]);
      expect(wrapper.vm.checklistSection.slice(12)[0]).toHaveProperty('svg')
    }
  )

  it('makeApplicant', () => {
    expect(wrapper.vm.applicantSection[0]).toEqual({
      'pageBreak': 'before',
      'text': 'APPLICANT MODE RESULTS',
      'style': 'header'
    });
    expect(wrapper.vm.applicantSection.slice(2, 6)).toEqual([
      {
        "style": "subheader",
        "text": "SUMMARY"
      },
      "\n",
      {
        "alignment": "justify",
        "text": "The response rate for Stage Gate 1 - 2 was 0%."
      },
      "\n"
    ]);
    expect(wrapper.vm.applicantSection.slice(6, 10)).toEqual([
      {
        "style": "subheader",
        "text": "METRIC RESULTS"
      },
      "\n",
      {
        "alignment": "justify",
        "text": "The table below shows the metric results for Stage Gate 1 - 2."
      },
      "\n"
    ]);
    expect(wrapper.vm.applicantSection.slice(12, 15)).toEqual([
      {
        "style": "subheader",
        "text": "APPLICANT ANSWERS"
      },
      "\n",
      {
        "alignment": "justify",
        "text": [
          "The applicant responses for the questions in Stage Gate 1 - 2 are provided in the ",
          {
            "style": "bold",
            "text": "Assessor Mode"
          },
          " section, together with the assessor scores and comments."
        ]
      }
    ]);
  })

  it('makeAssessor', () => {
    expect(wrapper.vm.assessorSection.slice(0, 2)).toEqual([
      {
        'pageBreak': 'before',
        'text': 'ASSESSOR MODE RESULTS',
        'style': 'header'
      },
      '\n',
    ]);
    expect(wrapper.vm.assessorSection.slice(2, 5)).toEqual([
      {
        'text': 'OVERALL SCORES',
        'style': 'subheader'
      },
      '\n',
      {
          'text': "The average and weighted average overall Assessor Scores for Stage Gate 1 - 2 are shown in the bar chart below.",
          'alignment': 'justify'
      }
    ]);
    expect(wrapper.vm.assessorSection.slice(7, 10)).toEqual([
      {
        'pageBreak': 'before',
        'text': 'QUESTION CATEGORY SCORES',
        'style': 'subheader'
      },
      '\n',
      {
        'text': "The weighted average and average Assessor Scores for each Question Category are shown in the figure below.",
        'alignment': 'justify'
      }
    ]);
    expect(wrapper.vm.assessorSection.slice(12, 15)).toEqual([
      {
        "pageBreak": "before",
        "style": "subheader",
        "text": "ASSESSOR SCORES AND COMMENTS"
      },
      "\n",
      {
        "alignment": "justify",
        "text": "The assessor scores and comments for each of the applicant answers in Stage Gate 1 - 2 are shown below, ordered by question category."
      }
    ]);
  })

  it('makeImprovAreas', () => {
    expect(wrapper.vm.reportContent).toEqual(expect.arrayContaining([
      {
        "pageBreak": "before",
        "style": "header",
        "text": "IMPROVEMENT AREAS"
      },
      "\n",
      {
        "alignment": "justify",
        "text": [
          "Below is the list of improvement areas that have been identified for this Stage Gate study. The improvement areas are those evaluation areas that have been highlighted as weaknesses of the technology or device. Improvement areas are defined by the ",
          {
            "italics": true,
            "text": "average improvement area score"
          },
          ", which is the average of the ",
          {
            "italics": true,
            "text": "checklist"
          },
          ", ",
          {
            "italics": true,
            "text": "metric"
          },
          " and ",
          {
            "italics": true,
            "text": "weighted average assessor"
          },
          " scores.\n\n"
        ]
      }
    ]))
  })
})
