import { shallowMount } from '@vue/test-utils'

import FrameworksHome from '@/views/frameworks/home.vue'

import ElementUI from 'element-ui';
import Vue from 'vue'

Vue.use(ElementUI);

describe('index.vue', () => {
    const $router = {
      push: jest.fn(),
    }
    const wrapper = shallowMount(FrameworksHome, {
      mocks: {
          $router
      }
    })
    it('viewFramework', async() => {
      wrapper.vm.viewFramework();
      await wrapper.vm.$nextTick()
      expect($router.push).toHaveBeenCalledWith({"name": 'Thresholds', "params": { "framework_id": 1 }});
  })
})
