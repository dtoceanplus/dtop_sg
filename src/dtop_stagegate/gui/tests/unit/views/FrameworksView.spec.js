import { shallowMount } from '@vue/test-utils'

import FrameworksView from '@/views/frameworks/view/index.vue'

import ElementUI from 'element-ui';
import Vue from 'vue'
import axios from 'axios'

import FrameworkViewJson from '../json/FrameworkViewJson.json'
import StageDataJson from '../json/StageDataJson.json'
import StageEvalDataJson from '../json/StageEvalDataJson.json'
import StageGateDataJson from '../json/StageGateDataJson.json'

Vue.use(ElementUI);
jest.mock("axios")

describe('index.vue', () => {
    let framework_id = 1
    let wrapper = shallowMount(FrameworksView, {
      mocks: {
        $route: {
          params: {
            framework_id: framework_id,
          }
        }
      }
    })

    it('getFramework', async() => {
      axios.resolveWith(FrameworkViewJson)
      await wrapper.vm.getFramework(wrapper.vm.frameworkID);
      await wrapper.vm.$nextTick()
      expect(wrapper.vm.framework_data).toEqual(FrameworkViewJson.data);
    })

    it('toggleStage', () => {
      expect(wrapper.vm.fillsStage).toEqual([
        "white",
        "white",
        "white",
        "white",
        "white",
        "white"
      ]);
      expect(wrapper.vm.fillsStageGate).toEqual([
        "white",
        "white",
        "white",
        "white",
        "white",
        "white"
      ]);
      wrapper.vm.toggleStage(0);
      expect(wrapper.vm.stageIdx).toEqual(0);
      expect(wrapper.vm.fillsStage).toEqual([
        "#e30613",
        "white",
        "white",
        "white",
        "white",
        "white"
      ]);
      expect(wrapper.vm.fillsStageGate).toEqual([
        "white",
        "white",
        "white",
        "white",
        "white",
        "white"
      ]);
    })


    it('toggleStageGate', () => {
      expect(wrapper.vm.fillsStage).toEqual([
        "#e30613",
        "white",
        "white",
        "white",
        "white",
        "white"
      ]);
      expect(wrapper.vm.fillsStageGate).toEqual([
        "white",
        "white",
        "white",
        "white",
        "white",
        "white"
      ]);
      wrapper.vm.toggleStageGate(0);
      expect(wrapper.vm.stageIdx).toEqual(0);
      expect(wrapper.vm.fillsStage).toEqual([
        "white",
        "white",
        "white",
        "white",
        "white",
        "white"
      ]);
      expect(wrapper.vm.fillsStageGate).toEqual([
        "#e30613",
        "white",
        "white",
        "white",
        "white",
        "white"
      ]);
    })

    it('activateStage', () => {
      expect(wrapper.vm.showStages).toEqual(false);
      expect(wrapper.vm.showStageGates).toEqual(true);
      wrapper.vm.activateStage(1);
      expect(wrapper.vm.showStages).toEqual(true);
      expect(wrapper.vm.showStageGates).toEqual(false);
      expect(wrapper.vm.stage_uri).toEqual(wrapper.vm.framework_data.stages[1]);
      expect(wrapper.vm.category).toEqual(false);
    })

    it('activateStage else', () => {
      wrapper.vm.category = true;
      expect(wrapper.vm.showStages).toEqual(true);
      expect(wrapper.vm.showStageGates).toEqual(false);
      wrapper.vm.activateStage(1);
      expect(wrapper.vm.showStages).toEqual(true);
      expect(wrapper.vm.showStageGates).toEqual(false);
      expect(wrapper.vm.stage_uri).toEqual(wrapper.vm.framework_data.stages[1]);
      expect(wrapper.vm.category).toEqual(true);
    })

    it('activateStageGate', () => {
      expect(wrapper.vm.showStages).toEqual(true);
      expect(wrapper.vm.showStageGates).toEqual(false);
      wrapper.vm.activateStageGate(1);
      expect(wrapper.vm.showStages).toEqual(false);
      expect(wrapper.vm.showStageGates).toEqual(true);
      expect(wrapper.vm.stage_uri).toEqual(wrapper.vm.framework_data.stage_gates[1]);
    })

    it('getStageData', async() => {
        axios.resolveWith(StageDataJson)
        let uri = "/api/stages/1";
        await wrapper.vm.getStageData(uri);
        await wrapper.vm.$nextTick()
        expect(wrapper.vm.stageData).toEqual(StageDataJson.data.activity_categories);
    })

    it('getStageData error', async() => {
      axios.resolveWith(StageDataJson, true)
      let uri = "/api/stages/1";
      await wrapper.vm.getStageData(uri);
      await wrapper.vm.$nextTick()
  })

    it('getStageEvalData', async() => {
        axios.resolveWith(StageEvalDataJson, false)
        await wrapper.vm.getStageEvalData("/api/stages/1");
        await wrapper.vm.$nextTick();
        expect(wrapper.vm.stageData).toEqual(StageEvalDataJson.data.evaluation_areas);
    })

    it('getStageEvalData error', async() => {
      axios.resolveWith(StageEvalDataJson, true)
      await wrapper.vm.getStageEvalData(wrapper.vm.framework_data.stages[0]);
      await wrapper.vm.$nextTick();
  })

    it('getStageGateData', async() => {
        axios.resolveWith(StageGateDataJson)
        await wrapper.vm.getStageGateData(wrapper.vm.framework_data.stage_gates[0]);
        await wrapper.vm.$nextTick()
        expect(wrapper.vm.stageGateData).toEqual(StageGateDataJson.data.question_categories);
    })

    it('getStageGateData error', async() => {
      axios.resolveWith(StageGateDataJson, true)
      await wrapper.vm.getStageGateData(wrapper.vm.framework_data.stage_gates[0]);
      await wrapper.vm.$nextTick()
  })

})
