import { shallowMount } from '@vue/test-utils'

import ApplicantInput from '@/views/stage_gate_studies/applicant/input/index'
import ElementUI from 'element-ui';
import ApplicantAnswersJson from '../json/ApplicantAnswersJson.json'
import EntityListJson from '../json/EntityListJson.json'
import MetricStatusJson from '../json/MetricStatusJson.json'

import Vue from 'vue'
import axios from 'axios'

Vue.use(ElementUI);
jest.mock("axios")
jest.useFakeTimers();

describe('index.vue', () => {

  let study_data = { "_links": { "collection": "/api/stage-gate-studies/", "self": "/api/stage-gate-studies/1" }, "checklist_study_complete": true, "description": "test", "framework": "DTOceanPlus Framework template", "framework_id": 1, "id": 1, "name": "test" }
  let app_stage_gate = {
    "complete": true,
    "id": 1,
    "stage_gate": "Stage Gate 1 - 2",
    "stage_gate_id": 1,
    "stage_gate_study": 1,
    "stage_gate_study_id": 1
  }
  const $router = {
    push: jest.fn(),
  }
  const wrapper = shallowMount(ApplicantInput, {
    mocks: {
        $route: {
            params: {
                study_data: study_data,
                app_stage_gate: app_stage_gate
            }
        },
        $router
    }
  })

  it('getApplicantAnswers', async() => {
    axios.resolveWith(ApplicantAnswersJson);
    await wrapper.vm.getApplicantAnswers(wrapper.vm.appStageGateData.id);
    await wrapper.vm.$nextTick();
    expect(wrapper.vm.applicantAnswers).toEqual(ApplicantAnswersJson.data);
  })

  it('extractQuestionData', () => {
    expect(wrapper.vm.applicantAnswers).toEqual(ApplicantAnswersJson.data);
    wrapper.vm.extractQuestionData();
    expect(wrapper.vm.qualitativeQuestions).toEqual([
      {
        "description": "Cupidatat dolore irure in sint minim in non.",
        "id": 1,
        "name": "Question Category A",
        "questions": [
          {
            "applicant_answers": {
              "id": 1,
              "justification": null,
              "response": "this is a test",
              "result": null
            },
            "description": "Cupidatat dolore irure in sint minim in non.",
            "further_details": "Further details describing the Stage Gate question.",
            "id": 1,
            "name": "Question A1",
            "number": 1,
            "question_category": 1,
            "question_metric": null,
            "rubric": 1,
            "scoring_criteria": [
              {
                "description": "Description of the first scoring criterion being assessed in the question",
                "id": 1,
                "name": "First scoring criterion",
                "question": 1
              },
              {
                "description": "Description of the second scoring criterion being assessed in the question",
                "id": 2,
                "name": "Second scoring criterion",
                "question": 1
              }
            ],
            "weighting": 0.25
          },
          {
            "applicant_answers": {
              "id": 2,
              "justification": null,
              "response": null,
              "result": null
            },
            "description": "Cupidatat dolore irure in sint minim in non.",
            "further_details": "Further details describing the Stage Gate question.",
            "id": 2,
            "name": "Question A2",
            "number": 2,
            "question_category": 1,
            "question_metric": null,
            "rubric": 1,
            "scoring_criteria": [
              {
                "description": "Description of the first scoring criterion being assessed in the question",
                "id": 3,
                "name": "First scoring criterion",
                "question": 2
              },
              {
                "description": "Description of the second scoring criterion being assessed in the question",
                "id": 4,
                "name": "Second scoring criterion",
                "question": 2
              }
            ],
            "weighting": 0.25
          }
        ],
        "stage_gate": 1
      }
    ]);
    expect(wrapper.vm.quantitativeQuestions).toEqual([
      {
        applicant_id: 3,
        metric: "LCOE",
        unit: "Euro/kWh",
        threshold_type: "upper",
        threshold: 150,
        threshold_bool: true,
        weighting: 0.25,
        evaluation_area: "Affordability",
        result: 40,
        justification: "Is this working? Yes it is.",
        description: "Cupidatat dolore irure in sint minim in non."
      },
      {
        applicant_id: 4,
        metric: "Capacity factor",
        unit: "%",
        threshold_type: "lower",
        threshold: 20,
        threshold_bool: true,
        weighting: 0.25,
        evaluation_area: "Energy Capture",
        result: 5,
        justification: null,
        description: "Cupidatat dolore irure in sint minim in non."
      }
    ]);
    expect(wrapper.vm.qualitativeCount).toEqual(2);
    expect(wrapper.vm.quantitativeCount).toEqual(2);
  })

  it('goBack', async() => {
    await wrapper.vm.goBack()
    await wrapper.vm.$nextTick()
    expect($router.push).toHaveBeenCalledWith({"name": "ApplicantHome", "params": {"study_data": study_data}});
  })

  it('next button', async() => {
    expect(wrapper.vm.active).toEqual(0);
    wrapper.vm.next();
    await wrapper.vm.$nextTick()
    expect(wrapper.vm.active).toEqual(1);
  })

  it('back button', async() => {
    expect(wrapper.vm.active).toEqual(1);
    wrapper.vm.back();
    await wrapper.vm.$nextTick()
    expect(wrapper.vm.active).toEqual(0);
  })

  it('getApplicantAnswerPayload', async() => {
    let payload = wrapper.vm.getApplicantAnswerPayload();
    await wrapper.vm.$nextTick()
    let app_answers = [
      {
        "id": 1,
        "justification": null,
        "response": "this is a test",
        "result": null
      },
      {
        "id": 2,
        "justification": null,
        "response": null,
        "result": null
      },
      {
        "id": 3,
        "justification": "Is this working? Yes it is.",
        "response": null,
        "result": 40
      },
      {
        "id": 4,
        "justification": null,
        "response": null,
        "result": 5
      }
    ]
    expect(payload).toEqual(app_answers)
  })

  it('onSaveApplicantInputs', async() => {
    await wrapper.vm.onSaveApplicantInputs();
  })

  it('saveApplicantInputs', async() => {
    axios.resolveWith(ApplicantAnswersJson, false)
    await wrapper.vm.saveApplicantInputs();
  })

  it('saveApplicantInputs error', async() => {
    axios.resolveWith(ApplicantAnswersJson, true)
    await wrapper.vm.saveApplicantInputs();
  })

  it('onSubmitApplicantInputs', async() => {
    await wrapper.vm.onSubmitApplicantInputs();
  })

  it('openApplicantOutputs', async() => {
      await wrapper.vm.openApplicantOutputs();
      await wrapper.vm.$nextTick;
      expect($router.push).toHaveBeenCalledWith({
        "name": "ApplicantOutput",
        "params": {
          "study_data": study_data,
          "app_stage_gate_data": app_stage_gate
        }
      });
  })

  it('updateApplicantStudyStatus', async() => {
    axios.resolveWith(ApplicantAnswersJson, false)
    await wrapper.vm.updateApplicantStudyStatus();
  })

  it('updateApplicantStudyStatus error', async() => {
    axios.resolveWith(ApplicantAnswersJson, true)
    await wrapper.vm.updateApplicantStudyStatus();
  })

  it('filterHandler false', () => {
    let test = wrapper.vm.filterHandler(10, { 'test': 7 }, { property: 'test'});
    expect(test).toEqual(false);
  })

  it('filterHandler true', () => {
    let test = wrapper.vm.filterHandler(10, { 'test': 10 }, { property: 'test'});
    expect(test).toEqual(true);
  })

  it('getProjectStrucutre', async() => {
    axios.resolveWith(EntityListJson);
    await wrapper.vm.getProjectStructure();
    await wrapper.vm.$nextTick();
    expect(wrapper.vm.entityList).toEqual([
      {
          "projectId": 1,
          "studyId": 1,
          "moduleId": 1,
          "entityId": 1
      },
      {
          "projectId": 1,
          "studyId": 1,
          "moduleId": 2,
          "entityId": 1
      },
      {
          "projectId": 1,
          "studyId": 1,
          "moduleId": 3,
          "entityId": 1
      },
      {
          "projectId": 1,
          "studyId": 1,
          "moduleId": 4,
          "entityId": 1
      },
      {
          "projectId": 1,
          "studyId": 1,
          "moduleId": 5,
          "entityId": 1
      },
      {
          "projectId": 1,
          "studyId": 1,
          "moduleId": 6,
          "entityId": 1
      },
      {
          "projectId": 1,
          "studyId": 1,
          "moduleId": 7,
          "entityId": 1
      },
      {
          "projectId": 1,
          "studyId": 1,
          "moduleId": 8,
          "entityId": 1
      },
      {
          "projectId": 1,
          "studyId": 1,
          "moduleId": 9,
          "entityId": 1
      },
      {
          "projectId": 1,
          "studyId": 1,
          "moduleId": 10,
          "entityId": 1
      },
      {
          "projectId": 1,
          "studyId": 1,
          "moduleId": 11,
          "entityId": 1
      },
      {
          "projectId": 1,
          "studyId": 1,
          "moduleId": 12,
          "entityId": 1
      },
      {
          "projectId": 1,
          "studyId": 1,
          "moduleId": 13,
          "entityId": 1
      }
    ])
  })

  it('loadMetrics', async() => {
    axios.resolveWith(MetricStatusJson);
    await wrapper.vm.loadMetrics();
    jest.runAllTimers();
    expect(wrapper.vm.metricStatus).toEqual(MetricStatusJson.data);
  })

  it('loadMetrics error', async() => {
    axios.resolveWith(MetricStatusJson, true)
    await wrapper.vm.loadMetrics();
  })

  it('temp navigateSc', () => {
    wrapper.vm.navigateSc();
  })

  it('computed properties', () => {
    expect(wrapper.vm.numQualitativeAnswered).toEqual(1);
    expect(wrapper.vm.qualitativeProgress).toEqual(50);
    expect(wrapper.vm.numQuantitativeAnswered).toEqual(1);
    expect(wrapper.vm.quantitativeProgress).toEqual(50);
    expect(wrapper.vm.totalProgress).toEqual(50)
  })
})
