import { shallowMount } from '@vue/test-utils'

import FrameworksThresholds from '@/views/frameworks/thresholds.vue'

import ElementUI from 'element-ui';
import Vue from 'vue'
import axios from 'axios'

import FrameworkViewJson from '../json/FrameworkViewJson.json'
import StageDataJson from '../json/StageDataJson.json'
import StageEvalDataJson from '../json/StageEvalDataJson.json'
import StageGateDataJson from '../json/StageGateDataJson.json'

Vue.use(ElementUI);
jest.mock("axios")

describe('index.vue', () => {
  let framework_id = 1
  let wrapper = shallowMount(FrameworksThresholds, {
      mocks: {
          $route: {
              params: {
                  framework_id: framework_id,
              }
          }
      }
  })

  it('getFramework', async() => {
      axios.resolveWith(FrameworkViewJson)
      await wrapper.vm.getFramework(wrapper.vm.frameworkID);
      await wrapper.vm.$nextTick()
      expect(wrapper.vm.frameworkData).toEqual(FrameworkViewJson.data);
  })

  it('handleChange', () => {
    let val = {name: "1"};
    wrapper.vm.handleChange(val);
    expect(wrapper.vm.stageGateUri).toEqual("/api/stage-gates/2")
  })

  it('getStageGateData', async() => {
      axios.resolveWith(StageGateDataJson)
      await wrapper.vm.getStageGateData(wrapper.vm.frameworkData.stage_gates[0]);
      await wrapper.vm.$nextTick()
      expect(wrapper.vm.data).toEqual(StageGateDataJson.data.question_categories);
  })

  it('getStageGateData error', async() => {
    axios.resolveWith(StageGateDataJson, true)
    await wrapper.vm.getStageGateData(wrapper.vm.frameworkData.stage_gates[0]);
    await wrapper.vm.$nextTick()
  })

  it('filterHandler false', () => {
    let test = wrapper.vm.filterHandler(10, { 'test': 7 }, { property: 'test'});
    expect(test).toEqual(false);
  })

  it('filterHandler true', () => {
    let test = wrapper.vm.filterHandler(10, { 'test': 10 }, { property: 'test'});
    expect(test).toEqual(true);
  })

  it('updateMetricThresholds', async() => {
    axios.resolveWith({})
    await wrapper.vm.updateMetricThresholds();
    await wrapper.vm.$nextTick()
  })

  it('updateMetricThresholds error', async() => {
    axios.resolveWith({}, true)
    await wrapper.vm.updateMetricThresholds();
    await wrapper.vm.$nextTick()
  })

  it('quantitativeQuestions computed value', () => {
    expect(wrapper.vm.quantitativeQuestions).toEqual([
      {
        id: 1,
        metric: "LCOE",
        unit: "Euro/kWh",
        threshold_type: "upper",
        threshold: 150,
        threshold_bool: true,
        weighting: 0.25,
        evaluation_area: "Affordability",
        description: "Cupidatat dolore irure in sint minim in non."
      },
      {
        id: 2,
        metric: "Capacity factor",
        unit: "%",
        threshold_type: "lower",
        threshold: 20,
        threshold_bool: true,
        weighting: 0.25,
        evaluation_area: "Energy Capture",
        description: "Cupidatat dolore irure in sint minim in non."
      }
    ]);
  })

  it('questionMetricPayload', () => {
    expect(wrapper.vm.questionMetricPayload).toEqual(
      [
        {
          id: 1,
          threshold: 150,
          threshold_bool: true
        },
        {
          id: 2,
          threshold: 20,
          threshold_bool: true
        }
      ]
    )
  })
})
