import { shallowMount } from '@vue/test-utils'

import ChecklistInput from '@/views/stage_gate_studies/checklists/input/index.vue'
import ElementUI from 'element-ui';
import FrameworkViewJson from '../json/FrameworkViewJson.json'
import StageDataJson from '../json/StageDataJson.json'
import StageEvalDataJson from '../json/StageEvalDataJson.json'

import Vue from 'vue'
import axios from 'axios'

Vue.use(ElementUI);
jest.mock("axios")

describe('index.vue', () => {
  const $router = {
    push: jest.fn(),
  }
  let study_data = {
    "_links": {
      "collection": "/api/stage-gate-studies/",
      "self": "/api/stage-gate-studies/1"
    },
    "checklist_study_complete": true,
    "description": "test",
    "framework": "DTOceanPlus Framework template",
    "framework_id": 1,
    "id": 1,
    "name": "test"
  }
  const wrapper = shallowMount(ChecklistInput, {
    mocks: {
      $route: {
          params: {
              study_data: study_data
          }
      },
      $router
    }
  })

  const computedWrapper = shallowMount(ChecklistInput, {
    mocks: {
      $route: {
          params: {
              study_data: study_data
          }
      },
      $router
    },
    computed: {
      visibleActivities() {
        return [1, 2, 3, 4]
      },
      checkedVisibleActivities() {
        return [1, 2]
      },
      uncheckedVisibleActivities() {
        return [3, 4]
      },
      stageActivities() {
        return [1, 2, 3, 4, 5, 6]
      },
      checkedStageActivities() {
        return [1, 2, 3, 4]
      },
      uncheckedStageActivities() {
        return [5, 6]
      }
    }
  })

  it('goBack', async() => {
    await wrapper.vm.goBack()
    await wrapper.vm.$nextTick()
    expect($router.push).toHaveBeenCalledWith({"name": "SgStudiesHome", "params": {"study_id": study_data.id}});
  })

  it('initialiseData', async() => {
    axios.resolveWith(FrameworkViewJson)
    await wrapper.vm.initialiseData(wrapper.vm.studyData.framework_id)
    expect(wrapper.vm.framework_data).toEqual(FrameworkViewJson.data)
    expect(wrapper.vm.end_idx).toEqual(wrapper.vm.framework_data.stages.length - 1)
    expect(wrapper.vm.stage_uri).toEqual(FrameworkViewJson.data.stages[wrapper.vm.stage_idx])
  })

  it('getStageData', async() => {
    axios.resolveWith(StageDataJson)
    await wrapper.vm.getStageData(wrapper.vm.stage_uri);
    expect(wrapper.vm.stageName).toEqual(StageDataJson.data.name);
    expect(wrapper.vm.stageData).toEqual(StageDataJson.data.activity_categories);
  })

  it('getStageData error', async() => {
    axios.resolveWith(StageDataJson, true)
    await wrapper.vm.getStageData(wrapper.vm.stage_uri);
  })

  it('getChecklistData', async() => {
    let ChecklistDataJson = {
      "data": {
        "completed_activities": [1],
        "stage_activities": {
          0: [1, 2, 3],
          1: [10, 11, 12],
          2: [20, 21, 22]
        }
      }
    }
    axios.resolveWith(ChecklistDataJson)
    await wrapper.vm.getChecklistData(wrapper.vm.studyData.id);
    expect(wrapper.vm.selectedActivities).toEqual(ChecklistDataJson.data.completed_activities);
    expect(wrapper.vm.stageActivityData).toEqual(ChecklistDataJson.data.stage_activities);
  })

  it('getChecklistData error', async() => {
    let ChecklistDataJson = { "data": { "completed_activities": [1] } }
    axios.resolveWith(ChecklistDataJson, true)
    await wrapper.vm.getChecklistData(wrapper.vm.studyData.id);
  })

  it('getStageEvalData', async() => {
    axios.resolveWith(StageEvalDataJson)
    await wrapper.vm.getStageEvalData(wrapper.vm.stage_uri);
    expect(wrapper.vm.stageName).toEqual(StageEvalDataJson.data.name);
    expect(wrapper.vm.stageData).toEqual(StageEvalDataJson.data.evaluation_areas);
  })

  it('getStageEvalData error', async() => {
    axios.resolveWith(StageEvalDataJson, true)
    await wrapper.vm.getStageEvalData(wrapper.vm.stage_uri);
  })

  it('activateData true', () => {
    wrapper.vm.category = true
    wrapper.vm.activateData()
  })

  it('activateData false', () => {
    wrapper.vm.category = false
    wrapper.vm.activateData()
  })

  it('loadStage', () => {
    let result = wrapper.vm.loadStage()
    expect(wrapper.vm.stage_uri).toEqual(wrapper.vm.framework_data.stages[wrapper.vm.stage_idx])
  })

  it('toggleStage', () => {
    let fillsInit = [
      "#e30613",
      "white",
      "white",
      "white",
      "white",
      "white"
    ];
    let fillsExpected = [
      "white",
      "#e94e0f",
      "white",
      "white",
      "white",
      "white"
    ]
    expect(wrapper.vm.fills).toEqual(fillsInit);
    wrapper.vm.toggleStage(1);
    expect(wrapper.vm.fills).toEqual(fillsExpected);
    expect(wrapper.vm.stage_idx).toEqual(1);
  })

  it('updateTab', () => {
    let val = {index: 3};
    computedWrapper.vm.updateTab(val);
    expect(computedWrapper.vm.tabIndex).toEqual(3);
  })

  it('updateCategoryCheckbox', () => {
    computedWrapper.vm.updateCategoryCheckbox();
    expect(computedWrapper.vm.categoryIndeterminate).toEqual(true);
    expect(computedWrapper.vm.checkCategory).toEqual(false);
  })

  it('updateCategoryCheckbox first if block', () => {
    let cmp = shallowMount(ChecklistInput, {
      mocks: {
        $route: {
            params: {
                study_data: study_data
            }
        },
        $router
      },
      computed: {
        visibleActivities() {
          return [1, 2, 3, 4]
        },
        checkedVisibleActivities() {
          return [1, 2, 3, 4]
        }
      }
    });
    cmp.vm.updateCategoryCheckbox();
    expect(cmp.vm.categoryIndeterminate).toEqual(false);
    expect(cmp.vm.checkCategory).toEqual(true);
  })

  it('updateCategoryCheckbox last if block', () => {
    let cmp = shallowMount(ChecklistInput, {
      mocks: {
        $route: {
            params: {
                study_data: study_data
            }
        },
        $router
      },
      computed: {
        visibleActivities() {
          return [1, 2, 3, 4]
        },
        checkedVisibleActivities() {
          return []
        }
      }
    });
    cmp.vm.updateCategoryCheckbox();
    expect(cmp.vm.categoryIndeterminate).toEqual(false);
    expect(cmp.vm.checkCategory).toEqual(false);
  })

  it('updateStageCheckbox', () => {
    computedWrapper.vm.updateStageCheckbox();
    expect(computedWrapper.vm.stageIndeterminate).toEqual(true);
    expect(computedWrapper.vm.checkStage).toEqual(false);
  })

  it('updateStageCheckbox first if block', () => {
    let cmp = shallowMount(ChecklistInput, {
      mocks: {
        $route: {
            params: {
                study_data: study_data
            }
        },
        $router
      },
      computed: {
        stageActivities() {
          return [1, 2, 3, 4]
        },
        checkedStageActivities() {
          return [1, 2, 3, 4]
        }
      }
    });
    cmp.vm.updateStageCheckbox();
    expect(cmp.vm.stageIndeterminate).toEqual(false);
    expect(cmp.vm.checkStage).toEqual(true);
  })

  it('updateStageCheckbox last if block', () => {
    let cmp = shallowMount(ChecklistInput, {
      mocks: {
        $route: {
            params: {
                study_data: study_data
            }
        },
        $router
      },
      computed: {
        stageActivities() {
          return [1, 2, 3, 4]
        },
        checkedStageActivities() {
          return []
        }
      }
    });
    cmp.vm.updateStageCheckbox();
    expect(cmp.vm.stageIndeterminate).toEqual(false);
    expect(cmp.vm.checkStage).toEqual(false);
  })

  it('handleCheckCategoryChange true', () => {
    computedWrapper.setData({ selectedActivities: [1, 2] })
    computedWrapper.vm.handleCheckCategoryChange(true);
    expect(computedWrapper.vm.selectedActivities).toEqual([1, 2, 3, 4]);
    expect(computedWrapper.vm.checkCategory).toEqual(true);
    expect(computedWrapper.vm.categoryIndeterminate).toEqual(false);
  })

  it('handleCheckCategoryChange false', () => {
    let cmp = shallowMount(ChecklistInput, {
      mocks: {
        $route: {
            params: {
                study_data: study_data
            }
        },
        $router
      },
      computed: {
        visibleActivities() {
          return [1, 2, 3, 4]
        },
        checkedVisibleActivities() {
          return [1, 2, 3, 4]
        }
      }
    });
    cmp.setData({ selectedActivities: [1, 2, 3, 4] })
    cmp.vm.handleCheckCategoryChange(false);
    expect(cmp.vm.selectedActivities).toEqual([]);
    expect(cmp.vm.checkCategory).toEqual(false);
    expect(cmp.vm.categoryIndeterminate).toEqual(false);
  })

  it('handleCheckStageChange true', () => {
    computedWrapper.setData({ selectedActivities: [1, 2, 3, 4] })
    computedWrapper.vm.handleCheckStageChange(true);
    expect(computedWrapper.vm.selectedActivities).toEqual([1, 2, 3, 4, 5, 6]);
    expect(computedWrapper.vm.checkStage).toEqual(true);
    expect(computedWrapper.vm.stageIndeterminate).toEqual(false);
  })


  it('handleCheckStageChange false', () => {
    let cmp = shallowMount(ChecklistInput, {
      mocks: {
        $route: {
            params: {
                study_data: study_data
            }
        },
        $router
      },
      computed: {
        visibleActivities() {
          return [1, 2, 3, 4]
        },
        checkedVisibleActivities() {
          return [1, 2, 3, 4]
        },
        stageActivities() {
          return [1, 2, 3, 4]
        },
        checkedStageActivities() {
          return [1, 2, 3, 4]
        }
      }
    });
    cmp.setData({ selectedActivities: [1, 2, 3, 4] })
    cmp.vm.handleCheckStageChange(false);
    expect(cmp.vm.selectedActivities).toEqual([]);
    expect(cmp.vm.checkStage).toEqual(false);
    expect(cmp.vm.stageIndeterminate).toEqual(false);
  })

  it('handleCheckedActivitiesChange', () => {
    computedWrapper.vm.handleCheckedActivitiesChange();
    expect(computedWrapper.vm.checkCategory).toEqual(false);
    expect(computedWrapper.vm.categoryIndeterminate).toEqual(true);
    expect(computedWrapper.vm.checkStage).toEqual(false);
    expect(computedWrapper.vm.stageIndeterminate).toEqual(true);
  })

  it('onSaveChecklist', () => {
    wrapper.vm.onSaveChecklist()
  })

  it('onSubmitChecklist', () => {
    wrapper.vm.onSubmitChecklist()
  })

  it('saveChecklist', async() => {
    const payload = {
        completed_activities: wrapper.vm.selectedActivities
    }
    let success_msg = {
      data: {
        message: 'fair job',
        type: 'success'
      }
    }
    axios.resolveWith(success_msg)
    await wrapper.vm.saveChecklist(payload, wrapper.vm.studyData.id)
  })

  it('openResultsPage', async() => {
    await wrapper.vm.openResultsPage()
    await wrapper.vm.$nextTick()
    expect($router.push).toHaveBeenCalledWith({"name": "ChecklistOutput", "params": {"study_data": study_data}});
  })

  it('visibleActivities', () => {
    expect(wrapper.vm.visibleActivities).toEqual([]);
    wrapper.setData({ tabIndex: 0 });
    // stageData is currently set to evaluation area category, so first set of activities are as defined as in StageEvalDataJson
    expect(wrapper.vm.visibleActivities).toEqual([6, 7, 8, 9, 10]);
  })

  it('checkedVisibleActivities', () => {
    let cmp = shallowMount(ChecklistInput, {
      mocks: {
        $route: {
            params: {
                study_data: study_data
            }
        },
        $router
      },
      computed: {
        visibleActivities() {
          return [1, 2, 3, 4]
        }
      }
    });
    cmp.setData({ selectedActivities: [1, 2] });
    expect(cmp.vm.checkedVisibleActivities).toEqual([1, 2])
  })

  it('uncheckedVisibleActivities', () => {
    let cmp = shallowMount(ChecklistInput, {
      mocks: {
        $route: {
            params: {
                study_data: study_data
            }
        },
        $router
      },
      computed: {
        visibleActivities() {
          return [1, 2, 3, 4]
        },
        checkedVisibleActivities() {
          return [1, 2]
        }
      }
    });
    expect(cmp.vm.uncheckedVisibleActivities).toEqual([3, 4]);
  })

  it('stageActivities', () => {
    expect(wrapper.vm.stageActivities).toEqual([6, 7, 8, 9, 10]);
  })

  it('checkedStageActivities', () => {
    wrapper.setData({ selectedActivities: [6, 7, 8] });
    expect(wrapper.vm.checkedStageActivities).toEqual([6, 7, 8]);
  })

  it('uncheckedStageActivities', () => {
    expect(wrapper.vm.uncheckedStageActivities).toEqual([9, 10]);
  })

  it('liveProgressData', () => {
    wrapper.setData({ selectedActivities: [1, 2, 3, 10] });
    expect(wrapper.vm.liveProgressData).toEqual({0: 100, 1: 33, 2: 0});
  })

  it('watch stageData', () => {
    computedWrapper.setData({ tabIndex: true });
    computedWrapper.setData({ stageData: StageDataJson.data.activity_categories });
    expect(computedWrapper.vm.checkCategory).toEqual(false);
  })
})
