import { shallowMount } from '@vue/test-utils'

import SgStudyComparisonResults from '@/views/stage_gate_studies/comparison/results/index'
import ElementUI from 'element-ui';
import getSgStudyJson from '../json/getSgStudy.json'

import Vue from 'vue'
import axios from 'axios'

Vue.use(ElementUI);
jest.mock("axios")

describe('index.vue', () => {

  const $router = {
    push: jest.fn(),
  }
  let studyIds = ["1", "2"]
  const wrapper = shallowMount(SgStudyComparisonResults, {
    mocks: {
      $route: {
        params: {
          studyIds: studyIds,
      }
      },
      $router
    }
  })

  it('getSgStudy', async() => {
    axios.resolveWith(getSgStudyJson);
    await wrapper.vm.getSelectedStudies();
    await wrapper.vm.$nextTick();
    expect(wrapper.vm.sgStudies[0]).toEqual(getSgStudyJson.data)
    expect(wrapper.vm.sgStudies[1]).toEqual(getSgStudyJson.data)
  })

  it('getQueryString', () => {
    wrapper.vm.getQueryString();
    expect(wrapper.vm.queryString).toEqual("1&id=2")
  })
})
