import { shallowMount } from '@vue/test-utils'

import AssessorInput from '@/views/stage_gate_studies/assessor/input/index'
import ElementUI from 'element-ui';
import AssessorScoresJson from '../json/AssessorScoresJson.json'

import Vue from 'vue'
import axios from 'axios'
import { _ } from 'core-js';

Vue.use(ElementUI);
jest.mock("axios")

describe('index.vue', () => {

  let study_data = { "_links": { "collection": "/api/stage-gate-studies/", "self": "/api/stage-gate-studies/1" }, "checklist_study_complete": true, "description": "test", "framework": "DTOceanPlus Framework template", "framework_id": 1, "id": 1, "name": "test" }
  let app_stage_gate = {
    "complete": true,
    "id": 1,
    "stage_gate": "Stage Gate 1 - 2",
    "stage_gate_id": 1,
    "stage_gate_study": 1,
    "stage_gate_study_id": 1
  }
  const $router = {
    push: jest.fn(),
  }
  const wrapper = shallowMount(AssessorInput, {
    mocks: {
        $route: {
            params: {
                study_data: study_data,
                app_stage_gate: app_stage_gate
            }
        },
        $router
    }
  })

  it('getAssessorScores', async() => {
    axios.resolveWith(AssessorScoresJson)
    await wrapper.vm.getAssessorScores(wrapper.vm.appStageGateData.id)
    await wrapper.vm.$nextTick()
    expect(wrapper.vm.assessorScores).toEqual(AssessorScoresJson.data)
    let s_list = [
      {
        "id": 1,
        "score": 2
      },
      {
        "id": 2,
        "score": 3
      },
      {
        "id": 3,
        "score": 2
      },
      {
        "id": 4,
        "score": 3
      }
    ]
    expect(wrapper.vm.score_list).toEqual(s_list)
  })

  it('separateQuestionData', () => {
    expect(wrapper.vm.assessorScores).toEqual(AssessorScoresJson.data);
    wrapper.vm.separateQuestionData();
    expect(wrapper.vm.qualitativeQuestions).toEqual([
      {
        "description": "Cupidatat dolore irure in sint minim in non.",
        "id": 1,
        "name": "Question Category A",
        "questions": [
          {
            "applicant_results": {
              "id": 1,
              "justification": null,
              "response": "this is a test",
              "result": null
            },
            "description": "Cupidatat dolore irure in sint minim in non.",
            "further_details": "Further details describing the Stage Gate question.",
            "id": 1,
            "name": "Question A1",
            "number": 1,
            "question_category": 1,
            "question_metric": null,
            "rubric": 1,
            "score": {
              "id": 1,
              "score": 2
            },
            "scoring_criteria": [
              {
                "comment": {
                  "comment": "this is a test comment",
                  "id": 1
                },
                "description": "Description of the first scoring criterion being assessed in the question",
                "id": 1,
                "name": "First scoring criterion",
                "question": 1
              },
              {
                "comment": {
                  "comment": "asdrfasdf",
                  "id": 2
                },
                "description": "Description of the second scoring criterion being assessed in the question",
                "id": 2,
                "name": "Second scoring criterion",
                "question": 1
              }
            ],
            "weighting": 0.05
          },
          {
            "applicant_results": {
              "id": 2,
              "justification": null,
              "response": null,
              "result": null
            },
            "description": "Cupidatat dolore irure in sint minim in non.",
            "further_details": "Further details describing the Stage Gate question.",
            "id": 2,
            "name": "Question A2",
            "number": 2,
            "question_category": 1,
            "question_metric": null,
            "rubric": 1,
            "score": {
              "id": 2,
              "score": 3
            },
            "scoring_criteria": [
              {
                "comment": {
                  "comment": "asdfwqer",
                  "id": 3
                },
                "description": "Description of the first scoring criterion being assessed in the question",
                "id": 3,
                "name": "First scoring criterion",
                "question": 2
              },
              {
                "comment": {
                  "comment": "fasderqwrqwe",
                  "id": 4
                },
                "description": "Description of the second scoring criterion being assessed in the question",
                "id": 4,
                "name": "Second scoring criterion",
                "question": 2
              }
            ],
            "weighting": 0.1
          }
        ],
        "stage_gate": 1
      }
    ]);
    expect(wrapper.vm.quantitativeQuestions).toEqual([
      {
        applicant_id: 1,
        metric: "LCOE",
        unit: "Euro/kWh",
        threshold_type: "upper",
        threshold: 150,
        threshold_bool: true,
        threshold_passed: true,
        weighting: 0.05,
        evaluation_area: "Affordability",
        result: 143,
        justification: "Is this working? Yes it is.",
        description: "Cupidatat dolore irure in sint minim in non.",
        absolute_distance: null,
        percent_distance: null,
        scoring_criteria: [
          {
            comment: {
              comment: "this is a test comment",
              id: 1
            },
            description: "Description of the first scoring criterion being assessed in the question",
            id: 1,
            name: "First scoring criterion",
            question: 1
          },
          {
            comment: {
              comment: "asdrfasdf",
              id: 2
            },
            description: "Description of the second scoring criterion being assessed in the question",
            id: 2,
            name: "Second scoring criterion",
            question: 1
          }
        ],
        score: {
          id: 3,
          score: 2
        }
      },
      {
        applicant_id: 2,
        metric: "LCOE",
        unit: "Euro/kWh",
        threshold_type: "upper",
        threshold: 150,
        threshold_bool: true,
        threshold_passed: true,
        weighting: 0.1,
        evaluation_area: "Affordability",
        result: 143,
        justification: "Is this working? Yes it is.",
        description: "Cupidatat dolore irure in sint minim in non.",
        absolute_distance: null,
        percent_distance: null,
        scoring_criteria: [
          {
            comment: {
              comment: "asdfwqer",
              id: 3
            },
            description: "Description of the first scoring criterion being assessed in the question",
            id: 3,
            name: "First scoring criterion",
            question: 2
          },
          {
            comment: {
              comment: "fasderqwrqwe",
              id: 4
            },
            description: "Description of the second scoring criterion being assessed in the question",
            id: 4,
            name: "Second scoring criterion",
            question: 2
          }
        ],
        score: {
          id: 4,
          score: 3
        }
      }
    ]);
    expect(wrapper.vm.qualitativeCount).toEqual(2);
    expect(wrapper.vm.quantitativeCount).toEqual(2);
  })

  it('goBack', async() => {
    await wrapper.vm.goBack()
    await wrapper.vm.$nextTick()
    expect($router.push).toHaveBeenCalledWith({"name": "AssessorHome", "params": {"study_data": study_data}});
  })

  it('next button', async() => {
    expect(wrapper.vm.active).toEqual(0);
    wrapper.vm.next();
    await wrapper.vm.$nextTick()
    expect(wrapper.vm.active).toEqual(1);
  })

  it('back button', async() => {
    expect(wrapper.vm.active).toEqual(1);
    wrapper.vm.back();
    await wrapper.vm.$nextTick()
    expect(wrapper.vm.active).toEqual(0);
  })

  it('filterHandler false', () => {
    let test = wrapper.vm.filterHandler(10, { 'test': 7 }, { property: 'test'});
    expect(test).toEqual(false);
  })

  it('filterHandler true', () => {
    let test = wrapper.vm.filterHandler(10, { 'test': 10 }, { property: 'test'});
    expect(test).toEqual(true);
  })

  it('isThresholdApplied', () => {
    let q_true = {
      threshold_bool: true
    };
    let q_threshold_true = wrapper.vm.isThresholdApplied(q_true);
    expect(q_threshold_true).toBeTruthy()
    let q_false = {
      threshold_bool: false
    };
    let q_threshold_false = wrapper.vm.isThresholdApplied(q_false);
    expect(q_threshold_false).toBeFalsy()
    let q_no_qm = {
      error: true
    };
    let q_no_qm_false = wrapper.vm.isThresholdApplied(q_no_qm);
    expect(q_no_qm_false).toBeFalsy()
  })

  it('isThresholdFailed', () => {
    let error = {
      error: true
    };
    let error_out = wrapper.vm.isThresholdFailed(error);
    expect(error_out).toBeFalsy()
    let threshold = {
      threshold_bool: true,
      threshold_passed: false
    };
    let threshold_out = wrapper.vm.isThresholdFailed(threshold);
    expect(threshold_out).toBeTruthy()
    let threshold_fail = {
      threshold_bool: true,
      threshold_passed: true
    };
    let threshold_failed_false = wrapper.vm.isThresholdFailed(threshold_fail);
    expect(threshold_failed_false).toBeFalsy()
  })

  it('returnThresholdPassed', () => {
    let q_yes = {
      threshold_bool: true,
      threshold_passed: true
    }
    let q_yes_out = wrapper.vm.returnThresholdPassed(q_yes);
    expect(q_yes_out).toEqual("Yes")
    let q_no = {
      threshold_bool: true,
      threshold_passed: false
    }
    let q_no_out = wrapper.vm.returnThresholdPassed(q_no);
    expect(q_no_out).toEqual("No")
  })

  it('getAssessorScoresPayload', () => {
    let payload = wrapper.vm.getAssessorScoresPayload();
    let assessor_scores = {
      'assessor_scores': [
        {
          "id": 1,
          "score": 2
        },
        {
          "id": 2,
          "score": 3
        },
        {
          "id": 3,
          "score": 2
        },
        {
          "id": 4,
          "score": 3
        },
      ],
      'assessor_comments': [
        {
          "id": 1,
          "comment": "this is a test comment"
        },
        {
          "id": 2,
          "comment": "asdrfasdf"
        },
        {
          "id": 3,
          "comment": "asdfwqer"
        },
        {
          "id": 4,
          "comment": "fasderqwrqwe"
        },
        {
          "id": 1,
          "comment": "this is a test comment"
        },
        {
          "id": 2,
          "comment": "asdrfasdf"
        },
        {
          "id": 3,
          "comment": "asdfwqer"
        },
        {
          "id": 4,
          "comment": "fasderqwrqwe"
        },
      ]
    };
    expect(payload).toEqual(assessor_scores)
  })

  it('checkAssessorInputPayload', () => {
    wrapper.vm.checkAssessorInputPayload()
    expect(wrapper.vm.defaultScoreDialogVisible).toBe(false)
    wrapper.setData({ score_list: [ {id: 1, score: 2}, {id: 2, score: null} ] })
    wrapper.vm.checkAssessorInputPayload();
    expect(wrapper.vm.defaultScoreDialogVisible).toBe(true)
  })

  it('onCancel', () => {
    wrapper.setData({ defaultScoreDialogVisible: true })
    wrapper.vm.onCancel()
    expect(wrapper.vm.defaultScoreDialogVisible).toBe(false)
  })

  it('onFillScores', () => {
    wrapper.setData({ score_list: [ {id: 1, score: "2"}, {id: 2, score: null} ] })
    wrapper.vm.onFillScores()
    expect(wrapper.vm.score_list).toEqual([{"id": 1, "score": "2"}, {"id": 2, "score": "2"}])
  })

  it('onSaveAssessorScores', async() => {
    await wrapper.vm.onSaveAssessorScores();
  })

  it('saveAssessorScores', async() => {
    axios.resolveWith(AssessorScoresJson, false)
    await wrapper.vm.saveAssessorScores();
  })

  it('saveAssessorScores error', async() => {
    axios.resolveWith(AssessorScoresJson, true)
    await wrapper.vm.saveAssessorScores();
  })

  it('onSubmitAssessorScores', async() => {
    await wrapper.vm.onSubmitAssessorScores();
  })

  it('openAssessorOutputs', async() => {
      await wrapper.vm.openAssessorOutputs();
      await wrapper.vm.$nextTick();
      expect($router.push).toHaveBeenCalledWith({
        "name": "AssessorOutput",
        "params": {
          "study_data": study_data,
          "app_stage_gate_data": app_stage_gate
        }
      });
  })

  it('updateAssessorStudyStatus', async() => {
    axios.resolveWith(AssessorScoresJson, false)
    await wrapper.vm.updateAssessorStudyStatus();
  })

  it('updateAssessorStudyStatus error', async() => {
    axios.resolveWith(AssessorScoresJson, true)
    await wrapper.vm.updateAssessorStudyStatus();
  })

  it('setRubricVisible', () => {
    expect(wrapper.vm.rubricVisible).toEqual(false);
    wrapper.vm.setRubricVisible();
    expect(wrapper.vm.rubricVisible).toEqual(true);
  })

  it('computed properties', () => {
    expect(wrapper.vm.numQualitativeMarked).toEqual(2);
    expect(wrapper.vm.qualitativeProgress).toEqual(100);
    expect(wrapper.vm.numQuantitativeMarked).toEqual(2);
    expect(wrapper.vm.quantitativeProgress).toEqual(100);
    expect(wrapper.vm.totalProgress).toEqual(100)
  })
})
