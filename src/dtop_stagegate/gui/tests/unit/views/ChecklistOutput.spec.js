import { shallowMount } from '@vue/test-utils'

import ChecklistOutput from '@/views/stage_gate_studies/checklists/output/index.vue'
import ElementUI from 'element-ui';
import CheckListOutputsJson from '../json/CheckListOutputsJson.json'
import ComplexityLevelJson from '../json/ComplexityLevelJson.json'
import StageGateAssessmentsJson from '../json/StageGateAssessmentsJson.json'

import Vue from 'vue'
import axios from 'axios'

Vue.use(ElementUI);
jest.mock("axios")

describe('index.vue', () => {
  let study_data = { "study_data": { "_links": { "collection": "/api/stage-gate-studies/", "self": "/api/stage-gate-studies/1" }, "checklist_study_complete": true, "description": "test", "framework": "DTOceanPlus Framework template", "framework_id": 1, "id": 1, "name": "test" } }
  const $router = {
    push: jest.fn(),
  }
  const wrapper = shallowMount(ChecklistOutput, {
    mocks: {
      $route: {
        params: study_data
      },
      $router
    }
  })

  it('goBack', async() => {
    await wrapper.vm.goBack()
    await wrapper.vm.$nextTick()
    expect($router.push).toHaveBeenCalledWith({"name": "SgStudiesHome", "params": {"study_id": study_data.study_data.id}});
  })

  it('getChecklistOutputs', async() => {
    axios.resolveWith(CheckListOutputsJson)
    await wrapper.vm.getChecklistOutputs()
    expect(wrapper.vm.summary_stage_data).toEqual(CheckListOutputsJson.data.results)
  })

  it('getComplexityLevels', async() => {
    axios.resolveWith(ComplexityLevelJson);
    await wrapper.vm.getComplexityLevels();
    expect(wrapper.vm.complexityLevels).toEqual(ComplexityLevelJson.data);
    expect(wrapper.vm.complexityLevelTableData).toEqual([
      {
        module: "Site Characterisation",
        cpx: 1
      },
      {
        module: "Machine Characterisation",
        cpx: 1
      },
      {
        module: "Energy Capture",
        cpx: 1
      },
      {
        module: "Energy Transformation",
        cpx: 1
      },
      {
        module: "Energy Delivery",
        cpx: 3
      },
      {
        module: "Station Keeping",
        cpx: 1
      },
      {
        module: "Logistics and Marine Operations",
        cpx: 1
      },
      {
        module: "System Lifetime Costs",
        cpx: 1
      },
      {
        module: "Reliability, Availability, Maintainability and Survivability",
        cpx: 1
      },
      {
        module: "Environmental and Social Acceptance",
        cpx: 1
      }
    ])
  })

  it('customColorMethod', () => {
    let red = wrapper.vm.getColour(30);
    expect(red).toEqual('#f56c6c');
    let grey = wrapper.vm.getColour(59);
    expect(grey).toEqual('#909399');
    let orange = wrapper.vm.getColour(99);
    expect(orange).toEqual('#e6a23c');
    let green = wrapper.vm.getColour(100);
    expect(green).toEqual('#5cb87a');
  })

  it('startIntegratedMode', () => {
    wrapper.vm.startIntegratedMode();
  })

  it('openChecklistStage', async() => {
    await wrapper.vm.openChecklistStage(0)
    await wrapper.vm.$nextTick()
    expect($router.push).toHaveBeenCalledWith({"name": "ChecklistStage", "params": {"study_data": study_data.study_data, stage_data: CheckListOutputsJson.data.results[0]}});
  })

  it('getStageGateData', async() => {
    axios.resolveWith(StageGateAssessmentsJson)
    await wrapper.vm.getStageGateData(wrapper.vm.studyData.id);
    await wrapper.vm.$nextTick()
    expect(wrapper.vm.stageGateData).toEqual(StageGateAssessmentsJson.data.stage_gate_assessments)
  })

  let app_stage_gate = {
    "applicant_complete": true,
    "assessor_complete": true,
    "id": 1,
    "stage_gate": "Stage Gate 0 - 1",
    "stage_gate_id": 1,
    "stage_gate_study": 1,
    "stage_gate_study_id": 1
  }
  it('launchApplicant', async() => {
    await wrapper.vm.launchApplicant()
    await wrapper.vm.$nextTick()
    expect($router.push).toHaveBeenCalledWith({
      "name": "ApplicantInput",
      "params": {
        "study_data": study_data.study_data,
        "app_stage_gate": app_stage_gate
      }
    });
  })
})
