// This is the Stage Gate module of the DTOceanPlus suite
// Copyright (C) 2021 Wave Energy Scotland - Ben Hudson
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
// License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.
import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

/* Layout */
import Layout from '@/layout'

export const constantRoutes = [
  // {
  //   path: '/login',
  //   component: () => import('@/views/login/index'),
  //   hidden: true
  // },

  {
    path: '/404',
    component: () => import('@/views/404'),
    hidden: true
  },

  {
    path: '/',
    component: Layout,
    redirect: '/dashboard',
    children: [{
      path: 'dashboard',
      name: 'Dashboard',
      component: () => import('@/views/dashboard/index'),
      meta: { title: 'Home', icon: 'dashboard' }
    }]
  },

  {
    path: '/frameworks',
    component: Layout,
    redirect: '/frameworks/home',
    name: 'Framework',
    meta: { title: 'Framework', icon: 'nested' },
    children: [
      {
        path: 'home',
        name: 'Home',
        component: () => import('@/views/frameworks/home'),
        meta: { title: 'Home'}
      },
      {
        path: 'explorer',
        name: 'Framework Explorer',
        component: () => import('@/views/frameworks/view/index'),
        meta: { title: 'Framework Explorer' }
      },
      {
        path: 'index',
        name: 'Metric Thresholds',
        component: () => import('@/views/frameworks/index'),
        meta: { title: 'Metric Thresholds' }
      },
      {
        path: ':framework_id',
        name: 'Thresholds',
        component: () => import('@/views/frameworks/thresholds'),
        meta: { title: 'Edit Thresholds' },
        hidden: true
      }
    ]
  },

  {
    path: '/stage-gate-studies',
    component: Layout,
    redirect: '/stage-gate-studies/index',
    name: 'StageGateStudies',
    meta: { title: 'Stage Gate Studies', icon: 'table' },
    children: [
      {
        path: 'index',
        name: 'StageGateStudiesIndex',
        component: () => import('@/views/stage_gate_studies/index'),
        meta: { title: 'Index' },
        hidden: true
      },
      {
        path: ':study_id',
        name: 'SgStudiesHome',
        component: () => import('@/views/stage_gate_studies/home/index'),
        meta: { title: 'Home' },
        hidden: true
      },
      {
        path: ':study_id/checklist-input',
        name: 'ChecklistInput',
        component: () => import('@/views/stage_gate_studies/checklists/input/index'),
        meta: { title: 'Checklist Input' },
        hidden: true
      },
      {
        path: ':study_id/checklist-output',
        name: 'ChecklistOutput',
        component: () => import('@/views/stage_gate_studies/checklists/output/index'),
        meta: { title: 'Checklist Output' },
        hidden: true
      },
      {
        path: ':study_id/checklist-stage',
        name: 'ChecklistStage',
        component: () => import('@/views/stage_gate_studies/checklists/output/stage/index'),
        meta: { title: 'Checklist Stage Results' },
        hidden: true
      },
      {
        path: ':study_id/applicant-home',
        name: 'ApplicantHome',
        component: () => import('@/views/stage_gate_studies/applicant/home/index'),
        meta: { title: 'Applicant Mode Home' },
        hidden: true
      },
      {
        path: ':study_id/applicant-input',
        name: 'ApplicantInput',
        component: () => import('@/views/stage_gate_studies/applicant/input/index'),
        meta: { title: 'Applicant Mode Input' },
        hidden: true
      },
      {
        path: ':study_id/applicant-output',
        name: 'ApplicantOutput',
        component: () => import('@/views/stage_gate_studies/applicant/output/index'),
        meta: { title: 'Applicant Mode Output' },
        hidden: true
      },
      {
        path: ':study_id/assessor-home',
        name: 'AssessorHome',
        component: () => import('@/views/stage_gate_studies/assessor/home/index'),
        meta: { title: 'Assessor Mode Home' },
        hidden: true
      },
      {
        path: ':study_id/assessor-input',
        name: 'AssessorInput',
        component: () => import('@/views/stage_gate_studies/assessor/input/index'),
        meta: { title: 'Assessor Mode Input' },
        hidden: true
      },
      {
        path: ':study_id/assessor-output',
        name: 'AssessorOutput',
        component: () => import('@/views/stage_gate_studies/assessor/output/index'),
        meta: { title: 'Assessor Mode Output' },
        hidden: true
      },
      {
        path: ':study_id/improvement-areas',
        name: 'ImprovementAreas',
        component: () => import('@/views/stage_gate_studies/improvement_areas/index'),
        meta: { title: 'Improvement Areas' },
        hidden: true
      },
      {
        path: 'comparison-home',
        name: 'SgStudyComparisonHome',
        component: () => import('@/views/stage_gate_studies/comparison/index'),
        meta: { title: 'Study Comparison Home' },
        hidden: true
      },
      {
        path: 'comparison-results',
        name: 'SgStudyComparisonResults',
        component: () => import('@/views/stage_gate_studies/comparison/results/index'),
        meta: { title: 'Study Comparison Results' },
        hidden: true
      },
      {
        path: ':study_id/report-export',
        name: 'ReportExport',
        component: () => import('@/views/stage_gate_studies/report_export/index'),
        meta: { title: 'Report Export'},
        hidden: true
      }
    ]
  },

  {
    path: 'external-link',
    component: Layout,
    name: 'Links',
    meta: {
      title: 'Links',
      icon: 'link'
    },
    children: [
      {
        path: 'https://wave-energy-scotland.gitlab.io/dtoceanplus/dtop_documentation/sg/docs/index.html',
        meta: { title: 'Documentation' }
      },
      {
        path: 'https://www.dtoceanplus.eu/',
        meta: { title: 'DTOceanPlus' }
      },
    ]
  },

  // 404 page must be placed at the end !!!
  { path: '*', redirect: '/404', hidden: true }
]

const createRouter = () => new Router({
  // mode: 'history', // require service support
  scrollBehavior: () => ({ y: 0 }),
  routes: constantRoutes
})

const router = createRouter()

// Detail see: https://github.com/vuejs/vue-router/issues/1234#issuecomment-357941465
export function resetRouter() {
  const newRouter = createRouter()
  router.matcher = newRouter.matcher // reset router
}

export default router
