<!--
This is the Stage Gate module of the DTOceanPlus suite
Copyright (C) 2021 Wave Energy Scotland - Ben Hudson

This program is free software: you can redistribute it and/or modify it
under the terms of the Affero GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
for more details.

You should have received a copy of the Affero GNU General Public License
along with this program. If not, see <https://www.gnu.org/licenses/>.
-->
<template>
  <div class="app-container">
    <el-header>
      <h1>Improvement Areas</h1>
      <br>
      <el-button
          icon="el-icon-arrow-left"
          @click="goBack()">Go Back</el-button>
      <br><br><br>
      <el-row type="flex" justify="center">
        <el-col :span="8">
          <b>Stage Gate Study: </b> <span data-cy-sg="title">{{ studyData.name }}</span>
        </el-col>
        <el-col :span="8">
          <b>Description: </b> {{ studyData.description }}
        </el-col>
        <el-col :span="8">
          <b>Threshold settings: </b> {{ studyData.framework }}
        </el-col>
      </el-row>
    </el-header>
    <div>
      <br><br><br><br><br><br><br><br>
      <el-row>
        <el-main>
          <el-row class="bg-white">
            <p>
              Please select the Stage to use as the basis for the Improvement Area analysis. Any applicant answers or assessor scores provided in the
              Stage Gate immediately preceding the selected Stage are also used in this analysis (where applicable). By default, the analysis uses the
              Activity Checklist results to identify the earliest Stage that has not been fully completed and selects this Stage as the basis of the analysis.
            </p>
          </el-row>
          <br>
          <el-row>
            <el-select v-model="stageIndex" placeholder="Select a Stage" @change="onSelectStage(stageIndex)" data-cy-sg="selectStage">
              <el-option
                v-for="item in stageOptions"
                :key="item.value"
                :label="item.label"
                :value="item.value"
                :data-cy-sg="`selectItem${item.value}`">
              </el-option>
            </el-select>
          </el-row>
          <br><br>
          <el-row class="bg-white">
            <p>
              The list of improvement areas that have been identified for this Stage Gate study will be shown below.
              Improvement areas are those evaluation areas that have been highlighted as weaknesses of the technology or device.
              Improvement areas are defined by the <i>average improvement area score</i>.
              This number is the average of the checklist, metric and weighted average assessor scores.
              The <i>checklist score</i> is the result of the Activity Checklist for each evaluation area.
              For each Evaluation Area, the <i>metric score</i> is calculated as the number of metrics that have passed their metric thresholds divided by the number of metrics for that evaluation area that have thresholds applied, expressed as a percentage.
              Finally, the <i>assessor score</i> is calculated as the weighted average assessor score for each evaluation area, expressed as a percentage with respect to the maximum possible score (4).
              The average improvement area score is based only on those scores that are available.
              For instance, in the case where the metric and assessor scores are not applicable (e.g. Applicant Mode and Assessor Mode have not been completed), then the average improvement area score is just equal to the checklist score for each evaluation area.
              If only one column is unavailable, the average improvement area score is the average of the two remaining columns.
              The table of results is sorted by the average improvement area score in ascending order.
            </p>
            <p>
              The default threshold level for the improvement area calculation is 50%, but this can be changed by editing the desired threshold below and pressing <b>Go</b>.
              Evaluation areas with an average improvement area score less than this threshold will be shown in the table.
            </p>
          </el-row>
          <br>
          <el-row>
            <el-input-number v-model="iaThreshold" :precision="0" :step="5" :min="0" :max="100"></el-input-number>
          </el-row>
          <br>
          <el-row>
            <el-button type="success" @click="onUpdateThreshold()" data-cy-sg="updateIaThreshold">Go</el-button>
          </el-row>
          <br><br><br>
          <el-table
              :data="improvementAreas">
            <el-table-column
              prop="name"
              label="Evaluation area"
              width="200">
            </el-table-column>
            <el-table-column
              prop="checklist_score"
              label="Checklist score (%)"
              width="200">
            </el-table-column>
            <el-table-column
              prop="metric_score"
              label="Metric score (%)"
              width="200">
            </el-table-column>
            <el-table-column
              prop="assessor_score"
              label="Weighted average assessor score (%)"
              width="300">
            </el-table-column>
            <el-table-column
              prop="average_score"
              label="Average improvement area score (%)"
              width="300">
            </el-table-column>
            </el-table>
        </el-main>
      </el-row>
    </div>
  </div>
</template>

<script>
import axios from 'axios'

export default {
  data() {
    return {
      studyData: {},
      improvementAreas: [],
      stageIndex: null,
      iaThreshold: 50,
      stageOptions: [{
        value: 0,
        label: 'Stage 0'
      }, {
        value: 1,
        label: 'Stage 1'
      }, {
        value: 2,
        label: 'Stage 2'
      }, {
        value: 3,
        label: 'Stage 3'
      }, {
        value: 4,
        label: 'Stage 4'
      }, {
        value: 5,
        label: 'Stage 5'
      }],
      payload: {
        'stage_index': null,
        'threshold': 50
      }
    }
  },
  created() {
    this.studyData = this.$route.params.study_data
    this.getImprovementAreas(this.studyData.id, this.payload)
  },
  methods: {
    goBack() {
      this.$router.push({ name: 'SgStudiesHome', params: { study_id: this.studyData.id } })
    },
    getImprovementAreas(studyId, payload) {
      const path = `${process.env.VUE_APP_API_URL}/api/stage-gate-studies/${studyId}/improvement-areas`
      axios.post(path, payload)
        .then((res) => {
          this.improvementAreas = res.data.improvement_areas
          this.stageIndex = res.data.stage_assessed
        })
        .catch((error) => {
          // eslint-disable-next-line
          // console.error(error);
        })
    },
    onSelectStage(stageIndex) {
      this.payload = {
        'stage_index': stageIndex,
        'threshold': this.iaThreshold
      }
      this.getImprovementAreas(this.studyData.id, this.payload)
    },
    onUpdateThreshold() {
      this.payload = {
        'stage_index': this.stageIndex,
        'threshold': this.iaThreshold
      }
      this.getImprovementAreas(this.studyData.id, this.payload)
    }
  }
}
</script>

<style>
  ul li {
    padding: 0px 0px;
  }
  .bg-white {
    background: white;
    border-radius: 4px;
    padding: 10px 20px;
  }
</style>
