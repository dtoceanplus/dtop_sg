// This is the Stage Gate module of the DTOceanPlus suite
// Copyright (C) 2021 Wave Energy Scotland - Ben Hudson
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
// License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.
var colorMap  = {
  customColorMethod(percentage) {
    if (percentage < 33) {
      return '#f56c6c';
    } else if (percentage < 66) {
      return '#909399';
    } else if (percentage < 100) {
      return '#e6a23c';
    } else {
      return '#5cb87a';
    }
  }
}

export default colorMap
