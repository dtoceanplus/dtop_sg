import defaultSettings from '@/settings'

const title = defaultSettings.title || 'SG – Stage Gate'

export default function getPageTitle(pageTitle) {
  if (pageTitle) {
    return `${pageTitle} - ${title}`
  }
  return `${title}`
}
