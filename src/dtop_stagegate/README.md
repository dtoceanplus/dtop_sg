This is `dtop_stagegate` package folder.

Python packages should contain `__init__.py` file.
Also it should not contain `-` in name. `_` is recommended.
