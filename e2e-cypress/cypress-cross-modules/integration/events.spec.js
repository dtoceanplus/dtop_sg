import STStudyPage from '../pages/st/study'

import SGEntityListPage from '../pages/sg/entity-list'
import SGEntityDetailPage from '../pages/sg/entity-detail'

describe('events', function () {
  it('create', () => {
    STStudyPage.openStudyPage()
  
    STStudyPage.createEntity('CT1')
    cy.wait(10000);

    cy.scrollTo(0,0)

    STStudyPage.execInIframe([
      SGEntityListPage.fillInput,
      SGEntityListPage.openSelect,
      SGEntityListPage.fillSelect,
      SGEntityListPage.submitCreateForm
    ])
    cy.wait(10000);
    STStudyPage.getEditButton('CT1')
    STStudyPage.getDeleteButton('CT1')
  })
  it('edit', () => {
    STStudyPage.editEntity('CT1')
    cy.wait(10000);

    STStudyPage.execInIframe([
      SGEntityDetailPage.getTitle,
      (iframe, element) => {
        element.invoke('text').should('equal', '1234')
      },
      SGEntityDetailPage.goToForm,
      (iframe, element) => {
        cy.wait(1000);
      },
      SGEntityDetailPage.submitForm
    ])
    cy.wait(10000);
  })
  it('delete', () => {
    cy.scrollTo(0,0)
    STStudyPage.deleteEntity('CT1')
    cy.wait(10000);

    cy.viewport(1000, 5000)
    STStudyPage.execInIframe([
      SGEntityListPage.submitDeleteForm
    ])
    cy.wait(10000);
    STStudyPage.getCreateButton('CT1')
  })
})
