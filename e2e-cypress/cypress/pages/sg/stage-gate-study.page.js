class StageGateStudyPage {
    static STUDY_TITLE = 'Study name'
    static STUDY_DESC = 'Study description'
    static STUDY_TITLE_EDIT = 'Study edited name'
    static STUDY_DESC_EDIT = 'Study edited description'

    configureRoute() {
        cy.server()
        cy.route('GET', '/api/stage-gate-studies').as('getStudies')
    }

    createStudy() {
        this.configureRoute();
        cy.visit('/#/stage-gate-studies/index')
        cy.get('[data-cy-sg=createSgStudyBtn]').click()
        cy.get('[data-cy-sg=input]').type(StageGateStudyPage.STUDY_TITLE)
        cy.get('[data-cy-sg=createStudyFormDesc]').type(StageGateStudyPage.STUDY_DESC)
        cy.get('[data-cy-sg=select]').click()
        cy.get('[data-cy-sg=selectItem1]').click()
        cy.get('[data-cy-sg=submitCreateButton]').click()
        cy.wait('@getStudies')
        cy.contains(StageGateStudyPage.STUDY_TITLE)
        cy.contains(StageGateStudyPage.STUDY_DESC)
    }

    editStudy() {
        this.configureRoute();
        cy.get('[data-cy-sg=editStudyBtn1]').click()
        cy.get('[data-cy-sg=editStudyFormName]').clear().type(StageGateStudyPage.STUDY_TITLE_EDIT)
        cy.get('[data-cy-sg=editStudyFormDesc]').clear().type(StageGateStudyPage.STUDY_DESC_EDIT)
        cy.get('[data-cy-sg=confirmUpdateStudyBtn]').click()
        cy.wait('@getStudies')
        cy.contains(StageGateStudyPage.STUDY_TITLE_EDIT)
        cy.contains(StageGateStudyPage.STUDY_DESC_EDIT)
    }

    deleteStudy(studyId) {
        this.configureRoute();
        cy.get(`[data-cy-sg=deleteStudyBtn${studyId}]`).click()
        cy.get('[data-cy-sg=submitDeleteButton]').click()
        cy.wait('@getStudies')
        cy.get('.el-table__row').should('not.contain', StageGateStudyPage.STUDY_TITLE_EDIT)
    }

}
export default StageGateStudyPage;
