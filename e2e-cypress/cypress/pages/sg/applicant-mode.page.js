class ApplicantModePage {

    configureRoutes() {
        cy.server()
        cy.route('GET', '/api/stage-gate-studies/*/stage-gate-assessments').as('getSgAssessments')
        cy.route('GET', '/api/stage-gate-assessments/*/applicant-answers').as('getApplicantAnswers')
        cy.route('GET', '/mm.dto.test/api/integration/modules/sg/entities/*').as('getEntityList')
    }

    navigateApplicantInput(studyId, stageGateId) {
        this.configureRoutes()
        cy.route('GET', '/api/stage-gate-studies/*').as('getSgStudy')
        cy.get(`[data-cy-sg=viewStudyBtn${studyId}]`).click()
        cy.wait('@getSgStudy')
        cy.get('[data-cy-sg=startApplicantModeBtn]').click()
        cy.wait('@getSgAssessments')
        cy.get(`[data-cy-sg=selectStageGateOutput${stageGateId}]`).should('be.disabled')
        cy.get(`[data-cy-sg=selectStageGateInput${stageGateId}]`).click()
        cy.wait('@getApplicantAnswers')
        cy.wait('@getEntityList')
    }

    testInputAnswers() {
        cy.contains('Technology').click()
        cy.contains('Degree of novelty and innovation (I)').click()
        cy.get('.el-form').find('.el-textarea').eq(0).type('response goes here')
        cy.contains("4%")
        cy.contains("14%")
        cy.get('[data-cy-sg=applicantNextBtn]').click()
        cy.contains('Installation duration')
        cy.get('td').eq(0).click()
        cy.contains('Result')
        cy.get('.el-form').find('.el-input-number__increase').eq(0).click()
        cy.get('[data-cy-sg=applicantBackBtn]').click()
        cy.contains('Technology').click()
        cy.get('[data-cy-sg=saveApplicantInputs]').click()
    }

    testExtractMetrics() {
        this.configureRoutes()
        cy.get('[data-cy-sg=applicantNextBtn]').click()
        cy.route('PUT', '/api/stage-gate-assessments/*/applicant-answers-integrated').as('getApplicantAnswersIntegrated')
        cy.contains('Use results from Deployment and Assessment modules').click()
        cy.wait('@getApplicantAnswersIntegrated')
        cy.wait('@getApplicantAnswers')
        cy.contains('6588.1')
        cy.contains('19188188.02')
        cy.contains('0.987')
        cy.contains('7654.3')
    }

    submitApplicantInputs() {
        cy.route('GET', '/api/stage-gate-assessments/*/applicant-results').as('getApplicantResults')
        cy.contains('Submit').click()
        cy.wait('@getApplicantResults')
    }

    testApplicantOutputs() {
        this.configureRoutes()
        this.submitApplicantInputs()
        cy.get('.el-progress__text').eq(0).contains('4%')
        cy.get('.el-table__row').eq(0).contains('Annual captured energy')
        cy.contains('Technology').click()
        cy.contains('Degree of novelty and innovation (I)').click()
        cy.contains('Weighting (%): 9')
        cy.contains('Go Back').click()
        cy.wait('@getSgAssessments')
        cy.get('[data-cy-sg=selectStageGateOutput2]').click()
        cy.wait('@getApplicantResults')
        cy.scrollTo('bottom')
    }

}
export default ApplicantModePage;
