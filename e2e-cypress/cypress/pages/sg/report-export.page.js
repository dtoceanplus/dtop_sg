class ReportExportPage {

    selectStudy() {
        cy.server()
        cy.route('GET', '/api/stage-gate-studies/*').as('getStudy')
        cy.get('[data-cy-sg=viewStudyBtn1]').click()
        cy.wait('@getStudy')
    }

    navigateReportExportPage() {
        this.selectStudy();
        cy.route('GET', '/api/stage-gate-studies/*/stage-gate-assessments').as('getSgAssessments')
        cy.get('[data-cy-sg=openReportHome]').click()
        cy.wait('@getSgAssessments')
    }

    checkDisabledButtons() {
        cy.get(`[data-cy-sg=generateReportBtn]`).should('be.disabled')
        cy.get(`[data-cy-sg=selectStage]`).should('not.be.visible')
        cy.get(`[data-cy-sg=selectStageGate]`).should('not.be.visible')
        cy.get('.el-checkbox').eq(0).find(':checkbox').should('not.be.disabled')
        cy.get('.el-checkbox').eq(12).find(':checkbox').should('not.be.disabled')
        cy.get('.el-checkbox').eq(4).find(':checkbox').should('be.disabled')
        cy.get('.el-checkbox').eq(8).find(':checkbox').should('be.disabled')
    }

    testActivityChecklist() {    
        cy.get('.el-checkbox').eq(0).click()
        cy.get(`[data-cy-sg=selectStage]`).should('be.visible')
        cy.get(`[data-cy-sg=generateReportBtn]`).should('be.disabled')
        cy.get('.el-checkbox').eq(2).click()
        cy.get(`[data-cy-sg=selectStage]`).should('be.visible')
        cy.get('.el-checkbox').eq(3).click()
        cy.get(`[data-cy-sg=selectStage]`).should('not.be.visible')
        cy.get(`[data-cy-sg=generateReportBtn]`).should('not.be.disabled')
    }

    generateReport() {
        cy.server()
        cy.route('POST', '/api/stage-gate-studies/*/report-data').as('getReportData')
        cy.get(`[data-cy-sg=generateReportBtn]`).click()
        cy.wait('@getReportData')
        cy.get('.el-message__content').contains('Report was compiled successfully')
    }

    testStageSelect() {
        cy.get('.el-checkbox').eq(2).click()
        cy.get(`[data-cy-sg=selectStage]`).should('be.visible')
        cy.get(`[data-cy-sg=selectStage]`).click()
        cy.get(`[data-cy-sg=stageIndex1]`).click()
    }
    
    testImprovementAreas() {
        cy.get('.el-checkbox').eq(0).click()
        cy.get('.el-checkbox').eq(0).click()
        cy.get(`[data-cy-sg=selectStage]`).should('not.be.visible')
        cy.get(`[data-cy-sg=generateReportBtn]`).should('be.disabled')
        cy.get('.el-checkbox').eq(12).click()
        cy.get(`[data-cy-sg=generateReportBtn]`).should('not.be.disabled')
        cy.get(`[data-cy-sg=selectStage]`).should('be.visible')
    }

    testApplicantMode() {
        cy.get(`[data-cy-sg=selectStage]`).should('not.be.visible')
        cy.get(`[data-cy-sg=selectStageGate]`).should('not.be.visible')
        cy.get(`[data-cy-sg=generateReportBtn]`).should('be.disabled')
        cy.get('.el-checkbox').eq(4).find(':checkbox').should('not.be.disabled')
        cy.get('.el-checkbox').eq(8).find(':checkbox').should('be.disabled')
        cy.get('.el-checkbox').eq(4).click()
        cy.get(`[data-cy-sg=selectStageGate]`).should('be.visible')
    }

    testStageGateSelect() {
        cy.get(`[data-cy-sg=selectStageGate]`).click()
        cy.get(`[data-cy-sg=stageGateIndex1]`).click()
    }

    testAssessorMode() {
        cy.get(`[data-cy-sg=selectStage]`).should('not.be.visible')
        cy.get(`[data-cy-sg=selectStageGate]`).should('not.be.visible')
        cy.get(`[data-cy-sg=generateReportBtn]`).should('be.disabled')
        cy.get('.el-checkbox').eq(4).find(':checkbox').should('not.be.disabled')
        cy.get('.el-checkbox').eq(8).find(':checkbox').should('not.be.disabled')
        cy.get('.el-checkbox').eq(8).click()
        cy.get(`[data-cy-sg=selectStageGate]`).should('be.visible')
    }
}

export default ReportExportPage;
