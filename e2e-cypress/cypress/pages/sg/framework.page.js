class FrameworkPage {
    static FRAMEWORK_TITLE = 'Framework name'
    static FRAMEWORK_DESC = 'Framework description'
    static FRAMEWORK_TITLE_EDIT = 'Framework edited name'
    static FRAMEWORK_TITLE_DESC = 'Framework edited description'

    createFramework() {
        cy.server()
        cy.route('GET', '/api/frameworks/').as('getFrameworks')
        cy.visit('/#/frameworks/index')
        cy.get('[data-cy-sg=createFrameworkBtn]').click()
        cy.get('[data-cy-sg=frameworkFormName]').type(FrameworkPage.FRAMEWORK_TITLE)
        cy.get('[data-cy-sg=frameworkFormDesc]').type(FrameworkPage.FRAMEWORK_DESC)
        cy.get('[data-cy-sg=frameworkFormCreateBtn]').click()
        cy.wait('@getFrameworks')
        cy.contains(FrameworkPage.FRAMEWORK_TITLE)
        cy.contains(FrameworkPage.FRAMEWORK_DESC)
    }

    editFramework() {
        cy.get('[data-cy-sg=editFrameworkBtn2]').click()
        cy.get('[data-cy-sg=editFrameworkFormName]').clear().type(FrameworkPage.FRAMEWORK_TITLE_EDIT)
        cy.get('[data-cy-sg=editFrameworkFormDesc]').clear().type(FrameworkPage.FRAMEWORK_TITLE_DESC)
        cy.get('[data-cy-sg=editFrameworkFormUpdateBtn]').click()
        cy.wait('@getFrameworks')
        cy.contains(FrameworkPage.FRAMEWORK_TITLE_EDIT)
        cy.contains(FrameworkPage.FRAMEWORK_TITLE_DESC)
    }

    frameworkExplorer() {
        cy.server()
        cy.route('GET', '/api/frameworks/*').as('getFramework')
        cy.route('GET', '/api/stages/*').as('getStage')
        cy.route('GET', '/api/stages/*/eval').as('getStageEval')
        cy.route('GET', '/api/stage-gates/*').as('getStageGate')
        // cy.get('.hamburger').click()
        cy.contains('Framework Explorer').click()
        cy.wait('@getFramework')
        cy.contains('Stage 1').click()
        cy.wait('@getStage')
        cy.get('.el-switch__core').click()
        cy.wait('@getStageEval')
        cy.contains('Evaluation Area').parent().should('have.class', 'is-active');
        cy.contains('0 - 1').click()
        cy.wait('@getStageGate')
        cy.contains('Qualitative Questions').click()
        cy.contains('Technology').click()
        cy.contains('Scientific credibility').click()
        cy.contains('underlying physical and scientific principles')
        cy.contains('1 - 2').click()
        cy.wait('@getStageGate')
        cy.contains('Quantitative Questions').click()
        cy.contains('Annual captured energy')
        cy.visit('/#/frameworks/index')
    }

    editMetricThreshold() {
        cy.server()
        cy.route('GET', '/api/frameworks/*').as('getFramework')
        cy.route('GET', '/api/stage-gates/*').as('getStageGate')
        cy.route('PUT', '/api/question-metrics').as('putQuestionMetrics')
        cy.get('[data-cy-sg=viewFrameworkBtn2]').scrollIntoView().should('be.visible').click()
        cy.wait('@getFramework')
        cy.contains('Stage Gate 1 - 2').click()
        cy.wait('@getStageGate')
        cy.contains('Annual captured energy')
        cy.get('[data-cy-sg=thresholdCheckbox97]').eq(0).click()
        cy.get('.el-input-number__increase').eq(0).click()
        cy.get('.el-input__inner').eq(0).should('have.value', '1')
        cy.get('.el-input-number__decrease').eq(0).click()
        cy.get('.el-input__inner').eq(0).should('have.value', '0')
        cy.get('.el-input-number').eq(0).should('be.visible')
        cy.get('.el-input__inner').eq(0).clear().type(20)
        cy.get('[data-cy-sg="saveThresholdsBtn"]').eq(0).click()
        cy.wait('@putQuestionMetrics') 
        cy.contains('Stage Gate 2 - 3').click()
        cy.wait('@getStageGate')
        cy.get('[data-cy-sg="saveThresholdsBtn"]').eq(1).click()
        cy.wait('@putQuestionMetrics') 
    }

    deleteFramework() {
        cy.visit('/#/frameworks/index')
        cy.get('[data-cy-sg=deleteFrameworkBtn2]').click()
        cy.get('[data-cy-sg=confirmDeleteFrameworkBtn]').click()
        cy.get('.el-table__row').should('not.contain', FrameworkPage.FRAMEWORK_TITLE_EDIT)
    }

}
export default FrameworkPage;

