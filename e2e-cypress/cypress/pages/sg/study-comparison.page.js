class StudyComparisonPage {

    prepareApplicantAnswers(applicantPage, studyId, stageGateId) {
        cy.visit('/#/stage-gate-studies/index')
        applicantPage.navigateApplicantInput(studyId, stageGateId);
        applicantPage.submitApplicantInputs();
    }

    prepareAssessorAnswers(assessorPage, studyId, stageGateId) {
        cy.visit('/#/stage-gate-studies/index')
        assessorPage.navigateAssessorInput(studyId, stageGateId);
        assessorPage.submitAssessorInputs();
    }

    navigateComparisonHome() {
        cy.visit('/#/stage-gate-studies/index')
        cy.server()
        cy.route('GET', '/api/frameworks/').as('getFrameworks')
        cy.route('GET', '/api/stage-gate-studies').as('getStudies')
        cy.get('[data-cy-sg=compareSgStudyBtn]').click()
        cy.wait('@getFrameworks')
        cy.wait('@getStudies')
    }

    testComparisonInputs() {
        cy.get('[data-cy-sg=selectFramework]').click()
        cy.get('[data-cy-sg=selectItem1]').click()
        cy.get('[data-cy-sg=comparisonStudy1]').click()
        cy.get('[data-cy-sg=openComparisonHomePageBtn]').should('be.disabled')
        cy.get('[data-cy-sg=comparisonStudy2]').click()
        cy.get('[data-cy-sg=openComparisonHomePageBtn]').should('not.be.disabled').click()
        cy.route('GET', '/api/stage-gate-studies/checklist-comparison?id=1&id=2').as('getChecklistComparison')
        cy.get('[data-cy-sg=openComparisonHomePageBtn]').click()
        cy.wait('@getChecklistComparison')
    }

    testComparisonOutputs() {
        cy.contains('Study name')
        cy.contains('Study edited name')
        cy.get('#tab-checklist').click()
        cy.contains('0%')
        cy.get('[data-cy-sg=selectStage]').click()
        cy.get('[data-cy-sg=selectStage1]').click()
        cy.contains('Environmental and social impacts')
        cy.contains('Power Conversion')
        cy.get('#tab-applicant').click()
        cy.get('[data-cy-sg=selectStageGateApp]').click()
        cy.route('GET', '/api/stage-gate-assessments/applicant-comparison?id=2&id=7').as('getApplicantComparison')
        cy.get('[data-cy-sg=selectStageGateApp1]').click()
        cy.wait('@getApplicantComparison')
        cy.contains(0)
        cy.contains("Annual captured energy")
        cy.get('#tab-assessor').click()
        cy.get('[data-cy-sg=selectStageGateAss]').click()
        cy.route('GET', '/api/stage-gate-assessments/assessor-comparison?id=2&id=7').as('getAssessorComparison')
        cy.get('[data-cy-sg=selectStageGateAss1]').click()
        cy.wait('@getAssessorComparison')
        cy.contains("Average")
        cy.contains("Weighted average")
        cy.contains('Environmental and social impacts')
        cy.contains('Power Conversion')
    }

}
export default StudyComparisonPage;
