class ImprovementAreaPage {

    selectStudy() {
        cy.server()
        cy.route('GET', '/api/stage-gate-studies/*').as('getStudy')
        cy.get('[data-cy-sg=viewStudyBtn1]').click()
        cy.wait('@getStudy')
    }

    checkButtons() {
        this.selectStudy()
        cy.get('[data-cy-sg=openImprovementAreas]').should('be.disabled')
    }

    navigateImprovementAreas() {
        cy.server()
        cy.route('POST', '/api/stage-gate-studies/*/improvement-areas').as('getImprovementAreas')
        this.selectStudy();
        cy.get('[data-cy-sg=openImprovementAreas]').click()
        cy.wait('@getImprovementAreas')
    }

    testImprovementAreasPage() {
        cy.contains('Improvement Areas')
        cy.contains('Please select the Stage')
        cy.contains('Stage 1')
        cy.contains('Acceptability')
        cy.contains('Installability')
        cy.get('[data-cy-sg=selectStage]').click()
        cy.get('[data-cy-sg=selectItem2]').click()
        cy.wait('@getImprovementAreas')
        cy.contains('Stage 2')
        cy.get('.el-input-number__increase').eq(0).click()
        cy.get('[data-cy-sg=updateIaThreshold]').click()
        cy.wait('@getImprovementAreas')
    }

}

export default ImprovementAreaPage;
