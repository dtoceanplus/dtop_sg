class ActivityChecklistPage {

    configureRoutes() {
        cy.server()
        cy.route('GET', '/api/stage-gate-studies/*').as('getStudy')
    }

    checkButtons() {
        this.configureRoutes()
        cy.get('[data-cy-sg=viewStudyBtn1]').click()
        cy.wait('@getStudy')
        cy.get('[data-cy-sg=performLink]').should('not.be.disabled')
        cy.get('[data-cy-sg=checklistResults]').should('be.disabled')
    }

    saveChecklistInput() {
        cy.route('GET', '/api/stages/*').as('getStage')
        cy.route('GET', '/api/stage-gate-studies/*/checklist-inputs').as('getChecklistInputs')
        cy.get('[data-cy-sg=performLink]').click()
        cy.wait(['@getStage', '@getChecklistInputs'])
        cy.contains('Requirements').click()
        cy.contains('Requirement definition (Affordability)')
        cy.get('[data-cy-sg=checklistTable]')
        cy.get('td').eq(0).click()
        cy.contains('Definition of technology and market requirements and challenges associated with Affordability (the problem statement)')
        cy.get('[data-cy-sg=checklistCheckbox1').click()
        cy.contains("4%")
        cy.get('[data-cy-sg=markStageComplete]').click()
        cy.contains("100%")
        cy.get('[data-cy-sg=getSecondStage]').click()
        cy.wait('@getStage')
        cy.contains("Concept characterisation")
        cy.get('[data-cy-sg=saveButton]').click()
    }

    submitChecklistInputs() {
        this.configureRoutes()
        cy.route('GET', '/api/stage-gate-studies/*/checklist-outputs').as('getChecklistOutputs')
        cy.route('GET', '/api/stage-gate-studies/*/complexity-levels').as('getComplexityLevels')
        cy.get('[data-cy-sg=submitButton]').click()
        cy.wait('@getChecklistOutputs')
        cy.wait('@getComplexityLevels')
    }

    testChecklistOutputs() {
        this.submitChecklistInputs();
        cy.get('.el-progress__text').eq(0).contains('100%')
        cy.contains('Go Back').click()
        cy.wait('@getStudy')
        cy.contains('View Results').click()
        cy.wait('@getChecklistOutputs')
        cy.wait('@getComplexityLevels')
        cy.contains("Stage 0")
        cy.contains("Stage Gate 0 - 1")
        cy.contains("Site Characterisation")
        cy.contains('Stage 2').click()
        cy.contains('Stage 2')
        cy.contains('Activity Categories')
        cy.contains('Evaluation Areas')
        cy.contains('Outstanding Activities')
        cy.scrollTo('bottom')
    }

}
export default ActivityChecklistPage;
