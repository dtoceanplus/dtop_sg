export const open = () => {
  cy.visit(`http://module.${Cypress.env('root_domain')}/`);
};
 
export default {
  open
};