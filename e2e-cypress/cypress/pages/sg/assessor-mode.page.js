class AssessorModePage {

    configureRoutes() {
        cy.server()
        cy.route('GET', '/api/stage-gate-studies/*/stage-gate-assessments').as('getSgAssessments')
    }

    navigateAssessorHome(studyId) {
        this.configureRoutes()
        cy.route('GET', '/api/stage-gate-studies/*').as('getSgStudy')
        cy.get(`[data-cy-sg=viewStudyBtn${studyId}]`).click()
        cy.wait('@getSgStudy')
        cy.get('[data-cy-sg=startAssessorModeBtn]').click()
        cy.wait('@getSgAssessments')
    }

    testDisabledButtons() {
        this.navigateAssessorHome('1');
        cy.get('[data-cy-sg=selectStageGateInput2]').should('be.disabled')
        cy.get('[data-cy-sg=selectStageGateOutput2]').should('be.disabled')
    }

    navigateAssessorInput(studyId, stageGateId) {
        cy.server()
        cy.route('GET', '/api/stage-gate-assessments/*/assessor-scores').as('getAssessorScores')
        this.navigateAssessorHome(studyId);
        cy.get(`[data-cy-sg=selectStageGateOutput${stageGateId}]`).should('be.disabled')
        cy.get(`[data-cy-sg=selectStageGateInput${stageGateId}]`).click()
        cy.wait('@getAssessorScores')
    }

    testAssessorInput() {
        cy.contains('Technology').click()
        cy.contains('Degree of novelty and innovation (I)').click()
        cy.get('[data-cy-sg=formAssessorScore20]').click()
        cy.get('[data-cy-sg=assessorScoreOption20-3]').click()
        cy.contains('4%')
        cy.contains('14%')
        cy.contains('0%')
        cy.contains('Technology readiness').click()
        cy.get('[data-cy-sg=formAssessorComment37]').type('this is a comment')
        cy.get('[data-cy-sg=assessorNextBtn]').click()
        cy.contains('Annual captured energy')
        cy.get('td').eq(0).click()
        cy.get('[data-cy-sg=formAssessorScore25]').click()
        cy.get('[data-cy-sg=assessorScoreOption25-1]').click()
        cy.contains('7%')
        cy.contains('14%')
        cy.contains('5%')
        cy.get('[data-cy-sg=assessorBackBtn]').click()
        cy.contains('Technology').click()
        cy.contains('Save').click()
    }

    submitAssessorInputs() {
        this.configureRoutes()
        cy.route('GET', '/api/stage-gate-assessments/*/assessor-results').as('getAssessorResults')
        cy.contains('Submit').click()
        cy.get('[data-cy-sg=confirmAutofillScoreBtn]').click()
        cy.wait('@getAssessorResults')
    }

    testAssessorOutput() {
        this.submitAssessorInputs();
        cy.get('.js-plotly-plot').should(($p) => {
            // should have found 3 elements
            expect($p).to.have.length(3)
        })
        cy.get('[data-cy-sg=assessorScoreComments]')
        cy.get('.el-tabs').contains('Technology').click()
        cy.contains('Degree of novelty and innovation (I)').click()
        cy.contains('Annual captured energy')
        cy.get('td').eq(0).click()
        cy.contains('Result: 1')
        cy.contains('Go Back').click()
        cy.wait('@getSgAssessments')
        cy.get('[data-cy-sg=selectStageGateOutput2]').click()
        cy.wait('@getAssessorResults')
        cy.scrollTo('bottom')
    }

}
export default AssessorModePage;
