export const getTitle = iframe => {
  return cy.wrap(iframe).find("[data-cy-sg=title]");
};
export const goToForm = iframe => {
  cy.wrap(iframe).find("[data-cy-sg=performLink]").click()
};
export const submitForm = iframe => {
  cy.wrap(iframe).find("[data-cy-sg=submitButton]").click()
};


export default {
  getTitle,
  goToForm,
  submitForm
};
