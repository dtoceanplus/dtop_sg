import ActivityChecklistPage from '../pages/sg/activity-checklist.page';
import StageGateStudyPage from '../pages/sg/stage-gate-study.page';
describe("Activity Checklist", () => {

    const checklist = new ActivityChecklistPage;
    const study = new StageGateStudyPage;

    before(() => {
        study.createStudy()
    })

    after(() => {
        cy.visit('/#/stage-gate-studies/index')
        study.deleteStudy('1')
    })

    it('check Activity Checklist input page', () => {
        checklist.checkButtons()
        checklist.saveChecklistInput()
    })
      
    it('check Activity Checklist output page', () => {
        checklist.testChecklistOutputs()
    })
})
