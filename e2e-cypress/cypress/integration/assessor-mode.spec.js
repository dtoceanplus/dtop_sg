import ApplicantModePage from '../pages/sg/applicant-mode.page';
import AssessorModePage from '../pages/sg/assessor-mode.page';
import StageGateStudyPage from '../pages/sg/stage-gate-study.page';
describe("Assessor Mode", () => {

    const applicant = new ApplicantModePage;
    const assessor = new AssessorModePage;
    const study = new StageGateStudyPage;

    before(() => {
        study.createStudy()
    })

    after(() => {
        cy.visit('/#/stage-gate-studies/index')
        study.deleteStudy('1')
    })

    it('check Assessor Mode buttons disabled', () => {
        cy.visit('/#/stage-gate-studies/index')
        assessor.testDisabledButtons();
    })

    it('submit Applicant Mode answers to test Assessor Mode', () => {
        cy.visit('/#/stage-gate-studies/index');
        applicant.navigateApplicantInput('1', '2');
        applicant.testInputAnswers();
        applicant.submitApplicantInputs();
    })

    it('check Assessor Mode input page', () => {
        cy.visit('/#/stage-gate-studies/index')
        assessor.navigateAssessorInput('1', '2');
        assessor.testAssessorInput();
    })

    it('check Assessor Mode output page', () => {
        assessor.testAssessorOutput()
    })
})
