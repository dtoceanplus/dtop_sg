import ImprovementAreaPage from '../pages/sg/improvement-areas.page';
import ActivityChecklistPage from '../pages/sg/activity-checklist.page';
import StageGateStudyPage from '../pages/sg/stage-gate-study.page';
describe("Improvement Areas", () => {

    const improvementArea = new ImprovementAreaPage;
    const checklist = new ActivityChecklistPage;
    const study = new StageGateStudyPage;

    before(() => {
        study.createStudy()
    })

    after(() => {
        cy.visit('/#/stage-gate-studies/index')
        study.deleteStudy('1')
    })

    it('check Improvement area button disabled', () => {
        improvementArea.checkButtons()
    })

    it('submit Activity Checklist inputs', () => {
        cy.visit('/#/stage-gate-studies/index')
        improvementArea.selectStudy();
        checklist.saveChecklistInput();
        checklist.submitChecklistInputs();
    })
      
    it('check Improvement Areas page', () => {
        cy.visit('/#/stage-gate-studies/index')
        improvementArea.navigateImprovementAreas();
        improvementArea.testImprovementAreasPage();
    })
})
