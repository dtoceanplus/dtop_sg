import ApplicantModePage from '../pages/sg/applicant-mode.page';
import StageGateStudyPage from '../pages/sg/stage-gate-study.page';
describe("Applicant Mode", () => {

    const applicant = new ApplicantModePage;
    const study = new StageGateStudyPage;

    before(() => {
        study.createStudy()
    })

    after(() => {
        cy.visit('/#/stage-gate-studies/index')
        study.deleteStudy('1')
    })

    it('check Applicant Mode input page', () => {
        applicant.navigateApplicantInput('1', '2');
        applicant.testInputAnswers();
        applicant.testExtractMetrics();
    })

    it('check Applicant Mode output page', () => {
        applicant.testApplicantOutputs()
    })
})
