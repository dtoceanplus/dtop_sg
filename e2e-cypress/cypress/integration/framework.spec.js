import FrameworkPage from '../pages/sg/framework.page';
describe("Framework", () => {

    const framework = new FrameworkPage();

    before(() => {
      framework.createFramework()
    })

    after(() =>{
      framework.deleteFramework()
    })

    it('Edit Framework', () => {
      framework.editFramework()
    })

    it('View Framework', () => {
      framework.frameworkExplorer()
    })

    it('Edit a metric threshold', () => {
      framework.editMetricThreshold()
    })

})
