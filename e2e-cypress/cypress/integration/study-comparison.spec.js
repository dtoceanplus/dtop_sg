import ApplicantModePage from '../pages/sg/applicant-mode.page';
import AssessorModePage from '../pages/sg/assessor-mode.page';
import StageGateStudyPage from '../pages/sg/stage-gate-study.page';
import StudyComparisonPage from '../pages/sg/study-comparison.page';
describe("Study Comparison", () => {

    const applicant = new ApplicantModePage;
    const assessor = new AssessorModePage;
    const study = new StageGateStudyPage;
    const comparison = new StudyComparisonPage;

    before(() => {
        study.createStudy()
        study.editStudy()
        study.createStudy()
    })

    after(() => {
        cy.visit('/#/stage-gate-studies/index')
        study.deleteStudy('1')
        study.deleteStudy('2')
        cy.get('.el-table__row').should('not.contain', 'Study name')
    })

    it('prepare for Study Comparison', () => {
        // submit applicant answers for 2x studies
        comparison.prepareApplicantAnswers(applicant, '1', '2')
        comparison.prepareApplicantAnswers(applicant, '2', '7')
        // submit assessor answers for 2x studies
        comparison.prepareAssessorAnswers(assessor, '1', '2')
        comparison.prepareAssessorAnswers(assessor, '2', '7')
    })

    Cypress.on('uncaught:exception', (err) => {
        if (err.message.includes('ResizeObserver')) {
          // returning false here prevents Cypress from
          // failing the test
          return false;
        }
        return true;
      });

    it('Compare two Stage Gate studies', () => {
        comparison.navigateComparisonHome();
        comparison.testComparisonInputs();
        comparison.testComparisonOutputs();
    })

})
