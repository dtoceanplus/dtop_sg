import ActivityChecklistPage from '../pages/sg/activity-checklist.page';
import ApplicantModePage from '../pages/sg/applicant-mode.page';
import AssessorModePage from '../pages/sg/assessor-mode.page';
import ReportExportPage from '../pages/sg/report-export.page';
import StageGateStudyPage from '../pages/sg/stage-gate-study.page';
describe("Report Export", () => {

    const reportExport = new ReportExportPage;
    const checklist = new ActivityChecklistPage;
    const study = new StageGateStudyPage;
    const applicantMode = new ApplicantModePage;
    const assessorMode = new AssessorModePage;

    before(() => {
        study.createStudy()
    })

    after(() => {
        cy.visit('/#/stage-gate-studies/index')
        study.deleteStudy('1')
    })

    it('submit Activity Checklist inputs', () => {
        cy.visit('/#/stage-gate-studies/index')
        reportExport.selectStudy();
        checklist.saveChecklistInput();
        checklist.submitChecklistInputs();
    })
      
    it('check Report Export default page', () => {
        cy.visit('/#/stage-gate-studies/index')
        reportExport.navigateReportExportPage();
        reportExport.checkDisabledButtons();
    })

    it('check Activity Checklist permutations', () => {
        reportExport.testActivityChecklist();
        reportExport.generateReport();
        reportExport.testStageSelect();
        reportExport.generateReport();
    })

    it('check Improvement Area permutations', () => {
        reportExport.testImprovementAreas();
        reportExport.generateReport();
    })

    it('check Applicant Mode permutations', () => {
        cy.visit('/#/stage-gate-studies/index')
        applicantMode.navigateApplicantInput('1', '2');
        applicantMode.testInputAnswers();
        applicantMode.submitApplicantInputs();
        cy.visit('/#/stage-gate-studies/index')
        reportExport.navigateReportExportPage();
        reportExport.testApplicantMode();
        reportExport.testStageGateSelect();
        reportExport.generateReport();
    })

    it('check Assessor Mode permutations', () => {
        cy.visit('/#/stage-gate-studies/index')
        assessorMode.navigateAssessorInput('1', '2');
        assessorMode.testAssessorInput();
        assessorMode.submitAssessorInputs();
        cy.visit('/#/stage-gate-studies/index')
        reportExport.navigateReportExportPage();
        reportExport.testAssessorMode();
        reportExport.testStageGateSelect();
        reportExport.generateReport();
    })
})
