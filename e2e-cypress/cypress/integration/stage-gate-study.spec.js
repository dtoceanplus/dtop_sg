import StageGateStudyPage from '../pages/sg/stage-gate-study.page';
describe("Stage Gate study", () => {

    const study = new StageGateStudyPage;

    it('Create Stage Gate study', () => {
        study.createStudy()
    })

    it('Edit Stage Gate study', () => {
        study.editStudy()
    })

    it('Delete Stage Gate study', () => {
        study.deleteStudy('1')
    })
})
