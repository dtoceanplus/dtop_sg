import SGMain from '../pages/sg/main';

describe('included auth', () => {
  before(() => {
    Cypress.Cookies.defaults({ whitelist: 'accessToken' });
  });

  it('auth', () => {
    SGMain.open()
    cy.wait(10000);
    cy.get('[data-cy-sg=authModulePlacement] iframe').then(($iframe) => {
      const iframeBody = $iframe.contents().find('body');
      cy.wrap(iframeBody).find('[data-cy-auth=loginInput]').type('admin@dtop.com');
      cy.wrap(iframeBody).find('[data-cy-auth=passwordInput]').type('j2zjf#afw21');
      cy.wrap(iframeBody).find('[data-cy-auth=formSubmitButton]').click();
    });
    cy.wait(10000);
    cy.get('[data-cy-sg=authModulePlacement] iframe').should('not.exist');
  });

  it('logout', () => {
    SGMain.open();
    cy.wait(10000);
    cy.get('[data-cy-sg=userPanelModulePlacement] iframe').then(($iframe) => {
      const iframeBody = $iframe.contents().find('body');
      cy.wrap(iframeBody).find('[data-cy-auth=logoutButton]').click();
    });
    cy.get('[data-cy-sg=userPanelModulePlacement] iframe').should('not.exist');
    cy.wait(10000);
  });
  
  it('a few actions during one session', () => {
    SGMain.open();
    cy.wait(10000);
    cy.get('[data-cy-sg=authModulePlacement] iframe').then(($iframe) => {
      const iframeBody = $iframe.contents().find('body');
      cy.wrap(iframeBody).find('[data-cy-auth=loginInput]').type('admin@dtop.com');
      cy.wrap(iframeBody).find('[data-cy-auth=passwordInput]').type('j2zjf#afw21');
      cy.wrap(iframeBody).find('[data-cy-auth=formSubmitButton]').click();
    });
    cy.wait(10000);
    cy.get('[data-cy-sg=authModulePlacement] iframe').should('not.exist');
    cy.get('[data-cy-sg=userPanelModulePlacement] iframe').then(($iframe) => {
      const iframeBody = $iframe.contents().find('body');
      cy.wrap(iframeBody).find('[data-cy-auth=logoutButton]').click();
    });
    cy.wait(10000);
    cy.get('[data-cy-sg=userPanelModulePlacement] iframe').should('not.exist');
    cy.get('[data-cy-sg=authModulePlacement] iframe').then(($iframe) => {
      const iframeBody = $iframe.contents().find('body');
      cy.wrap(iframeBody).find('[data-cy-auth=loginInput]').type('admin@dtop.com');
      cy.wrap(iframeBody).find('[data-cy-auth=passwordInput]').type('j2zjf#afw21');
      cy.wrap(iframeBody).find('[data-cy-auth=formSubmitButton]').click();
    });
    cy.wait(10000);
    cy.get('[data-cy-sg=authModulePlacement] iframe').should('not.exist');
    cy.get('[data-cy-sg=userPanelModulePlacement] iframe').then(($iframe) => {
      const iframeBody = $iframe.contents().find('body');
      cy.wrap(iframeBody).find('[data-cy-auth=logoutButton]').click();
    });
  });
});
