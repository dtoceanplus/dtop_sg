# Overview

The Stage Gate (SG) module is a module of the DTOceanPlus application. 
The aim of this tool is to guide the technology development process and facilitate the assessment of ocean energy technologies.

The Stage Gate module has seven major functionalities:

- Stage Gate Framework: functionality for viewing the Stage Activity and Stage Gate Question data of the Stage Gate Framework developed for DTOceanPlus. 
This functionality also enables the user to edit the Stage Gate Framework by specifying the metric thresholds that are applied.

- Activity Checklist: allows the user to work through the required activities for each stage of the Stage Gate programme in turn and record whether they have been completed or not. 
This enables SG to identify the specific stage that a device or technology has reached.

- Applicant Mode: the first component of the Stage Gate assessment functionality. 
Presents to the user a set of qualitative and quantitative questions about their technology that they must answer. 
Emulates the application process at the stage gate of a typical technology development programme, from the point of view of the Applicant.

- Assessor Mode: the second component of the Stage Gate assessment functionality. 
Presents to the user the answers supplied by an Applicant in a previous Applicant Mode assessment and requests Assessor scores and comments.
Emulates the assessment process of a Stage Gate of a typical technology development programme, from the point of view of the Assessor.

- Improvement Areas: the methodology for identifying the improvement areas highlighted by a Stage Gate analysis. 
These refer to the characteristics of a device or technology that the SG module has identified as needing further development or refinement.

- Report Export: generates a standardised report in PDF format that summarise all the key information associated with a Stage Gate analysis.

- Study Comparison: compares the results of two or more Stage Gate Assessments that have been performed by the user.

Stage Gate is distributed under the AGPL license see LICENSE file for more details. 

Several images have been reproduced with permission of the International Energy Agency - Ocean Energy Systems. 
See the original report *International Energy Agency - Ocean Energy Systems (2021) An International Evaluation and Guidance Framework for Ocean Energy Technology*, available at https://www.ocean-energy-systems.org/publications/oes-documents/. 


# Running the module locally for development

Follow the instructions below to get a copy of the Stage Gate tool running on your local machine for development.
The recommended method is to use `docker` and `make` but note that this requires `make` and `docker` to be installed. 

## Running using make and Docker

The easiest way to run the Stage Gate module locally is to use Docker. 
You can run the backend and front-end together by simply running: 

```
make dev-local
```

This should first build the backend, client, Mountebank mocks and nginx images required to run locally. 
If it does not then you can try running these commands before running `dev-local` once more:

```
make build-backend
make build-client
make build-e2e-cypress
```

Once `make dev-local` has been run, the front-end is available at `http://localhost:80/`.
The front-end and back-end should be connected. 
You can test this by creating a test Stage Gate Study and the study should appear in the Stage Gate Studies index page. 

## Running in separate terminals

Alternatively, the module can be run using a separate terminal for the front-end and back-end.

First, create and activate the required `conda` environment for the local Windows environment described by `dev.dependency.yml`:

```bash
conda env create --name dtop-sg-win --file dev.dependency.yml
conda activate dtop-sg-win
```

Then install for local development:

```bash
pip install -e .
```

And initialise the local database:

```bash
flask init-db
```

The default Stage Gate framework also needs to be installed and addedd to the database. Run the following command to do so:
```bash
flask install-db
```

To run the `server` side, enter the command:
```bash
flask run
```

To run the `client` side, use the following commands:
```bash
cd src/dtop_stagegate/gui
npm run dev
```
This will launch the `frontend` automatically.
In this case, the frontend will be available at `http://localhost:8080/`.

# Testing

The makefile-based continuous integration (MBCI) approach has been adopted for the Stage Gate tool. 
All tests can be run using simple `make` commands. 
(Again note that `make` is a prerequisite)

For all tests except for the front-end (FE) tests, there are two versions of the command that can be issued: 

* TEST and 
* TEST-nodeps. 

The TEST version builds the relevant images before executing the tests. 
The TEST-nodeps version does not execute the build before running the tests. 

These commands work for the Python back-end tests, DREDD tests and Cypress E2E tests:

```bash
# Python unit tests
make pytest  # Re-build image(s)
make pytest-nodeps  # No re-build

# DREDD tests
make dredd  # Re-build image
make dredd-nodeps  # No re-build

# Cypress
make e2e-cypress  # Re-build image(s)
make e2e-cypress-nodeps  # No re-build
```

For the front-end tests, there is no command to build the FE images. 
The FE tests can be run using

```bash
# Run frontend tests using Jest
make fe-tests
```

# Documentation

Documentation for the full suite of tools is available at the temporary link; https://wave-energy-scotland.gitlab.io/dtoceanplus/dtop_documentation/sg/docs/index.html.
This documentation will be moved to a permanent address upon the final release of the suite of tools. 
